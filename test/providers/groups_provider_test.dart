import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/providers/groups_provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_test/flutter_test.dart';

import '../mock/mock.dart';

void main() {
  setupFirebaseAuthMocks();
  group('Groups provider', () {
    MockFirestoreInstance firestoreDB;
    GroupsDataProvider provider;
    Group group;
    List<String> coachIds;
    List<String> userIds;

    setUpAll(() async {
      await Firebase.initializeApp();
    });

    setUp(() async {
      coachIds = ["c"];
      userIds = ["u"];
      firestoreDB = MockFirestoreInstance();
      provider = GroupsDataProvider.fromFirestore(firestoreDB);
      group = await provider.addGroup("g", coachIds, userIds);
    });

    test('Should add group', () async {
      final snapshot = await firestoreDB
          .collection(GroupsDataProvider.GROUPS_COLLECTION_PATH)
          .get();
      expect(snapshot.docs.first.exists, true,
          reason: 'First element doesn\'t exist');
      expect(snapshot.docs.length, 1, reason: 'Invalid number of groups');
      expect(snapshot.docs.first.data()["name"], "g",
          reason: 'Invalid group content');
    });

    test('Should update groups name', () async {
      String newName = "ihateithere";
      await provider.updateName(group.groupId, newName);

      final snapshot = await firestoreDB
          .collection(GroupsDataProvider.GROUPS_COLLECTION_PATH)
          .doc(group.groupId)
          .get();
      expect(snapshot.data()["name"], newName, reason: 'Invalid group name');
    });

    test('Should add coach to group', () async {
      String coachId = "c1";
      await provider.addCoach(group.groupId, coachId);
      coachIds.add(coachId);

      final snapshot = await firestoreDB
          .collection(GroupsDataProvider.GROUPS_COLLECTION_PATH)
          .doc(group.groupId)
          .get();
      expect(snapshot.data()["coachIds"], coachIds, reason: "Invalid coachIds");
    });

    test('Should remove coach from group', () async {
      await provider.removeCoach(group.groupId, coachIds.first);
      coachIds.clear();

      final snapshot = await firestoreDB
          .collection(GroupsDataProvider.GROUPS_COLLECTION_PATH)
          .doc(group.groupId)
          .get();
      expect(snapshot.data()["coachIds"], coachIds, reason: "Invalid coachIds");
    });

    test('Should update coaches', () async {
      coachIds = ["c0", "c1", "c2"];
      await provider.updateCoaches(group.groupId, coachIds);

      final snapshot = await firestoreDB
          .collection(GroupsDataProvider.GROUPS_COLLECTION_PATH)
          .doc(group.groupId)
          .get();
      expect(snapshot.data()["coachIds"], coachIds, reason: "Invalid coachIds");
    });

    test('Should delete group', () async {
      await provider.deleteGroup(group.groupId);

      final snapshot = await firestoreDB
          .collection(GroupsDataProvider.GROUPS_COLLECTION_PATH)
          .get();
      expect(snapshot.docs.length, 0, reason: 'Invalid number of groups');
    });

    test('Should get group', () async {
      Group fetchedGroup = await provider.getGroup(group.groupId);

      expect(fetchedGroup.name, group.name, reason: 'Names doesn\'t match');
      expect(fetchedGroup.groupId, group.groupId, reason: 'Ids doesn\'t match');
      expect(fetchedGroup.coachIds, group.coachIds,
          reason: 'Coach Ids doesn\'t match');
      expect(fetchedGroup.participantIds, group.participantIds,
          reason: 'Participant Ids doesn\'t match');
    });

    test('Should add user to group', () async {
      String userId = "u1";
      await provider.addUserToGroup(group.groupId, userId);
      userIds.add(userId);

      final snapshot = await firestoreDB
          .collection(GroupsDataProvider.GROUPS_COLLECTION_PATH)
          .doc(group.groupId)
          .get();
      expect(snapshot.data()['participantIds'], userIds,
          reason: 'Users doesn\'t match');
    });

    test('Should delete user from group', () async {
      await provider.deleteUserFromGroup(group.groupId, userIds.first);
      userIds.clear();

      final snapshot = await firestoreDB
          .collection(GroupsDataProvider.GROUPS_COLLECTION_PATH)
          .doc(group.groupId)
          .get();
      expect(snapshot.data()['participantIds'], userIds,
          reason: 'Users doesn\'t match');
    });

    test('Should get all groups', () async {
      List<Group> addedGroups = List.empty(growable: true);
      for (int i = 0; i < 15; i++) {
        addedGroups.add(await provider.addGroup("g$i", coachIds, userIds));
        coachIds.add("c$i");
        userIds.add("u$i");
      }

      List<Group> fetchedGroups = await provider.getAllGroups();
      expect(fetchedGroups.length, addedGroups.length + 1,
          reason: 'Groups lenght doesn\'t match');
    });

    // test('Should get groups from ids list', () async {
    //   List<String> ids = List.empty(growable: true);
    //   ids.add(group.groupId);

    //   final snapshot = await firestoreDB
    //       .collection(GroupsDataProvider.GROUPS_COLLECTION_PATH)
    //       .get();
    //   expect(snapshot.docs.first.exists, true,
    //       reason: 'First element doesn\'t exist');
    //   expect(snapshot.docs.length, 1, reason: 'Invalid number of groups');

    //   List<Group> fetchedGroups = await provider.getGroups(ids);

    //   expect(fetchedGroups.length, ids.length,
    //       reason: 'Lenghts doesn\'t match');
    // });
  });
}
