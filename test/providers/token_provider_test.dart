import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';
import 'package:dreamteam_project/models/Token.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/models/UserToken.dart';
import 'package:dreamteam_project/providers/token_provider.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('Token provider', () {
    MockFirestoreInstance firestore;
    TokenProvider provider;
    User user;
    Token token;
    DateTime date;

    // Once before all tests
    setUpAll(() {
      date = DateTime.now().add(Duration(days: 2));
    });
    // tearDownAll(() {});

    // Once before each test
    setUp(() async {
      firestore = MockFirestoreInstance();
      provider = TokenProvider.fromFirestore(firestore);
      final doc = await firestore
          .collection(TokenProvider.USERS_COLLECTION_PATH)
          .add(User(userId: "x", name: "Jan", surname: "Kowalski").toMap());
      user = User.fromFirebase(await doc.get());
      token = await provider.addToken("First token");
    });

    tearDown(() {
      // print(firestore.dump());
    });

    test('should add access token type', () async {
      final snapshot = await firestore
          .collection(TokenProvider.TOKENS_COLLECTION_PATH)
          .get();
      expect(snapshot.docs.length, 1, reason: 'Invalid number of documents');
      expect(snapshot.docs.first.exists, true,
          reason: 'First element doesn\'t exist');
      expect(snapshot.docs.first.data()["name"], "First token",
          reason: 'Invalid token name');
    });

    test('should add access token to user', () async {
      await provider.addUserToken(user, token, date);
      final snapshot = await firestore
          .collection(TokenProvider.USERS_COLLECTION_PATH)
          .doc(user.userId)
          .collection(TokenProvider.USERS_TOKENS_COLLECTION_PATH)
          .get();
      final userTokensData = snapshot.docs;
      expect(userTokensData.length, 1, reason: 'Invalid number of userTokens');

      UserToken userToken = UserToken.fromFirebase(userTokensData.first);
      expect(userToken.userId, user.userId,
          reason: 'Invalid userId for userToken');
      expect(userToken.tokenId, token.id,
          reason: 'Invalid tokenId for userToken');
      expect(userToken.expirationDate.compareTo(date), 0,
          reason: 'Invalid expiration Date for userToken');
    });

    test('should get a list of all tokens', () async {
      List<Token> tokens = await provider.getAllTokens();
      expect(tokens.length, 1, reason: 'Invalid number of tokens');
      expect(tokens.first.name, "First token", reason: 'Invalid token name');
    });

    test('should get a list of all accessTokens for user', () async {
      await provider.addUserToken(user, token, date);
      List<UserToken> userTokens = await provider.getTokensForUser(user);
      expect(userTokens.length, 1, reason: 'Invalid number of userTokens');
      var userToken = userTokens[0];
      expect(userToken.userId, user.userId,
          reason: 'Invalid userId for userToken');
      expect(userToken.tokenId, token.id,
          reason: 'Invalid tokenId for userToken');
      expect(userToken.expirationDate.compareTo(date), 0,
          reason: 'Invalid expiration Date for userToken');
    });

    test('should verify accessToken', () async {
      bool verification = await provider.verifyToken(user, token);
      expect(verification, false, reason: 'User shouldn\'t have the token yet');

      await provider.addUserToken(user, token, date);
      verification = await provider.verifyToken(user, token);
      expect(verification, true, reason: 'User should have the token now');
    });

    test('should extend token\'s expiration date for user', () async {
      await provider.addUserToken(user, token, date);
      List<UserToken> userTokens = await provider.getTokensForUser(user);
      expect(userTokens.length, 1, reason: 'Invalid number of userTokens');
      UserToken userToken = userTokens[0];
      expect(userToken.expirationDate.compareTo(date), 0,
          reason: 'Invalid initial expiration Date');

      Duration extendDuration = Duration(days: 12);
      await provider.extendExpirationDate(userToken, extendDuration);
      userTokens = await provider.getTokensForUser(user);
      expect(userTokens.length, 1, reason: 'Invalid number of userTokens');
      userToken = userTokens[0];
      expect(userToken.expirationDate.compareTo(date.add(extendDuration)), 0,
          reason: 'Invalid extended expiration Date');
    });
  });
}
