import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';
import 'package:dreamteam_project/models/Attendance.dart';
import 'package:dreamteam_project/models/Schedule.dart';
import 'package:dreamteam_project/providers/schedule_provider.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('Schedule provider', () {
    MockFirestoreInstance firestoreDB;
    ScheduleProvider provider;
    Schedule schedule;
    Schedule scheduleToAdd;
    DateTime fromDate;
    DateTime toDate;
    DateTime startDate;
    List<String> groupsId;
    List<Attendance> attendanceList;
    List<String> coachIds;

    setUpAll(() {
      fromDate = DateTime.now();
      startDate = new DateTime(
          fromDate.year, fromDate.month, fromDate.day, fromDate.hour - 2);
      toDate = new DateTime(
          fromDate.year, fromDate.month + 1, fromDate.day, fromDate.hour);
      scheduleToAdd = new Schedule(
          groupId: "y",
          scheduleId: "x",
          dateEnd: toDate,
          dateStart: startDate,
          classSubject: "class",
          roomId: "1",
          coachIds: ["a", "b"]);
      groupsId = ["y"];
      attendanceList = [
        new Attendance(
            scheduleId: "x",
            userId: "z",
            dateModified: startDate,
            presence: false)
      ];
      coachIds = ["c", "d"];
    });

    setUp(() async {
      firestoreDB = new MockFirestoreInstance();
      provider = ScheduleProvider.fromFirestore(firestoreDB);
      schedule = await provider.addSchedule(scheduleToAdd);
    });

    test('Should add schedule', () async {
      final snapshot = await firestoreDB
          .collection(ScheduleProvider.SCHEDULE_COLLECTION_PATH)
          .get();
      expect(snapshot.docs.first.exists, true,
          reason: 'First element doesn\'t exist');
      expect(snapshot.docs.length, 1, reason: 'Invalid number of schedules');
      expect(snapshot.docs.first.data()["scheduleId"], "x",
          reason: 'Invalid schedule content');
    });

    // HELP ME????
    // test('Should add schedules', () async {
    //   await provider.addSchedules(schedule, toDate);
    //   final snapshot = await firestoreDB
    //       .collection(ScheduleProvider.SCHEDULE_COLLECTION_PATH)
    //       .get();
    //   expect(snapshot.docs.first.exists, true,
    //       reason: 'First element doesn\'t exist');
    //   expect(snapshot.docs.length, 2, reason: 'Invalid number of schedules');
    //   expect(snapshot.docs.first.data()["scheduleId"], "x",
    //       reason: 'Invalid schedule content');
    // });

    // test('Should get schedules', () async {
    //   List<Schedule> schedules = await provider.getSchedule(groupsId, toDate);
    //   expect(schedules.length, 1, reason: 'Invalid number of schedules');
    //   expect(schedules.first.groupId, "y", reason: 'Invalid group name');
    // });

    test('Should get schedules', () async {
      List<Schedule> schedules =
          await provider.getSchedule(startDate, toDate, groupIds: groupsId);
      expect(schedules.length, 1, reason: 'Invalid number of schedules');
      expect(schedules.first.groupId, "y", reason: 'Invalid group name');
    });

    //TODO: Attendance provider!!
    // test('Should update attendance', () async {
    //   await provider.updateAttendance("x", attendanceList);
    //    final snapshot = await firestoreDB
    //       .collection(ScheduleProvider.SCHEDULE_COLLECTION_PATH)
    //       .doc(schedule.scheduleId)
    //       .get();

    // });
    test('Should update class subject', () async {
      await provider.updateClassSubject(schedule.scheduleId, "subject");

      final snapshot = await firestoreDB
          .collection(ScheduleProvider.SCHEDULE_COLLECTION_PATH)
          .doc(schedule.scheduleId)
          .get();

      expect(snapshot.exists, true, reason: 'First element doesn\'t exist');
      expect(snapshot.data()["classSubject"], "subject",
          reason: 'Invalid class subject name');
    });

    test('Should update coaches', () async {
      await provider.updateCoaches(schedule.scheduleId, coachIds);

      final snapshot = await firestoreDB
          .collection(ScheduleProvider.SCHEDULE_COLLECTION_PATH)
          .doc(schedule.scheduleId)
          .get();

      expect(snapshot.exists, true, reason: 'First element doesn\'t exist');
      expect(snapshot.data()["coachesId"], coachIds,
          reason: 'Invalid coaches ids');
    });

    test('Should update room', () async {
      await provider.updateRoom(schedule.scheduleId, "2");

      final snapshot = await firestoreDB
          .collection(ScheduleProvider.SCHEDULE_COLLECTION_PATH)
          .doc(schedule.scheduleId)
          .get();

      expect(snapshot.exists, true, reason: 'First element doesn\'t exist');
      expect(snapshot.data()["roomId"], "2", reason: 'Invalid room id');
    });
  });
}
