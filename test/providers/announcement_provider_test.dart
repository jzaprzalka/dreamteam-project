import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';
import 'package:dreamteam_project/models/Announcement.dart';
import 'package:dreamteam_project/providers/announcement_provider.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('Announcement provider', () {
    MockFirestoreInstance firestoreDB;
    AnnouncementProvider provider;
    Announcement announcement;
    DateTime date;
    List<String> groupIds;

    setUpAll(() {
      date = DateTime.now();
      groupIds = ["x"];
    });

    setUp(() async {
      firestoreDB = MockFirestoreInstance();
      provider = AnnouncementProvider.fromFirestore(firestoreDB);
      announcement =
          await provider.addAnnouncement(date, "Announcement", groupIds);
    });

    test('Should add announcement', () async {
      final snapshot = await firestoreDB
          .collection(AnnouncementProvider.ANNOUNCEMENTS_COLLECTION_PATH)
          .get();
      expect(snapshot.docs.first.exists, true,
          reason: 'First element doesn\'t exist');
      expect(snapshot.docs.length, 1, reason: 'Invalid number of documents');
      expect(snapshot.docs.first.data()["announcement"], "Announcement",
          reason: 'Invalid announcement content');
    });

    test('Should delete announcement', () async {
      await provider.deleteAnnouncement(announcement.announcementId);
      final snapshot = await firestoreDB
          .collection(AnnouncementProvider.ANNOUNCEMENTS_COLLECTION_PATH)
          .get();
      expect(snapshot.docs.length, 0, reason: 'Invalid number of documents');
    });

    test('Should get a list of all announcements', () async {
      List<Announcement> announcements = await provider.getAllAnnouncements();
      expect(announcements.length, 1,
          reason: 'Invalid number of announcements');
      expect(announcements.first.announcement, "Announcement",
          reason: 'Invalid announcement content');
    });

    test('Should get a list of announcements for groups', () async {
      // Adding announcement for another group
      await provider.addAnnouncement(date, "Announcement2", ["notX"]);
      List<Announcement> announcements =
          await provider.getGroupsAnnouncements(groupIds);
      expect(announcements.length, 1,
          reason: 'Invalid number of announcements');
      Announcement groupAnnouncement = announcements[0];
      expect(groupAnnouncement.groupIds[0], groupIds[0],
          reason: 'Invalid groupId');
      expect(groupAnnouncement.announcement, "Announcement",
          reason: 'Invalid announcement content');
    });

    test('Should get an announcement by id', () async {
      // Adding announcement for another group
      await provider.addAnnouncement(date, "Announcement2", ["notX"]);
      Announcement fetched =
          await provider.getAnnouncement(announcement.announcementId);
      expect(fetched.announcementId, announcement.announcementId,
          reason: 'Invalid announcementId');
      expect(fetched.groupIds[0], groupIds[0], reason: 'Invalid groupId');
      expect(fetched.announcement, "Announcement",
          reason: 'Invalid announcement content');
    });
  });
}
