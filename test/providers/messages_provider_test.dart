import 'package:dreamteam_project/models/Chatroom.dart';
import 'package:dreamteam_project/models/Messages.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';
import 'package:dreamteam_project/providers/messages_provider.dart';

void main() {
  group('Messages provider', () {
    MockFirestoreInstance firestore;
    MessagesProvider provider;
    Chatroom chatroom;

    // Once before all tests
    setUpAll(() {});

    // Once before each test
    setUp(() async {
      firestore = MockFirestoreInstance();
      provider = MessagesProvider.fromFirestore(firestore);
      final doc = await firestore
          .collection(MessagesProvider.CHATROOM_COLLECTION_PATH)
          .add(Chatroom(chatroomId: "1", userIds: ["z"]).toMap());
      chatroom = Chatroom.fromFirebase(await doc.get());
    });

    test('Should add message', () async {
      TextMessage message = TextMessage("Henlo", DateTime.now(), "X");
      await provider.addMessage(
          message, chatroom.chatroomId);
      final snapshotMessages = await firestore.collection(MessagesProvider.CHATROOM_COLLECTION_PATH)
        .doc(chatroom.chatroomId)
        .collection(MessagesProvider.MESSAGE_PATH).get();

      // Checking messages collection
      expect(snapshotMessages.docs.length, 1,
          reason: 'Invalid number of documents (messages)');
      expect(snapshotMessages.docs.first.exists, true,
          reason: 'First element doesn\'t exist');
      expect(snapshotMessages.docs.first.data()["text"], message.text,
          reason: 'Invalid message text');
    });

    test('Should add chatroom', () async {
      await provider.createChatroom(Chatroom(chatroomId: "2"));
      final snapshot = await firestore
          .collection(MessagesProvider.CHATROOM_COLLECTION_PATH)
          .get();
      expect(snapshot.docs.length, 2, reason: 'Invalid number of documents');
      expect(snapshot.docs.elementAt(1).exists, true,
          reason: 'Second element doesn\'t exist');
      expect(snapshot.docs.elementAt(1).data()["chatroomId"], "2",
          reason: 'Invalid chatroomId');
    });

    test('Should delete chatroom', () async {
      await provider.deleteChatroom(chatroom.chatroomId);
      final snapshot = await firestore
          .collection(MessagesProvider.CHATROOM_COLLECTION_PATH)
          .get();
      expect(snapshot.docs.length, 0, reason: 'Invalid number of documents');
    });

    test('Should update chatroom participants', () async {
      List<String> participantIds = ["a", "b"];
      await provider.updateChatroomParticipants(
          chatroom.chatroomId, participantIds);
      final snapshot = await firestore
          .collection(MessagesProvider.CHATROOM_COLLECTION_PATH)
          .doc(chatroom.chatroomId)
          .get();
      expect(snapshot.exists, true,
          reason: 'Participant collection doesn\'t exist');
      expect(snapshot.data()[MessagesProvider.USER_IDS_PATH], participantIds,
          reason: 'Participant ids doesn\'t match');
    });

    test('Should get messages for chatroom', () async {
      int messagesNumber = 15;
      for (int i = 0; i < messagesNumber; i++) {
        await provider.addMessage(
          TextMessage("Henlo $i", DateTime.now(), "X"),
          chatroom.chatroomId,
        );
      }
      List<Message> messages =
          await provider.getMessagesForChatroom(chatroom.chatroomId);
      expect(messages.length, messagesNumber,
          reason: 'Invalid number of elements');
    });

    test('Should get chatroom', () async {
      Chatroom chatroomFromFirebase =
          await provider.getChatroom(chatroom.chatroomId);
      expect(chatroomFromFirebase.chatroomId, chatroom.chatroomId,
          reason: 'Wrong id');
    });
  });
}
