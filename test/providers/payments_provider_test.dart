import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:dreamteam_project/models/Token.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/providers/payments_provider.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('Payments provider', () {
    MockFirestoreInstance firestoreDB;
    PaymentsProvider provider;
    DateTime dateNow;
    Payment payment;
    List<Payment> payments;
    Group group;
    List<Group> groups;
    Token token;
    User user;

    setUpAll(() {
      dateNow = DateTime.now();
    });

    setUp(() async {
      firestoreDB = MockFirestoreInstance();
      provider = PaymentsProvider.fromFirestore(firestoreDB);
      group = new Group(groupId: "g", name: "g", participantIds: ["1", "2"]);
      groups = [group];
      user = new User(userId: "u", name: "a", surname: "b");
    });

    test('should acocunt payment', () async {
      await provider.accountPayment(
          new Payment(paymentId: "x", paid: false), dateNow);

      final snapshot = await firestoreDB
          .collection(PaymentsProvider.PAYMENTS_COLLECTION_PATH)
          .doc("x")
          .get();

      expect(snapshot.exists, true, reason: 'First element doesn\'t exist');
      expect(snapshot.data()["paid"], true, reason: 'Invalid paid value');
    });
    test('should add payment', () async {
      payment = await provider.addPayment(new Payment(paymentId: "x"));
      final snapshot = await firestoreDB
          .collection(PaymentsProvider.PAYMENTS_COLLECTION_PATH)
          .get();
      expect(snapshot.docs.first.exists, true,
          reason: 'First element doesn\'t exist');
      expect(snapshot.docs.length, 1, reason: 'Invalid number of payments');
      expect(snapshot.docs.first.data()["paymentId"], "x",
          reason: 'Invalid payment content');
    });
    test('should add payments', () async {
      payments = [new Payment(paymentId: "x")];
      await provider.addPayments(payments);
      final snapshot = await firestoreDB
          .collection(PaymentsProvider.PAYMENTS_COLLECTION_PATH)
          .get();
      expect(snapshot.docs.first.exists, true,
          reason: 'First element doesn\'t exist');
      expect(snapshot.docs.length, 1, reason: 'Invalid number of payments');
      expect(snapshot.docs.first.data()["paymentId"], "x",
          reason: 'Invalid payment content');
    });
    test('should add payment for group', () async {
      payment = new Payment(
          amount: 100,
          paymentName: "payment",
          documentId: "z",
          dateDue: new DateTime(dateNow.year, dateNow.month + 1, dateNow.day),
          datePaid: new DateTime(dateNow.year, dateNow.month + 2, dateNow.day));

      await provider.addPaymentForGroup(payment, group);

      final snapshot = await firestoreDB
          .collection(PaymentsProvider.PAYMENTS_COLLECTION_PATH)
          .get();

      expect(snapshot.docs.first.exists, true,
          reason: 'First element doesn\'t exist');
      expect(snapshot.docs.length, 2, reason: 'Invalid number of payments');
      expect(snapshot.docs.first.data()["userId"], "1",
          reason: 'Invalid payment content');
      expect(snapshot.docs.first.data()["documentId"], "z",
          reason: 'Invalid payment content');
    });

    test('should get all payments', () async {
      payment = await provider.addPayment(new Payment(paymentId: "x"));
      List<Payment> payments = await provider.getAllPayments();
      expect(payments.length, 1, reason: 'Invalid number of payments');
      expect(payments.first.paymentId, payment.paymentId,
          reason: 'Invalid payment content');
    });
    test('should get all paid payments', () async {
      payments = [
        new Payment(paymentId: "x", paid: false),
        new Payment(paymentId: "y", paid: true)
      ];
      await provider.addPayments(payments);
      List<Payment> paymentList = await provider.getAllPaymentsPaid();
      expect(paymentList.length, 1, reason: 'Invalid number of payments');
    });

    test('should get all payments past due', () async {
      payments = [
        new Payment(
            paymentId: "x",
            paid: false,
            dateDue:
                new DateTime(dateNow.year, dateNow.month - 1, dateNow.day)),
        new Payment(
            paymentId: "y",
            paid: true,
            dateDue: new DateTime(dateNow.year, dateNow.month - 1, dateNow.day))
      ];
      await provider.addPayments(payments);
      List<Payment> paymentList = await provider.getAllPaymentsPastDue();
      expect(paymentList.length, 1, reason: 'Invalid number of payments');
    });

    test('should get group payments', () async {
      payment = new Payment(
          amount: 100,
          paymentName: "payment",
          documentId: "z",
          dateDue: new DateTime(dateNow.year, dateNow.month + 1, dateNow.day),
          datePaid: new DateTime(dateNow.year, dateNow.month + 2, dateNow.day));

      await provider.addPaymentForGroup(payment, group);

      List<Payment> paymentList = await provider.getGroupPayments(group);
      expect(paymentList.length, 2, reason: 'Invalid number of payments');
    });

    test('should get groups payments', () async {
      payment = new Payment(
          amount: 100,
          paymentName: "payment",
          documentId: "z",
          dateDue: new DateTime(dateNow.year, dateNow.month + 1, dateNow.day),
          datePaid: new DateTime(dateNow.year, dateNow.month + 2, dateNow.day));

      await provider.addPaymentForGroup(payment, group);

      List<Payment> paymentList = await provider.getGroupsPayments(groups);
      expect(paymentList.length, 2, reason: 'Invalid number of payments');
    });
    test('should get approaching due groups payments', () async {
      payment = new Payment(
          amount: 100,
          paymentName: "payment",
          documentId: "z",
          dateDue: DateTime.now().add(Duration(days: 10)),
          paid: false);

      await provider.addPaymentForGroup(payment, group);

      List<Payment> paymentList =
          await provider.getGroupsPaymentsApproachingDue(groups);
      expect(paymentList.length, 2, reason: 'Invalid number of payments');
    });
    test('should get paid groups payments', () async {
      payment = new Payment(
          amount: 100,
          paymentName: "payment",
          documentId: "z",
          dateDue: new DateTime(dateNow.year, dateNow.month, dateNow.day - 10),
          paid: true);

      await provider.addPaymentForGroup(payment, group);

      List<Payment> paymentList = await provider.getGroupsPaymentsPaid(groups);
      expect(paymentList.length, 2, reason: 'Invalid number of payments');
    });
    test('should get past due groups payments', () async {
      payment = new Payment(
          amount: 100,
          paymentName: "payment",
          documentId: "z",
          dateDue: new DateTime(dateNow.year, dateNow.month, dateNow.day - 11),
          paid: false);

      await provider.addPaymentForGroup(payment, group);

      List<Payment> paymentList =
          await provider.getGroupsPaymentsPastDue(groups);
      expect(paymentList.length, 2, reason: 'Invalid number of payments');
    });
    test('should get aproaching due payments', () async {
      payment = await provider.addPayment(new Payment(
          amount: 100,
          paymentName: "payment",
          documentId: "z",
          dateDue: DateTime.now().add(Duration(days: 10)),
          paid: false,
          userId: "u",
          paymentId: "x"));
      List<Payment> paymentList =
          await provider.getPaymentsApproachingDue(user.userId);

      expect(paymentList.length, 1, reason: 'Invalid number of payments');
      expect(paymentList.first.paymentId, payment.paymentId,
          reason: 'Invalid payment content');
    });
    test('should get payments for', () async {
      payment = await provider.addPayment(new Payment(
          amount: 100,
          paymentName: "payment",
          documentId: "z",
          dateDue: new DateTime(dateNow.year, dateNow.month, dateNow.day - 10),
          paid: false,
          userId: "u",
          paymentId: "x"));
      List<Payment> paymentList = await provider.getPaymentsFor(user.userId);

      expect(paymentList.length, 1, reason: 'Invalid number of payments');
      expect(paymentList.first.paymentId, payment.paymentId,
          reason: 'Invalid payment content');
    });
    test('should get paid payments', () async {
      payment = await provider.addPayment(new Payment(
          amount: 100,
          paymentName: "payment",
          documentId: "z",
          dateDue: new DateTime(dateNow.year, dateNow.month, dateNow.day - 10),
          paid: true,
          userId: "u",
          paymentId: "x"));
      List<Payment> paymentList = await provider.getPaymentsFor(user.userId);

      expect(paymentList.length, 1, reason: 'Invalid number of payments');
      expect(paymentList.first.paymentId, payment.paymentId,
          reason: 'Invalid payment content');
    });
    test('should get payments past due', () async {
      payment = await provider.addPayment(new Payment(
          amount: 100,
          paymentName: "payment",
          documentId: "z",
          dateDue: new DateTime(dateNow.year, dateNow.month, dateNow.day - 11),
          paid: false,
          userId: "u",
          paymentId: "x"));
      List<Payment> paymentList = await provider.getPaymentsFor(user.userId);

      expect(paymentList.length, 1, reason: 'Invalid number of payments');
      expect(paymentList.first.paymentId, payment.paymentId,
          reason: 'Invalid payment content');
    });
  });
}
