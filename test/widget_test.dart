// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:dreamteam_project/configuration/logs/predefined_loggers.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test supported locales', (WidgetTester tester) async {
    // Verify language is supported
    expect(S.delegate.isSupported(Locale('pl', 'PL')), true,
        reason: "Polish locale not supported");
    expect(S.delegate.isSupported(Locale('en', '')), true,
        reason: "Main locale (en) not supported");
  });

  // testWidgets('Test signing in', (WidgetTester tester) async {
  //   // Verify if signing in throws exceptions
  //   FirebaseAuthMock firebaseAuth = FirebaseAuthMock();
  //   expect(() => firebaseAuth.signInUser(null, null), throwsException);
  //   expect(() => firebaseAuth.signInUser(null, null), throwsException);
  // });

  group('Test logger file output', () {
    testWidgets('Test debug logger', (WidgetTester tester) async {
      // Verify if logger creates file and writes to file
      expect(() => fileDebugLogger.d("Debug logger test"), returnsNormally);
    });

    testWidgets('Test deploy logger', (WidgetTester tester) async {
      // Verify if logger creates file and writes to file
      expect(() => deployLogger.i("Debug logger test"), returnsNormally);
    });
  });
}
