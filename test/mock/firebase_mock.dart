import 'package:dreamteam_project/providers/authentication_provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:mockito/mockito.dart';

class FirebaseAuthMock extends Mock implements AuthenticationProvider {}

class FirebaseUserMock extends Mock implements User {
  @override
  String get displayName => 'John Doe';
  @override
  String get uid => 'uid';
  @override
  String get email => 'johndoe@mail.com';
}
