import 'package:dreamteam_project/models/Announcement.dart';
import 'package:dreamteam_project/models/Attendance.dart';
import 'package:dreamteam_project/models/Competition.dart';
import 'package:dreamteam_project/models/CompetitionApplication.dart';
import 'package:dreamteam_project/models/Document.dart';
import 'package:dreamteam_project/models/Event.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:dreamteam_project/models/Room.dart';
import 'package:dreamteam_project/models/RoomReservation.dart';
import 'package:dreamteam_project/models/Schedule.dart';
import 'package:dreamteam_project/models/Token.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/models/UserToken.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('Json Annotation', () {
    Announcement announcement;
    Attendance attendance;
    Competition competition;
    CompetitionApplication competitionApplication;
    Document document;
    Event event;
    Group group;
    Payment payment;
    Room room;
    RoomReservation roomReservation;
    Schedule schedule;
    Token token;
    User user;
    UserToken userToken;

    // Once before all tests
    setUpAll(() {
      announcement = Announcement();
      attendance = Attendance(scheduleId: "12");
      competition = Competition();
      competitionApplication = CompetitionApplication();
      document = Document();
      event = Event();
      group = Group(name: "Nazwa grupy");
      payment = Payment();
      room = Room();
      roomReservation = RoomReservation();
      schedule = Schedule(scheduleId: "1", groupId: "1");
      token = Token();
      user = User(userId: "1", name: "Jan", surname: "Nowak");
      userToken = UserToken();
    });

    test('should properly unmap mapped values for Announcement', () {
      Announcement a = Announcement.fromMap(announcement.toMap());
      expect(a.announcement, announcement.announcement);
      expect(a.announcementDate, announcement.announcementDate);
      expect(a.announcementId, announcement.announcementId);
      expect(a.groupIds, announcement.groupIds);
    });

    test('should properly unmap mapped values for Attendance', () {
      Attendance a = Attendance.fromMap(attendance.toMap());
      expect(a.dateModified, attendance.dateModified);
      expect(a.presence, attendance.presence);
      expect(a.scheduleId, null);
      expect(a.userId, attendance.userId);
    });

    test('should properly unmap mapped values for Competition', () {
      Competition c = Competition.fromMap(competition.toMap());
      expect(c.competitionId, competition.competitionId);
      expect(c.address, competition.address);
      expect(c.applicationIds, competition.applicationIds);
      expect(c.competitionDate, competition.competitionDate);
      expect(c.description, competition.description);
      expect(c.name, competition.name);
    });

    test('should properly unmap mapped values for CompetitionApplication', () {
      CompetitionApplication ca =
          CompetitionApplication.fromMap(competitionApplication.toMap());
      expect(ca.competitionId, competitionApplication.competitionId);
      expect(ca.applicationDate, competitionApplication.applicationDate);
      expect(ca.approved, competitionApplication.approved ?? false);
      expect(ca.competitionApplicationId,
          competitionApplication.competitionApplicationId);
      expect(ca.details, competitionApplication.details);
      expect(ca.userId, competitionApplication.userId);
    });

    test('should properly unmap mapped values for Document', () {
      Document d = Document.fromMap(document.toMap());
      expect(d.accessTokenId, document.accessTokenId);
      expect(d.dateCreated, document.dateCreated);
      expect(d.documentId, document.documentId);
      expect(d.filePath, document.filePath);
      expect(d.type, document.type);
    });

    test('should properly unmap mapped values for Event', () {
      Event e = Event.fromMap(event.toMap());
      expect(e.address, event.address);
      expect(e.date, event.date);
      expect(e.description, event.description);
      expect(e.eventId, event.eventId);
      expect(e.name, event.name);
    });

    test('should properly unmap mapped values for Group', () {
      Group g = Group.fromMap(group.toMap());
      expect(g.groupId, group.groupId);
      expect(g.coachIds, group.coachIds);
      expect(g.name, group.name);
      expect(g.participantIds, group.participantIds);
    });

    test('should properly unmap mapped values for Payment', () {
      Payment p = Payment.fromMap(payment.toMap());
      expect(p.amount, payment.amount);
      expect(p.dateAdded, payment.dateAdded);
      expect(p.dateDue, payment.dateDue);
      expect(p.datePaid, payment.datePaid);
      expect(p.documentId, payment.documentId);
      expect(p.paid, payment.paid ?? false);
      expect(p.paymentId, payment.paymentId);
      expect(p.paymentName, payment.paymentName);
      expect(p.userId, payment.userId);
    });

    test('should properly unmap mapped values for Room', () {
      Room r = Room.fromMap(room.toMap());
      expect(r.buildingAddress, room.buildingAddress);
      expect(r.number, room.number);
      expect(r.reservations, room.reservations);
      expect(r.roomId, room.roomId);
    });

    test('should properly unmap mapped values for RoomReservation', () {
      RoomReservation rr = RoomReservation.fromMap(roomReservation.toMap());
      expect(rr.reservationId, roomReservation.reservationId);
      expect(rr.reservationEnd, roomReservation.reservationEnd);
      expect(rr.reservationStart, roomReservation.reservationStart);
      expect(rr.addDate, roomReservation.addDate);
      expect(rr.bookingPersonId, roomReservation.bookingPersonId);
      expect(rr.private, roomReservation.private);
      expect(rr.roomId, roomReservation.roomId);
    });

    test('should properly unmap mapped values for Schedule', () {
      Schedule s = Schedule.fromMap(schedule.toMap());
      expect(s.scheduleId, schedule.scheduleId);
      expect(s.attendance, schedule.attendance);
      expect(s.classSubject, schedule.classSubject);
      expect(s.coachIds, schedule.coachIds);
      expect(s.dateEnd, schedule.dateEnd);
      expect(s.dateStart, schedule.dateStart);
      expect(s.groupId, schedule.groupId);
      expect(s.roomId, schedule.roomId);
    });

    test('should properly unmap mapped values for Token', () {
      Token t = Token.fromMap(token.toMap());
      expect(t.id, token.id);
      expect(t.name, token.name);
    });

    test('should properly unmap mapped values for User', () {
      User u = User.fromMap(user.toMap());
      expect(u.birthDate, user.birthDate);
      expect(u.email, user.email);
      expect(u.name, user.name);
      expect(u.surname, user.surname);
      expect(u.nip, user.nip);
      expect(u.password, user.password);
      expect(u.pesel, user.pesel);
      expect(u.phoneNumber, user.phoneNumber);
      expect(u.userId, user.userId);
    });

    test('should properly unmap mapped values for UserToken', () {
      UserToken ut = UserToken.fromMap(userToken.toMap());
      expect(ut.tokenId, userToken.tokenId);
      expect(ut.expirationDate, userToken.expirationDate);
      expect(ut.userId, userToken.userId);
      expect(ut.userTokenId, userToken.userTokenId);
    });
  });
}
