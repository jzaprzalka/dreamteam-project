import 'package:dreamteam_project/blocks/user/user_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/models/UserToken.dart';
import 'package:dreamteam_project/models/value_notifier/user_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ProfileScreenState();
}

class _UserProperty extends ChangeNotifier {
  String propName;
  String _displayValue;
  dynamic _value;
  Function(dynamic newValue) setProperty;
  bool _selected = false;
  bool requiresAuth;
  bool isPassword;

  bool get selected => _selected;
  set selected(bool value) {
    _selected = value;
    notifyListeners();
  }

  dynamic get displayValue => _displayValue ?? value;

  dynamic get value => _value;
  set value(dynamic value) {
    _value = value;
    notifyListeners();
  }

  _UserProperty({
    String propName,
    dynamic value,
    String displayValue,
    this.setProperty,
    this.requiresAuth = false,
    this.isPassword = false,
  }) {
    this.propName = propName;
    this.value = value;
    this._displayValue = displayValue;
  }

  @override
  String toString() {
    return "_UserProperty: {" +
        "propName: $propName, " +
        "value: $value, " +
        "setProperty: $setProperty, " +
        "selected: $selected, " +
        "isPassword: $isPassword" +
        "}";
  }
}

class _ProfileScreenState extends State<ProfileScreen> {
  User _user;
  NumberFormat _formatter = NumberFormat("000000");
  List<_UserProperty> _userPropertiresList;

  Future<void> _showDialog({
    @required Widget title,
    @required Widget content,
    @required Function confirm,
    @required Function cancel,
  }) async {
    // Cancel
    Widget cancelBtn = TextButton(
      child: Text(S.current.cancelBtn),
      onPressed: cancel,
    );
    // Save
    Widget saveBtn = TextButton(
      child: Text(S.current.submitBtn),
      onPressed: confirm,
    );

    await showDialog(
      context: context,
      builder: (_) => AlertDialog(
          title: title, actions: [cancelBtn, saveBtn], content: content),
    );
  }

  void _showPasswordEditDialog(
      _UserProperty passwordProperty, BuildContext context) async {
    TextEditingController emailController = TextEditingController();
    TextEditingController oldPassowrdController = TextEditingController();
    TextEditingController newPassowrdController = TextEditingController();
    TextEditingController confirmPassowrdController = TextEditingController();

    await _showDialog(
      title: Text(
        S.current.authRequired(passwordProperty.propName),
        textAlign: TextAlign.center,
      ),
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: [
          // Email
          Padding(
            padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
            child: TextFormField(
              maxLines: 1,
              keyboardType: TextInputType.emailAddress,
              autofocus: false,
              controller: emailController,
              decoration: InputDecoration(
                labelText: S.current.email,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide(),
                ),
              ),
            ),
          ),
          // Old password
          Padding(
            padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
            child: TextFormField(
              maxLines: 1,
              obscureText: true,
              autofocus: false,
              decoration: InputDecoration(
                labelText: S.current.oldPass,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide(),
                ),
              ),
              controller: oldPassowrdController,
            ),
          ),
          // New Password
          Padding(
            padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
            child: TextFormField(
              maxLines: 1,
              obscureText: true,
              autofocus: false,
              decoration: InputDecoration(
                labelText: S.current.newPass,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide(),
                ),
              ),
              controller: newPassowrdController,
            ),
          ),
          // Confirm new Password
          Padding(
            padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
            child: TextFormField(
              maxLines: 1,
              obscureText: true,
              autofocus: false,
              decoration: InputDecoration(
                labelText: S.current.confirmPassword,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide(),
                ),
              ),
              controller: confirmPassowrdController,
            ),
          ),
        ],
      ),
      confirm: () {
        if (newPassowrdController.text != confirmPassowrdController.text) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text(S.current.passDontMatch)),
          );
          return;
        }
        _saveAuthProperty(passwordProperty,
            value: newPassowrdController.text,
            email: emailController.text,
            password: oldPassowrdController.text);
        Navigator.pop(context);
      },
      cancel: () {
        passwordProperty.selected = false;
        logger.i("Cancelled password editing");
        Navigator.pop(context);
      },
    );
  }

  void _startEditproperty(_UserProperty property, BuildContext context) {
    if (property.isPassword) {
      logger.i("Showing passowrd edit dialog");
      _showPasswordEditDialog(property, context);
    } else {
      property.selected = !property.selected;
      logger.i("Entered property editing for property:" + "\n\t$property");
    }
  }

  void _saveProperty(_UserProperty property, {@required dynamic value}) {
    property.selected = !property.selected;
    if (property.setProperty != null) {
      property.setProperty(value);
      logger.i("Saving property:" + "\n\t$property");
    } else {
      logger.e("Impossible to save property: ${property.propName}." +
          "No setProperty function provided");
    }
  }

  void _saveAuthProperty(_UserProperty property,
      {@required dynamic value,
      @required String email,
      @required String password}) {
    property.selected = !property.selected;
    if (property.setProperty != null) {
      try {
        _user.email = email;
        _user.password = password;
        property.setProperty(value);
        logger.i("Saving property:" + "\n\t$property");
      } catch (e) {
        logger.e("Save property failed: ", e);
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
              content: Text(
            S.current.authFailed,
            style: TextStyle(color: Colors.red),
          )),
        );
      }
    } else {
      logger.e("Impossible to save property: ${property.propName}." +
          "No setProperty function provided");
    }
  }

  void _initPropList(UserBloc userBloc) {
    // _user = userNotifier.value;
    _userPropertiresList = [
      _UserProperty(
        propName: S.current.name,
        value: _user.name,
        setProperty: (value) => userBloc
            .add(UserEvent.updatedName(userId: _user.userId, name: value)),
      ),
      _UserProperty(
        propName: S.current.surname,
        value: _user.surname,
        setProperty: (value) => userBloc.add(
            UserEvent.updatedSurname(userId: _user.userId, surname: value)),
      ),
      _UserProperty(
        propName: S.current.email,
        value: _user.email,
        requiresAuth: true,
        setProperty: (value) => userBloc.add(UserEvent.updatedEmail(
            email: _user.email, password: _user.password, newEmail: value)),
      ),
      _UserProperty(
        propName: S.current.birthdate,
        value: _user.birthDate,
        setProperty: null,
      ),
      _UserProperty(
        propName: S.current.password,
        value: "",
        displayValue: S.current.edit_password,
        requiresAuth: true,
        setProperty: (value) => userBloc.add(UserEvent.updatedPassword(
            email: _user.email, password: _user.password, newPassword: value)),
        isPassword: true,
      ),
      _UserProperty(
        propName: S.current.phoneNumber,
        value: _user.phoneNumber,
        setProperty: (value) => userBloc.add(
            UserEvent.updatedPhone(userId: _user.userId, phoneNumber: value)),
      ),
      _UserProperty(
        propName: S.current.nip,
        value: _user.nip,
        setProperty: null,
      ),
    ];
  }

  Widget _userDetailField(BuildContext context, int index) {
    _UserProperty property = _userPropertiresList[index];
    TextEditingController inputController =
        TextEditingController(text: "${property.value ?? ""}");
    Function() _save = () async {
      if (!property.requiresAuth)
        return _saveProperty(property, value: inputController.text);
      else {
        TextEditingController emailInputController = TextEditingController();
        TextEditingController passowrdInputController = TextEditingController();
        await _showDialog(
          title: Text(
            S.current.authRequired(property.propName),
            textAlign: TextAlign.center,
          ),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
                child: TextFormField(
                  maxLines: 1,
                  keyboardType: TextInputType.emailAddress,
                  autofocus: false,
                  controller: emailInputController,
                  decoration: InputDecoration(
                    labelText: S.current.email,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      borderSide: BorderSide(),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
                child: TextFormField(
                  maxLines: 1,
                  obscureText: true,
                  autofocus: false,
                  decoration: InputDecoration(
                    labelText: S.current.password,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      borderSide: BorderSide(),
                    ),
                  ),
                  controller: passowrdInputController,
                ),
              ),
            ],
          ),
          confirm: () {
            _saveAuthProperty(property,
                value: inputController.text,
                email: emailInputController.text,
                password: passowrdInputController.text);
            Navigator.pop(context);
          },
          cancel: () {
            property.selected = !property.selected;
            logger.i("Cancelled property editing for ${property.propName}");
            Navigator.pop(context);
          },
        );
      }
    };
    return ChangeNotifierProvider<_UserProperty>(
        create: (context) => property,
        child: Container(
            margin:
                EdgeInsets.only(top: 2.0, bottom: 2.0, left: 3.0, right: 3.0),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                border: Border.all(
                  width: 1,
                  color: Colors.primaries[index % 10],
                ),
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                gradient: LinearGradient(
                  colors: [
                    Colors.primaries[index % 10].withAlpha(100),
                    Colors.accents[index % 10].withAlpha(100),
                  ],
                  begin: FractionalOffset(1, 0),
                  end: FractionalOffset(0, 1),
                  stops: [0, 1],
                  tileMode: TileMode.clamp,
                )),
            child: Consumer<_UserProperty>(
                builder: (context, model, _) => Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10.0),
                          child: Text(
                            "${property.propName}: ",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.underline),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10.0),
                          child: property.selected
                              ? SizedBox(
                                  height: 20.0,
                                  width: 150.0,
                                  child: TextFormField(
                                    maxLines: 1,
                                    controller: inputController,
                                    onFieldSubmitted: (_) => _save(),
                                  ),
                                )
                              : Text(
                                  "${property.displayValue ?? S.current.no_value}",
                                ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10.0),
                          child: GestureDetector(
                            onTap: () => property.selected
                                ? _save()
                                : _startEditproperty(property, context),
                            child: property.setProperty != null
                                ? Icon(
                                    property.selected ? Icons.save : Icons.edit,
                                    color: Colors.grey[700],
                                  )
                                : Container(),
                          ),
                        ),
                      ],
                    ))));
  }

  Widget _userAccessTokenField(BuildContext context, int index) {
    UserToken _token = _user.userTokens[index];
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 3.0),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            border: Border.all(
              width: 1,
              color: Colors.primaries[index % 10],
            ),
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            gradient: LinearGradient(
              colors: [
                Colors.primaries[index % 10].withAlpha(100),
                Colors.accents[index % 10].withAlpha(100),
              ],
              begin: FractionalOffset(1, 0),
              end: FractionalOffset(0, 1),
              stops: [0, 1],
              tileMode: TileMode.clamp,
            )),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      S.current.accessTokenN(_token.tokenId),
                      // _formatter.format(_token.tokenId),
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      S.current.expiresOn(_token.expirationDate ?? "???"),
                      textAlign: TextAlign.center,
                      textScaleFactor: 0.7,
                    ),
                  ],
                )),
          ],
        ));
  }

  UserNotifier userNotifier;

  @override
  Widget build(BuildContext context) {
    userNotifier = Provider.of<UserNotifier>(context, listen: true);
    _user = userNotifier.value;
    _user.password = "";
    UserBloc bloc = BlocProvider.of<UserBloc>(context);
    _initPropList(bloc);
    return Consumer<UserNotifier>(
      builder: (context, value, child) {
        _user = value.value;
        _initPropList(bloc);
        return child;
      },
      child: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            pinned: false,
            floating: true,
            leading: Container(),
            backgroundColor: Colors.cyan[50],
            expandedHeight: 300.0,
            flexibleSpace: FlexibleSpaceBar(
              title: Text("${_user.name} ${_user.surname}"),
              centerTitle: true,
              background: Padding(
                padding: EdgeInsets.only(top: 20, bottom: 20),
                child: Container(
                  child: Image.asset(
                    'assets/icon/logo.png',
                  ),
                ),
              ),
            ),
          ),
          SliverAppBar(
            pinned: true,
            leading: Container(),
            title: Text(S.current.userDetails),
            centerTitle: true,
          ),
          SliverGrid(
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 550.0,
              mainAxisSpacing: 5.0,
              crossAxisSpacing: 5.0,
              childAspectRatio: 10.0,
            ),
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) =>
                  _userDetailField(context, index),
              childCount: _userPropertiresList == null
                  ? 0
                  : _userPropertiresList.length,
            ),
          ),
          SliverAppBar(
            pinned: true,
            leading: Container(),
            title: Text(S.current.accessTokens),
            centerTitle: true,
          ),
          SliverGrid(
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 250.0,
              mainAxisSpacing: 5.0,
              crossAxisSpacing: 5.0,
              childAspectRatio: 5.0,
            ),
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) =>
                  _userAccessTokenField(context, index),
              childCount:
                  _user.userTokens == null ? 0 : _user.userTokens.length,
            ),
          ),
        ],
      ),
    );
  }
}
