import 'package:dreamteam_project/blocks/admin_payments/admin_payments_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/screens/admin/payments_screen/add_payment_base.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AddPaymentStudent extends StatefulWidget {
  final User user;
  final Payment payment;

  const AddPaymentStudent({Key key, @required this.user, this.payment})
      : super(key: key);
  @override
  State<StatefulWidget> createState() => _AddPaymentStudentState(user, payment);
}

class _AddPaymentStudentState extends AddPaymentBase<AddPaymentStudent> {
  final User user;
  final Payment payment;

  bool accounted = true;

  _AddPaymentStudentState(this.user, [this.payment]) : super(payment);

  @override
  void initState() {
    super.initState();
    accounted = payment?.paid ?? false;
  }

  @override
  bool get inputValid => accounted == null ? false : super.inputValid;

  @override
  String get title => S.current.addStudentPayment;

  @override
  AdminPaymentsEvent get addPaymentEvent => AdminPaymentsEvent.addedUserPayment(
        userIds: [user.userId],
        startDueDate: currentDate,
        paymentName: nameController.text,
        amount: int.parse(amountController.text),
        repeatMode: dropdownValue,
      );

  @override
  AdminPaymentsEvent get updatePaymentEvent =>
      AdminPaymentsEvent.updatedPayment(
        paymentId: payment?.paymentId,
        newDueDate: currentDate,
        newName: nameController.text,
        newAmount: int.parse(amountController.text),
        isAccounted: accounted,
      );

  @override
  List<Widget> get accountedSection => [
        SizedBox(
          height: 15,
        ),
        Text(
          S.of(context).isAccounted,
          style: TextStyle(fontSize: 20.0, color: Colors.grey[700]),
        ),
        CheckboxListTile(
          title: Text(S.of(context).accounted),
          value: accounted,
          onChanged: (newValue) {
            setState(() {
              accounted = newValue;
            });
          },
        ),
      ];

  @override
  void notifyNotifierListeners() {}

  @override
  String get forWhomText => user.name;
}
