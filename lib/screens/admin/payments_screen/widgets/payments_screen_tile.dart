import 'package:dreamteam_project/screens/admin/payments_screen/admin_payments_view_base.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

abstract class AdminPaymentsTile<T extends StatefulWidget> extends State<T> {
  final ValueNotifier<PaymentsStatus> statusNotifier;
  PaymentsStatus paymentsStatus;

  AdminPaymentsTile(this.statusNotifier);

  @override
  void initState() {
    super.initState();
    paymentsStatus = statusNotifier?.value;
  }

  List<Widget> get pastDueStatus {
    if (paymentsStatus.paymentsPastDue <= 0) return List.empty();
    return <Widget>[
      Icon(
        Icons.notification_important_outlined,
        size: 30.0,
        color: Colors.red,
      ),
      Text(
        "${paymentsStatus.paymentsPastDue} | ",
        style: TextStyle(
          color: Colors.grey[700],
          fontSize: 22.0,
        ),
      ),
    ];
  }

  List<Widget> get paidStatus {
    return <Widget>[
      Icon(
        paymentsStatus.paymentsPastDue == 0
            ? Icons.check_circle
            : Icons.check_circle_outline_outlined,
        size: 30.0,
        color: Colors.green,
      ),
      Text(
        paymentsStatus.paymentsPaid.toString(),
        style: TextStyle(
          color: Colors.grey[700],
          fontSize: 22.0,
        ),
      ),
    ];
  }

  Widget title();

  Widget tileClickNextScreen(BuildContext context);

  Widget buildWidget(BuildContext context) {
    paymentsStatus = statusNotifier?.value;
    return Card(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Padding(
        padding: EdgeInsets.all(12.0),
        child: ListTile(
          title: title(),
          subtitle: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: paymentsStatus == null
                ? <Widget>[
                    // TODO: Customize Loading placeholder maybe(?)
                    Icon(
                      Icons.autorenew,
                      size: 30,
                      color: Colors.grey[700],
                    ),
                  ]
                : <Widget>[
                    ...pastDueStatus,
                    ...paidStatus,
                  ],
          ),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: tileClickNextScreen,
              ),
            );
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => statusNotifier,
      child: buildWidget(context),
    );
  }
}
