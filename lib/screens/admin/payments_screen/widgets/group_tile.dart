import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/screens/admin/payments_screen/admin_payments_view_base.dart';
import 'package:dreamteam_project/screens/admin/payments_screen/widgets/payments_screen_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../group_payments_screen.dart';

class GroupsPaymentsStatusTile extends StatefulWidget {
  final ValueNotifier<PaymentsStatus> statusNotifier;
  final Group group;

  const GroupsPaymentsStatusTile({
    Key key,
    @required this.statusNotifier,
    @required this.group,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _GroupsPaymentsStatusTileState(
        this.statusNotifier,
        this.group,
      );
}

class _GroupsPaymentsStatusTileState
    extends AdminPaymentsTile<GroupsPaymentsStatusTile> {
  final ValueNotifier<PaymentsStatus> statusNotifier;
  final Group group;
  PaymentsStatus paymentsStatus;

  _GroupsPaymentsStatusTileState(this.statusNotifier, this.group)
      : super(statusNotifier);

  @override
  Widget tileClickNextScreen(BuildContext context) =>
      GroupPaymentsList(group: group);

  @override
  Widget title() => Text(
        group.name,
        style: TextStyle(
          fontSize: 22.0,
          color: Colors.grey[700],
          fontWeight: FontWeight.bold,
        ),
      );
}
