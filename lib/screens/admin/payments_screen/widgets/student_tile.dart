import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/screens/admin/payments_screen/admin_payments_view_base.dart';
import 'package:dreamteam_project/screens/admin/payments_screen/widgets/payments_screen_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../student_payments_screen.dart';

class StudentPaymentsStatusTile extends StatefulWidget {
  final ValueNotifier<PaymentsStatus> statusNotifier;
  final User user;

  const StudentPaymentsStatusTile({
    Key key,
    @required this.statusNotifier,
    @required this.user,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _StudentPaymentsStatusTileState(
        this.statusNotifier,
        this.user,
      );
}

class _StudentPaymentsStatusTileState
    extends AdminPaymentsTile<StudentPaymentsStatusTile> {
  final ValueNotifier<PaymentsStatus> statusNotifier;
  final User user;
  PaymentsStatus paymentsStatus;

  _StudentPaymentsStatusTileState(this.statusNotifier, this.user)
      : super(statusNotifier);

  @override
  void initState() {
    super.initState();
    paymentsStatus = statusNotifier?.value ?? null;
  }

  @override
  Widget title() => Text(
        (user?.name ?? "Name") + " " + (user?.surname ?? "Surname"),
        style: TextStyle(
          fontSize: 22.0,
          color: Colors.grey[700],
          fontWeight: FontWeight.bold,
        ),
      );

  @override
  Widget tileClickNextScreen(BuildContext context) =>
      StudentPaymentsList(user: user);
}
