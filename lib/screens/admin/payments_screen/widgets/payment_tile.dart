import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../add_payment_student_screen.dart';

class StudentPaymentTile extends StatelessWidget {
  final Payment payment;
  final User user;
  final Icon icon;
  final void Function(BuildContext context, Payment payment)
      showAccountPaymentDialog;

  static String getDateDueTrailing(Payment payment) =>
      S.current.timeDue +
      ": " +
      DateFormat('dd.MM.yyyy').format(payment.dateDue);

  String getDatePaidTrailing(Payment payment) =>
      S.current.timePaid +
      ": " +
      DateFormat('dd.MM.yyyy').format(payment.datePaid);

  const StudentPaymentTile({
    Key key,
    @required this.payment,
    @required this.user,
    @required this.icon,
    this.showAccountPaymentDialog,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Padding(
        padding: EdgeInsets.all(12.0),
        child: ListTile(
          leading: IconButton(
            icon: icon,
            onPressed: () {
              if (showAccountPaymentDialog != null)
                showAccountPaymentDialog(context, payment);
            },
          ),
          title: Text(
            payment.amount.toString() + " zł",
            style: TextStyle(
              fontSize: 22.0,
              color: Colors.grey[700],
              fontWeight: FontWeight.bold,
            ),
          ),
          subtitle: Text(payment.paymentName),
          trailing: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: payment.paid
                ? [
                    Text(getDateDueTrailing(payment)),
                    Text(
                      getDatePaidTrailing(payment),
                      style: TextStyle(
                        color: payment.datePaid.isAfter(payment.dateDue)
                            ? Colors.red
                            : Colors.green,
                      ),
                    )
                  ]
                : [Text(getDateDueTrailing(payment))],
          ),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AddPaymentStudent(
                  user: user,
                  payment: payment,
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
