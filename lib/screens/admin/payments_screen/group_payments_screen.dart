import 'package:dreamteam_project/blocks/admin_payments/admin_payments_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/screens/admin/payments_screen/widgets/student_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'admin_payments_view_base.dart';

class GroupPaymentsList extends StatefulWidget {
  final Group group;

  const GroupPaymentsList({
    Key key,
    @required this.group,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _GroupPaymentsListState(group);
}

class _GroupPaymentsListState extends AdminPaymentsViewBase<GroupPaymentsList> {
  final Group group;
  List<User> users = [];
  // List<User> usersPaid = [];
  // DateTime dateNow = DateTime.now();
  // DateTime datePaid = DateTime.utc(2021, 4, 4);

  _GroupPaymentsListState(this.group);

  @override
  void initState() {
    super.initState();
    // users.add(new User(userId: "1", name: "Jan", surname: "Nowak"));
    // usersPaid.add(new User(userId: "1", name: "Jan", surname: "Makowski"));
    // usersPaid.add(new User(userId: "2", name: "Marek", surname: "Kowalski"));
    // usersPaid.add(new User(userId: "3", name: "Jerzy", surname: "Jeżyk"));
  }

  @override
  void notifyNotifierListeners() {
    setState(() {
      users = usersNotifier.value;
    });
  }

  List<AdminPaymentsEvent> get listStartupEvents =>
      [AdminPaymentsEvent.loadedGroupPayments(groupId: group.groupId)];

  @override
  Widget buildScreen(BuildContext context) {
    users = usersNotifier.value;
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).studentsPaymentsList),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Text(
                S.of(context).paymentsPastDue,
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey[700],
                ),
              ),
            ),
            ListView.builder(
              itemCount: users?.length,
              shrinkWrap: true,
              physics: ScrollPhysics(),
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              itemBuilder: (context, position) {
                User student = users[position];
                var notifier = userStatusNotifiers.containsKey(student.userId)
                    ? userStatusNotifiers[student.userId]
                    : null;
                return StudentPaymentsStatusTile(
                  statusNotifier: notifier,
                  user: student,
                );
              },
            ),
            // Padding(
            //   padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            //   child: Text(
            //     S.of(context).paymentsPaid,
            //     style: TextStyle(
            //       fontSize: 22.0,
            //       fontWeight: FontWeight.bold,
            //       color: Colors.grey[700],
            //     ),
            //   ),
            // ),
            // ListView.builder(
            //   itemCount: usersPaid?.length,
            //   shrinkWrap: true,
            //   physics: ScrollPhysics(),
            //   padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
            //   itemBuilder: (context, position) {
            //     return Card(
            //       elevation: 10,
            //       shape: RoundedRectangleBorder(
            //         borderRadius: BorderRadius.circular(15.0),
            //       ),
            //       child: Padding(
            //         padding: EdgeInsets.all(12.0),
            //         child: ListTile(
            //           title: Text(
            //             usersPaid[position].name +
            //                 " " +
            //                 usersPaid[position].surname,
            //             style: TextStyle(
            //               fontSize: 22.0,
            //               color: Colors.grey[700],
            //               fontWeight: FontWeight.bold,
            //             ),
            //           ),
            //           subtitle: Row(
            //             mainAxisAlignment: MainAxisAlignment.end,
            //             children: <Widget>[
            //               Icon(
            //                 Icons.check_circle_outline_outlined,
            //                 size: 30.0,
            //                 color: Colors.green,
            //               ),
            //               SizedBox(
            //                 width: 10,
            //               ),
            //               Text(
            //                 S.of(context).timeDue +
            //                     ": " +
            //                     DateFormat('dd-MM-yyyy').format(datePaid),
            //                 style: TextStyle(
            //                   color: Colors.grey[700],
            //                   fontSize: 22.0,
            //                 ),
            //               ),
            //             ],
            //           ),
            //           onTap: () {
            //             Navigator.push(
            //               context,
            //               MaterialPageRoute(
            //                 builder: (context) => StudentPaymentsList(),
            //               ),
            //             );
            //           },
            //         ),
            //       ),
            //     );
            //   },
            // ),
          ],
        ),
      ),
    );
  }
}
