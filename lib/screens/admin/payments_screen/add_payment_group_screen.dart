import 'package:dreamteam_project/blocks/admin_payments/admin_payments_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/screens/admin/payments_screen/add_payment_base.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AddPaymentGroup extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AddPaymentGroupState();
}

class _AddPaymentGroupState extends AddPaymentBase<AddPaymentGroup> {
  List<Group> groups = [];
  List<String> filters = [];

  _AddPaymentGroupState() : super(null);

  List<Widget> get groupsSelectSection => [
        SizedBox(
          height: 15,
        ),
        Text(
          S.of(context).Groups,
          style: TextStyle(
            fontSize: 20.0,
            color: Colors.grey[700],
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Wrap(
          children: userGroupPosition.toList(),
        ),
      ];

  @override
  void notifyNotifierListeners() {
    setState(() {
      groups = groupsNotifier.value;
    });
  }

  @override
  Widget buildScreen(BuildContext context) {
    groups = groupsNotifier.value;
    return super.buildScreen(context);
  }

  @override
  bool get inputValid => filters.isEmpty ? false : super.inputValid;

  @override
  String get title => S.current.addGroupPayment;

  @override
  String get forWhomText => filters.join(", ");

  @override
  List<AdminPaymentsEvent> get listStartupEvents =>
      [AdminPaymentsEvent.loadedGroups()];

  @override
  AdminPaymentsEvent get addPaymentEvent =>
      AdminPaymentsEvent.addedGroupPayment(
        groupIds: filters,
        startDueDate: currentDate,
        paymentName: nameController.text,
        amount: int.parse(amountController.text),
        repeatMode: dropdownValue,
      );

  Iterable<Widget> get userGroupPosition sync* {
    for (Group group in groups) {
      yield Padding(
        padding: const EdgeInsets.all(6.0),
        child: FilterChip(
          backgroundColor: Colors.tealAccent[200],
          avatar: CircleAvatar(
            backgroundColor: Colors.cyan,
            child: Text(
              group.name[0].toUpperCase(),
              style: TextStyle(color: Colors.white),
            ),
          ),
          label: Text(
            group.name,
          ),
          selected: filters.contains(group.groupId),
          selectedColor: Colors.purpleAccent,
          onSelected: (bool selected) {
            setState(() {
              if (selected) {
                filters.add(group.groupId);
              } else {
                filters.removeWhere((String groupId) {
                  return groupId == group.groupId;
                });
              }
            });
          },
        ),
      );
    }
  }
}
