import 'package:dreamteam_project/blocks/admin_payments/admin_payments_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'admin_payments_view_base.dart';
import 'widgets/payment_tile.dart';

class StudentHistoryPaymentsList extends StatefulWidget {
  final User user;

  const StudentHistoryPaymentsList({Key key, @required this.user})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _StudentHistoryPaymentsListState(user);
}

class _StudentHistoryPaymentsListState
    extends AdminPaymentsViewBase<StudentHistoryPaymentsList> {
  List<Payment> payments = [];
  final User user;

  _StudentHistoryPaymentsListState(this.user);

  @override
  void notifyNotifierListeners() {
    setState(
        () => payments = paymentsNotifier.value.where((p) => p.paid).toList());
  }

  List<AdminPaymentsEvent> get listStartupEvents =>
      [AdminPaymentsEvent.loadedUserHistoryPayments(userId: user.userId)];

  @override
  Widget buildScreen(BuildContext context) {
    payments = paymentsNotifier.value.where((p) => p.paid).toList();
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).studentPaymentsList),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Text(
                S.of(context).history,
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey[700],
                ),
              ),
            ),
            ListView.builder(
              itemCount: payments?.length,
              shrinkWrap: true,
              physics: ScrollPhysics(),
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              itemBuilder: (context, position) {
                Payment p = payments[position];
                return StudentPaymentTile(
                  payment: p,
                  user: user,
                  icon: Icon(
                    Icons.check_circle_outline_outlined,
                    size: 30.0,
                    color: Colors.green,
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
