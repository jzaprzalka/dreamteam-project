import 'package:dreamteam_project/blocks/admin_payments/admin_payments_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import 'admin_payments_view_base.dart';

abstract class AddPaymentBase<T> extends AdminPaymentsViewBase {
  final Payment payment;

  RepeatMode dropdownValue;
  DateTime currentDate = DateTime.now().add(Duration(days: 10));
  TextEditingController nameController = TextEditingController();
  TextEditingController amountController = TextEditingController();

  AddPaymentBase(this.payment);

  @override
  void initState() {
    super.initState();
    nameController.text = payment?.paymentName ?? "";
    amountController.text = payment?.amount?.toString() ?? "";
    currentDate = payment?.dateDue ?? DateTime.now().add(Duration(days: 10));
    dropdownValue = RepeatMode.OneTime;
  }

  bool get inputValid {
    if (dropdownValue == null) return false;
    if (currentDate == null) return false;
    if (nameController.text.isEmpty) return false;
    if (amountController.text.isEmpty) return false;
    return true;
  }

  AdminPaymentsEvent get addPaymentEvent;
  AdminPaymentsEvent get updatePaymentEvent => null;

  String get forWhomText;

  void addPayment(BuildContext context) {
    if (!inputValid) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(S.current.addPaymentFailed),
        ),
      );
      return;
    }
    if (payment == null || updatePaymentEvent == null)
      bloc.add(addPaymentEvent);
    else
      bloc.add(updatePaymentEvent);
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(S.current.addPaymentSuccess(
          forWhomText,
          dropdownValue.toString(),
        )),
      ),
    );
    Navigator.pop(context);
  }

  String get title;

  List<Widget> get groupsSelectSection => [];

  List<Widget> get accountedSection => [];

  String repeatModeText(RepeatMode repeatMode) {
    String strValue;
    switch (repeatMode) {
      case RepeatMode.OneTime:
        strValue = S.of(context).dontRepeat;
        break;
      case RepeatMode.Weekly:
        strValue = S.of(context).repeatEveryWeek;
        break;
      case RepeatMode.Monthly:
        strValue = S.of(context).repeatEveryMonth;
        break;
    }
    return strValue;
  }

  @override
  List<AdminPaymentsEvent> get listStartupEvents =>
      [AdminPaymentsEvent.loadedGroups()];

  @override
  Widget buildScreen(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.save,
              color: Colors.black,
            ),
            onPressed: () => addPayment(context),
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text(
                S.of(context).paymentName,
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.grey[700],
                ),
              ),
              SizedBox(
                height: 5,
              ),
              TextField(
                decoration: InputDecoration(
                  labelText: S.of(context).enterPaymentName,
                  border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.teal),
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                controller: nameController,
                keyboardType: TextInputType.name,
                maxLines: 1,
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                S.of(context).paymentAmount,
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.grey[700],
                ),
              ),
              SizedBox(
                height: 5,
              ),
              TextField(
                decoration: InputDecoration(
                  labelText: S.of(context).enterPaymentAmount,
                  border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.teal),
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                controller: amountController,
                keyboardType: TextInputType.numberWithOptions(decimal: true),
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp(r'[0-9.,]'))
                ],
                maxLines: 1,
              ),
              ...groupsSelectSection,
              SizedBox(
                height: 15,
              ),
              Text(
                S.of(context).paymentDeadline,
                style: TextStyle(fontSize: 20.0, color: Colors.grey[700]),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey,
                  ),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    DateFormat('dd.MM.yyyy').format(currentDate),
                    style: TextStyle(fontSize: 15.0, color: Colors.grey[700]),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  TextButton(
                    onPressed: () => _selectDate(context),
                    child: Text(
                      S.of(context).changePaymentDeadline,
                      style: TextStyle(
                        fontSize: 15.0,
                      ),
                    ),
                  ),
                ],
              ),
              Text(
                S.of(context).repeatPayment,
                style: TextStyle(fontSize: 20.0, color: Colors.grey[700]),
              ),
              Padding(
                padding: const EdgeInsets.all(5),
                child: DropdownButton<RepeatMode>(
                  items: RepeatMode.values
                      .map((value) => DropdownMenuItem<RepeatMode>(
                            value: value,
                            child: Text(repeatModeText(value)),
                          ))
                      .toList(),
                  onChanged: (newValue) {
                    setState(() {
                      dropdownValue = newValue;
                    });
                  },
                  value: dropdownValue,
                  iconSize: 30.0,
                  isExpanded: true,
                ),
              ),
              ...accountedSection,
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime pickedDate = await showDatePicker(
      context: context,
      initialDate: currentDate,
      firstDate: DateTime(DateTime.now().year - 5),
      lastDate: DateTime(DateTime.now().year + 5),
    );
    if (pickedDate != null && pickedDate != currentDate)
      setState(() {
        currentDate = pickedDate;
      });
  }
}
