import 'package:dreamteam_project/blocks/admin_payments/admin_payments_bloc.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/screens/admin/payments_screen/add_payment_group_screen.dart';
import 'package:dreamteam_project/screens/admin/payments_screen/admin_payments_view_base.dart';
import 'package:dreamteam_project/screens/admin/payments_screen/widgets/group_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dreamteam_project/generated/l10n.dart';

class GroupsPaymentsList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _GroupsPaymentsListState();
}

class _GroupsPaymentsListState
    extends AdminPaymentsViewBase<GroupsPaymentsList> {
  List<Group> groups = [];

  @override
  void initState() {
    super.initState();
    // groups.add(new Group(
    //   groupId: "1",
    //   name: "Group 1",
    // ));
    // groups.add(new Group(groupId: "2", name: "Group 2"));
    // groups.add(new Group(groupId: "3", name: "Group 3"));
  }

  @override
  void notifyNotifierListeners() {
    setState(() {
      groups = groupsNotifier.value;
    });
  }

  @override
  List<AdminPaymentsEvent> get listStartupEvents =>
      [AdminPaymentsEvent.loadedPayments()];

  @override
  Widget buildScreen(BuildContext context) {
    groups = groupsNotifier.value;
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).groupsPaymentsList),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.add,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AddPaymentGroup()),
              );
            },
          ),
        ],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ListView.builder(
              itemCount: groups?.length,
              shrinkWrap: true,
              physics: ScrollPhysics(),
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              itemBuilder: (context, position) {
                Group group = groups[position];
                return GroupsPaymentsStatusTile(
                  statusNotifier: groupStatusNotifiers[group.groupId],
                  group: group,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
