import 'package:dreamteam_project/blocks/admin_payments/admin_payments_bloc.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class PaymentsStatus {
  final int paymentsPastDue;
  final int paymentsPaid;
  final int paymentsApproachingDue;

  PaymentsStatus({
    @required this.paymentsPastDue,
    @required this.paymentsPaid,
    this.paymentsApproachingDue,
  });

  @override
  String toString() {
    return "$paymentsPaid | $paymentsPastDue";
  }
}

abstract class AdminPaymentsViewBase<T extends StatefulWidget>
    extends State<T> {
  ValueNotifier<List<Group>> groupsNotifier;
  ValueNotifier<List<User>> usersNotifier;
  ValueNotifier<List<Payment>> paymentsNotifier;
  ValueNotifier<PaymentsStatus> overallStatusNotifier;
  Map<String, ValueNotifier<PaymentsStatus>> groupStatusNotifiers;
  Map<String, ValueNotifier<PaymentsStatus>> userStatusNotifiers;
  AdminPaymentsBloc bloc;

  @override
  void initState() {
    super.initState();
    groupsNotifier = ValueNotifier(List.empty());
    usersNotifier = ValueNotifier(List.empty());
    paymentsNotifier = ValueNotifier(List.empty());
    overallStatusNotifier = ValueNotifier(null);
    groupStatusNotifiers = Map();
    userStatusNotifiers = Map();
  }

  List<AdminPaymentsEvent> get listStartupEvents => [];

  List<AdminPaymentsEvent> get statusStartupEvents => [];

  List<AdminPaymentsEvent> get _startupEvents =>
      [...listStartupEvents, ...statusStartupEvents];

  void notifyNotifierListeners();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => groupsNotifier,
      child: BlocProvider<AdminPaymentsBloc>(
        create: (context) {
          bloc = AdminPaymentsBloc();
          logger.d(
              "Dispatching Admin Payments startup events (${_startupEvents.length})");
          for (AdminPaymentsEvent ape in _startupEvents) bloc.add(ape);
          return bloc;
        },
        child: BlocConsumer<AdminPaymentsBloc, AdminPaymentsState>(
          listener: (context, state) {
            state.map(
              initial: (s) {
                logger.i("Dispatching startup events for Admin Payments Bloc");
                for (AdminPaymentsEvent ape in _startupEvents) bloc.add(ape);
              },
              paymentsInvalid: (s) {
                logger.i("Refreshing list startup events");
                for (AdminPaymentsEvent ape in listStartupEvents) bloc.add(ape);
              },
              paymentsStatusInvalid: (s) {
                logger.i("Refreshing status startup events");
                for (AdminPaymentsEvent ape in statusStartupEvents)
                  bloc.add(ape);
              },
              userPaymentsUpdated: (s) {
                logger.i("Refreshing user payments");
                paymentsNotifier.value = s.payments;
              },
              userPaymentsHistoryUpdated: (s) {
                logger.i("Refreshing user history payments");
                paymentsNotifier.value = s.payments;
              },
              overallPaymentsStatusUpdated: (s) {
                logger.i("Refreshing overall payments status");
                overallStatusNotifier.value = PaymentsStatus(
                  paymentsPastDue: s.paymentsPastDue,
                  paymentsPaid: s.paymentsPaid,
                  paymentsApproachingDue: s.paymentsApproachingDue,
                );
              },
              groupPaymentsStatusUpdated: (s) {
                logger.i("Refreshing group payments status");
                var ps = PaymentsStatus(
                  paymentsPastDue: s.paymentsPastDue,
                  paymentsPaid: s.paymentsPaid,
                );
                groupStatusNotifiers.update(
                  s.groupId,
                  (valueNotifier) {
                    valueNotifier.value = ps;
                    return valueNotifier;
                  },
                  ifAbsent: () {
                    logger.i("No user group notifier for ${s.groupId}");
                    return ValueNotifier(ps);
                  },
                );
              },
              userPaymentsStatusUpdated: (s) {
                logger.i("Refreshing user payments status");
                var ps = PaymentsStatus(
                  paymentsPastDue: s.paymentsPastDue,
                  paymentsPaid: s.paymentsPaid,
                );
                userStatusNotifiers.update(
                  s.userId,
                  (valueNotifier) {
                    valueNotifier.value = ps;
                    return valueNotifier;
                  },
                  ifAbsent: () {
                    logger.i("No user status notifier for ${s.userId}");
                    return ValueNotifier(ps);
                  },
                );
              },
              groupListUpdated: (s) {
                logger.i("Refreshing group list");
                for (Group g in s.groups) {
                  groupStatusNotifiers.update(
                    g.groupId,
                    (valueNotifier) => valueNotifier ?? ValueNotifier(null),
                    ifAbsent: () => ValueNotifier(null),
                  );
                }
                groupsNotifier.value = s.groups;
              },
              userListUpdated: (s) {
                logger.i("Refreshing users list");
                for (User g in s.users) {
                  userStatusNotifiers.update(
                    g.userId,
                    (valueNotifier) => valueNotifier ?? ValueNotifier(null),
                    ifAbsent: () => ValueNotifier(null),
                  );
                }
                usersNotifier.value = s.users;
              },
            );
            notifyNotifierListeners();
          },
          builder: (context, state) => buildScreen(context),
        ),
      ),
    );
  }

  Widget buildScreen(BuildContext context);
}
