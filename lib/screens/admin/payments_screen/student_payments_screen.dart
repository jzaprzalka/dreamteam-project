import 'package:dreamteam_project/blocks/admin_payments/admin_payments_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/screens/admin/payments_screen/add_payment_student_screen.dart';
import 'package:dreamteam_project/screens/admin/payments_screen/admin_payments_view_base.dart';
import 'package:dreamteam_project/screens/admin/payments_screen/student_history_payments_screen.dart';
import 'package:dreamteam_project/screens/admin/payments_screen/widgets/payment_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StudentPaymentsList extends StatefulWidget {
  final User user;

  const StudentPaymentsList({Key key, @required this.user}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _StudentPaymentsListState(user);
}

class _StudentPaymentsListState
    extends AdminPaymentsViewBase<StudentPaymentsList> {
  List<Payment> payments = [];
  List<Payment> paymentsPastDue = [];
  List<Payment> paymentsApproachingDue = [];
  final User user;

  _StudentPaymentsListState(this.user);

  @override
  void notifyNotifierListeners() {
    setState(() {
      payments = paymentsNotifier.value;
      // paymentsPastDue =
      //     payments.where((p) => p.dateDue.isBefore(DateTime.now())).toList();
      // paymentsApproachingDue =
      //     payments.where((p) => !p.dateDue.isBefore(DateTime.now())).toList();
    });
  }

  List<AdminPaymentsEvent> get listStartupEvents =>
      [AdminPaymentsEvent.loadedUserPayments(userId: user.userId)];

  @override
  Widget buildScreen(BuildContext context) {
    payments = paymentsNotifier.value;
    paymentsPastDue =
        payments.where((p) => p.dateDue.isBefore(DateTime.now())).toList();
    paymentsApproachingDue =
        payments.where((p) => !p.dateDue.isBefore(DateTime.now())).toList();
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).studentPaymentsList),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.add,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => AddPaymentStudent(
                          user: user,
                        )),
              );
            },
          ),
          IconButton(
            icon: Icon(
              Icons.history_outlined,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        StudentHistoryPaymentsList(user: user)),
              );
            },
          ),
        ],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Text(
                S.of(context).paymentsPastDue,
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey[700],
                ),
              ),
            ),
            ListView.builder(
              itemCount: paymentsPastDue?.length,
              shrinkWrap: true,
              physics: ScrollPhysics(),
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              itemBuilder: (context, position) {
                Payment p = paymentsPastDue[position];
                return StudentPaymentTile(
                  payment: p,
                  user: user,
                  icon: Icon(
                    Icons.notification_important_outlined,
                    size: 30.0,
                    color: Colors.red,
                  ),
                  showAccountPaymentDialog: showAlertDialog,
                );
              },
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Text(
                S.of(context).paymentsApproachingDue,
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey[700],
                ),
              ),
            ),
            ListView.builder(
              itemCount: paymentsApproachingDue?.length,
              shrinkWrap: true,
              physics: ScrollPhysics(),
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              itemBuilder: (context, position) {
                Payment p = paymentsApproachingDue[position];
                return StudentPaymentTile(
                  payment: p,
                  user: user,
                  icon: Icon(
                    Icons.notifications_outlined,
                    size: 30.0,
                    color: Colors.orange,
                  ),
                  showAccountPaymentDialog: showAlertDialog,
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  void showAlertDialog(BuildContext context, Payment payment) {
    // set up the buttons
    Widget cancelButton = TextButton(
      child: Text(S.of(context).no),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = TextButton(
      child: Text(S.of(context).yes),
      onPressed: () {
        bloc.add(AdminPaymentsEvent.paymentAccounted(
          payment: payment,
          dateAccounted: DateTime.now(),
        ));
        Navigator.of(context).pop();
      },
    );

    // Set up and show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Text(S.of(context).accountPayment),
        content: Text(S.of(context).accountQuestion +
            "\n${payment.paymentName}: ${payment.amount} zł"),
        actions: [
          cancelButton,
          continueButton,
        ],
      ),
    );
  }
}
