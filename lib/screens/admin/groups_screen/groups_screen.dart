import 'package:dreamteam_project/blocks/groups/groups_bloc.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

abstract class AdminGroupsScreenStateBase<T extends StatefulWidget>
    extends State<T> {
  ValueNotifier<List<Group>> groupsNotifier;
  ValueNotifier<List<User>> usersNotifier;
  ValueNotifier<Group> currentGroupNotifier;
  // List<Group> _groups = [];
  GroupsBloc bloc;

  @override
  void initState() {
    super.initState();
    groupsNotifier = ValueNotifier(List.empty());
    usersNotifier = ValueNotifier(List.empty());
    currentGroupNotifier = ValueNotifier(null);
  }

  List<GroupsEvent> get additionalStartupEvents => [];

  List<GroupsEvent> get _startupEvents =>
      [GroupsEvent.loadedGroups(isCoach: true), ...additionalStartupEvents];

  void notifyNotifierListeners() => null;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => groupsNotifier,
      child: BlocProvider<GroupsBloc>(
        create: (context) {
          bloc = GroupsBloc();
          logger
              .d("Dispatching Group startup events (${_startupEvents.length})");
          for (GroupsEvent ge in _startupEvents) bloc.add(ge);
          return bloc;
        },
        child: BlocConsumer<GroupsBloc, GroupsState>(
          listener: (context, state) {
            if (state is GroupsUpdated) {
              Provider.of<ValueNotifier<List<Group>>>(context, listen: false)
                  .value = state.groups;
            }
            if (state is UsersUpdated) usersNotifier.value = state.users;
            if (state is GroupUpdated) currentGroupNotifier.value = state.group;
            notifyNotifierListeners();
          },
          builder: (context, state) => buildScreen(context),
        ),
      ),
    );
  }

  Widget buildScreen(BuildContext context);

  DropdownMenuItem<User> _getDropDownItem(User user) {
    return new DropdownMenuItem(
        child: new Text(
          "${user.name} ${user.surname}",
          style: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
              color: Colors.grey[600]),
        ),
        value: user);
  }

  List<DropdownMenuItem<User>> getDropDownMenuItems(List<User> users) =>
      users.map((user) => _getDropDownItem(user)).toList();
}
