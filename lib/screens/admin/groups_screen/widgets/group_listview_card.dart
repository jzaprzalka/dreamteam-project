import 'package:dreamteam_project/blocks/groups/groups_bloc.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../group_attendance_screen.dart';
import '../group_edit_screen.dart';

class AdminGroupListViewCardWidget extends StatelessWidget {
  final Group group;

  const AdminGroupListViewCardWidget({Key key, @required this.group})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 6.0),
                child: Text(
                  group.name,
                  style: TextStyle(
                      fontSize: 22.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.grey[700]),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) {
                            return BlocProvider(
                              create: (context) => GroupsBloc(),
                              child: AdminEditGroup(editGroup: group),
                            );
                          }),
                        );
                      },
                      child:
                          Icon(Icons.edit, size: 35.0, color: Colors.cyan[100]),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AttendanceList(
                                      attendanceList: group,
                                    )),
                          );
                        },
                        child: Icon(
                          Icons.group_outlined,
                          size: 35.0,
                          color: Colors.cyan[100],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
