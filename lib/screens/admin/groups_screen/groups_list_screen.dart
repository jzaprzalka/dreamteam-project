import 'package:dreamteam_project/blocks/groups/groups_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/screens/admin/groups_screen/widgets/group_listview_card.dart';
import 'package:dreamteam_project/screens/coach/groups_screen/widgets/group_listview_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'group_add_screen.dart';
import 'groups_screen.dart';

class AdminGroupsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AdminGroupsScreenState();
}

class _AdminGroupsScreenState extends AdminGroupsScreenStateBase {
  @override
  Widget buildScreen(BuildContext context) {
    List<Group> groups = groupsNotifier.value;
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).Groups),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.add,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) {
                  return BlocProvider(
                    create: (context) => GroupsBloc(),
                    child: AdminAddGroup(),
                  );
                }),
              );
            },
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: ListView.builder(
          itemBuilder: (context, position) =>
              AdminGroupListViewCardWidget(group: groups[position]),
          itemCount: groups?.length ?? 0),
    );
  }
}
