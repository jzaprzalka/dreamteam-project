import 'package:dreamteam_project/blocks/groups/groups_bloc.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'groups_screen.dart';

class AdminAddGroup extends StatefulWidget {
  @override
  _AdminAddGroup createState() => _AdminAddGroup();
}

class _AdminAddGroup extends AdminGroupsScreenStateBase<AdminAddGroup> {
  List<User> _students = List.empty(growable: true);
  List<User> _coaches = List.empty(growable: true);
  List<User> _allUsers = [];
  List<User> _usersToAdd = List.empty(growable: true);
  List<User> _coachesToAdd = List.empty(growable: true);
  List<DropdownMenuItem<User>> _dropDownMenu;
  List<DropdownMenuItem<User>> _dropDownMenuCoach;
  User _currentStudent;
  User _currentCoach;
  TextEditingController groupText;

  @override
  void initState() {
    super.initState();
    groupText = TextEditingController();
    _allUsers = usersNotifier.value;
    _usersToAdd = List.from(_allUsers);
    _coachesToAdd = List.from(_allUsers);
    updateDropdown();
    updateDropdownCoach();
  }

  @override
  void notifyNotifierListeners() {
    _allUsers = usersNotifier.value;
    _usersToAdd = List.from(_allUsers);
    _coachesToAdd = List.from(_allUsers);
    updateDropdown(changeSelection: false);
    updateDropdownCoach(changeSelection: false);
  }

  @override
  List<GroupsEvent> get additionalStartupEvents => [GroupsEvent.loadedUsers()];

  void changedDropDown(User selectedStudent) {
    setState(() {
      _currentStudent = selectedStudent;
    });
  }

  void changedDropDownCoach(User selectedCoach) {
    setState(() {
      _currentCoach = selectedCoach;
    });
  }

  void updateDropdown({bool changeSelection = true}) {
    _dropDownMenu = getDropDownMenuItems(_usersToAdd);
    if (!changeSelection) return;
    _currentStudent = _usersToAdd.firstOrNull ?? null;
  }

  void updateDropdownCoach({bool changeSelection = true}) {
    _dropDownMenuCoach = getDropDownMenuItems(_coachesToAdd);
    if (!changeSelection) return;
    _currentCoach = _coachesToAdd.firstOrNull ?? null;
  }

  void initScreen(BuildContext context) {}

  @override
  Widget buildScreen(BuildContext context) {
    initScreen(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(S.current.addGroup),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.save,
              color: Colors.black,
            ),
            onPressed: () {
              String name = groupText.text;
              logger.d("Adding group with name: $name");
              List<String> userIds = _students.map((s) => s.userId).toList();
              List<String> coachIds = _coaches.map((s) => s.userId).toList();
              bloc.add(GroupsEvent.addedGroupAdmin(
                  name: name, coachIds: coachIds, userIds: userIds));
              Navigator.pop(context);
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: Text(S.current.addGroupSuccess(name))),
              );
            },
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    S.current.groupName,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Colors.grey[700]),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: TextFormField(
                      controller: groupText,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15.0),
                            borderSide: BorderSide.none,
                          ),
                          fillColor: Color(0xfff3f3f4),
                          filled: true),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    S.current.addCoach,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Colors.grey[700]),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  DropdownButton(
                    isExpanded: true,
                    items: _dropDownMenuCoach,
                    onChanged: changedDropDownCoach,
                    value: _currentCoach ?? null,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      RaisedButton(
                        onPressed: () {
                          if(_currentCoach == null) return;
                          setState(() {
                            _coaches.insert(0, _currentCoach);
                            _coachesToAdd.remove(_currentCoach);
                            updateDropdownCoach();
                          });
                        },
                        child: Text(
                          S.current.add,
                          style: TextStyle(fontSize: 18),
                        ),
                        elevation: 5.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(
                            color: Colors.green[300],
                          ),
                        ),
                        color: Colors.green[300],
                        textColor: Colors.black54,
                        padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    S.current.coachesAdded,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Colors.grey[700]),
                  ),
                  ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                    itemCount: _coaches?.length,
                    itemBuilder: (BuildContext context, int position) {
                      User coach = _coaches[position];
                      return Container(
                        height: 20,
                        margin: EdgeInsets.all(5),
                        child: Text(
                          "${coach.name} ${coach.surname}",
                          style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.grey[700]),
                        ),
                      );
                    },
                  ),
                  if (_coaches?.length == 0) ...[
                    SizedBox(
                      height: 20,
                    ),
                  ],
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    S.current.addPerson,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Colors.grey[700]),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  DropdownButton(
                    isExpanded: true,
                    items: _dropDownMenu,
                    onChanged: changedDropDown,
                    value: _currentStudent ?? null,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      RaisedButton(
                        onPressed: () {
                          if(_currentStudent == null) return;
                          setState(() {
                            _students.insert(0, _currentStudent);
                            _usersToAdd.remove(_currentStudent);
                            updateDropdown();
                          });
                        },
                        child: Text(
                          S.current.add,
                          style: TextStyle(fontSize: 18),
                        ),
                        elevation: 5.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(
                            color: Colors.green[300],
                          ),
                        ),
                        color: Colors.green[300],
                        textColor: Colors.black54,
                        padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    S.current.studentsAdded,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Colors.grey[700]),
                  ),
                ],
              ),
            ),
            ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              itemCount: _students?.length,
              itemBuilder: (BuildContext context, int position) {
                User student = _students[position];
                return Container(
                  height: 20,
                  margin: EdgeInsets.all(5),
                  child: Text(
                    "${student.name} ${student.surname}",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey[700]),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
