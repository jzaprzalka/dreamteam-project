import 'package:dreamteam_project/blocks/schedule/schedule_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Schedule.dart';
import 'package:dreamteam_project/screens/calendar_screen/calendar_base.dart';
import 'package:dreamteam_project/screens/calendar_screen/widgets/calendar_listview_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ClassesHomeWidget extends StatefulWidget {
  final TabController tabController;
  final int showSchedules;

  const ClassesHomeWidget(
      {Key key, @required this.tabController, this.showSchedules = 3})
      : super(key: key);

  @override
  State<StatefulWidget> createState() =>
      _ClassesState(tabController, showSchedules);
}

class _ClassesState extends CalendarBase<ClassesHomeWidget> {
  final TabController tabController;
  final int showSchedules;

  _ClassesState(this.tabController, this.showSchedules) : super();

  @override
  List<ScheduleEvent> get additionalStartupEvents =>
      [ScheduleEvent.loadedSchedules(toDate: DateTime.now())];

  @override
  Widget buildScreen(BuildContext context) {
    List<Schedule> schedules = schedulesNotifier.value;
    return Container(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: [
          Text(
            S.current.UpcominigClasses,
            style: Theme.of(context)
                .textTheme
                .headline5
                .copyWith(color: Colors.black),
          ),
          Spacer(),
          GestureDetector(
            onTap: () {
              tabController.animateTo(1);
            },
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.all(Radius.circular(20)),
              ),
              child: Text(S.current.more,
                  style: TextStyle(fontSize: 18, color: Colors.grey[50])),
            ),
          ),
        ]),
        ListView.builder(
            padding: EdgeInsets.symmetric(vertical: 10),
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, position) {
              return CalendarListviewCardWidget(schedule: schedules[position]);
            },
            itemCount: schedules.length),
      ],
    ));
  }
}
