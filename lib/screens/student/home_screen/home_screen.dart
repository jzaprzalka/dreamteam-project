import 'package:dreamteam_project/models/Schedule.dart';
import 'package:dreamteam_project/models/value_notifier/user_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import 'widgets/classes_home_widget.dart';
import 'widgets/groups_home_widget.dart';

class StudentHomeScreen extends StatefulWidget {
  final TabController tabController;

  const StudentHomeScreen({Key key, @required this.tabController})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<StudentHomeScreen>
    with AutomaticKeepAliveClientMixin {
  List<Schedule> schedules = [];

  @override
  void initState() {
    schedules.add(new Schedule(
      scheduleId: "1",
      dateStart: new DateTime.now(),
      groupId: "1",
    ));
    schedules.add(new Schedule(
      scheduleId: "2",
      dateStart: new DateTime.utc(2021, DateTime.january, 12),
      groupId: "1",
    ));
    schedules.add(new Schedule(
      scheduleId: "3",
      dateStart: new DateTime.utc(2021, DateTime.january, 13),
      groupId: "2",
    ));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      color: Colors.white,
      child: CustomScrollView(
        physics: BouncingScrollPhysics(),
        slivers: [
          SliverAppBar(
            pinned: true,
            floating: false,
            leading: Container(),
            backgroundColor: Colors.white,
            shadowColor: Colors.black,
            elevation: 30,
            expandedHeight: 300.0,
            flexibleSpace: FlexibleSpaceBar(
              title: Text(
                  Provider.of<UserNotifier>(context, listen: true).value.name +
                      " " +
                      Provider.of<UserNotifier>(context, listen: false)
                          .value
                          .surname),
              centerTitle: true,
              background: Padding(
                padding: EdgeInsets.only(top: 10, bottom: 30),
                child: Container(
                  child: Image.asset(
                    'assets/icon/logo.png',
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              color: Colors.transparent,
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 6),
              child: Column(
                children: [
                  ClassesHomeWidget(
                    showSchedules: 3,
                    tabController: widget.tabController,
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  GroupsHomeWidget(
                    tabController: widget.tabController,
                    showGroups: 3,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
