import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/screens/student/groups_screen/widgets/group_listview_card.dart';
import 'package:dreamteam_project/screens/student/groups_screen/groups_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class GroupsHomeWidget extends StatefulWidget {
  final TabController tabController;
  final int showGroups;

  const GroupsHomeWidget(
      {Key key, @required this.tabController, this.showGroups = 3})
      : super(key: key);

  @override
  State<StatefulWidget> createState() =>
      _GroupsState(tabController, showGroups);
}

class _GroupsState extends GroupsScreenStateBase<GroupsHomeWidget> {
  final TabController tabController;
  final int showGroups;

  _GroupsState(this.tabController, this.showGroups) : super();

  @override
  Widget buildScreen(BuildContext context) {
    List<Group> groups = groupsNotifier.value;
    if (groups.length > showGroups)
      groups = groups.getRange(0, showGroups).toList();
    return Container(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: [
          Text(
            S.current.MyGroups,
            style: Theme.of(context)
                .textTheme
                .headline5
                .copyWith(color: Colors.black),
          ),
          Spacer(),
          GestureDetector(
            onTap: () {
              tabController.animateTo(2);
            },
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.all(Radius.circular(20)),
              ),
              child: Text(S.current.more,
                  style: TextStyle(fontSize: 18, color: Colors.grey[50])),
            ),
          ),
        ]),
        ListView.builder(
            padding: EdgeInsets.symmetric(vertical: 10),
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, position) {
              return GroupListViewCardWidget(group: groups[position]);
            },
            itemCount: groups.length),
      ],
    ));
  }
}
