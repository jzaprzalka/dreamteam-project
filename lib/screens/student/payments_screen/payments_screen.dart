import 'package:dreamteam_project/blocks/student_payments/payments_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

abstract class PaymentsView<T extends StatefulWidget> extends State<T> {
  List<Payment> _payments = [];
  ValueNotifier<List<Payment>> paymentsNotifier;
  PaymentsBloc bloc;

  List<Widget> paymentWidgets(List<Payment> payments, BuildContext context);

  List<Widget> appBarActions(BuildContext context) => [];

  @override
  void initState() {
    super.initState();
    paymentsNotifier = ValueNotifier(_payments);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => paymentsNotifier,
      child: BlocProvider<PaymentsBloc>(
        create: (context) {
          bloc = PaymentsBloc();
          logger.d("Payments loaded. Dispatching event");
          bloc.add(PaymentsEvent.loadedPayments());
          return bloc;
        },
        child: BlocConsumer<PaymentsBloc, PaymentsState>(
          listener: (context, state) {
            logger.d("Listening to changes in payments bloc");
            if (state is PaymentsUpdated)
              Provider.of<ValueNotifier<List<Payment>>>(context, listen: false)
                  .value = state.payments;
          },
          builder: (context, state) => buildPaymentsScreen(context),
        ),
      ),
    );
  }

  Widget buildPaymentsScreen(BuildContext context) {
    List<Payment> payments = paymentsNotifier.value;
    return Scaffold(
      appBar: AppBar(
        title: Text(S.current.Payments),
        actions: appBarActions(context),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: paymentWidgets(payments, context),
        ),
      ),
    );
  }
}
