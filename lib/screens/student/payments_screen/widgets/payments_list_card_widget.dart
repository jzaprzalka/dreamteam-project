import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:intl/intl.dart';

abstract class PaymentsListCardWidget extends StatelessWidget {
  String get title;
  Icon get listIcon;
  final List<Payment> listPayments;

  PaymentsListCardWidget({Key key, @required this.listPayments})
      : super(key: key);

  Widget _paymentInfo(Payment p, Icon icon) {
    final df = DateFormat.yMMMMEEEEd(Intl.getCurrentLocale());
    return Card(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: ListTile(
            title: Text(
              "${p.amount} zł",
              style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey[700]),
            ),
            subtitle: Text(p.paymentName),
            trailing: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                icon,
                Text(
                  "${S.current.timeDue}: ${df.format(p.dateDue)}",
                ),
              ],
            )),
      ),
    );
  }

  Widget _currentPaymentInfo(Payment payment) =>
      _paymentInfo(payment, listIcon);

  Widget titleBlock(String title) => Text(
        title,
        style: TextStyle(
          fontSize: 22.0,
          fontWeight: FontWeight.bold,
          color: Colors.grey[700],
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 6.0),
          child: titleBlock(title),
        ),
        listPayments.length != 0
            ? ListView.builder(
                primary: false,
                shrinkWrap: true,
                physics: ScrollPhysics(),
                padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                itemCount: listPayments?.length,
                itemBuilder: (context, index) =>
                    _currentPaymentInfo(listPayments[index]),
              )
            : Center(
                child: Text(S.current.nothingToShow),
              ),
      ],
    );
  }
}
