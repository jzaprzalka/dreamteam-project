import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'payments_list_card_widget.dart';

class PaymentsPaidCardWidget extends PaymentsListCardWidget {
  final int allPaymentsCount;

  int get paymentsPaid => listPayments?.length;

  PaymentsPaidCardWidget(
      {Key key,
      @required List<Payment> paymentsList,
      @required this.allPaymentsCount})
      : super(key: key, listPayments: paymentsList);

  @override
  Icon get listIcon => Icon(
        Icons.check_circle,
        size: 30.0,
        color: Colors.green,
      );

  @override
  String get title => S.current.paymentsPaid;

  @override
  Widget titleBlock(String title) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          super.titleBlock(title),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Icon(
                paymentsPaid / allPaymentsCount < 1
                    ? Icons.check_circle_outline
                    : Icons.check_circle,
                size: 30.0,
                color: Colors.green,
              ),
              Text(
                "$paymentsPaid/$allPaymentsCount",
                style: (super.titleBlock(title) as Text)
                    .style
                    .copyWith(fontWeight: FontWeight.normal),
              ),
            ],
          ),
        ],
      );
}
