import 'package:dreamteam_project/models/Payment.dart';
import 'package:dreamteam_project/screens/student/payments_screen/widgets/payments_paid_card_widget.dart';
import 'package:flutter/material.dart';

import 'payments_screen.dart';

class History extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HistoryState();
}

class _HistoryState extends PaymentsView<History> {
  Widget paymentsPaid(BuildContext context, List<Payment> payments) {
    List<Payment> paymentsPaid =
        payments.where((payment) => payment.paid == true).toList();
    return PaymentsPaidCardWidget(
      paymentsList: paymentsPaid,
      allPaymentsCount: payments.length,
    );
  }

  @override
  List<Widget> paymentWidgets(List<Payment> payments, BuildContext context) =>
      [paymentsPaid(context, payments)];
}
