import 'package:dreamteam_project/models/Payment.dart';
import 'package:dreamteam_project/screens/student/payments_screen/payments_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/cupertino.dart';

import 'payments_history.dart';
import 'widgets/payments_approaching_due_card_widget.dart';
import 'widgets/payments_past_due_card_widget.dart';

class StudentPaymentsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _StudentPaymentsScreenState();
}

class _StudentPaymentsScreenState extends PaymentsView<StudentPaymentsScreen> {
  @override
  List<Widget> appBarActions(BuildContext context) => [
        IconButton(
          icon: Icon(
            Icons.history,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => History()),
            );
          },
        ),
      ];

  Widget paymentsPastDue(BuildContext context, List<Payment> payments) {
    List<Payment> paymentsPastDue = payments
        .where((payment) =>
            payment.paid == false && payment.dateDue.isBefore(DateTime.now()))
        .toList();
    return PaymentsPastDueCardWidget(
      paymentsList: paymentsPastDue,
    );
  }

  Widget paymentsApproachingDue(BuildContext context, List<Payment> payments) {
    List<Payment> paymentsApproachingDue = payments
        .where((payment) =>
            payment.paid == false &&
            payment.dateDue.isBefore(DateTime.now().add(Duration(days: 10))) &&
            payment.dateDue.isAfter(DateTime.now()))
        .toList();
    return PaymentsApproacingDueCardWidget(
      paymentsList: paymentsApproachingDue,
    );
  }

  @override
  List<Widget> paymentWidgets(List<Payment> payments, BuildContext context) =>
      <Widget>[
        paymentsPastDue(context, payments),
        paymentsApproachingDue(context, payments),
      ];
}
