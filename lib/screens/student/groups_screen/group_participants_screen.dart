import 'package:dreamteam_project/blocks/student_groups/groups_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/screens/student/groups_screen/groups_screen.dart';
import 'package:flutter/material.dart';

class ParticipantsList extends StatefulWidget {
  final String groupId;
  @override
  _ParticipantsList createState() =>
      new _ParticipantsList(groupId: this.groupId);
  ParticipantsList({Key key, @required this.groupId}) : super(key: key);
}

class _ParticipantsList extends GroupsScreenStateBase<ParticipantsList> {
  final String groupId;
  _ParticipantsList({@required this.groupId});

  @override
  List<GroupsEvent> get additionalStartupEvents =>
      [GroupsEvent.loadedGroup(groupId: groupId)];

  @override
  Widget buildScreen(BuildContext context) {
    Group group = currentGroupNotifier.value;
    List<User> _users = group?.participants ?? [];
    List<User> _coaches = group?.coaches ?? [];
    String groupName = group?.name ?? S.current.loadingGroup;
    return Scaffold(
      appBar: AppBar(
        title: Text(groupName),
      ),
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 6.0),
                child: Text(
                  S.current.coaches,
                  style: TextStyle(
                      fontSize: 22.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.grey[700]),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: ListView.builder(
                  itemCount: _coaches?.length,
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                  itemBuilder: (context, position) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(30.0, 6.0, 6.0, 6.0),
                          child: Text(
                            "${_coaches[position].name} ${_coaches[position].surname}",
                            style: TextStyle(
                              fontSize: 15.0,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Icon(
                                    Icons.phone_callback_outlined,
                                    color: Colors.grey[700],
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Icon(
                                    Icons.email_outlined,
                                    color: Colors.grey[700],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    );
                  },
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 6.0),
                child: Text(
                  S.current.participants,
                  style: TextStyle(
                      fontSize: 22.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.grey[700]),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: ListView.builder(
                  itemCount: _users?.length,
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                  itemBuilder: (context, position) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(30.0, 6.0, 6.0, 6.0),
                          child: Text(
                            "${_users[position].name} ${_users[position].surname}",
                            style: TextStyle(
                              fontSize: 15.0,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Icon(
                                    Icons.email_outlined,
                                    color: Colors.grey[700],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    );
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
