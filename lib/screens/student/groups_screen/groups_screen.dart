import 'package:dreamteam_project/blocks/student_groups/groups_bloc.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/Attendance.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

abstract class GroupsScreenStateBase<T extends StatefulWidget>
    extends State<T> {
  ValueNotifier<List<Group>> groupsNotifier;
  ValueNotifier<List<Attendance>> attendanceNotifier;
  ValueNotifier<Group> currentGroupNotifier;
  GroupsBloc bloc;

  @override
  void initState() {
    super.initState();
    groupsNotifier = ValueNotifier(List.empty());
    attendanceNotifier = ValueNotifier(List.empty());
    currentGroupNotifier = ValueNotifier(null);
  }

  List<GroupsEvent> get additionalStartupEvents => [];

  List<GroupsEvent> get _startupEvents =>
      [GroupsEvent.loadedGroups(), ...additionalStartupEvents];

  void notifyNotifierListeners() => null;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => groupsNotifier,
      child: BlocProvider<GroupsBloc>(
        create: (context) {
          bloc = GroupsBloc();
          logger
              .d("Dispatching Group startup events (${_startupEvents.length})");
          for (GroupsEvent ge in _startupEvents) bloc.add(ge);
          return bloc;
        },
        child: BlocConsumer<GroupsBloc, GroupsState>(
          listener: (context, state) {
            if (state is GroupsUpdated) {
              Provider.of<ValueNotifier<List<Group>>>(context, listen: false)
                  .value = state.groups;
            }
            if (state is AttendanceUpdated)
              attendanceNotifier.value = state.attendance;
            if (state is GroupUpdated) currentGroupNotifier.value = state.group;
            notifyNotifierListeners();
          },
          builder: (context, state) => buildScreen(context),
        ),
      ),
    );
  }

  Widget buildScreen(BuildContext context);
}
