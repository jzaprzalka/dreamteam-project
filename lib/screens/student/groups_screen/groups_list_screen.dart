import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:flutter/material.dart';

import 'groups_screen.dart';
import 'widgets/group_listview_card.dart';

class StudentGroupsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _GroupsScreenState();
}

class _GroupsScreenState extends GroupsScreenStateBase {
  @override
  Widget buildScreen(BuildContext context) {
    List<Group> groups = groupsNotifier.value;
    return Scaffold(
      appBar: AppBar(
        title: Text(S.current.Groups),
      ),
      backgroundColor: Colors.white,
      body: ListView.builder(
          itemBuilder: (context, position) =>
              GroupListViewCardWidget(group: groups[position]),
          itemCount: groups?.length ?? 0),
    );
  }
}
