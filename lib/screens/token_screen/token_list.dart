import 'package:dreamteam_project/blocks/admin_token/admin_token_bloc.dart';
import 'package:dreamteam_project/models/Token.dart';
import 'package:dreamteam_project/screens/token_screen/token_add.dart';
import 'package:dreamteam_project/screens/token_screen/token_edit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TokenList extends StatefulWidget {
  @override
  _TokenListState createState() => _TokenListState();
}

class _TokenListState extends State<TokenList> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AdminTokenBloc()..add(AdminTokenEvent.loadTokens()),
      child: Scaffold(
        appBar: AppBar(
          title: Text(S.of(context).tokenList),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.add,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => TokenAdd()),
                );
              },
            )
          ],
        ),
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              BlocBuilder<AdminTokenBloc, AdminTokenState>(
                builder: (context, state) {
                  BlocProvider.of<AdminTokenBloc>(context)
                      .add(AdminTokenEvent.loadTokens());
                  return ListView.builder(
                    itemCount: state.tokens?.length,
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                    itemBuilder: (context, position) {
                      return Card(
                        elevation: 10,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(12.0),
                          child: ListTile(
                            title: Text(
                              state.tokens[position].name,
                              style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.grey[700],
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => TokenEdit(
                                    token: state.tokens[position],
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      );
                    },
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
