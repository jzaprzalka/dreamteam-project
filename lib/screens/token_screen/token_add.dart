import 'package:dreamteam_project/blocks/admin_token/admin_token_bloc.dart';
import 'package:dreamteam_project/repositories/token_repository.dart';
import 'package:flutter/material.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TokenAdd extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _TokenAddState();
}

class _TokenAddState extends State<TokenAdd> {
  TextEditingController textController;
  List<String> access = [];
  @override
  void initState() {
    textController = TextEditingController();
    access.add("Group 1");
    access.add("Group 2");
    access.add("file #99829092");

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).addToken),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.save,
              color: Colors.black,
            ),
            onPressed: () {
              TokenRepository().addToken(textController.text);
              // BlocProvider.of<AdminTokenBloc>(context)
              //     .add(AdminTokenEvent.addToken(name: textController.text));
              Navigator.pop(context);
            },
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(16.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              new Text(
                S.of(context).tokenName,
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.grey[700],
                ),
              ),
              new SizedBox(
                height: 5,
              ),
              new TextField(
                controller: textController,
                decoration: InputDecoration(
                  labelText: S.of(context).enterTokenName,
                  border: new OutlineInputBorder(
                    borderSide: new BorderSide(color: Colors.teal),
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                keyboardType: TextInputType.name,
                maxLines: 1,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
