import 'package:dreamteam_project/blocks/admin_token/admin_token_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Token.dart';
import 'package:dreamteam_project/repositories/token_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TokenEdit extends StatefulWidget {
  final Token token;

  const TokenEdit({Key key, this.token}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _TokenEditState();
}

class _TokenEditState extends State<TokenEdit> {
  TextEditingController textEditingController;
  List<String> access = [];
  @override
  void initState() {
    textEditingController = TextEditingController(text: widget.token.name);
    access.add("Group 1");
    access.add("Group 2");
    access.add("file #99829092");

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).editToken),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.save,
              color: Colors.black,
            ),
            onPressed: () {
              widget.token.name = textEditingController.text;
              TokenRepository().editToken(widget.token);
              Navigator.pop(context);
            },
          ),
          IconButton(
            icon: Icon(
              Icons.delete_forever_outlined,
              color: Colors.black,
            ),
            onPressed: () {
              TokenRepository().deleteToken(widget.token);
              Navigator.pop(context);
            },
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(16.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              new Text(
                S.of(context).tokenName,
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.grey[700],
                ),
              ),
              new SizedBox(
                height: 5,
              ),
              new TextField(
                controller: textEditingController,
                decoration: InputDecoration(
                  labelText: S.of(context).enterTokenName,
                  border: new OutlineInputBorder(
                    borderSide: new BorderSide(color: Colors.teal),
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                keyboardType: TextInputType.name,
                maxLines: 1,
              ),
              new SizedBox(
                height: 15,
              ),
              new Text(
                S.of(context).accessTo,
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.grey[700],
                ),
              ),
              new SizedBox(
                height: 5,
              ),
              new Card(
                elevation: 10,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                child: new ListView.builder(
                    itemCount: access?.length,
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    itemBuilder: (context, position) {
                      return Padding(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          access[position],
                          style: TextStyle(
                            fontSize: 18.0,
                            color: Colors.grey[700],
                          ),
                        ),
                      );
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }
}
