import 'package:dreamteam_project/blocks/schedule/schedule_bloc.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/models/Schedule.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

abstract class CalendarBase<T extends StatefulWidget> extends State<T> {
  ValueNotifier<List<Schedule>> schedulesNotifier;
  ValueNotifier<List<Group>> groupsNotifier;
  ValueNotifier<Schedule> scheduleNotifier;
  ScheduleBloc bloc;

  @override
  void initState() {
    super.initState();
    schedulesNotifier = ValueNotifier(List.empty());
    groupsNotifier = ValueNotifier(List.empty());
    scheduleNotifier = ValueNotifier(null);
  }

  List<ScheduleEvent> get additionalStartupEvents;

  List<ScheduleEvent> get _startupEvents => [...additionalStartupEvents];

  void notifyNotifierListeners() => null;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => schedulesNotifier,
      child: BlocProvider<ScheduleBloc>(
        create: (context) {
          bloc = ScheduleBloc();
          logger.d(
              "Dispatching Announcement startup events (${_startupEvents.length})");
          for (ScheduleEvent ge in _startupEvents) bloc.add(ge);
          return bloc;
        },
        child: BlocConsumer<ScheduleBloc, ScheduleState>(
          listener: (context, state) {
            state.map(
              scheduleInitial: (s) => null,
              scheduleUpdated: (s) => scheduleNotifier.value = s.schedule,
              schedulesUpdated: (s) =>
                  Provider.of<ValueNotifier<List<Schedule>>>(context,
                          listen: false)
                      .value = s.schedules,
              groupsUpdated: (s) => groupsNotifier.value = s.groups,
            );
            notifyNotifierListeners();
          },
          builder: (context, state) => buildScreen(context),
        ),
      ),
    );
  }

  Widget buildScreen(BuildContext context);
}
