import 'package:dreamteam_project/blocks/schedule/schedule_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Schedule.dart';
import 'package:dreamteam_project/screens/calendar_screen/calendar_base.dart';
import 'package:dreamteam_project/screens/calendar_screen/widgets/calendar_listview_card.dart';
import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';

import 'schedule_screen/schedule_list.dart';

class CalendarScreen extends StatefulWidget {
  final bool isManager;

  const CalendarScreen({Key key, this.isManager = false}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CalendarScreenState();
}

class _CalendarScreenState extends CalendarBase<CalendarScreen> {
  CalendarController _calendarController;
  // List<Schedule> schedules = [];

  @override
  void initState() {
    super.initState();
    _calendarController = CalendarController();
  }

  @override
  List<ScheduleEvent> get additionalStartupEvents => [
        ScheduleEvent.loadedSchedules(),
      ];

  @override
  Widget buildScreen(BuildContext context) {
    List<Schedule> schedules = schedulesNotifier.value;
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).Calendar),
        actions: widget.isManager
            ? <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.add,
                    color: Colors.black,
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AllScheduleList()),
                    );
                  },
                )
              ]
            : [],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Card(
              elevation: 10,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              clipBehavior: Clip.antiAlias,
              margin: const EdgeInsets.all(8.0),
              child: TableCalendar(
                calendarController: _calendarController,
                startingDayOfWeek: StartingDayOfWeek.monday,
                availableCalendarFormats: {
                  CalendarFormat.month: S.current.Month,
                  CalendarFormat.twoWeeks: S.current.TwoWeeks,
                  CalendarFormat.week: S.current.Week
                },
                onDaySelected: (selectedDate, _l1, _l2) =>
                    bloc.add(ScheduleEvent.loadedSchedules(
                  fromDate: selectedDate,
                  toDate: selectedDate,
                )),
              ),
            ),
            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: schedules.length,
              itemBuilder: (BuildContext context, int index) {
                return CalendarListviewCardWidget(
                  schedule: schedules[index],
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
