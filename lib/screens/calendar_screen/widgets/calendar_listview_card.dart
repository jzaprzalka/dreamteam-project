import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/models/Schedule.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

class CalendarListviewCardWidget extends StatelessWidget {
  final Schedule schedule;
  static var firestoreInstance = FirebaseFirestore.instance;

  const CalendarListviewCardWidget({Key key, this.schedule}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      margin: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(12, 12, 12, 6),
                child: Text(
                  schedule.classSubject,
                  style: TextStyle(
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.grey[700],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Icon(
                          Icons.calendar_today_outlined,
                          color: Colors.grey[700],
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          DateFormat('dd.MM.yyyy').format(schedule.dateStart),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Icon(
                                Icons.watch_later_outlined,
                                color: Colors.grey[700],
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Text(
                                DateFormat('EEEE').format(schedule.dateStart),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
