import 'package:dreamteam_project/blocks/schedule/schedule_bloc.dart';
import 'package:dreamteam_project/models/Schedule.dart';
import 'package:dreamteam_project/screens/calendar_screen/calendar_base.dart';
import 'schedule_add.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AllScheduleList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AllScheduleListState();
}

class _AllScheduleListState extends CalendarBase<AllScheduleList> {
  List<Schedule> schedules = [];
  DateTime dateNow = DateTime.now();
  DateTime datePaid = DateTime.utc(2021, 4, 4);

  @override
  void initState() {
    // schedules.add(
    //     new Schedule(groupId: "1", scheduleId: "1", dateStart: DateTime.now()));
    // schedules.add(
    //     new Schedule(groupId: "2", scheduleId: "2", dateStart: DateTime.now()));
    // schedules.add(
    //     new Schedule(groupId: "3", scheduleId: "3", dateStart: DateTime.now()));
    super.initState();
  }

  @override
  List<ScheduleEvent> get additionalStartupEvents => [
        LoadedSchedules(
          fromDate: DateTime.utc(1000, 04, 20),
          toDate: DateTime.utc(3000, 09, 13),
        ),
      ];

  @override
  Widget buildScreen(BuildContext context) {
    schedules = schedulesNotifier.value ?? [];
    return Scaffold(
      appBar: AppBar(
        title: Text("Wszystkie zajęcia"),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.add,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AddSchedule()),
              );
            },
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ListView.builder(
              itemCount: schedules?.length,
              shrinkWrap: true,
              physics: ScrollPhysics(),
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              itemBuilder: (context, position) {
                return Card(
                  elevation: 10,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  margin: const EdgeInsets.all(8.0),
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => AddSchedule(),
                        ),
                      );
                    },
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(12, 12, 12, 6),
                              child: Text(
                                schedules[position].classSubject,
                                style: TextStyle(
                                  fontSize: 22.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[700],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Icon(
                                        Icons.calendar_today_outlined,
                                        color: Colors.grey[700],
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        DateFormat('dd.MM.yyyy').format(
                                            schedules[position].dateStart),
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Icon(
                                              Icons.watch_later_outlined,
                                              color: Colors.grey[700],
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              DateFormat('EEEE').format(
                                                  schedules[position]
                                                      .dateStart),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
