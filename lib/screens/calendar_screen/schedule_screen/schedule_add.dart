import 'package:dreamteam_project/blocks/schedule/schedule_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/models/Schedule.dart';
import 'package:dreamteam_project/screens/calendar_screen/calendar_base.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AddSchedule extends StatefulWidget {
  final Schedule schedule;

  const AddSchedule({Key key, this.schedule}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AddScheduleState();
}

class _AddScheduleState extends CalendarBase<AddSchedule> {
  List<Group> groups = [];
  List<String> filters = [];
  DateTime currentDate = DateTime.now();
  TextEditingController subjectController;
  String dropdownValue;

  @override
  void initState() {
    super.initState();
    subjectController =
        TextEditingController(text: widget.schedule?.classSubject ?? "");
    // groups.add(new Group(groupId: "1", name: "Group 1"));
    // groups.add(new Group(groupId: "2", name: "Group 2"));
    // groups.add(new Group(groupId: "3", name: "Group 3"));
    // groups.add(new Group(groupId: "4", name: "Group 4"));
    dropdownValue = S.current.dontRepeat;
  }

  @override
  List<ScheduleEvent> get additionalStartupEvents => [LoadGroups()];

  @override
  Widget buildScreen(BuildContext context) {
    groups = groupsNotifier.value ?? [];
    return Scaffold(
      appBar: AppBar(
        title: Text("Edytuj/Dodaj"),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.delete,
              color: Colors.black,
            ),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(
              Icons.save,
              color: Colors.black,
            ),
            onPressed: () {
              bloc.add(AddedSchedule(
                  schedule: Schedule(
                classSubject: subjectController.text,
                groupId: filters[0],
                dateStart: currentDate,
                dateEnd: currentDate.add(Duration(hours: 2)),
              )));
              Navigator.pop(context);
            },
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(16.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              new SizedBox(
                height: 5,
              ),
              new Text(
                S.of(context).Groups,
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.grey[700],
                ),
              ),
              new SizedBox(
                height: 5,
              ),
              Wrap(
                children: userGroupPosition.toList(),
              ),
              new SizedBox(
                height: 15,
              ),
              new TextField(
                controller: subjectController,
                style: TextStyle(fontSize: 20.0, color: Colors.grey[700]),
              ),
              new SizedBox(
                height: 15,
              ),
              new Text(
                S.of(context).firstClass,
                style: TextStyle(fontSize: 20.0, color: Colors.grey[700]),
              ),
              new SizedBox(
                height: 15,
              ),
              new Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey,
                  ),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: new Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: new Text(
                    DateFormat('dd-MM-yyyy hh:mm a').format(currentDate),
                    style: TextStyle(fontSize: 15.0, color: Colors.grey[700]),
                  ),
                ),
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  new TextButton(
                    onPressed: () => _selectDate(context),
                    child: Text(
                      S.of(context).changeSchedule,
                      style: TextStyle(
                        fontSize: 15.0,
                      ),
                    ),
                  ),
                ],
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  new TextButton(
                    onPressed: () => _selectTime(context),
                    child: Text(
                      S.of(context).changeTime,
                      style: TextStyle(
                        fontSize: 15.0,
                      ),
                    ),
                  ),
                ],
              ),
              // new Text(
              //   S.of(context).repeat,
              //   style: TextStyle(fontSize: 20.0, color: Colors.grey[700]),
              // ),
              // new Padding(
              //   padding: const EdgeInsets.all(5),
              //   child: new DropdownButton<String>(
              //     items: <String>[
              //       S.of(context).dontRepeat,
              //       S.of(context).repeatEveryWeek,
              //       S.of(context).repeatEveryMonth
              //     ].map((String value) {
              //       return new DropdownMenuItem<String>(
              //         value: value,
              //         child: new Text(value),
              //       );
              //     }).toList(),
              //     onChanged: (newValue) {
              //       setState(() {
              //         dropdownValue = newValue;
              //       });
              //     },
              //     value: dropdownValue,
              //     iconSize: 30.0,
              //     isExpanded: true,
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }

  Iterable<Widget> get userGroupPosition sync* {
    for (Group group in groups) {
      yield Padding(
        padding: const EdgeInsets.all(6.0),
        child: FilterChip(
          backgroundColor: Colors.tealAccent[200],
          avatar: CircleAvatar(
            backgroundColor: Colors.cyan,
            child: Text(
              group.name[0].toUpperCase(),
              style: TextStyle(color: Colors.white),
            ),
          ),
          label: Text(
            group.name,
          ),
          selected: filters.contains(group.name),
          selectedColor: Colors.purpleAccent,
          onSelected: (bool selected) {
            setState(() {
              if (selected) {
                filters.add(group.name);
              } else {
                filters.removeWhere((String name) {
                  return name == group.name;
                });
              }
            });
          },
        ),
      );
    }
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate,
        firstDate: DateTime(2015),
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != currentDate)
      setState(() {
        currentDate = new DateTime(pickedDate.year, pickedDate.month,
            pickedDate.day, currentDate.hour, currentDate.minute);
      });
  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime:
          TimeOfDay(hour: currentDate.hour, minute: currentDate.minute),
    );
    if (picked != null)
      setState(() {
        currentDate = new DateTime(currentDate.year, currentDate.month,
            currentDate.day, picked.hour, picked.minute);
      });
  }
}
