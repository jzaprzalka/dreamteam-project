import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/screens/admin/groups_screen/groups_list_screen.dart';
import 'package:dreamteam_project/screens/admin/home_screen/home_screen.dart';
import 'package:dreamteam_project/screens/admin/payments_screen/all_payments_screen.dart';
import 'package:dreamteam_project/screens/calendar_screen/calendar_screen.dart';
import 'package:dreamteam_project/screens/coach/groups_screen/groups_list_screen.dart';
import 'package:dreamteam_project/screens/coach/home_screen/home_screen.dart';
import 'package:dreamteam_project/screens/main_drawer/animated_main_drawer.dart';
import 'package:dreamteam_project/screens/main_screen/components/TabIcon.dart';
import 'package:dreamteam_project/screens/coach/payments_screen/payments_screen.dart';
import 'package:dreamteam_project/screens/profile_screen/profile_screen.dart';
import 'package:dreamteam_project/screens/student/groups_screen/groups_list_screen.dart';
import 'package:dreamteam_project/screens/student/home_screen/home_screen.dart';
import 'package:dreamteam_project/screens/student/payments_screen/payments_todo_screen.dart';
import 'package:dreamteam_project/screens/token_screen/token_list.dart';
import 'package:dreamteam_project/screens/userToken_screen/userToken_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MainScreenState();
}

enum Role { coach, student, admin }

class _MainScreenState extends State<MainScreen> with TickerProviderStateMixin {
  TabController _tabController;
  Role _selectedRole;
  String _currentRoleTitle;

  @override
  void initState() {
    _selectedRole = Role.coach;
    _currentRoleTitle = S.current.Coach_View;
    _tabController = _currentTabController;
    super.initState();
  }

  List<TabIcon> get _currentTabs {
    switch (_selectedRole) {
      case Role.student:
      case Role.coach:
        return _regularTabs;
      case Role.admin:
        return _adminTabs;
      default:
        return [];
    }
  }

  List<TabIcon> get _regularTabs => [
        TabIcon(icon: Icons.home, text: S.current.Overview),
        TabIcon(icon: Icons.calendar_today, text: S.current.Calendar),
        TabIcon(icon: Icons.group, text: S.current.Groups),
        TabIcon(icon: Icons.payment, text: S.current.Payments),
        TabIcon(icon: Icons.person, text: S.current.Profil),
      ];

  List<TabIcon> get _adminTabs => [
        TabIcon(icon: Icons.home, text: S.current.Overview),
        TabIcon(icon: Icons.calendar_today, text: S.current.Calendar),
        TabIcon(icon: Icons.group, text: S.current.Groups),
        TabIcon(icon: Icons.payment, text: S.current.Payments),
        TabIcon(icon: Icons.person, text: S.current.Profil),
        TabIcon(icon: Icons.accessibility_new, text: S.current.accessTokens),
        TabIcon(icon: Icons.more_horiz, text: S.current.More),
      ];

  TabController get _currentTabController =>
      TabController(length: _currentTabs.length, vsync: this);

  void updateRole(Role newRole) {
    this.setState(() {
      _selectedRole = newRole;
      _tabController = _currentTabController;
      switch (_selectedRole) {
        case Role.admin:
          _currentRoleTitle = S.current.Admin_View;
          break;
        case Role.student:
          _currentRoleTitle = S.current.Student_View;
          break;
        case Role.coach:
          _currentRoleTitle = S.current.Coach_View;
          break;
        default:
          _currentRoleTitle = "404";
      }
    });
  }

  Widget get _coachScreen => TabBarView(
        controller: _tabController,
        children: [
          HomeScreen(tabController: _tabController),
          CalendarScreen(isManager: true),
          GroupsScreen(),
          PaymentsScreen(),
          ProfileScreen()
        ],
      );

  Widget get _studentScreen => TabBarView(
        controller: _tabController,
        children: [
          StudentHomeScreen(tabController: _tabController),
          CalendarScreen(),
          StudentGroupsScreen(),
          StudentPaymentsScreen(),
          ProfileScreen()
        ],
      );

  Widget get _adminScreen => TabBarView(
        controller: _tabController,
        children: [
          AdminHomeScreen(tabController: _tabController),
          CalendarScreen(isManager: true),
          AdminGroupsScreen(),
          GroupsPaymentsList(),
          ProfileScreen(),
          TokenList(),
          UserTokenList(),
        ],
      );

  Widget getCurrentRoleScreen() {
    Widget body;
    switch (_selectedRole) {
      case Role.coach:
        body = _coachScreen;
        break;
      case Role.admin:
        body = _adminScreen;
        break;
      case Role.student:
        body = _studentScreen;
        break;
      default:
        body = Center(
          child: Text("404"),
        );
    }
    return body;
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedMainDrawer(
      updateRole: this.updateRole,
      homeWidget: Scaffold(
        appBar: AppBar(
          title: Padding(
            padding: const EdgeInsets.only(left: 30),
            child: Text(
              "${S.current.applicationName} | $_currentRoleTitle",
            ),
          ),
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.all(8.0),
              child: DropdownButton(
                onChanged: (Locale locale) {
                  setState(() {
                    S.load(locale);
                  });
                  logger.i("Locale changed to: " + locale.toString());
                },
                underline: SizedBox(),
                icon: Icon(
                  Icons.language,
                  color: Colors.white,
                ),
                items: S.delegate.supportedLocales
                    .toList()
                    .map<DropdownMenuItem<Locale>>((locale) => DropdownMenuItem(
                          value: locale,
                          child: Text(locale.toLanguageTag()),
                        ))
                    .toList(),
              ),
            )
          ],
        ),
        body: getCurrentRoleScreen(),
        bottomNavigationBar: Container(
          decoration:
              BoxDecoration(color: Theme.of(context).primaryColor, boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.3),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, -3),
            )
          ]),
          child: TabBar(
            controller: _tabController,
            indicator: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                color: Colors.grey.withOpacity(0.45)),
            indicatorColor: Colors.white,
            labelColor: Colors.black,
            unselectedLabelColor: Colors.black.withOpacity(0.4),
            labelPadding: EdgeInsets.symmetric(vertical: 5),
            tabs: _currentTabs,
          ),
        ),
      ),
    );
  }
}
