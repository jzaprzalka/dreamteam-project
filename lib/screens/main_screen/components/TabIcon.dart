import 'package:flutter/material.dart';

class TabIcon extends StatelessWidget {
  final IconData icon;
  final String text;

  const TabIcon({Key key, @required this.icon, @required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(icon),
          Text(text)
          // FittedBox(
          //   fit: BoxFit.fitWidth,
          //   child: Text(text, maxLines: 1))
        ],
      ),
    );
  }
}