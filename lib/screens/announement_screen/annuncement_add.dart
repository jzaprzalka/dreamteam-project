import 'package:dreamteam_project/blocks/announcement/announcement_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/screens/announement_screen/announcement_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AnnouncementsAdd extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AnnouncementsAddState();
}

class _AnnouncementsAddState extends AnnouncementScreenBase<AnnouncementsAdd> {
  List<Group> groups = [];
  List<String> selectedGroupIds = [];
  TextEditingController announcementText = TextEditingController(text: "");

  @override
  void notifyNotifierListeners() => groups = groupsNotifier.value;

  @override
  List<AnnouncementEvent> get additionalStartupEvents =>
      [AnnouncementEvent.loadedGroups()];

  bool get _isContentValid =>
      selectedGroupIds.length > 0 && announcementText.text.isNotEmpty;

  @override
  Widget buildScreen(BuildContext context) {
    groups = groupsNotifier.value;
    return Scaffold(
      appBar: AppBar(
        title: Text(S.current.post),
        actions: [
          IconButton(
            icon: Icon(
              Icons.save,
              // color: Colors.black,
            ),
            onPressed: _isContentValid
                ? () {
                    bloc.add(AnnouncementEvent.addedAnnouncement(
                      announcementDate: DateTime.now(),
                      announcement: announcementText.text,
                      groupIds: selectedGroupIds,
                    ));
                    logger.i("Announcement added successfully");
                    Navigator.pop(context);
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text(S.current.addAnnouncementSuccess)),
                    );
                  }
                : null,
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: Container(
        padding: const EdgeInsets.all(16.0),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            new Text(
              S.of(context).date +
                  ": " +
                  DateFormat('dd.MM.yyyy').format(DateTime.now()),
              textAlign: TextAlign.right,
              style: TextStyle(fontSize: 20.0, color: Colors.grey[700]),
            ),
            new SizedBox(
              height: 10,
            ),
            new Text(
              S.of(context).Groups,
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 25.0, color: Colors.black),
            ),
            new SizedBox(
              height: 8,
            ),
            Wrap(
              children: groupPosition.toList(),
            ),
            new SizedBox(
              height: 8,
            ),
            new TextField(
              controller: announcementText,
              decoration:
                  InputDecoration(labelText: S.of(context).enterAnnouncement),
              keyboardType: TextInputType.multiline,
              maxLines: null,
            ),
            new SizedBox(
              height: 8,
            ),
          ],
        ),
      ),
    );
  }

  Iterable<Widget> get groupPosition sync* {
    for (Group group in groups) {
      yield Padding(
        padding: const EdgeInsets.all(6.0),
        child: FilterChip(
          backgroundColor: Colors.tealAccent[200],
          avatar: CircleAvatar(
            backgroundColor: Colors.cyan,
            child: Text(
              group.name[0].toUpperCase(),
              style: TextStyle(color: Colors.white),
            ),
          ),
          label: Text(
            group.name,
          ),
          selected: selectedGroupIds.contains(group.groupId),
          selectedColor: Colors.purpleAccent,
          onSelected: (bool selected) {
            setState(() {
              if (selected) {
                selectedGroupIds.add(group.groupId);
              } else {
                selectedGroupIds
                    .removeWhere((groupId) => groupId == group.groupId);
              }
            });
          },
        ),
      );
    }
  }
}
