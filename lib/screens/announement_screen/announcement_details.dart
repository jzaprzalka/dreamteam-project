import 'package:dreamteam_project/blocks/announcement/announcement_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Announcement.dart';
import 'package:dreamteam_project/screens/announement_screen/announcement_screen.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AnnouncementsDetails extends StatefulWidget {
  final String announcementId;

  const AnnouncementsDetails({Key key, this.announcementId}) : super(key: key);
  @override
  State<StatefulWidget> createState() =>
      _AnnouncementsDetailsState(announcementId);
}

class _AnnouncementsDetailsState
    extends AnnouncementScreenBase<AnnouncementsDetails> {
  final String announcementId;
  Announcement announcement;

  _AnnouncementsDetailsState(this.announcementId);

  @override
  List<AnnouncementEvent> get additionalStartupEvents =>
      [AnnouncementEvent.loadedAnnouncement(announcementId: announcementId)];

  @override
  void notifyNotifierListeners() {
    announcement = currentAnnouncementsNotifier.value;
  }

  @override
  Widget buildScreen(BuildContext context) {
    announcement = currentAnnouncementsNotifier.value;
    return Scaffold(
      appBar: AppBar(
        title: Text(S.current.post),
      ),
      backgroundColor: Colors.white,
      body: Container(
        padding: const EdgeInsets.all(16.0),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            new Text(
              announcement != null
                  ? DateFormat('dd.MM.yyyy')
                      .format(announcement.announcementDate)
                  : S.current.loadingShort,
              textAlign: TextAlign.right,
              style: TextStyle(fontSize: 20.0, color: Colors.grey[700]),
            ),
            new SizedBox(
              height: 10,
            ),
            new Text(
              announcement?.announcement ?? S.current.loading,
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 25.0, color: Colors.black),
            ),
          ],
        ),
      ),
    );
  }
}
