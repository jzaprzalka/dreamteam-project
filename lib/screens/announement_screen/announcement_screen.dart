import 'package:dreamteam_project/blocks/announcement/announcement_bloc.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/Announcement.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

abstract class AnnouncementScreenBase<T extends StatefulWidget>
    extends State<T> {
  ValueNotifier<List<Announcement>> announcementsNotifier;
  ValueNotifier<List<Group>> groupsNotifier;
  ValueNotifier<Announcement> currentAnnouncementsNotifier;
  AnnouncementBloc bloc;

  @override
  void initState() {
    super.initState();
    announcementsNotifier = ValueNotifier(List.empty());
    groupsNotifier = ValueNotifier(List.empty());
    currentAnnouncementsNotifier = ValueNotifier(null);
  }

  List<AnnouncementEvent> get additionalStartupEvents;

  List<AnnouncementEvent> get _startupEvents => [...additionalStartupEvents];

  void notifyNotifierListeners() => null;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => announcementsNotifier,
      child: BlocProvider<AnnouncementBloc>(
        create: (context) {
          bloc = AnnouncementBloc();
          logger.d(
              "Dispatching Announcement startup events (${_startupEvents.length})");
          for (AnnouncementEvent ge in _startupEvents) bloc.add(ge);
          return bloc;
        },
        child: BlocConsumer<AnnouncementBloc, AnnouncementState>(
          listener: (context, state) {
            state.map(
              announcementInitial: (s) => null,
              announcementsUpdated: (s) =>
                  Provider.of<ValueNotifier<List<Announcement>>>(context,
                          listen: false)
                      .value = s.announcements,
              announcementUpdated: (s) =>
                  currentAnnouncementsNotifier.value = s.announcement,
              loadedGroups: (s) => groupsNotifier.value = s.groups,
            );
            notifyNotifierListeners();
          },
          builder: (context, state) => buildScreen(context),
        ),
      ),
    );
  }

  Widget buildScreen(BuildContext context);
}
