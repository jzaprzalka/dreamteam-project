import 'package:dreamteam_project/blocks/announcement/announcement_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Announcement.dart';
import 'package:dreamteam_project/screens/announement_screen/announcement_details.dart';
import 'package:dreamteam_project/screens/announement_screen/announcement_screen.dart';
import 'package:dreamteam_project/screens/announement_screen/annuncement_add.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AnnouncementsList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AnnouncementsListState();
}

class _AnnouncementsListState
    extends AnnouncementScreenBase<AnnouncementsList> {
  static const int MAX_LENGTH = 35;
  List<Announcement> announcements;
  @override
  List<AnnouncementEvent> get additionalStartupEvents =>
      [AnnouncementEvent.loadedAnnouncementList()];

  @override
  void notifyNotifierListeners() {
    setState(() {
      announcements = announcementsNotifier.value;
    });
  }

  @override
  Widget buildScreen(BuildContext context) {
    announcements = announcementsNotifier.value;
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).Posts),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.add,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AnnouncementsAdd()),
              );
            },
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ListView.builder(
              itemCount: announcements?.length,
              shrinkWrap: true,
              physics: ScrollPhysics(),
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              itemBuilder: (context, position) {
                Announcement announcement = announcements[position];
                return Card(
                  elevation: 10,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(12.0),
                    child: ListTile(
                      title: new Row(
                        children: <Widget>[
                          Text(
                            DateFormat('dd.MM.yyyy')
                                .format(announcement.announcementDate),
                            style: TextStyle(
                                fontSize: 14.0, color: Colors.grey[700]),
                          ),
                        ],
                        mainAxisAlignment: MainAxisAlignment.end,
                      ),
                      subtitle: Text(
                        announcement.announcement.length > MAX_LENGTH
                            ? "${announcement.announcement.substring(0, MAX_LENGTH)}..."
                            : announcement.announcement,
                        style: TextStyle(fontSize: 20.0, color: Colors.black),
                      ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => AnnouncementsDetails(
                              announcementId: announcement.announcementId,
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
