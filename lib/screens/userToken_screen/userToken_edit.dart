import 'package:dreamteam_project/blocks/admin_user_token/admin_user_token_bloc.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/screens/userToken_screen/userToken_edit_body.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UserTokenEdit extends StatelessWidget {
  final User user;

  const UserTokenEdit({Key key, @required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) =>
            AdminUserTokenBloc()..add(AdminUserTokenEvent.loadUserTokens(user)),
        child: UserTokenEditBody());
  }
}
