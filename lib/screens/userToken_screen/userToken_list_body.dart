import 'package:dreamteam_project/blocks/admin_user_token_list/admin_user_token_list_bloc.dart';
import 'package:dreamteam_project/screens/userToken_screen/userToken_edit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UserTokenListBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(S.of(context).userTokenList),
        ),
        backgroundColor: Colors.white,
        body: BlocBuilder<AdminUserTokenListBloc, AdminUserTokenListState>(
          buildWhen: (old,state) => old.users != state.users,
          builder: (context, state) {
            return ListView.builder(
              itemCount: state.users?.length,
              shrinkWrap: true,
              physics: ScrollPhysics(),
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              itemBuilder: (context, position) {
                return Card(
                  elevation: 10,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(12.0),
                    child: ListTile(
                      title: Text(
                        (state.users[position].name ?? "null") + " " + (state.users[position].surname ?? "null"),
                        style: TextStyle(
                          fontSize: 18.0,
                          color: Colors.grey[700],
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => UserTokenEdit(user: state.users[position]),
                          ),
                        );
                      },
                    ),
                  ),
                );
              },
            );
          },
        ));
  }
}

