import 'package:dreamteam_project/blocks/admin_user_token/admin_user_token_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Token.dart';
import 'package:dreamteam_project/models/UserToken.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class UserTokenEditBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit User Token"),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.save,
              color: Colors.black,
            ),
            onPressed: () {
              BlocProvider.of<AdminUserTokenBloc>(context)
                  .add(AdminUserTokenEvent.save());
            },
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(16.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              new Text(
                S.of(context).tokenList,
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.grey[700],
                ),
              ),
              new SizedBox(
                height: 5,
              ),
              BlocBuilder<AdminUserTokenBloc, AdminUserTokenState>(
                buildWhen: (old,state) => old.userTokens != state.userTokens,
                builder: (context, state) {
                  return Wrap(
                    children: userTokenPosition(
                            state.tokens, state.userTokens, context),
                  );
                },
              ),
              new SizedBox(
                height: 15,
              ),
              new Text(
                S.of(context).expireDate,
                style: TextStyle(fontSize: 20.0, color: Colors.grey[700]),
              ),
              new SizedBox(
                height: 15,
              ),
              new Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey,
                  ),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: new Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: BlocBuilder<AdminUserTokenBloc, AdminUserTokenState>(
                    builder: (context, state) {
                      return Text(
                        DateFormat('dd-MM-yyyy').format(state.currentDate),
                        style:
                            TextStyle(fontSize: 15.0, color: Colors.grey[700]),
                      );
                    },
                  ),
                ),
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  new TextButton(
                    onPressed: () => _selectDate(context),
                    child: Text(
                      S.of(context).changeExpireDate,
                      style: TextStyle(
                        fontSize: 15.0,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> userTokenPosition(
      List<Token> tokens, List<UserToken> userTokens, BuildContext context) {
    List<Widget> widgets = [];
    for (Token token in tokens) {
      widgets.add(Padding(
        padding: const EdgeInsets.all(6.0),
        child: FilterChip(
          backgroundColor: Colors.tealAccent[200],
          avatar: CircleAvatar(
            backgroundColor: Colors.cyan,
            child: Text(
              token.name[0].toUpperCase(),
              style: TextStyle(color: Colors.white),
            ),
          ),
          label: Text(
            token.name,
          ),
          selected:
              userTokens.firstWhere((us) => us.tokenId == token.id, orElse: () => null) == null
                  ? false
                  : true,
          selectedColor: Colors.purpleAccent,
          onSelected: (bool selected) {
            if (selected) {
              BlocProvider.of<AdminUserTokenBloc>(context)
                  .add(AdminUserTokenEvent.addUserToken(token));
            } else {
              BlocProvider.of<AdminUserTokenBloc>(context)
                  .add(AdminUserTokenEvent.removeUserToken(token));
            }
          },
        ),
      ));
    }
    return widgets;
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime pickedDate = await showDatePicker(
        context: context,
        initialDate:
            BlocProvider.of<AdminUserTokenBloc>(context).state.currentDate,
        firstDate: DateTime(2015),
        lastDate: DateTime(2050));
    if (pickedDate != null &&
        pickedDate !=
            BlocProvider.of<AdminUserTokenBloc>(context).state.currentDate) {
      BlocProvider.of<AdminUserTokenBloc>(context)
          .add(AdminUserTokenEvent.setDate(pickedDate));
    }
  }
}
