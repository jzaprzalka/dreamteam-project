import 'package:dreamteam_project/blocks/admin_user_token_list/admin_user_token_list_bloc.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/screens/token_screen/token_list.dart';
import 'package:dreamteam_project/screens/userToken_screen/userToken_edit.dart';
import 'package:dreamteam_project/screens/userToken_screen/userToken_list_body.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UserTokenList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) =>
            AdminUserTokenListBloc()..add(AdminUserTokenListEvent.loadUsers()),
        child: UserTokenListBody());
  }
}
