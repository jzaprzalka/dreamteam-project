import 'package:dreamteam_project/blocks/auth/authentication_bloc.dart';
import 'package:dreamteam_project/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginScreenSate();
}

class _LoginScreenSate extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  String _email;
  String _password;
  String _errorMessage;

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit() async {
    if (validateAndSave()) {
      logger.d("Validated form and saved. Now submitting...");
      BlocProvider.of<AuthenticationBloc>(context)
          .add(AuthenticationEvent.logIn(email: _email, password: _password));
    }
  }

  Widget showErrorMessage() {
    logger.d("Showing error message for the inputs");
    if (_errorMessage != null && _errorMessage.length > 0) {
      return Padding(
        padding: EdgeInsets.only(top: 20),
        child: Text(
          _errorMessage,
          style: TextStyle(
              fontSize: 13.0,
              color: Colors.red,
              height: 1.0,
              fontWeight: FontWeight.w300),
        ),
      );
    } else {
      return Container(
        height: 0.0,
      );
    }
  }

  @override
  void initState() {
    logger.d("Initializing Login state");
    _errorMessage = "";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent,
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: BlocListener<AuthenticationBloc, AuthenticationState>(
              listenWhen: (oldState, newState) {
                if (newState is UnAuthenticated) {
                  if (newState.exception != null)
                    return true;
                  else
                    return false;
                } else
                  return false;
              },
              listener: (context, state) {
                if (state is UnAuthenticated) {
                  // logger.e("Exception on login: ${state.exception.message}");
                  final snackBar =
                      SnackBar(content: Text(state.exception.message));
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                }
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                  colors: [
                    Theme.of(context).scaffoldBackgroundColor,
                    Colors.white
                  ],
                  begin: FractionalOffset(1, 0),
                  end: FractionalOffset(0, 1),
                  stops: [0.3, 1],
                  tileMode: TileMode.clamp,
                )),
                child: SafeArea(
                  child: Form(
                    key: _formKey,
                    child: Stack(children: [
                      SingleChildScrollView(
                        child: Center(
                          child: Column(children: [
                            Container(
                              width: (MediaQuery.of(context).size.width > 950
                                      ? 0.4
                                      : 0.8) *
                                  MediaQuery.of(context).size.width,
                              child: Column(
                                children: [
                                  Padding(
                                    padding:
                                        EdgeInsets.only(top: 20, bottom: 20),
                                    child: Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              3 /
                                              11,
                                      child: Image.asset(
                                        'assets/icon/logo.png',
                                      ),
                                    ),
                                  ),
                                  TextFormField(
                                    maxLines: 1,
                                    keyboardType: TextInputType.emailAddress,
                                    autofocus: false,
                                    decoration: InputDecoration(
                                      labelText: S.current.email,
                                      border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(25.0),
                                        borderSide: BorderSide(),
                                      ),
                                    ),
                                    style: TextStyle(fontSize: 24),
                                    validator: (value) => value.isEmpty
                                        ? S.current
                                            .invalidField(S.current.email)
                                        : null,
                                    onSaved: (value) => _email = value.trim(),
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsets.only(top: 10, bottom: 20),
                                    child: TextFormField(
                                      maxLines: 1,
                                      obscureText: true,
                                      autofocus: false,
                                      decoration: InputDecoration(
                                        labelText: S.current.password,
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(25.0),
                                          borderSide: BorderSide(),
                                        ),
                                      ),
                                      style: TextStyle(fontSize: 24),
                                      validator: (value) => value.isEmpty
                                          ? S.current
                                              .invalidField(S.current.password)
                                          : null,
                                      onSaved: (value) =>
                                          _password = value.trim(),
                                      onFieldSubmitted: (s) {
                                        validateAndSubmit();
                                      },
                                    ),
                                  ),
                                  FlatButton(
                                      minWidth: double.infinity,
                                      height: 50,
                                      color: Theme.of(context).primaryColor,
                                      splashColor:
                                          Colors.white24.withOpacity(0.6),
                                      onPressed: validateAndSubmit,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              new BorderRadius.circular(30.0)),
                                      child: Center(
                                          child: Text(
                                        S.current.loginBtn,
                                        style: TextStyle(fontSize: 24),
                                      ))),
                                  SizedBox(
                                    height: 40,
                                  ),
                                  Container(
                                      padding:
                                          EdgeInsets.only(bottom: 10, top: 10),
                                      width: double.infinity,
                                      child: Center(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              S.current.noAccount,
                                              style: TextStyle(fontSize: 16),
                                            ),
                                            Padding(
                                              padding:
                                                  EdgeInsets.only(left: 10),
                                              child: GestureDetector(
                                                onTap: () {},
                                                child: Text(
                                                  S.current.register,
                                                  style: TextStyle(
                                                      color: Colors.blue,
                                                      fontSize: 16),
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      )),
                                  showErrorMessage(),
                                ],
                              ),
                            ),
                          ]),
                        ),
                      ),
                      Container(
                        alignment: Alignment.topRight,
                        padding: EdgeInsets.all(10.0),
                        child: DropdownButton(
                          onChanged: (Locale locale) {
                            setState(() {
                              S.load(locale);
                            });
                            logger.i("Locale changed to: " + locale.toString());
                          },
                          icon: Icon(
                            Icons.language,
                            color: Colors.white,
                          ),
                          underline: Container(),
                          items: S.delegate.supportedLocales
                              .toList()
                              .map<DropdownMenuItem<Locale>>(
                                  (locale) => DropdownMenuItem(
                                        value: locale,
                                        child: Text(locale.toLanguageTag()),
                                      ))
                              .toList(),
                        ),
                      ),
                    ]),
                  ),
                ),
              ),
            )));
  }
}
// listenWhen: (oldState, newState) {
//           if (newState is UnAuthenticated) {
//             if (newState.exception != null)
//               return true;
//             else
//               return false;
//           } else
//             return false;
//         },
//         listener: (context, state) {
//           if (state is UnAuthenticated) {
//             final snackBar = SnackBar(content: Text(state.exception.message));
//             Scaffold.of(context).showSnackBar(snackBar);
//           }

// Container(
//                 width: MediaQuery.of(context).size.width,
//                 height: MediaQuery.of(context).size.height,
//                 decoration: BoxDecoration(
//                     gradient: LinearGradient(
//                   colors: [
//                     Theme.of(context).scaffoldBackgroundColor,
//                     Colors.white
//                   ],
//                   begin: FractionalOffset(1, 0),
//                   end: FractionalOffset(0, 1),
//                   stops: [0.3, 1],
//                   tileMode: TileMode.clamp,
//                 )),
//                 child: SafeArea(
//                   child: Form(
//                     key: _formKey,
//                     child: Stack(children: [
//                       SingleChildScrollView(
//                         child: Center(
//                           child: Column(children: [
//                             Container(
//                               width: (MediaQuery.of(context).size.width > 950
//                                       ? 0.4
//                                       : 0.8) *
//                                   MediaQuery.of(context).size.width,
//                               child: Column(
//                                 children: [
//                                   Padding(
//                                     padding:
//                                         EdgeInsets.only(top: 20, bottom: 20),
//                                     child: Container(
//                                       height:
//                                           MediaQuery.of(context).size.height *
//                                               3 /
//                                               11,
//                                       child: Image.asset(
//                                         'assets/icon/logo.png',
//                                       ),
//                                     ),
//                                   ),
//                                   TextFormField(
//                                     maxLines: 1,
//                                     keyboardType: TextInputType.emailAddress,
//                                     autofocus: false,
//                                     decoration: InputDecoration(
//                                       labelText: S.current.email,
//                                       border: OutlineInputBorder(
//                                         borderRadius:
//                                             BorderRadius.circular(25.0),
//                                         borderSide: BorderSide(),
//                                       ),
//                                     ),
//                                     style: TextStyle(fontSize: 24),
//                                     validator: (value) => value.isEmpty
//                                         ? S.current
//                                             .invalidField(S.current.email)
//                                         : null,
//                                     onSaved: (value) => _email = value.trim(),
//                                   ),
//                                   Padding(
//                                     padding:
//                                         EdgeInsets.only(top: 10, bottom: 20),
//                                     child: TextFormField(
//                                       maxLines: 1,
//                                       obscureText: true,
//                                       autofocus: false,
//                                       decoration: InputDecoration(
//                                         labelText: S.current.password,
//                                         border: OutlineInputBorder(
//                                           borderRadius:
//                                               BorderRadius.circular(25.0),
//                                           borderSide: BorderSide(),
//                                         ),
//                                       ),
//                                       style: TextStyle(fontSize: 24),
//                                       validator: (value) => value.isEmpty
//                                           ? S.current
//                                               .invalidField(S.current.password)
//                                           : null,
//                                       onSaved: (value) =>
//                                           _password = value.trim(),
//                                       onFieldSubmitted: (s) {
//                                         validateAndSubmit();
//                                       },
//                                     ),
//                                   ),
//                                   FlatButton(
//                                       minWidth: double.infinity,
//                                       height: 50,
//                                       color: Theme.of(context).primaryColor,
//                                       splashColor:
//                                           Colors.white24.withOpacity(0.6),
//                                       onPressed: validateAndSubmit,
//                                       shape: RoundedRectangleBorder(
//                                           borderRadius:
//                                               new BorderRadius.circular(30.0)),
//                                       child: Center(
//                                           child: Text(
//                                         S.current.loginBtn,
//                                         style: TextStyle(fontSize: 24),
//                                       ))),
//                                   Padding(
//                                     padding: EdgeInsets.only(top: 20),
//                                     child: Align(
//                                       alignment: Alignment.topRight,
//                                       child: GestureDetector(
//                                           onTap: () {},
//                                           child: Text(S.current.forgotPassword,
//                                               style: TextStyle(fontSize: 16))),
//                                     ),
//                                   ),
//                                   SizedBox(
//                                     height: 40,
//                                   ),
//                                   Container(
//                                       padding:
//                                           EdgeInsets.only(bottom: 10, top: 10),
//                                       width: double.infinity,
//                                       child: Center(
//                                         child: Row(
//                                           mainAxisAlignment:
//                                               MainAxisAlignment.center,
//                                           children: [
//                                             Text(
//                                               S.current.noAccount,
//                                               style: TextStyle(fontSize: 16),
//                                             ),
//                                             Padding(
//                                               padding:
//                                                   EdgeInsets.only(left: 10),
//                                               child: GestureDetector(
//                                                 onTap: () {},
//                                                 child: Text(
//                                                   S.current.register,
//                                                   style: TextStyle(
//                                                       color: Colors.blue,
//                                                       fontSize: 16),
//                                                 ),
//                                               ),
//                                             )
//                                           ],
//                                         ),
//                                       )),
//                                   showErrorMessage(),
//                                 ],
//                               ),
//                             ),
//                           ]),
//                         ),
//                       ),
//                       Container(
//                         alignment: Alignment.topRight,
//                         padding: EdgeInsets.all(10.0),
//                         child: DropdownButton(
//                           onChanged: (Locale locale) {
//                             setState(() {
//                               S.load(locale);
//                             });
//                             logger.i("Locale changed to: " + locale.toString());
//                           },
//                           icon: Icon(
//                             Icons.language,
//                             color: Colors.white,
//                           ),
//                           underline: Container(),
//                           items: S.delegate.supportedLocales
//                               .toList()
//                               .map<DropdownMenuItem<Locale>>(
//                                   (locale) => DropdownMenuItem(
//                                         value: locale,
//                                         child: Text(locale.toLanguageTag()),
//                                       ))
//                               .toList(),
//                         ),
//                       ),
//                     ]),
//                   ),
//                 ),
//               ),
