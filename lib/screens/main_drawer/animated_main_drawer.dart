import 'package:animated_drawer/views/animated_drawer.dart';
import 'package:dreamteam_project/blocks/auth/authentication_bloc.dart';
import 'package:dreamteam_project/blocks/messages/messages_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/screens/announement_screen/announcement_list.dart';
import 'package:dreamteam_project/screens/main_screen/main_screen.dart';
import 'package:dreamteam_project/screens/message_screen/chatroom_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AnimatedMainDrawer extends StatelessWidget {
  final Widget homeWidget;
  final Function(Role) updateRole;

  const AnimatedMainDrawer(
      {Key key, @required this.homeWidget, @required this.updateRole})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedDrawer(
      homePageXValue: 200,
      homePageYValue: 80,
      homePageAngle: -0.2,
      homePageSpeed: 250,
      shadowXValue: 172,
      shadowYValue: 110,
      shadowAngle: -0.275,
      shadowSpeed: 550,
      openIcon: Icon(Icons.menu),
      closeIcon: Icon(Icons.arrow_back_ios),
      shadowColor: Theme.of(context).scaffoldBackgroundColor,
      backgroundGradient: LinearGradient(
        colors: [
          Theme.of(context).primaryColor,
          Theme.of(context).scaffoldBackgroundColor,
        ],
      ),
      menuPageContent: Padding(
        padding: const EdgeInsets.only(top: 100.0, left: 15),
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width / 3,
                  child: Container(
                    child: Image.asset(
                      'assets/icon/logo.png',
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  S.current.applicationName,
                  style: TextStyle(
                      fontSize: 24,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 40,
                ),
                AnimatedDrawerButton(
                  icon: Icons.messenger,
                  isActive: true,
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return ChatroomList();
                    }));
                  },
                  text: S.current.Messages,
                ),
                SizedBox(
                  height: 20,
                ),
                AnimatedDrawerButton(
                  icon: Icons.dashboard,
                  isActive: true,
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AnnouncementsList()));
                  },
                  text: S.current.Posts,
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 4,
                  child: Divider(
                    color: Color(0xFF5950a0),
                    thickness: 2,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                // Role views
                AnimatedDrawerButton(
                  icon: Icons.radio_button_unchecked,
                  // TODO: Add token dependency for isActive
                  isActive: true,
                  onTap: () {
                    updateRole(Role.coach);
                  },
                  text: S.current.Coach_View,
                ),
                SizedBox(
                  height: 10,
                ),
                AnimatedDrawerButton(
                  icon: Icons.radio_button_unchecked,
                  // TODO: Add token dependency for isActive
                  isActive: true,
                  onTap: () {
                    updateRole(Role.student);
                  },
                  text: S.current.Student_View,
                ),
                SizedBox(
                  height: 10,
                ),
                AnimatedDrawerButton(
                  icon: Icons.radio_button_unchecked,
                  // TODO: Add token dependency for isActive
                  isActive: true,
                  onTap: () {
                    updateRole(Role.admin);
                  },
                  text: S.current.Admin_View,
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 4,
                  child: Divider(
                    color: Color(0xFF5950a0),
                    thickness: 2,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                // AnimatedDrawerButton(
                //   icon: Icons.settings,
                //   isActive: true,
                //   onTap: () {},
                //   text: S.current.Settings,
                // ),
                // SizedBox(
                //   height: 20,
                // ),
                AnimatedDrawerButton(
                  icon: Icons.logout,
                  isActive: true,
                  onTap: () {
                    BlocProvider.of<AuthenticationBloc>(context)
                        .add(AuthenticationEvent.signedOut());
                  },
                  text: S.current.logout,
                ),
              ],
            ),
          ),
        ),
      ),
      homePageContent: homeWidget,
    );
  }
}

class AnimatedDrawerButton extends StatelessWidget {
  final bool isActive;
  final VoidCallback onTap;
  final String text;
  final IconData icon;

  const AnimatedDrawerButton(
      {Key key,
      @required this.isActive,
      @required this.onTap,
      @required this.text,
      this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: isActive ? onTap : null,
      child: Row(
        children: [
          if (icon != null)
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Icon(
                icon,
                color: isActive ? Colors.white : Colors.grey,
              ),
            ),
          Text(
            text,
            style: TextStyle(
              fontSize: 20,
              color: isActive ? Colors.white : Colors.grey[300],
            ),
          ),
        ],
      ),
    );
  }
}
