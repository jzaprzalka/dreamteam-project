import 'package:dreamteam_project/blocks/auth/authentication_bloc.dart';
import 'package:dreamteam_project/blocks/groups/groups_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/screens/coach/groups_screen/groups_list_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            child: DrawerHeader(
              margin: EdgeInsets.only(top: 10),
              child: ListTile(
                title: Icon(
                  Icons.airline_seat_legroom_extra,
                  size: 70,
                ),
                onTap: null,
              ),
            ),
            color: Theme.of(context).primaryColor,
          ),
          ListTile(
            leading: Icon(Icons.person),
            title: Text(
              'Groups',
              style: TextStyle(
                fontSize: 18,
              ),
            ),
            onTap: () {
              Navigator.pop(context);
              // Navigator.pushNamed(context, '/groups');
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return BlocProvider(
                  create: (context) => GroupsBloc(),
                  child: GroupsScreen(),
                );
              }));
            },
          ),
          ListTile(
            leading: Icon(Icons.accessible),
            title: Text(
              'Calendar',
              style: TextStyle(
                fontSize: 18,
              ),
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, '/calendar');
            },
          ),
          ListTile(
            leading: Icon(Icons.adb_sharp),
            title: Text(
              'Payments',
              style: TextStyle(
                fontSize: 18,
              ),
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, '/payments');
            },
          ),
          ListTile(
            leading: Icon(Icons.logout),
            title: Text(
              S.current.logout,
              style: TextStyle(
                fontSize: 18,
              ),
            ),
            onTap: () {
              BlocProvider.of<AuthenticationBloc>(context)
                  .add(AuthenticationEvent.signedOut());
            },
          ),
        ],
      ),
    );
  }
}
