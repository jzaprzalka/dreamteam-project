import 'package:dreamteam_project/blocks/auth/authentication_bloc.dart';
import 'package:dreamteam_project/blocks/user/user_bloc.dart';
import 'package:dreamteam_project/models/value_notifier/user_notifier.dart';
import 'package:dreamteam_project/screens/login_screen/login_screen.dart';
import 'package:dreamteam_project/screens/main_screen/main_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class RootScreen extends StatelessWidget {
  Widget buildWaitingScreen() {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: CircularProgressIndicator(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthenticationBloc, AuthenticationState>(
      listenWhen: (prev, now) {
        bool t =
            now.map(authenticated: (_) => true, unAuthenticated: (_) => false);
        return t;
      },
      listener: (context, state) {
        if (state is Authenticated) {
          Provider.of<UserNotifier>(context, listen: false).value = state.user;
        }
      },
      builder: (context, state) {
        return state.map(authenticated: (s) {
          return BlocProvider<UserBloc>(
            create: (context) => UserBloc(),
            child: MainScreen(),
          );
        }, unAuthenticated: (s) {
          if (s.isLoading) {
            return buildWaitingScreen();
          } else if (!s.isRegister) {
            return LoginScreen();
          } else {
            return buildWaitingScreen();
          }
        });
      },
    );
  }
}
