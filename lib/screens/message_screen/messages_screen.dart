// import 'package:dreamteam_project/blocks/messages/messages_bloc.dart';
// import 'package:dreamteam_project/generated/l10n.dart';
// import 'package:dreamteam_project/models/Chatroom.dart';
// import 'package:dreamteam_project/models/Messages.dart';
// import 'package:dreamteam_project/models/User.dart';
// import 'package:dreamteam_project/models/value_notifier/user_notifier.dart';
// import 'package:dreamteam_project/screens/message_screen/messages_body_screen.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:provider/provider.dart';

// class MessagesScreen extends StatelessWidget {
//   final Chatroom chatroom;
//   ValueNotifier<List<Chatroom>> chatroomsValueNotifier;
//   ValueNotifier<List<Message>> messagesNotifier;

//   MessagesBloc bloc;

//   MessagesScreen({Key key, this.chatroom}) : super(key: key);

//   List<MessagesEvent> get _startupEvents =>
//       [MessagesEvent.loadedMessages(chatroomId: chatroom.chatroomId)];

//   @override
//   Widget build(BuildContext context) {
//     //add BlockProvider
//     return ChangeNotifierProvider(
//       create: (_) => chatroomsValueNotifier,
//       child: BlocProvider<MessagesBloc>(
//         create: (context) {
//           bloc = MessagesBloc();
//           logger.d(
//               "Dispatching Message startup events (${_startupEvents.length})");
//           for (MessagesEvent me in _startupEvents) bloc.add(me);
//           return bloc;
//         },
//     );
//   }

//   Widget buildScreen(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(S.current.Messages),
//       ),
//       body: MessagesBodyScreen(
//         userId: Provider.of<UserNotifier>(context, listen: false).value.userId,
//         chatroom: contact.userId,
//         bloc: bloc,
//       ),
//     );
//   }
// }
