import 'package:dreamteam_project/blocks/chatroom_add_bloc/chatroom_add_bloc.dart';
import 'package:dreamteam_project/blocks/chatroom_list/chatroom_list_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Chatroom.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class ChatroomAdd extends StatefulWidget {
  final Chatroom chatroom;

  const ChatroomAdd({Key key, this.chatroom}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ChatroomAddState();
}

class _ChatroomAddState extends State<ChatroomAdd> {
  List<User> users = [];
  List<String> filters = [];
  TextEditingController nameController;
  DateTime currentDate = DateTime.now();
  String dropdownValue;
  ChatroomAddBloc bloc;

  @override
  void initState() {
    bloc = ChatroomAddBloc();
    nameController = TextEditingController(text: widget.chatroom?.name ?? "");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ChatroomAddBloc>(
      create: (context) => bloc..add(ChatroomAddEvent.loadUsers()),
      child: BlocBuilder<ChatroomAddBloc, ChatroomAddState>(
        builder: (context, state) {
          users = state.users ?? [];
          return buildScreen(context);
        },
      ),
    );
  }

  Widget buildScreen(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dodaj/Edytuj"),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.delete,
              color: Colors.black,
            ),
            onPressed: () {
              if (widget.chatroom != null)
                bloc.add(ChatroomAddEvent.deleteChatroom(
                  chatroomId: widget.chatroom.chatroomId,
                ));
              Navigator.pop(context);
            },
          ),
          IconButton(
            icon: Icon(
              Icons.save,
              color: Colors.black,
            ),
            onPressed: () {
              if (widget.chatroom == null) {
                bloc.add(ChatroomAddEvent.addChatroom(
                  chatroom: Chatroom(
                    name: nameController.text,
                    messageIds: [],
                    userIds: filters,
                  ),
                ));
              } else {
                bloc.add(ChatroomAddEvent.editChatroom(
                  chatroomId: widget.chatroom.chatroomId,
                  chatroom: Chatroom(
                    name: nameController.text,
                    userIds: filters,
                  ),
                ));
              }
              Navigator.pop(context);
            },
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(16.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              new Text(
                S.of(context).chatName,
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.grey[700],
                ),
              ),
              new SizedBox(
                height: 5,
              ),
              new TextField(
                controller: nameController,
                decoration: InputDecoration(
                  labelText: S.of(context).enterChatName,
                  border: new OutlineInputBorder(
                    borderSide: new BorderSide(color: Colors.teal),
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                keyboardType: TextInputType.name,
                maxLines: 1,
              ),
              new SizedBox(
                height: 15,
              ),
              new Text(
                S.of(context).addPeople,
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.grey[700],
                ),
              ),
              new SizedBox(
                height: 5,
              ),
              Wrap(
                children: usersPosition.toList(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Iterable<Widget> get usersPosition sync* {
    for (User user in users) {
      yield Padding(
        padding: const EdgeInsets.all(6.0),
        child: FilterChip(
          backgroundColor: Colors.tealAccent[200],
          avatar: CircleAvatar(
            backgroundColor: Colors.cyan,
            child: Text(
              (user?.name ?? "NN")[0].toUpperCase(),
              style: TextStyle(color: Colors.white),
            ),
          ),
          label: Text(
              (user?.name ?? "<noname>") + " " + (user?.surname ?? "<noname>")),
          selected: filters.contains(user.userId),
          selectedColor: Colors.purpleAccent,
          onSelected: (bool selected) {
            setState(() {
              if (selected) {
                filters.add(user.userId);
              } else {
                filters.removeWhere((String name) {
                  return name == user.userId;
                });
              }
            });
          },
        ),
      );
    }
  }
}
