import 'package:dreamteam_project/blocks/messages/messages_bloc.dart';
import 'package:dreamteam_project/screens/message_screen/widgets/message_tile.dart';
import 'package:dreamteam_project/screens/message_screen/widgets/messages_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ChatroomScreenBody extends StatelessWidget {
  const ChatroomScreenBody({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: BlocBuilder<MessagesBloc, MessagesState>(
            builder: (context, state) {
              return ListView.builder(
                padding: EdgeInsets.all(10.0),
                itemBuilder: (context, index) => MessageTile(
                  user: state.user,
                  message: state.messages[index],
                  sender: state.chatroom.users[state.messages[index].fromUID],
                  displayName: index == state.messages.length - 1 || state.messages[index].fromUID != state.messages[index +1].fromUID,
                ),
                itemCount: state.messages.length,
                reverse: true,
              );
            },
          ),
        ),
        ChatBottomBar(),
      ],
    );
  }
}
