import 'package:dreamteam_project/blocks/chatroom_list/chatroom_list_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/value_notifier/user_notifier.dart';
import 'package:dreamteam_project/screens/message_screen/chatroom_list_body.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

import 'chatroom_add.dart';

class ChatroomList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<ChatroomListBloc>(
      create: (context) => ChatroomListBloc()
        ..add(ChatroomListEvent.loadChatroomForUser(
            Provider.of<UserNotifier>(context, listen: false).value.userId)),
      child: Scaffold(
        appBar: AppBar(
          title: Text(S.of(context).chatroomList),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.add,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ChatroomAdd()),
                );
              },
            )
          ],
        ),
        backgroundColor: Colors.white,
        body: ChatroomListBody(),
      ),
    );
  }
}
