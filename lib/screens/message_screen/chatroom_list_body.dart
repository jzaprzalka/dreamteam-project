import 'dart:math' as math;
import 'package:dreamteam_project/blocks/chatroom_list/chatroom_list_bloc.dart';
import 'package:dreamteam_project/models/Chatroom.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/screens/message_screen/chatroom_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ChatroomListBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          BlocBuilder<ChatroomListBloc, ChatroomListState>(
            builder: (context, state) {
              return ListView.builder(
                itemCount: state.chatrooms?.length,
                shrinkWrap: true,
                physics: ScrollPhysics(),
                padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                itemBuilder: (context, position) =>
                    chatroomCard(state.chatrooms[position], context),
              );
            },
          ),
        ],
      ),
    );
  }

  Widget chatroomCard(Chatroom chatroom, BuildContext context) {
    return Card(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      margin: const EdgeInsets.all(12.0),
      child: InkWell(
        onTap: () async {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return ChatroomScreen(
                  chatroom: chatroom,
                );
              },
            ),
          );
        },
        child: Padding(
          padding: EdgeInsets.all(12.0),
          child: ListTile(
            title: Text(
              chatroom.name,
              style: TextStyle(
                fontSize: 20.0,
                color: Colors.grey[700],
                fontWeight: FontWeight.bold,
              ),
            ),
            dense: true,
            leading: CircleAvatar(
              backgroundColor:
                  Color((math.Random().nextDouble() * 0xFFFFFF).toInt())
                      .withOpacity(1.0),
            ),
            subtitle: Text(
              // chatroom.userIds.join(", "),
              _createSubtitle(chatroom.users),
              style: TextStyle(fontSize: 15),
            ),
          ),
        ),
      ),
    );
  }

  String _createSubtitle(Map<String, User> users) {
    String subtitle = "";
    if (users == null) return subtitle;
    users.forEach((s, u) {
      subtitle += "${u.name} ${u.surname}, ";
    });
    return subtitle.substring(0, subtitle.length - 2);
  }
}
