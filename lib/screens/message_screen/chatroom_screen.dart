import 'package:dreamteam_project/blocks/messages/messages_bloc.dart';
import 'package:dreamteam_project/models/Chatroom.dart';
import 'package:dreamteam_project/models/value_notifier/user_notifier.dart';
import 'package:dreamteam_project/screens/message_screen/chatroom_screen_body.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class ChatroomScreen extends StatelessWidget {
  final Chatroom chatroom;
  const ChatroomScreen({
    Key key,
    @required this.chatroom,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<MessagesBloc>(
      create: (context) => MessagesBloc()
        ..add(MessagesEvent.loadMessages(
            chatroom: chatroom,
            user: Provider.of<UserNotifier>(context, listen: false).value)),
      child: Scaffold(
        appBar: AppBar(
          title: BlocBuilder<MessagesBloc, MessagesState>(
            builder: (context, state) {
              return Text(state.chatroom.name ?? "");
            },
          ),
        ),
        backgroundColor: Colors.white,
        body: ChatroomScreenBody(),
      ),
    );
  }
}
