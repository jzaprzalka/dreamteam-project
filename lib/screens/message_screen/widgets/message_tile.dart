import 'package:dreamteam_project/models/Messages.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

class MessageTile extends StatefulWidget {
  final Message message;
  final User user;
  final User sender;
  final bool displayName;

  MessageTile({@required this.message, this.user, this.sender, this.displayName});

  @override
  State<StatefulWidget> createState() => _MessageTileState();
}

class _MessageTileState extends State<MessageTile> {
  bool sendByMe;
  bool showTime = false;

  @override
  void initState() {
    sendByMe = widget.message.fromUID == widget.user.userId;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          widget.displayName ? _buildName(sendByMe, widget.sender.name) : Container(),
          _buildMessageContainer(sendByMe, widget.message),
          showTime ? _buildTimestamp(sendByMe, widget.message) : Container(),
        ],
      ),
    );
  }

  Widget _buildMessageContainer(bool sendByMe, Message message) {
    return Container(
      padding: EdgeInsets.only(
          top: 5, bottom: 5, left: sendByMe ? 0 : 5, right: sendByMe ? 5 : 0),
      alignment: sendByMe ? Alignment.centerRight : Alignment.centerLeft,
      child: Container(
        margin:
            sendByMe ? EdgeInsets.only(left: 30) : EdgeInsets.only(right: 30),
        padding: message is TextMessage
            ? EdgeInsets.only(top: 17, bottom: 17, left: 20, right: 20)
            : null,
        decoration: BoxDecoration(
            borderRadius: sendByMe
                ? BorderRadius.only(
                    topLeft: Radius.circular(23),
                    topRight: Radius.circular(23),
                    bottomLeft: Radius.circular(23))
                : BorderRadius.only(
                    topLeft: Radius.circular(23),
                    topRight: Radius.circular(23),
                    bottomRight: Radius.circular(23)),
            gradient: LinearGradient(
              colors: sendByMe
                  ? [const Color(0xff007EF4), const Color(0xff2A75BC)]
                  : [Colors.red[400], Colors.pink[200]],
            )),
        child: _buildMessageContent(sendByMe, message, context),
      ),
    );
  }

  Widget _buildMessageContent(
      bool sendByMe, Message message, BuildContext context) {
    if (message is TextMessage) {
      return GestureDetector(
        onTap: () {
          setState(() {
            showTime = !showTime;
          });
        },
        child: Text(message.text,
            textAlign: TextAlign.start,
            style: TextStyle(
                color: sendByMe ? Colors.white : Colors.black,
                fontSize: 16,
                fontFamily: 'OverpassRegular',
                fontWeight: FontWeight.w300)),
      );
    } else if (message is ImageMessage) {
      return GestureDetector(
        onTap: () {},
        // child: Hero(
        //   tag: 'ImageMessage_${message.imageUrl}',
        child: message.imageUrl == null
            ? Container()
            : Container(
                constraints: BoxConstraints(
                    maxHeight: MediaQuery.of(context).size.height / 2),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: Image.network(message.imageUrl),
                ),
              ), //Image.asset(Assets.placeholder
        // ),
      );
    } else {
      return Container();
    }
  }

  Widget _buildTimestamp(bool sendByMe, Message message) {
    return Row(
        mainAxisAlignment:
            sendByMe ? MainAxisAlignment.end : MainAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Text(
              DateFormat('HH:mm, dd-MM-yyy').format(message.timestamp),
              style: Theme.of(context).textTheme.caption,
            ),
            margin: EdgeInsets.only(
                left: sendByMe ? 5.0 : 0.0,
                right: sendByMe ? 0.0 : 5.0,
                top: 5.0,
                bottom: 5.0),
          )
        ]);
  }

  Widget _buildName(bool sendByMe, String name) {
    return Row(
        mainAxisAlignment:
            sendByMe ? MainAxisAlignment.end : MainAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Text(
              name
            ),
            margin: EdgeInsets.only(
                left: sendByMe ? 5.0 : 0.0,
                right: sendByMe ? 0.0 : 5.0,
                top: 5.0,
                bottom: 5.0),
          )
        ]);
  }
}
