import 'package:dreamteam_project/blocks/messages/messages_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ChatBottomBar extends StatefulWidget {
  ChatBottomBar();

  @override
  State<StatefulWidget> createState() => _ChatBottomBarState();
}

class _ChatBottomBarState extends State<ChatBottomBar> {
  TextEditingController messageEditingController = TextEditingController();

  _ChatBottomBarState();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomCenter,
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
      color: Colors.grey[400], // Color(0x24FAFFFF),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              keyboardType: TextInputType.multiline,
              minLines: 1,
              maxLines: 3,
              controller: messageEditingController,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.fromLTRB(12, 8, 12, 8),
                isDense: true,
                hintText: S.current.Message3dot,
                hintStyle: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  borderSide: BorderSide(),
                ),
              ),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Container(
            height: 40,
            width: 40,
            child: ClipOval(
              child: Material(
                color: Color(0x36FFFFFF), // button color
                child: InkWell(
                  splashColor: Colors.blue, // inkwell color
                  child:
                      SizedBox(width: 40, height: 40, child: Icon(Icons.send)),
                  onTap: () {
                    if (messageEditingController.text.isEmpty) return;
                    BlocProvider.of<MessagesBloc>(context).add(
                        MessagesEvent.sendMessage(
                            messageText: messageEditingController.text));
                    messageEditingController.clear();
                  },
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
