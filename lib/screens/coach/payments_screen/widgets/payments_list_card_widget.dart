import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

abstract class PaymentsListCardWidget extends StatelessWidget {
  String get title;
  Icon get listIcon;
  final List<Payment> listPayments;

  PaymentsListCardWidget({Key key, @required this.listPayments})
      : super(key: key);

  Widget _paymentInfo(Payment p, Icon icon) {
    final df = DateFormat.yMMMMEEEEd(Intl.getCurrentLocale());
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(6.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                p.user == null
                    ? "Person ${p.userId}"
                    : "${p.user.name} ${p.user.surname}",
                style: TextStyle(fontSize: 17.0),
              ),
              Text(
                "${S.current.timeDue}: ${df.format(p.dateDue)}",
                style: TextStyle(
                  fontSize: 12.0,
                  color: Colors.blueGrey,
                ),
              ),
            ],
          ),
        ),
        Padding(
            padding: const EdgeInsets.all(6.0),
            child: GestureDetector(
              onTap: () {},
              child: icon,
            ))
      ],
    );
  }

  Widget _currentPaymentInfo(Payment payment) =>
      _paymentInfo(payment, listIcon);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      clipBehavior: Clip.antiAlias,
      margin: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 6.0),
                  child: Text(
                    title,
                    style: TextStyle(
                      fontSize: 22.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.grey[700],
                    ),
                  ),
                ),
              ]),
          Padding(
            padding: const EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 6.0),
            child: ListView.builder(
              primary: false,
              shrinkWrap: true,
              itemCount: listPayments?.length,
              itemBuilder: (context, index) =>
                  _currentPaymentInfo(listPayments[index]),
            ),
          )
        ],
      ),
    );
  }
}
