import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:dreamteam_project/screens/coach/payments_screen/widgets/payments_list_card_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class PaymentsApproacingDueCardWidget extends PaymentsListCardWidget {
  PaymentsApproacingDueCardWidget(
      {Key key, @required List<Payment> paymentsList})
      : super(key: key, listPayments: paymentsList);

  @override
  String get title => S.current.paymentsApproachingDue;

  @override
  Icon get listIcon => Icon(
        Icons.notifications_outlined,
        size: 30.0,
        color: Colors.orangeAccent,
      );
}
