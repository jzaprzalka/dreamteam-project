import 'package:dreamteam_project/blocks/payments/payments_bloc.dart';
import 'package:dreamteam_project/generated/l10n.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:dreamteam_project/screens/coach/payments_screen/widgets/payments_approaching_due_card_widget.dart';
import 'package:dreamteam_project/screens/coach/payments_screen/widgets/payments_paid_card_widget.dart';
import 'package:dreamteam_project/screens/coach/payments_screen/widgets/payments_past_due_card_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class PaymentsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PaymentsScreenState();
}

class _PaymentsScreenState extends State<PaymentsScreen> {
  List<Payment> _payments = [];
  ValueNotifier<List<Payment>> paymentsNotifier;
  PaymentsBloc bloc;

  @override
  void initState() {
    super.initState();
    paymentsNotifier = ValueNotifier(_payments);
  }

  Widget paymentsPaid(BuildContext context, List<Payment> payments) {
    List<Payment> paymentsPaid =
        payments.where((payment) => payment.paid == true).toList();
    return PaymentsPaidCardWidget(
      paymentsPaid: paymentsPaid.length,
      allPayments: payments.length,
    );
  }

  Widget paymentsPastDue(BuildContext context, List<Payment> payments) {
    List<Payment> paymentsPastDue = payments
        .where((payment) =>
            payment.paid == false && payment.dateDue.isBefore(DateTime.now()))
        .toList();
    return PaymentsPastDueCardWidget(
      paymentsList: paymentsPastDue,
    );
  }

  Widget paymentsApproachingDue(BuildContext context, List<Payment> payments) {
    List<Payment> paymentsApproachingDue = payments
        .where((payment) =>
            payment.paid == false &&
            payment.dateDue.isBefore(DateTime.now().add(Duration(days: 10))) &&
            payment.dateDue.isAfter(DateTime.now()))
        .toList();
    return PaymentsApproacingDueCardWidget(
      paymentsList: paymentsApproachingDue,
    );
  }

  Widget buildPaymentsScreen(BuildContext context) {
    List<Payment> payments = paymentsNotifier.value;

    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).Payments),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            paymentsPaid(context, payments),
            paymentsPastDue(context, payments),
            paymentsApproachingDue(context, payments),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => paymentsNotifier,
      child: BlocProvider<PaymentsBloc>(
        create: (context) {
          bloc = PaymentsBloc();
          logger.d("Payments loaded. Dispatching event");
          bloc.add(PaymentsEvent.loadedPayments());
          return bloc;
        },
        child: BlocConsumer<PaymentsBloc, PaymentsState>(
          listener: (context, state) {
            logger.d("Listening to changes in payments bloc");
            if (state is PaymentsUpdated)
              Provider.of<ValueNotifier<List<Payment>>>(context, listen: false)
                  .value = state.payments;
          },
          builder: (context, state) => buildPaymentsScreen(context),
        ),
      ),
    );
  }
}
