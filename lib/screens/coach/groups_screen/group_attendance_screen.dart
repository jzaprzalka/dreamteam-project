import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:flutter/material.dart';

class AttendanceList extends StatefulWidget {
  final Group attendanceList;
  @override
  _AttendanceList createState() =>
      new _AttendanceList(attendanceList: this.attendanceList);
  AttendanceList({Key key, @required this.attendanceList}) : super(key: key);
}

class _AttendanceList extends State<AttendanceList> {
  final Group attendanceList;
  _AttendanceList({@required this.attendanceList});
  List<User> _users = List.filled(2, null, growable: true);
  List<bool> _checkedValue;

  @override
  void initState() {
    super.initState();
    if (attendanceList.groupId == "1") {
      _users[0] = User(name: "Jan", surname: "Kowalski", userId: "1");
      _users[1] = User(name: "Anna", surname: "Nowak", userId: "2");
    } else {
      _users[0] = User(name: "Anna", surname: "Kowalska", userId: "1");
      _users[1] = User(name: "Jan", surname: "Nowak", userId: "2");
    }
    _checkedValue = List<bool>.filled(_users.length, false, growable: true);
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Attendance List"),
      ),
      backgroundColor: Colors.white,
      body: ListView.builder(
        itemCount: _users?.length,
        itemBuilder: (context, position) {
          return CheckboxListTile(
            title: Text(
              "${_users[position].name} ${_users[position].surname}",
              style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey[700]),
            ),
            value: _checkedValue[position],
            onChanged: (val) {
              setState(
                () {
                  _checkedValue[position] = val;
                },
              );
            },
          );
        },
      ),
    );
  }
}
