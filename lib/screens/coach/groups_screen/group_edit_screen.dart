import 'package:dreamteam_project/blocks/groups/groups_bloc.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:dreamteam_project/generated/l10n.dart';

import 'groups_screen.dart';

class EditGroup extends StatefulWidget {
  final Group editGroup;

  @override
  _EditGroup createState() => new _EditGroup(editGroup: this.editGroup);
  EditGroup({Key key, @required this.editGroup}) : super(key: key);
}

class _EditGroup extends GroupsScreenStateBase<EditGroup> {
  Group editGroup;
  _EditGroup({@required this.editGroup});
  List<User> _users = List.empty(growable: true);
  List<User> _students = List.empty(growable: true);
  List<User> _allUsers;
  List<DropdownMenuItem<User>> _dropDownMenu;
  List<DropdownMenuItem<User>> _dropDownDeleteMenu;
  User _currentStudent;
  User _currentStudentToDelete;
  TextEditingController groupText;

  @override
  void initState() {
    super.initState();
    currentGroupNotifier = ValueNotifier(editGroup);
    editGroup = currentGroupNotifier.value ?? editGroup;
    _students = editGroup.participants ?? List.empty(growable: true);
    _allUsers = usersNotifier.value ?? List.empty(growable: false);
    _users = _allUsers.whereNot((u) => _students.contains(u)).toList() ??
        List.empty(growable: true);
    updateDropdowns();
    groupText = TextEditingController()..text = editGroup.name;
  }

  @override
  List<GroupsEvent> get additionalStartupEvents => [
        GroupsEvent.loadedUsers(),
        GroupsEvent.loadedGroup(groupId: editGroup.groupId),
      ];

  void updateDropdowns({bool changeSelection = true}) {
    _dropDownMenu = getDropDownMenuItems(_users);
    _dropDownDeleteMenu = getDropDownMenuItems(_students);
    if (!changeSelection) return;
    _currentStudent = _users.firstOrNull ?? null;
    _currentStudentToDelete = _students.firstOrNull ?? null;
  }

  @override
  void notifyNotifierListeners() {
    editGroup = currentGroupNotifier.value ?? editGroup;
    _allUsers = usersNotifier.value ?? List.empty(growable: false);
    _students = _allUsers
            .where((u) => editGroup.participantIds.contains(u.userId))
            .toList() ??
        List.empty(growable: true);
    // _students = editGroup.participants ?? List.empty(growable: true);
    _users = _allUsers.where((u) => !_students.contains(u)).toList() ??
        List.empty(growable: true);
    updateDropdowns(changeSelection: true);
  }

  void initScreen(BuildContext context) {}

  @override
  Widget buildScreen(BuildContext context) {
    initScreen(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(S.current.editGroup),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.save,
              color: Colors.black,
            ),
            onPressed: () {
              logger.d("Saving group name");
              bloc.add(GroupsEvent.updatedGroupName(
                  groupId: editGroup.groupId, newName: groupText.text));
              Navigator.pop(context);
            },
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: Container(
        child: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        RaisedButton(
                          onPressed: () {
                            // Delete group behaviour
                            bloc.add(GroupsEvent.deletedGroup(
                                groupId: editGroup.groupId));
                          },
                          child: Text(
                            S.current.deleteGroup,
                            style: TextStyle(fontSize: 18),
                          ),
                          elevation: 5.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(
                              color: Colors.red[300],
                            ),
                          ),
                          color: Colors.red[300],
                          textColor: Colors.white,
                          padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        )
                      ],
                    ),
                    Text(
                      S.current.groupName,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: Colors.grey[700]),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: TextField(
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15.0),
                              borderSide: BorderSide.none,
                            ),
                            fillColor: Color(0xfff3f3f4),
                            filled: true),
                        controller: groupText,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      S.current.addPerson,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: Colors.grey[700]),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    DropdownButton(
                      isExpanded: true,
                      items: _dropDownMenu,
                      onChanged: changedDropDown,
                      value: _currentStudent,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        RaisedButton(
                          onPressed: () {
                            logger.d(
                                "Adding user ${_currentStudent.name} to group");
                            bloc.add(GroupsEvent.addedUserToGroup(
                                groupId: editGroup.groupId,
                                userId: _currentStudent.userId));
                            setState(() {
                              _students.insert(0, _currentStudent);
                              _users.remove(_currentStudent);
                              // updateDropdowns();
                            });
                          },
                          child: Text(
                            S.current.add,
                            style: TextStyle(fontSize: 18),
                          ),
                          elevation: 5.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(
                              color: Colors.green[300],
                            ),
                          ),
                          color: Colors.green[300],
                          textColor: Colors.black54,
                          padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      S.current.deletePerson,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: Colors.grey[700]),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    DropdownButton(
                      isExpanded: true,
                      items: _dropDownDeleteMenu,
                      onChanged: changedDropDownDelete,
                      value: _currentStudentToDelete,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        RaisedButton(
                          onPressed: () {
                            logger.d(
                                "Removing user ${_currentStudentToDelete.name} from group");
                            bloc.add(GroupsEvent.deletedUserFromGroup(
                                groupId: editGroup.groupId,
                                userId: _currentStudentToDelete.userId));
                            setState(() {
                              _users.insert(0, _currentStudentToDelete);
                              _students.remove(_currentStudentToDelete);
                              // updateDropdowns();
                            });
                          },
                          child: Text(
                            S.current.delete,
                            style: TextStyle(fontSize: 18),
                          ),
                          elevation: 5.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(
                              color: Colors.red[300],
                            ),
                          ),
                          color: Colors.red[300],
                          textColor: Colors.white,
                          padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void changedDropDown(User selectedStudent) {
    setState(() {
      _currentStudent = selectedStudent;
    });
  }

  void changedDropDownDelete(User selectedStudent) {
    setState(() {
      _currentStudentToDelete = selectedStudent;
    });
  }

  // @override
  // Widget build(BuildContext context) {
  //   return ChangeNotifierProvider(
  //     create: (_) => usersNotifier,
  //     child: BlocProvider<GroupsBloc>(
  //       create: (context) {
  //         bloc = GroupsBloc();
  //         bloc.add(GroupsEvent.loadedUsers());
  //         return bloc;
  //       },
  //       child: BlocConsumer<GroupsBloc, GroupsState>(
  //         listener: (context, state) {
  //           if (state is GroupsUpdated) {
  //             Provider.of<ValueNotifier<List<User>>>(context, listen: false)
  //                 .value = state.;
  //           }
  //         },
  //         builder: (context, state) => buildScreen(context),
  //       ),
  //     ),
  //   );
  // }
}
