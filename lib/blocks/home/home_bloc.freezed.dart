// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'home_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$HomeViewEventTearOff {
  const _$HomeViewEventTearOff();

// ignore: unused_element
  HomeViewLoading homeViewLoading() {
    return const HomeViewLoading();
  }

// ignore: unused_element
  LoadedGroups loadedGroups() {
    return const LoadedGroups();
  }

// ignore: unused_element
  LoadedPayments loadedPayments() {
    return const LoadedPayments();
  }

// ignore: unused_element
  LoadedSchedule loadedSchedule() {
    return const LoadedSchedule();
  }
}

/// @nodoc
// ignore: unused_element
const $HomeViewEvent = _$HomeViewEventTearOff();

/// @nodoc
mixin _$HomeViewEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult homeViewLoading(),
    @required TResult loadedGroups(),
    @required TResult loadedPayments(),
    @required TResult loadedSchedule(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult homeViewLoading(),
    TResult loadedGroups(),
    TResult loadedPayments(),
    TResult loadedSchedule(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult homeViewLoading(HomeViewLoading value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedPayments(LoadedPayments value),
    @required TResult loadedSchedule(LoadedSchedule value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult homeViewLoading(HomeViewLoading value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedPayments(LoadedPayments value),
    TResult loadedSchedule(LoadedSchedule value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $HomeViewEventCopyWith<$Res> {
  factory $HomeViewEventCopyWith(
          HomeViewEvent value, $Res Function(HomeViewEvent) then) =
      _$HomeViewEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$HomeViewEventCopyWithImpl<$Res>
    implements $HomeViewEventCopyWith<$Res> {
  _$HomeViewEventCopyWithImpl(this._value, this._then);

  final HomeViewEvent _value;
  // ignore: unused_field
  final $Res Function(HomeViewEvent) _then;
}

/// @nodoc
abstract class $HomeViewLoadingCopyWith<$Res> {
  factory $HomeViewLoadingCopyWith(
          HomeViewLoading value, $Res Function(HomeViewLoading) then) =
      _$HomeViewLoadingCopyWithImpl<$Res>;
}

/// @nodoc
class _$HomeViewLoadingCopyWithImpl<$Res>
    extends _$HomeViewEventCopyWithImpl<$Res>
    implements $HomeViewLoadingCopyWith<$Res> {
  _$HomeViewLoadingCopyWithImpl(
      HomeViewLoading _value, $Res Function(HomeViewLoading) _then)
      : super(_value, (v) => _then(v as HomeViewLoading));

  @override
  HomeViewLoading get _value => super._value as HomeViewLoading;
}

/// @nodoc
class _$HomeViewLoading implements HomeViewLoading {
  const _$HomeViewLoading();

  @override
  String toString() {
    return 'HomeViewEvent.homeViewLoading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is HomeViewLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult homeViewLoading(),
    @required TResult loadedGroups(),
    @required TResult loadedPayments(),
    @required TResult loadedSchedule(),
  }) {
    assert(homeViewLoading != null);
    assert(loadedGroups != null);
    assert(loadedPayments != null);
    assert(loadedSchedule != null);
    return homeViewLoading();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult homeViewLoading(),
    TResult loadedGroups(),
    TResult loadedPayments(),
    TResult loadedSchedule(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (homeViewLoading != null) {
      return homeViewLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult homeViewLoading(HomeViewLoading value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedPayments(LoadedPayments value),
    @required TResult loadedSchedule(LoadedSchedule value),
  }) {
    assert(homeViewLoading != null);
    assert(loadedGroups != null);
    assert(loadedPayments != null);
    assert(loadedSchedule != null);
    return homeViewLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult homeViewLoading(HomeViewLoading value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedPayments(LoadedPayments value),
    TResult loadedSchedule(LoadedSchedule value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (homeViewLoading != null) {
      return homeViewLoading(this);
    }
    return orElse();
  }
}

abstract class HomeViewLoading implements HomeViewEvent {
  const factory HomeViewLoading() = _$HomeViewLoading;
}

/// @nodoc
abstract class $LoadedGroupsCopyWith<$Res> {
  factory $LoadedGroupsCopyWith(
          LoadedGroups value, $Res Function(LoadedGroups) then) =
      _$LoadedGroupsCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoadedGroupsCopyWithImpl<$Res> extends _$HomeViewEventCopyWithImpl<$Res>
    implements $LoadedGroupsCopyWith<$Res> {
  _$LoadedGroupsCopyWithImpl(
      LoadedGroups _value, $Res Function(LoadedGroups) _then)
      : super(_value, (v) => _then(v as LoadedGroups));

  @override
  LoadedGroups get _value => super._value as LoadedGroups;
}

/// @nodoc
class _$LoadedGroups implements LoadedGroups {
  const _$LoadedGroups();

  @override
  String toString() {
    return 'HomeViewEvent.loadedGroups()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoadedGroups);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult homeViewLoading(),
    @required TResult loadedGroups(),
    @required TResult loadedPayments(),
    @required TResult loadedSchedule(),
  }) {
    assert(homeViewLoading != null);
    assert(loadedGroups != null);
    assert(loadedPayments != null);
    assert(loadedSchedule != null);
    return loadedGroups();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult homeViewLoading(),
    TResult loadedGroups(),
    TResult loadedPayments(),
    TResult loadedSchedule(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedGroups != null) {
      return loadedGroups();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult homeViewLoading(HomeViewLoading value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedPayments(LoadedPayments value),
    @required TResult loadedSchedule(LoadedSchedule value),
  }) {
    assert(homeViewLoading != null);
    assert(loadedGroups != null);
    assert(loadedPayments != null);
    assert(loadedSchedule != null);
    return loadedGroups(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult homeViewLoading(HomeViewLoading value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedPayments(LoadedPayments value),
    TResult loadedSchedule(LoadedSchedule value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedGroups != null) {
      return loadedGroups(this);
    }
    return orElse();
  }
}

abstract class LoadedGroups implements HomeViewEvent {
  const factory LoadedGroups() = _$LoadedGroups;
}

/// @nodoc
abstract class $LoadedPaymentsCopyWith<$Res> {
  factory $LoadedPaymentsCopyWith(
          LoadedPayments value, $Res Function(LoadedPayments) then) =
      _$LoadedPaymentsCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoadedPaymentsCopyWithImpl<$Res>
    extends _$HomeViewEventCopyWithImpl<$Res>
    implements $LoadedPaymentsCopyWith<$Res> {
  _$LoadedPaymentsCopyWithImpl(
      LoadedPayments _value, $Res Function(LoadedPayments) _then)
      : super(_value, (v) => _then(v as LoadedPayments));

  @override
  LoadedPayments get _value => super._value as LoadedPayments;
}

/// @nodoc
class _$LoadedPayments implements LoadedPayments {
  const _$LoadedPayments();

  @override
  String toString() {
    return 'HomeViewEvent.loadedPayments()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoadedPayments);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult homeViewLoading(),
    @required TResult loadedGroups(),
    @required TResult loadedPayments(),
    @required TResult loadedSchedule(),
  }) {
    assert(homeViewLoading != null);
    assert(loadedGroups != null);
    assert(loadedPayments != null);
    assert(loadedSchedule != null);
    return loadedPayments();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult homeViewLoading(),
    TResult loadedGroups(),
    TResult loadedPayments(),
    TResult loadedSchedule(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedPayments != null) {
      return loadedPayments();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult homeViewLoading(HomeViewLoading value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedPayments(LoadedPayments value),
    @required TResult loadedSchedule(LoadedSchedule value),
  }) {
    assert(homeViewLoading != null);
    assert(loadedGroups != null);
    assert(loadedPayments != null);
    assert(loadedSchedule != null);
    return loadedPayments(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult homeViewLoading(HomeViewLoading value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedPayments(LoadedPayments value),
    TResult loadedSchedule(LoadedSchedule value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedPayments != null) {
      return loadedPayments(this);
    }
    return orElse();
  }
}

abstract class LoadedPayments implements HomeViewEvent {
  const factory LoadedPayments() = _$LoadedPayments;
}

/// @nodoc
abstract class $LoadedScheduleCopyWith<$Res> {
  factory $LoadedScheduleCopyWith(
          LoadedSchedule value, $Res Function(LoadedSchedule) then) =
      _$LoadedScheduleCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoadedScheduleCopyWithImpl<$Res>
    extends _$HomeViewEventCopyWithImpl<$Res>
    implements $LoadedScheduleCopyWith<$Res> {
  _$LoadedScheduleCopyWithImpl(
      LoadedSchedule _value, $Res Function(LoadedSchedule) _then)
      : super(_value, (v) => _then(v as LoadedSchedule));

  @override
  LoadedSchedule get _value => super._value as LoadedSchedule;
}

/// @nodoc
class _$LoadedSchedule implements LoadedSchedule {
  const _$LoadedSchedule();

  @override
  String toString() {
    return 'HomeViewEvent.loadedSchedule()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoadedSchedule);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult homeViewLoading(),
    @required TResult loadedGroups(),
    @required TResult loadedPayments(),
    @required TResult loadedSchedule(),
  }) {
    assert(homeViewLoading != null);
    assert(loadedGroups != null);
    assert(loadedPayments != null);
    assert(loadedSchedule != null);
    return loadedSchedule();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult homeViewLoading(),
    TResult loadedGroups(),
    TResult loadedPayments(),
    TResult loadedSchedule(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedSchedule != null) {
      return loadedSchedule();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult homeViewLoading(HomeViewLoading value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedPayments(LoadedPayments value),
    @required TResult loadedSchedule(LoadedSchedule value),
  }) {
    assert(homeViewLoading != null);
    assert(loadedGroups != null);
    assert(loadedPayments != null);
    assert(loadedSchedule != null);
    return loadedSchedule(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult homeViewLoading(HomeViewLoading value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedPayments(LoadedPayments value),
    TResult loadedSchedule(LoadedSchedule value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedSchedule != null) {
      return loadedSchedule(this);
    }
    return orElse();
  }
}

abstract class LoadedSchedule implements HomeViewEvent {
  const factory LoadedSchedule() = _$LoadedSchedule;
}

/// @nodoc
class _$HomeViewStateTearOff {
  const _$HomeViewStateTearOff();

// ignore: unused_element
  HomeViewInitial initial() {
    return HomeViewInitial();
  }

// ignore: unused_element
  HomeViewLoaded loaded(
      {@required List<Group> groups,
      @required List<Payment> payments,
      @required List<Schedule> schedule}) {
    return HomeViewLoaded(
      groups: groups,
      payments: payments,
      schedule: schedule,
    );
  }

// ignore: unused_element
  HomeViewGroupsUpdated groupsUpdated(
      {@required List<Group> groups, @required bool isUpdated}) {
    return HomeViewGroupsUpdated(
      groups: groups,
      isUpdated: isUpdated,
    );
  }

// ignore: unused_element
  HomeViewPaymentsUpdated paymentsUpdated(
      {@required List<Payment> payments, @required bool isUpdated}) {
    return HomeViewPaymentsUpdated(
      payments: payments,
      isUpdated: isUpdated,
    );
  }

// ignore: unused_element
  HomeViewScheduleUpdated scheduleUpdated(
      {@required List<Schedule> schedule, @required bool isUpdated}) {
    return HomeViewScheduleUpdated(
      schedule: schedule,
      isUpdated: isUpdated,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $HomeViewState = _$HomeViewStateTearOff();

/// @nodoc
mixin _$HomeViewState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required
        TResult loaded(List<Group> groups, List<Payment> payments,
            List<Schedule> schedule),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required TResult paymentsUpdated(List<Payment> payments, bool isUpdated),
    @required TResult scheduleUpdated(List<Schedule> schedule, bool isUpdated),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loaded(
        List<Group> groups, List<Payment> payments, List<Schedule> schedule),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    TResult paymentsUpdated(List<Payment> payments, bool isUpdated),
    TResult scheduleUpdated(List<Schedule> schedule, bool isUpdated),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(HomeViewInitial value),
    @required TResult loaded(HomeViewLoaded value),
    @required TResult groupsUpdated(HomeViewGroupsUpdated value),
    @required TResult paymentsUpdated(HomeViewPaymentsUpdated value),
    @required TResult scheduleUpdated(HomeViewScheduleUpdated value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(HomeViewInitial value),
    TResult loaded(HomeViewLoaded value),
    TResult groupsUpdated(HomeViewGroupsUpdated value),
    TResult paymentsUpdated(HomeViewPaymentsUpdated value),
    TResult scheduleUpdated(HomeViewScheduleUpdated value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $HomeViewStateCopyWith<$Res> {
  factory $HomeViewStateCopyWith(
          HomeViewState value, $Res Function(HomeViewState) then) =
      _$HomeViewStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$HomeViewStateCopyWithImpl<$Res>
    implements $HomeViewStateCopyWith<$Res> {
  _$HomeViewStateCopyWithImpl(this._value, this._then);

  final HomeViewState _value;
  // ignore: unused_field
  final $Res Function(HomeViewState) _then;
}

/// @nodoc
abstract class $HomeViewInitialCopyWith<$Res> {
  factory $HomeViewInitialCopyWith(
          HomeViewInitial value, $Res Function(HomeViewInitial) then) =
      _$HomeViewInitialCopyWithImpl<$Res>;
}

/// @nodoc
class _$HomeViewInitialCopyWithImpl<$Res>
    extends _$HomeViewStateCopyWithImpl<$Res>
    implements $HomeViewInitialCopyWith<$Res> {
  _$HomeViewInitialCopyWithImpl(
      HomeViewInitial _value, $Res Function(HomeViewInitial) _then)
      : super(_value, (v) => _then(v as HomeViewInitial));

  @override
  HomeViewInitial get _value => super._value as HomeViewInitial;
}

/// @nodoc
class _$HomeViewInitial implements HomeViewInitial {
  _$HomeViewInitial();

  @override
  String toString() {
    return 'HomeViewState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is HomeViewInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required
        TResult loaded(List<Group> groups, List<Payment> payments,
            List<Schedule> schedule),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required TResult paymentsUpdated(List<Payment> payments, bool isUpdated),
    @required TResult scheduleUpdated(List<Schedule> schedule, bool isUpdated),
  }) {
    assert(initial != null);
    assert(loaded != null);
    assert(groupsUpdated != null);
    assert(paymentsUpdated != null);
    assert(scheduleUpdated != null);
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loaded(
        List<Group> groups, List<Payment> payments, List<Schedule> schedule),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    TResult paymentsUpdated(List<Payment> payments, bool isUpdated),
    TResult scheduleUpdated(List<Schedule> schedule, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(HomeViewInitial value),
    @required TResult loaded(HomeViewLoaded value),
    @required TResult groupsUpdated(HomeViewGroupsUpdated value),
    @required TResult paymentsUpdated(HomeViewPaymentsUpdated value),
    @required TResult scheduleUpdated(HomeViewScheduleUpdated value),
  }) {
    assert(initial != null);
    assert(loaded != null);
    assert(groupsUpdated != null);
    assert(paymentsUpdated != null);
    assert(scheduleUpdated != null);
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(HomeViewInitial value),
    TResult loaded(HomeViewLoaded value),
    TResult groupsUpdated(HomeViewGroupsUpdated value),
    TResult paymentsUpdated(HomeViewPaymentsUpdated value),
    TResult scheduleUpdated(HomeViewScheduleUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class HomeViewInitial implements HomeViewState {
  factory HomeViewInitial() = _$HomeViewInitial;
}

/// @nodoc
abstract class $HomeViewLoadedCopyWith<$Res> {
  factory $HomeViewLoadedCopyWith(
          HomeViewLoaded value, $Res Function(HomeViewLoaded) then) =
      _$HomeViewLoadedCopyWithImpl<$Res>;
  $Res call(
      {List<Group> groups, List<Payment> payments, List<Schedule> schedule});
}

/// @nodoc
class _$HomeViewLoadedCopyWithImpl<$Res>
    extends _$HomeViewStateCopyWithImpl<$Res>
    implements $HomeViewLoadedCopyWith<$Res> {
  _$HomeViewLoadedCopyWithImpl(
      HomeViewLoaded _value, $Res Function(HomeViewLoaded) _then)
      : super(_value, (v) => _then(v as HomeViewLoaded));

  @override
  HomeViewLoaded get _value => super._value as HomeViewLoaded;

  @override
  $Res call({
    Object groups = freezed,
    Object payments = freezed,
    Object schedule = freezed,
  }) {
    return _then(HomeViewLoaded(
      groups: groups == freezed ? _value.groups : groups as List<Group>,
      payments:
          payments == freezed ? _value.payments : payments as List<Payment>,
      schedule:
          schedule == freezed ? _value.schedule : schedule as List<Schedule>,
    ));
  }
}

/// @nodoc
class _$HomeViewLoaded implements HomeViewLoaded {
  _$HomeViewLoaded(
      {@required this.groups, @required this.payments, @required this.schedule})
      : assert(groups != null),
        assert(payments != null),
        assert(schedule != null);

  @override
  final List<Group> groups;
  @override
  final List<Payment> payments;
  @override
  final List<Schedule> schedule;

  @override
  String toString() {
    return 'HomeViewState.loaded(groups: $groups, payments: $payments, schedule: $schedule)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is HomeViewLoaded &&
            (identical(other.groups, groups) ||
                const DeepCollectionEquality().equals(other.groups, groups)) &&
            (identical(other.payments, payments) ||
                const DeepCollectionEquality()
                    .equals(other.payments, payments)) &&
            (identical(other.schedule, schedule) ||
                const DeepCollectionEquality()
                    .equals(other.schedule, schedule)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(groups) ^
      const DeepCollectionEquality().hash(payments) ^
      const DeepCollectionEquality().hash(schedule);

  @JsonKey(ignore: true)
  @override
  $HomeViewLoadedCopyWith<HomeViewLoaded> get copyWith =>
      _$HomeViewLoadedCopyWithImpl<HomeViewLoaded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required
        TResult loaded(List<Group> groups, List<Payment> payments,
            List<Schedule> schedule),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required TResult paymentsUpdated(List<Payment> payments, bool isUpdated),
    @required TResult scheduleUpdated(List<Schedule> schedule, bool isUpdated),
  }) {
    assert(initial != null);
    assert(loaded != null);
    assert(groupsUpdated != null);
    assert(paymentsUpdated != null);
    assert(scheduleUpdated != null);
    return loaded(groups, payments, schedule);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loaded(
        List<Group> groups, List<Payment> payments, List<Schedule> schedule),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    TResult paymentsUpdated(List<Payment> payments, bool isUpdated),
    TResult scheduleUpdated(List<Schedule> schedule, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loaded != null) {
      return loaded(groups, payments, schedule);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(HomeViewInitial value),
    @required TResult loaded(HomeViewLoaded value),
    @required TResult groupsUpdated(HomeViewGroupsUpdated value),
    @required TResult paymentsUpdated(HomeViewPaymentsUpdated value),
    @required TResult scheduleUpdated(HomeViewScheduleUpdated value),
  }) {
    assert(initial != null);
    assert(loaded != null);
    assert(groupsUpdated != null);
    assert(paymentsUpdated != null);
    assert(scheduleUpdated != null);
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(HomeViewInitial value),
    TResult loaded(HomeViewLoaded value),
    TResult groupsUpdated(HomeViewGroupsUpdated value),
    TResult paymentsUpdated(HomeViewPaymentsUpdated value),
    TResult scheduleUpdated(HomeViewScheduleUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class HomeViewLoaded implements HomeViewState {
  factory HomeViewLoaded(
      {@required List<Group> groups,
      @required List<Payment> payments,
      @required List<Schedule> schedule}) = _$HomeViewLoaded;

  List<Group> get groups;
  List<Payment> get payments;
  List<Schedule> get schedule;
  @JsonKey(ignore: true)
  $HomeViewLoadedCopyWith<HomeViewLoaded> get copyWith;
}

/// @nodoc
abstract class $HomeViewGroupsUpdatedCopyWith<$Res> {
  factory $HomeViewGroupsUpdatedCopyWith(HomeViewGroupsUpdated value,
          $Res Function(HomeViewGroupsUpdated) then) =
      _$HomeViewGroupsUpdatedCopyWithImpl<$Res>;
  $Res call({List<Group> groups, bool isUpdated});
}

/// @nodoc
class _$HomeViewGroupsUpdatedCopyWithImpl<$Res>
    extends _$HomeViewStateCopyWithImpl<$Res>
    implements $HomeViewGroupsUpdatedCopyWith<$Res> {
  _$HomeViewGroupsUpdatedCopyWithImpl(
      HomeViewGroupsUpdated _value, $Res Function(HomeViewGroupsUpdated) _then)
      : super(_value, (v) => _then(v as HomeViewGroupsUpdated));

  @override
  HomeViewGroupsUpdated get _value => super._value as HomeViewGroupsUpdated;

  @override
  $Res call({
    Object groups = freezed,
    Object isUpdated = freezed,
  }) {
    return _then(HomeViewGroupsUpdated(
      groups: groups == freezed ? _value.groups : groups as List<Group>,
      isUpdated: isUpdated == freezed ? _value.isUpdated : isUpdated as bool,
    ));
  }
}

/// @nodoc
class _$HomeViewGroupsUpdated implements HomeViewGroupsUpdated {
  _$HomeViewGroupsUpdated({@required this.groups, @required this.isUpdated})
      : assert(groups != null),
        assert(isUpdated != null);

  @override
  final List<Group> groups;
  @override
  final bool isUpdated;

  @override
  String toString() {
    return 'HomeViewState.groupsUpdated(groups: $groups, isUpdated: $isUpdated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is HomeViewGroupsUpdated &&
            (identical(other.groups, groups) ||
                const DeepCollectionEquality().equals(other.groups, groups)) &&
            (identical(other.isUpdated, isUpdated) ||
                const DeepCollectionEquality()
                    .equals(other.isUpdated, isUpdated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(groups) ^
      const DeepCollectionEquality().hash(isUpdated);

  @JsonKey(ignore: true)
  @override
  $HomeViewGroupsUpdatedCopyWith<HomeViewGroupsUpdated> get copyWith =>
      _$HomeViewGroupsUpdatedCopyWithImpl<HomeViewGroupsUpdated>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required
        TResult loaded(List<Group> groups, List<Payment> payments,
            List<Schedule> schedule),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required TResult paymentsUpdated(List<Payment> payments, bool isUpdated),
    @required TResult scheduleUpdated(List<Schedule> schedule, bool isUpdated),
  }) {
    assert(initial != null);
    assert(loaded != null);
    assert(groupsUpdated != null);
    assert(paymentsUpdated != null);
    assert(scheduleUpdated != null);
    return groupsUpdated(groups, isUpdated);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loaded(
        List<Group> groups, List<Payment> payments, List<Schedule> schedule),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    TResult paymentsUpdated(List<Payment> payments, bool isUpdated),
    TResult scheduleUpdated(List<Schedule> schedule, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (groupsUpdated != null) {
      return groupsUpdated(groups, isUpdated);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(HomeViewInitial value),
    @required TResult loaded(HomeViewLoaded value),
    @required TResult groupsUpdated(HomeViewGroupsUpdated value),
    @required TResult paymentsUpdated(HomeViewPaymentsUpdated value),
    @required TResult scheduleUpdated(HomeViewScheduleUpdated value),
  }) {
    assert(initial != null);
    assert(loaded != null);
    assert(groupsUpdated != null);
    assert(paymentsUpdated != null);
    assert(scheduleUpdated != null);
    return groupsUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(HomeViewInitial value),
    TResult loaded(HomeViewLoaded value),
    TResult groupsUpdated(HomeViewGroupsUpdated value),
    TResult paymentsUpdated(HomeViewPaymentsUpdated value),
    TResult scheduleUpdated(HomeViewScheduleUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (groupsUpdated != null) {
      return groupsUpdated(this);
    }
    return orElse();
  }
}

abstract class HomeViewGroupsUpdated implements HomeViewState {
  factory HomeViewGroupsUpdated(
      {@required List<Group> groups,
      @required bool isUpdated}) = _$HomeViewGroupsUpdated;

  List<Group> get groups;
  bool get isUpdated;
  @JsonKey(ignore: true)
  $HomeViewGroupsUpdatedCopyWith<HomeViewGroupsUpdated> get copyWith;
}

/// @nodoc
abstract class $HomeViewPaymentsUpdatedCopyWith<$Res> {
  factory $HomeViewPaymentsUpdatedCopyWith(HomeViewPaymentsUpdated value,
          $Res Function(HomeViewPaymentsUpdated) then) =
      _$HomeViewPaymentsUpdatedCopyWithImpl<$Res>;
  $Res call({List<Payment> payments, bool isUpdated});
}

/// @nodoc
class _$HomeViewPaymentsUpdatedCopyWithImpl<$Res>
    extends _$HomeViewStateCopyWithImpl<$Res>
    implements $HomeViewPaymentsUpdatedCopyWith<$Res> {
  _$HomeViewPaymentsUpdatedCopyWithImpl(HomeViewPaymentsUpdated _value,
      $Res Function(HomeViewPaymentsUpdated) _then)
      : super(_value, (v) => _then(v as HomeViewPaymentsUpdated));

  @override
  HomeViewPaymentsUpdated get _value => super._value as HomeViewPaymentsUpdated;

  @override
  $Res call({
    Object payments = freezed,
    Object isUpdated = freezed,
  }) {
    return _then(HomeViewPaymentsUpdated(
      payments:
          payments == freezed ? _value.payments : payments as List<Payment>,
      isUpdated: isUpdated == freezed ? _value.isUpdated : isUpdated as bool,
    ));
  }
}

/// @nodoc
class _$HomeViewPaymentsUpdated implements HomeViewPaymentsUpdated {
  _$HomeViewPaymentsUpdated({@required this.payments, @required this.isUpdated})
      : assert(payments != null),
        assert(isUpdated != null);

  @override
  final List<Payment> payments;
  @override
  final bool isUpdated;

  @override
  String toString() {
    return 'HomeViewState.paymentsUpdated(payments: $payments, isUpdated: $isUpdated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is HomeViewPaymentsUpdated &&
            (identical(other.payments, payments) ||
                const DeepCollectionEquality()
                    .equals(other.payments, payments)) &&
            (identical(other.isUpdated, isUpdated) ||
                const DeepCollectionEquality()
                    .equals(other.isUpdated, isUpdated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(payments) ^
      const DeepCollectionEquality().hash(isUpdated);

  @JsonKey(ignore: true)
  @override
  $HomeViewPaymentsUpdatedCopyWith<HomeViewPaymentsUpdated> get copyWith =>
      _$HomeViewPaymentsUpdatedCopyWithImpl<HomeViewPaymentsUpdated>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required
        TResult loaded(List<Group> groups, List<Payment> payments,
            List<Schedule> schedule),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required TResult paymentsUpdated(List<Payment> payments, bool isUpdated),
    @required TResult scheduleUpdated(List<Schedule> schedule, bool isUpdated),
  }) {
    assert(initial != null);
    assert(loaded != null);
    assert(groupsUpdated != null);
    assert(paymentsUpdated != null);
    assert(scheduleUpdated != null);
    return paymentsUpdated(payments, isUpdated);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loaded(
        List<Group> groups, List<Payment> payments, List<Schedule> schedule),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    TResult paymentsUpdated(List<Payment> payments, bool isUpdated),
    TResult scheduleUpdated(List<Schedule> schedule, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (paymentsUpdated != null) {
      return paymentsUpdated(payments, isUpdated);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(HomeViewInitial value),
    @required TResult loaded(HomeViewLoaded value),
    @required TResult groupsUpdated(HomeViewGroupsUpdated value),
    @required TResult paymentsUpdated(HomeViewPaymentsUpdated value),
    @required TResult scheduleUpdated(HomeViewScheduleUpdated value),
  }) {
    assert(initial != null);
    assert(loaded != null);
    assert(groupsUpdated != null);
    assert(paymentsUpdated != null);
    assert(scheduleUpdated != null);
    return paymentsUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(HomeViewInitial value),
    TResult loaded(HomeViewLoaded value),
    TResult groupsUpdated(HomeViewGroupsUpdated value),
    TResult paymentsUpdated(HomeViewPaymentsUpdated value),
    TResult scheduleUpdated(HomeViewScheduleUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (paymentsUpdated != null) {
      return paymentsUpdated(this);
    }
    return orElse();
  }
}

abstract class HomeViewPaymentsUpdated implements HomeViewState {
  factory HomeViewPaymentsUpdated(
      {@required List<Payment> payments,
      @required bool isUpdated}) = _$HomeViewPaymentsUpdated;

  List<Payment> get payments;
  bool get isUpdated;
  @JsonKey(ignore: true)
  $HomeViewPaymentsUpdatedCopyWith<HomeViewPaymentsUpdated> get copyWith;
}

/// @nodoc
abstract class $HomeViewScheduleUpdatedCopyWith<$Res> {
  factory $HomeViewScheduleUpdatedCopyWith(HomeViewScheduleUpdated value,
          $Res Function(HomeViewScheduleUpdated) then) =
      _$HomeViewScheduleUpdatedCopyWithImpl<$Res>;
  $Res call({List<Schedule> schedule, bool isUpdated});
}

/// @nodoc
class _$HomeViewScheduleUpdatedCopyWithImpl<$Res>
    extends _$HomeViewStateCopyWithImpl<$Res>
    implements $HomeViewScheduleUpdatedCopyWith<$Res> {
  _$HomeViewScheduleUpdatedCopyWithImpl(HomeViewScheduleUpdated _value,
      $Res Function(HomeViewScheduleUpdated) _then)
      : super(_value, (v) => _then(v as HomeViewScheduleUpdated));

  @override
  HomeViewScheduleUpdated get _value => super._value as HomeViewScheduleUpdated;

  @override
  $Res call({
    Object schedule = freezed,
    Object isUpdated = freezed,
  }) {
    return _then(HomeViewScheduleUpdated(
      schedule:
          schedule == freezed ? _value.schedule : schedule as List<Schedule>,
      isUpdated: isUpdated == freezed ? _value.isUpdated : isUpdated as bool,
    ));
  }
}

/// @nodoc
class _$HomeViewScheduleUpdated implements HomeViewScheduleUpdated {
  _$HomeViewScheduleUpdated({@required this.schedule, @required this.isUpdated})
      : assert(schedule != null),
        assert(isUpdated != null);

  @override
  final List<Schedule> schedule;
  @override
  final bool isUpdated;

  @override
  String toString() {
    return 'HomeViewState.scheduleUpdated(schedule: $schedule, isUpdated: $isUpdated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is HomeViewScheduleUpdated &&
            (identical(other.schedule, schedule) ||
                const DeepCollectionEquality()
                    .equals(other.schedule, schedule)) &&
            (identical(other.isUpdated, isUpdated) ||
                const DeepCollectionEquality()
                    .equals(other.isUpdated, isUpdated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(schedule) ^
      const DeepCollectionEquality().hash(isUpdated);

  @JsonKey(ignore: true)
  @override
  $HomeViewScheduleUpdatedCopyWith<HomeViewScheduleUpdated> get copyWith =>
      _$HomeViewScheduleUpdatedCopyWithImpl<HomeViewScheduleUpdated>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required
        TResult loaded(List<Group> groups, List<Payment> payments,
            List<Schedule> schedule),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required TResult paymentsUpdated(List<Payment> payments, bool isUpdated),
    @required TResult scheduleUpdated(List<Schedule> schedule, bool isUpdated),
  }) {
    assert(initial != null);
    assert(loaded != null);
    assert(groupsUpdated != null);
    assert(paymentsUpdated != null);
    assert(scheduleUpdated != null);
    return scheduleUpdated(schedule, isUpdated);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loaded(
        List<Group> groups, List<Payment> payments, List<Schedule> schedule),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    TResult paymentsUpdated(List<Payment> payments, bool isUpdated),
    TResult scheduleUpdated(List<Schedule> schedule, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (scheduleUpdated != null) {
      return scheduleUpdated(schedule, isUpdated);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(HomeViewInitial value),
    @required TResult loaded(HomeViewLoaded value),
    @required TResult groupsUpdated(HomeViewGroupsUpdated value),
    @required TResult paymentsUpdated(HomeViewPaymentsUpdated value),
    @required TResult scheduleUpdated(HomeViewScheduleUpdated value),
  }) {
    assert(initial != null);
    assert(loaded != null);
    assert(groupsUpdated != null);
    assert(paymentsUpdated != null);
    assert(scheduleUpdated != null);
    return scheduleUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(HomeViewInitial value),
    TResult loaded(HomeViewLoaded value),
    TResult groupsUpdated(HomeViewGroupsUpdated value),
    TResult paymentsUpdated(HomeViewPaymentsUpdated value),
    TResult scheduleUpdated(HomeViewScheduleUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (scheduleUpdated != null) {
      return scheduleUpdated(this);
    }
    return orElse();
  }
}

abstract class HomeViewScheduleUpdated implements HomeViewState {
  factory HomeViewScheduleUpdated(
      {@required List<Schedule> schedule,
      @required bool isUpdated}) = _$HomeViewScheduleUpdated;

  List<Schedule> get schedule;
  bool get isUpdated;
  @JsonKey(ignore: true)
  $HomeViewScheduleUpdatedCopyWith<HomeViewScheduleUpdated> get copyWith;
}
