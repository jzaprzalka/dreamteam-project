part of 'home_bloc.dart';

@freezed
abstract class HomeViewEvent with _$HomeViewEvent {
  const factory HomeViewEvent.homeViewLoading() = HomeViewLoading;
  const factory HomeViewEvent.loadedGroups() = LoadedGroups;
  const factory HomeViewEvent.loadedPayments() = LoadedPayments;
  const factory HomeViewEvent.loadedSchedule() = LoadedSchedule;
}
