import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/repositories/home_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:dreamteam_project/models/Schedule.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'home_event.dart';
part 'home_state.dart';
part 'home_bloc.freezed.dart';

class HomeViewBloc extends Bloc<HomeViewEvent, HomeViewState> {
  HomeRepository homeRepository = HomeRepository();

  HomeViewBloc() : super(HomeViewState.initial());

  @override
  Stream<HomeViewState> mapEventToState(HomeViewEvent event) async* {
    List<Payment> payments;
    List<Group> groups;
    List<Schedule> schedule;
    try {
      groups = await homeRepository.getCoachesGroups() ?? List.empty();
      payments = await homeRepository.getGroupsPayments(groups) ?? List.empty();
      schedule = await homeRepository.getCoachesSchedule() ?? List.empty();
      yield HomeViewState.loaded(
          groups: groups, payments: payments, schedule: schedule);
      if (event is LoadedGroups) {
        yield HomeViewState.groupsUpdated(groups: groups, isUpdated: true);
      }
      if (event is LoadedPayments) {
        yield HomeViewState.paymentsUpdated(
            payments: payments, isUpdated: true);
      }
      if (event is LoadedSchedule) {
        yield HomeViewState.scheduleUpdated(
            schedule: schedule, isUpdated: true);
      }
    } catch (e) {
      logger.e("Error mapping HomeEvent to HomeState: $e");
    }
  }
}
