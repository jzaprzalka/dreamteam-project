part of 'home_bloc.dart';

@freezed
abstract class HomeViewState with _$HomeViewState {
  factory HomeViewState.initial() = HomeViewInitial;
  factory HomeViewState.loaded(
      {@required List<Group> groups,
      @required List<Payment> payments,
      @required List<Schedule> schedule}) = HomeViewLoaded;
  factory HomeViewState.groupsUpdated(
      {@required List<Group> groups,
      @required bool isUpdated}) = HomeViewGroupsUpdated;
  factory HomeViewState.paymentsUpdated(
      {@required List<Payment> payments,
      @required bool isUpdated}) = HomeViewPaymentsUpdated;
  factory HomeViewState.scheduleUpdated(
      {@required List<Schedule> schedule,
      @required bool isUpdated}) = HomeViewScheduleUpdated;
}
