part of 'admin_payments_bloc.dart';

@freezed
abstract class AdminPaymentsState with _$AdminPaymentsState {
  factory AdminPaymentsState.initial() = PaymentsInitial;
  factory AdminPaymentsState.paymentsInvalid() = PaymentsInvalid;
  factory AdminPaymentsState.paymentsStatusInvalid() = PaymentsStatusInvalid;
  factory AdminPaymentsState.userPaymentsUpdated({
    @required String userId,
    @required List<Payment> payments,
  }) = UserPaymentsUpdated;
  factory AdminPaymentsState.userPaymentsHistoryUpdated({
    @required String userId,
    @required List<Payment> payments,
  }) = UserPaymentsHistoryUpdated;
  factory AdminPaymentsState.overallPaymentsStatusUpdated({
    @required int paymentsPastDue,
    @required int paymentsPaid,
    @required int paymentsApproachingDue,
  }) = OverallPaymentsStatusUpdated;
  factory AdminPaymentsState.groupPaymentsStatusUpdated({
    @required String groupId,
    @required int paymentsPastDue,
    @required int paymentsPaid,
  }) = GroupPaymentsStatusUpdated;
  factory AdminPaymentsState.userPaymentsStatusUpdated({
    @required String userId,
    @required int paymentsPastDue,
    @required int paymentsPaid,
  }) = UserPaymentsStatusUpdated;
  factory AdminPaymentsState.userListUpdated({
    @required String groupId,
    @required List<User> users,
  }) = UserListUpdated;
  factory AdminPaymentsState.groupListUpdated({
    @required List<Group> groups,
  }) = GroupListUpdated;
}
