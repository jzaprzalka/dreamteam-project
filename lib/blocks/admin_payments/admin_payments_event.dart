part of 'admin_payments_bloc.dart';

@freezed
abstract class AdminPaymentsEvent with _$AdminPaymentsEvent {
  const factory AdminPaymentsEvent.loadedPayments() = LoadedPayments;
  const factory AdminPaymentsEvent.loadedGroups() = LoadedGroups;
  const factory AdminPaymentsEvent.loadedUserPayments(
      {@required String userId}) = LoadedUserPayments;
  const factory AdminPaymentsEvent.loadedUserHistoryPayments(
      {@required String userId}) = LoadedUserHistoryPayments;
  const factory AdminPaymentsEvent.loadedGroupPayments(
      {@required String groupId}) = LoadedGroupPayments;
  const factory AdminPaymentsEvent.addedUserPayment({
    @required List<String> userIds,
    @required DateTime startDueDate,
    DateTime endDueDate,
    @required String paymentName,
    @required int amount,
    @required RepeatMode repeatMode,
  }) = AddedUserPayment;
  const factory AdminPaymentsEvent.addedGroupPayment({
    @required List<String> groupIds,
    @required DateTime startDueDate,
    DateTime endDueDate,
    @required String paymentName,
    @required int amount,
    @required RepeatMode repeatMode,
  }) = AddedGroupPayment;
  const factory AdminPaymentsEvent.updatedPayment({
    @required String paymentId,
    DateTime newDueDate,
    String newName,
    int newAmount,
    bool isAccounted,
  }) = UpdatedPayment;
  const factory AdminPaymentsEvent.paymentAccounted({
    @required Payment payment,
    @required DateTime dateAccounted,
  }) = PaymentAccounted;
}

enum RepeatMode { Monthly, Weekly, OneTime }
