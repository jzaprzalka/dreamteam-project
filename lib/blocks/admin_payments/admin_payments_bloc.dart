import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/repositories/authentication_repository.dart';
import 'package:dreamteam_project/repositories/groups_data_repository.dart';
import 'package:dreamteam_project/repositories/payments_repository.dart';
import 'package:dreamteam_project/repositories/user_data_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'admin_payments_bloc.freezed.dart';
part 'admin_payments_event.dart';
part 'admin_payments_state.dart';

class AdminPaymentsBloc extends Bloc<AdminPaymentsEvent, AdminPaymentsState> {
  PaymentsDataRepository paymentsDataRepository = PaymentsDataRepository();
  UserDataRepository userDataRepository = UserDataRepository();
  GroupsDataRepository groupsDataRepository = GroupsDataRepository();
  AuthenticationRepository authenticationRepository =
      AuthenticationRepository();

  AdminPaymentsBloc() : super(AdminPaymentsState.initial());

  Future<List<Payment>> _getUsersPayments(String userId,
      {bool history = false}) async {
    List<Payment> payments;
    try {
      if (history)
        payments = await paymentsDataRepository.getPaymentsFor(userId);
      else {
        var pAD =
            await paymentsDataRepository.getUserPaymentsApproachingDue(userId);
        var pPD = await paymentsDataRepository.getUserPaymentsPastDue(userId);
        pPD = pPD
            .where((p) => !pAD.map((ad) => ad.paymentId).contains(p.paymentId))
            .toList();
        payments = [...pAD, ...pPD];
      }
      logger.i("Fetched ${payments.length} payments for user $userId");
      for (Payment p in payments) {
        try {
          p.user = await userDataRepository.getUser(p.userId);
        } catch (e) {
          logger.e("Failed to load user for payment ${p.paymentId}: $e");
        }
      }
    } catch (e) {
      logger.e("Error occurred in loading payments: $e");
      return null;
    }
    return payments;
  }

  @override
  Stream<AdminPaymentsState> mapEventToState(AdminPaymentsEvent event) async* {
    try {
      yield* event.map(
        loadedPayments: (e) async* {
          try {
            var totalPaymentsPastDue =
                await paymentsDataRepository.getAllPaymentsPastDue();
            var totalPaymentsPaid =
                await paymentsDataRepository.getAllPaymentsPaid();
            var totalPaymentsApproachingDue =
                await paymentsDataRepository.getAllPaymentsApproachingDue();
            yield AdminPaymentsState.overallPaymentsStatusUpdated(
              paymentsPastDue: totalPaymentsPastDue.length,
              paymentsPaid: totalPaymentsPaid.length,
              paymentsApproachingDue: totalPaymentsApproachingDue.length,
            );
          } catch (e) {
            logger.e("Failed to load overall payments status: $e");
          }
          try {
            List<Group> groups = await groupsDataRepository.getAllGroups();
            yield AdminPaymentsState.groupListUpdated(groups: groups);
            for (Group g in groups) {
              try {
                var paymentsPastDue =
                    await paymentsDataRepository.getGroupsPaymentsPastDue([g]);
                var paymentsPaid =
                    await paymentsDataRepository.getGroupsPaymentsPaid([g]);
                yield AdminPaymentsState.groupPaymentsStatusUpdated(
                  groupId: g.groupId,
                  paymentsPastDue: paymentsPastDue.length,
                  paymentsPaid: paymentsPaid.length,
                );
              } catch (e) {
                logger.e(
                  "Failed to load payments status for group [${g.groupId}]",
                  e,
                );
              }
            }
          } catch (exp) {
            logger.e(
              "Failed to load group list: ",
              exp,
            );
          }
        },
        loadedGroups: (e) async* {
          try {
            List<Group> groups = await groupsDataRepository.getAllGroups();
            yield AdminPaymentsState.groupListUpdated(groups: groups);
          } catch (exp) {
            logger.e(
              "Failed to load group list: ",
              exp,
            );
          }
        },
        loadedUserPayments: (e) async* {
          yield AdminPaymentsState.userPaymentsUpdated(
            userId: e.userId,
            payments: await _getUsersPayments(e.userId),
          );
        },
        loadedUserHistoryPayments: (e) async* {
          yield AdminPaymentsState.userPaymentsHistoryUpdated(
            userId: e.userId,
            payments: await _getUsersPayments(e.userId, history: true),
          );
        },
        loadedGroupPayments: (e) async* {
          Group g = await groupsDataRepository.getGroup(e.groupId);
          g = await groupsDataRepository.loadParticipants(g);
          yield AdminPaymentsState.userListUpdated(
            groupId: e.groupId,
            users: g.participants,
          );
          for (String userId in g.participantIds) {
            try {
              var paymentsPastDue =
                  await paymentsDataRepository.getUserPaymentsPastDue(userId);
              var paymentsPaid =
                  await paymentsDataRepository.getUserPaymentsPaid(userId);
              yield AdminPaymentsState.userPaymentsStatusUpdated(
                userId: userId,
                paymentsPastDue: paymentsPastDue.length,
                paymentsPaid: paymentsPaid.length,
              );
            } catch (e) {
              logger.e("Failed to load payments status for user $userId: $e");
            }
          }
        },
        addedUserPayment: (e) async* {
          switch (e.repeatMode) {
            case RepeatMode.Monthly:
              await paymentsDataRepository.addMonthlyPayment(
                e.userIds,
                e.startDueDate,
                e.endDueDate ?? DateTime.now().add(Duration(days: 365)),
                e.paymentName,
                e.amount,
              );
              break;
            case RepeatMode.Weekly:
              await paymentsDataRepository.addWeeklyPayment(
                e.userIds,
                e.startDueDate,
                e.endDueDate ?? DateTime.now().add(Duration(days: 365)),
                e.paymentName,
                e.amount,
              );
              break;
            case RepeatMode.OneTime:
              await paymentsDataRepository.addUsersPayment(
                e.userIds,
                e.startDueDate,
                e.paymentName,
                e.amount,
              );
              break;
          }
          yield AdminPaymentsState.paymentsInvalid();
          yield AdminPaymentsState.paymentsStatusInvalid();
        },
        addedGroupPayment: (e) async* {
          switch (e.repeatMode) {
            case RepeatMode.Monthly:
              await paymentsDataRepository.addMonthlyGroupPayment(
                e.groupIds,
                e.startDueDate,
                e.endDueDate ?? DateTime.now().add(Duration(days: 365)),
                e.paymentName,
                e.amount,
              );
              break;
            case RepeatMode.Weekly:
              await paymentsDataRepository.addWeeklyGroupPayment(
                e.groupIds,
                e.startDueDate,
                e.endDueDate ?? DateTime.now().add(Duration(days: 365)),
                e.paymentName,
                e.amount,
              );
              break;
            case RepeatMode.OneTime:
              await paymentsDataRepository.addGroupsPayment(
                e.groupIds,
                e.startDueDate,
                e.paymentName,
                e.amount,
              );
              break;
          }
          yield AdminPaymentsState.paymentsInvalid();
          yield AdminPaymentsState.paymentsStatusInvalid();
        },
        updatedPayment: (e) async* {
          await paymentsDataRepository.updatePayment(
            e.paymentId,
            newDueDate: e.newDueDate,
            newName: e.newName,
            newAmount: e.newAmount,
            isAccounted: e.isAccounted,
          );
          yield AdminPaymentsState.paymentsStatusInvalid();
        },
        paymentAccounted: (e) async* {
          try {
            await paymentsDataRepository.accountPayment(
              e.payment,
              e.dateAccounted,
            );
            // yield AdminPaymentsState.paymentsInvalid();
            yield AdminPaymentsState.paymentsStatusInvalid();
          } catch (exp) {
            logger.e(
              "Accounting payment with id ${e.payment.paymentId} falied",
              exp,
            );
          }
        },
      );
    } catch (e) {
      logger.e("Error occurred in AdminPaymentsBloc: ");
      logger.e("\tMapping event to admin payments state failed: $e");
    }
  }
}
