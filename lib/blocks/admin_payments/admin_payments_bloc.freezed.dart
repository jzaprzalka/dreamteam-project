// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'admin_payments_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$AdminPaymentsEventTearOff {
  const _$AdminPaymentsEventTearOff();

// ignore: unused_element
  LoadedPayments loadedPayments() {
    return const LoadedPayments();
  }

// ignore: unused_element
  LoadedGroups loadedGroups() {
    return const LoadedGroups();
  }

// ignore: unused_element
  LoadedUserPayments loadedUserPayments({@required String userId}) {
    return LoadedUserPayments(
      userId: userId,
    );
  }

// ignore: unused_element
  LoadedUserHistoryPayments loadedUserHistoryPayments(
      {@required String userId}) {
    return LoadedUserHistoryPayments(
      userId: userId,
    );
  }

// ignore: unused_element
  LoadedGroupPayments loadedGroupPayments({@required String groupId}) {
    return LoadedGroupPayments(
      groupId: groupId,
    );
  }

// ignore: unused_element
  AddedUserPayment addedUserPayment(
      {@required List<String> userIds,
      @required DateTime startDueDate,
      DateTime endDueDate,
      @required String paymentName,
      @required int amount,
      @required RepeatMode repeatMode}) {
    return AddedUserPayment(
      userIds: userIds,
      startDueDate: startDueDate,
      endDueDate: endDueDate,
      paymentName: paymentName,
      amount: amount,
      repeatMode: repeatMode,
    );
  }

// ignore: unused_element
  AddedGroupPayment addedGroupPayment(
      {@required List<String> groupIds,
      @required DateTime startDueDate,
      DateTime endDueDate,
      @required String paymentName,
      @required int amount,
      @required RepeatMode repeatMode}) {
    return AddedGroupPayment(
      groupIds: groupIds,
      startDueDate: startDueDate,
      endDueDate: endDueDate,
      paymentName: paymentName,
      amount: amount,
      repeatMode: repeatMode,
    );
  }

// ignore: unused_element
  UpdatedPayment updatedPayment(
      {@required String paymentId,
      DateTime newDueDate,
      String newName,
      int newAmount,
      bool isAccounted}) {
    return UpdatedPayment(
      paymentId: paymentId,
      newDueDate: newDueDate,
      newName: newName,
      newAmount: newAmount,
      isAccounted: isAccounted,
    );
  }

// ignore: unused_element
  PaymentAccounted paymentAccounted(
      {@required Payment payment, @required DateTime dateAccounted}) {
    return PaymentAccounted(
      payment: payment,
      dateAccounted: dateAccounted,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $AdminPaymentsEvent = _$AdminPaymentsEventTearOff();

/// @nodoc
mixin _$AdminPaymentsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadedPayments(),
    @required TResult loadedGroups(),
    @required TResult loadedUserPayments(String userId),
    @required TResult loadedUserHistoryPayments(String userId),
    @required TResult loadedGroupPayments(String groupId),
    @required
        TResult addedUserPayment(
            List<String> userIds,
            DateTime startDueDate,
            DateTime endDueDate,
            String paymentName,
            int amount,
            RepeatMode repeatMode),
    @required
        TResult addedGroupPayment(
            List<String> groupIds,
            DateTime startDueDate,
            DateTime endDueDate,
            String paymentName,
            int amount,
            RepeatMode repeatMode),
    @required
        TResult updatedPayment(String paymentId, DateTime newDueDate,
            String newName, int newAmount, bool isAccounted),
    @required TResult paymentAccounted(Payment payment, DateTime dateAccounted),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadedPayments(),
    TResult loadedGroups(),
    TResult loadedUserPayments(String userId),
    TResult loadedUserHistoryPayments(String userId),
    TResult loadedGroupPayments(String groupId),
    TResult addedUserPayment(
        List<String> userIds,
        DateTime startDueDate,
        DateTime endDueDate,
        String paymentName,
        int amount,
        RepeatMode repeatMode),
    TResult addedGroupPayment(
        List<String> groupIds,
        DateTime startDueDate,
        DateTime endDueDate,
        String paymentName,
        int amount,
        RepeatMode repeatMode),
    TResult updatedPayment(String paymentId, DateTime newDueDate,
        String newName, int newAmount, bool isAccounted),
    TResult paymentAccounted(Payment payment, DateTime dateAccounted),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadedPayments(LoadedPayments value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUserPayments(LoadedUserPayments value),
    @required
        TResult loadedUserHistoryPayments(LoadedUserHistoryPayments value),
    @required TResult loadedGroupPayments(LoadedGroupPayments value),
    @required TResult addedUserPayment(AddedUserPayment value),
    @required TResult addedGroupPayment(AddedGroupPayment value),
    @required TResult updatedPayment(UpdatedPayment value),
    @required TResult paymentAccounted(PaymentAccounted value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadedPayments(LoadedPayments value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUserPayments(LoadedUserPayments value),
    TResult loadedUserHistoryPayments(LoadedUserHistoryPayments value),
    TResult loadedGroupPayments(LoadedGroupPayments value),
    TResult addedUserPayment(AddedUserPayment value),
    TResult addedGroupPayment(AddedGroupPayment value),
    TResult updatedPayment(UpdatedPayment value),
    TResult paymentAccounted(PaymentAccounted value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $AdminPaymentsEventCopyWith<$Res> {
  factory $AdminPaymentsEventCopyWith(
          AdminPaymentsEvent value, $Res Function(AdminPaymentsEvent) then) =
      _$AdminPaymentsEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$AdminPaymentsEventCopyWithImpl<$Res>
    implements $AdminPaymentsEventCopyWith<$Res> {
  _$AdminPaymentsEventCopyWithImpl(this._value, this._then);

  final AdminPaymentsEvent _value;
  // ignore: unused_field
  final $Res Function(AdminPaymentsEvent) _then;
}

/// @nodoc
abstract class $LoadedPaymentsCopyWith<$Res> {
  factory $LoadedPaymentsCopyWith(
          LoadedPayments value, $Res Function(LoadedPayments) then) =
      _$LoadedPaymentsCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoadedPaymentsCopyWithImpl<$Res>
    extends _$AdminPaymentsEventCopyWithImpl<$Res>
    implements $LoadedPaymentsCopyWith<$Res> {
  _$LoadedPaymentsCopyWithImpl(
      LoadedPayments _value, $Res Function(LoadedPayments) _then)
      : super(_value, (v) => _then(v as LoadedPayments));

  @override
  LoadedPayments get _value => super._value as LoadedPayments;
}

/// @nodoc
class _$LoadedPayments implements LoadedPayments {
  const _$LoadedPayments();

  @override
  String toString() {
    return 'AdminPaymentsEvent.loadedPayments()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoadedPayments);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadedPayments(),
    @required TResult loadedGroups(),
    @required TResult loadedUserPayments(String userId),
    @required TResult loadedUserHistoryPayments(String userId),
    @required TResult loadedGroupPayments(String groupId),
    @required
        TResult addedUserPayment(
            List<String> userIds,
            DateTime startDueDate,
            DateTime endDueDate,
            String paymentName,
            int amount,
            RepeatMode repeatMode),
    @required
        TResult addedGroupPayment(
            List<String> groupIds,
            DateTime startDueDate,
            DateTime endDueDate,
            String paymentName,
            int amount,
            RepeatMode repeatMode),
    @required
        TResult updatedPayment(String paymentId, DateTime newDueDate,
            String newName, int newAmount, bool isAccounted),
    @required TResult paymentAccounted(Payment payment, DateTime dateAccounted),
  }) {
    assert(loadedPayments != null);
    assert(loadedGroups != null);
    assert(loadedUserPayments != null);
    assert(loadedUserHistoryPayments != null);
    assert(loadedGroupPayments != null);
    assert(addedUserPayment != null);
    assert(addedGroupPayment != null);
    assert(updatedPayment != null);
    assert(paymentAccounted != null);
    return loadedPayments();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadedPayments(),
    TResult loadedGroups(),
    TResult loadedUserPayments(String userId),
    TResult loadedUserHistoryPayments(String userId),
    TResult loadedGroupPayments(String groupId),
    TResult addedUserPayment(
        List<String> userIds,
        DateTime startDueDate,
        DateTime endDueDate,
        String paymentName,
        int amount,
        RepeatMode repeatMode),
    TResult addedGroupPayment(
        List<String> groupIds,
        DateTime startDueDate,
        DateTime endDueDate,
        String paymentName,
        int amount,
        RepeatMode repeatMode),
    TResult updatedPayment(String paymentId, DateTime newDueDate,
        String newName, int newAmount, bool isAccounted),
    TResult paymentAccounted(Payment payment, DateTime dateAccounted),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedPayments != null) {
      return loadedPayments();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadedPayments(LoadedPayments value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUserPayments(LoadedUserPayments value),
    @required
        TResult loadedUserHistoryPayments(LoadedUserHistoryPayments value),
    @required TResult loadedGroupPayments(LoadedGroupPayments value),
    @required TResult addedUserPayment(AddedUserPayment value),
    @required TResult addedGroupPayment(AddedGroupPayment value),
    @required TResult updatedPayment(UpdatedPayment value),
    @required TResult paymentAccounted(PaymentAccounted value),
  }) {
    assert(loadedPayments != null);
    assert(loadedGroups != null);
    assert(loadedUserPayments != null);
    assert(loadedUserHistoryPayments != null);
    assert(loadedGroupPayments != null);
    assert(addedUserPayment != null);
    assert(addedGroupPayment != null);
    assert(updatedPayment != null);
    assert(paymentAccounted != null);
    return loadedPayments(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadedPayments(LoadedPayments value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUserPayments(LoadedUserPayments value),
    TResult loadedUserHistoryPayments(LoadedUserHistoryPayments value),
    TResult loadedGroupPayments(LoadedGroupPayments value),
    TResult addedUserPayment(AddedUserPayment value),
    TResult addedGroupPayment(AddedGroupPayment value),
    TResult updatedPayment(UpdatedPayment value),
    TResult paymentAccounted(PaymentAccounted value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedPayments != null) {
      return loadedPayments(this);
    }
    return orElse();
  }
}

abstract class LoadedPayments implements AdminPaymentsEvent {
  const factory LoadedPayments() = _$LoadedPayments;
}

/// @nodoc
abstract class $LoadedGroupsCopyWith<$Res> {
  factory $LoadedGroupsCopyWith(
          LoadedGroups value, $Res Function(LoadedGroups) then) =
      _$LoadedGroupsCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoadedGroupsCopyWithImpl<$Res>
    extends _$AdminPaymentsEventCopyWithImpl<$Res>
    implements $LoadedGroupsCopyWith<$Res> {
  _$LoadedGroupsCopyWithImpl(
      LoadedGroups _value, $Res Function(LoadedGroups) _then)
      : super(_value, (v) => _then(v as LoadedGroups));

  @override
  LoadedGroups get _value => super._value as LoadedGroups;
}

/// @nodoc
class _$LoadedGroups implements LoadedGroups {
  const _$LoadedGroups();

  @override
  String toString() {
    return 'AdminPaymentsEvent.loadedGroups()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoadedGroups);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadedPayments(),
    @required TResult loadedGroups(),
    @required TResult loadedUserPayments(String userId),
    @required TResult loadedUserHistoryPayments(String userId),
    @required TResult loadedGroupPayments(String groupId),
    @required
        TResult addedUserPayment(
            List<String> userIds,
            DateTime startDueDate,
            DateTime endDueDate,
            String paymentName,
            int amount,
            RepeatMode repeatMode),
    @required
        TResult addedGroupPayment(
            List<String> groupIds,
            DateTime startDueDate,
            DateTime endDueDate,
            String paymentName,
            int amount,
            RepeatMode repeatMode),
    @required
        TResult updatedPayment(String paymentId, DateTime newDueDate,
            String newName, int newAmount, bool isAccounted),
    @required TResult paymentAccounted(Payment payment, DateTime dateAccounted),
  }) {
    assert(loadedPayments != null);
    assert(loadedGroups != null);
    assert(loadedUserPayments != null);
    assert(loadedUserHistoryPayments != null);
    assert(loadedGroupPayments != null);
    assert(addedUserPayment != null);
    assert(addedGroupPayment != null);
    assert(updatedPayment != null);
    assert(paymentAccounted != null);
    return loadedGroups();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadedPayments(),
    TResult loadedGroups(),
    TResult loadedUserPayments(String userId),
    TResult loadedUserHistoryPayments(String userId),
    TResult loadedGroupPayments(String groupId),
    TResult addedUserPayment(
        List<String> userIds,
        DateTime startDueDate,
        DateTime endDueDate,
        String paymentName,
        int amount,
        RepeatMode repeatMode),
    TResult addedGroupPayment(
        List<String> groupIds,
        DateTime startDueDate,
        DateTime endDueDate,
        String paymentName,
        int amount,
        RepeatMode repeatMode),
    TResult updatedPayment(String paymentId, DateTime newDueDate,
        String newName, int newAmount, bool isAccounted),
    TResult paymentAccounted(Payment payment, DateTime dateAccounted),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedGroups != null) {
      return loadedGroups();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadedPayments(LoadedPayments value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUserPayments(LoadedUserPayments value),
    @required
        TResult loadedUserHistoryPayments(LoadedUserHistoryPayments value),
    @required TResult loadedGroupPayments(LoadedGroupPayments value),
    @required TResult addedUserPayment(AddedUserPayment value),
    @required TResult addedGroupPayment(AddedGroupPayment value),
    @required TResult updatedPayment(UpdatedPayment value),
    @required TResult paymentAccounted(PaymentAccounted value),
  }) {
    assert(loadedPayments != null);
    assert(loadedGroups != null);
    assert(loadedUserPayments != null);
    assert(loadedUserHistoryPayments != null);
    assert(loadedGroupPayments != null);
    assert(addedUserPayment != null);
    assert(addedGroupPayment != null);
    assert(updatedPayment != null);
    assert(paymentAccounted != null);
    return loadedGroups(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadedPayments(LoadedPayments value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUserPayments(LoadedUserPayments value),
    TResult loadedUserHistoryPayments(LoadedUserHistoryPayments value),
    TResult loadedGroupPayments(LoadedGroupPayments value),
    TResult addedUserPayment(AddedUserPayment value),
    TResult addedGroupPayment(AddedGroupPayment value),
    TResult updatedPayment(UpdatedPayment value),
    TResult paymentAccounted(PaymentAccounted value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedGroups != null) {
      return loadedGroups(this);
    }
    return orElse();
  }
}

abstract class LoadedGroups implements AdminPaymentsEvent {
  const factory LoadedGroups() = _$LoadedGroups;
}

/// @nodoc
abstract class $LoadedUserPaymentsCopyWith<$Res> {
  factory $LoadedUserPaymentsCopyWith(
          LoadedUserPayments value, $Res Function(LoadedUserPayments) then) =
      _$LoadedUserPaymentsCopyWithImpl<$Res>;
  $Res call({String userId});
}

/// @nodoc
class _$LoadedUserPaymentsCopyWithImpl<$Res>
    extends _$AdminPaymentsEventCopyWithImpl<$Res>
    implements $LoadedUserPaymentsCopyWith<$Res> {
  _$LoadedUserPaymentsCopyWithImpl(
      LoadedUserPayments _value, $Res Function(LoadedUserPayments) _then)
      : super(_value, (v) => _then(v as LoadedUserPayments));

  @override
  LoadedUserPayments get _value => super._value as LoadedUserPayments;

  @override
  $Res call({
    Object userId = freezed,
  }) {
    return _then(LoadedUserPayments(
      userId: userId == freezed ? _value.userId : userId as String,
    ));
  }
}

/// @nodoc
class _$LoadedUserPayments implements LoadedUserPayments {
  const _$LoadedUserPayments({@required this.userId}) : assert(userId != null);

  @override
  final String userId;

  @override
  String toString() {
    return 'AdminPaymentsEvent.loadedUserPayments(userId: $userId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoadedUserPayments &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(userId);

  @JsonKey(ignore: true)
  @override
  $LoadedUserPaymentsCopyWith<LoadedUserPayments> get copyWith =>
      _$LoadedUserPaymentsCopyWithImpl<LoadedUserPayments>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadedPayments(),
    @required TResult loadedGroups(),
    @required TResult loadedUserPayments(String userId),
    @required TResult loadedUserHistoryPayments(String userId),
    @required TResult loadedGroupPayments(String groupId),
    @required
        TResult addedUserPayment(
            List<String> userIds,
            DateTime startDueDate,
            DateTime endDueDate,
            String paymentName,
            int amount,
            RepeatMode repeatMode),
    @required
        TResult addedGroupPayment(
            List<String> groupIds,
            DateTime startDueDate,
            DateTime endDueDate,
            String paymentName,
            int amount,
            RepeatMode repeatMode),
    @required
        TResult updatedPayment(String paymentId, DateTime newDueDate,
            String newName, int newAmount, bool isAccounted),
    @required TResult paymentAccounted(Payment payment, DateTime dateAccounted),
  }) {
    assert(loadedPayments != null);
    assert(loadedGroups != null);
    assert(loadedUserPayments != null);
    assert(loadedUserHistoryPayments != null);
    assert(loadedGroupPayments != null);
    assert(addedUserPayment != null);
    assert(addedGroupPayment != null);
    assert(updatedPayment != null);
    assert(paymentAccounted != null);
    return loadedUserPayments(userId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadedPayments(),
    TResult loadedGroups(),
    TResult loadedUserPayments(String userId),
    TResult loadedUserHistoryPayments(String userId),
    TResult loadedGroupPayments(String groupId),
    TResult addedUserPayment(
        List<String> userIds,
        DateTime startDueDate,
        DateTime endDueDate,
        String paymentName,
        int amount,
        RepeatMode repeatMode),
    TResult addedGroupPayment(
        List<String> groupIds,
        DateTime startDueDate,
        DateTime endDueDate,
        String paymentName,
        int amount,
        RepeatMode repeatMode),
    TResult updatedPayment(String paymentId, DateTime newDueDate,
        String newName, int newAmount, bool isAccounted),
    TResult paymentAccounted(Payment payment, DateTime dateAccounted),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedUserPayments != null) {
      return loadedUserPayments(userId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadedPayments(LoadedPayments value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUserPayments(LoadedUserPayments value),
    @required
        TResult loadedUserHistoryPayments(LoadedUserHistoryPayments value),
    @required TResult loadedGroupPayments(LoadedGroupPayments value),
    @required TResult addedUserPayment(AddedUserPayment value),
    @required TResult addedGroupPayment(AddedGroupPayment value),
    @required TResult updatedPayment(UpdatedPayment value),
    @required TResult paymentAccounted(PaymentAccounted value),
  }) {
    assert(loadedPayments != null);
    assert(loadedGroups != null);
    assert(loadedUserPayments != null);
    assert(loadedUserHistoryPayments != null);
    assert(loadedGroupPayments != null);
    assert(addedUserPayment != null);
    assert(addedGroupPayment != null);
    assert(updatedPayment != null);
    assert(paymentAccounted != null);
    return loadedUserPayments(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadedPayments(LoadedPayments value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUserPayments(LoadedUserPayments value),
    TResult loadedUserHistoryPayments(LoadedUserHistoryPayments value),
    TResult loadedGroupPayments(LoadedGroupPayments value),
    TResult addedUserPayment(AddedUserPayment value),
    TResult addedGroupPayment(AddedGroupPayment value),
    TResult updatedPayment(UpdatedPayment value),
    TResult paymentAccounted(PaymentAccounted value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedUserPayments != null) {
      return loadedUserPayments(this);
    }
    return orElse();
  }
}

abstract class LoadedUserPayments implements AdminPaymentsEvent {
  const factory LoadedUserPayments({@required String userId}) =
      _$LoadedUserPayments;

  String get userId;
  @JsonKey(ignore: true)
  $LoadedUserPaymentsCopyWith<LoadedUserPayments> get copyWith;
}

/// @nodoc
abstract class $LoadedUserHistoryPaymentsCopyWith<$Res> {
  factory $LoadedUserHistoryPaymentsCopyWith(LoadedUserHistoryPayments value,
          $Res Function(LoadedUserHistoryPayments) then) =
      _$LoadedUserHistoryPaymentsCopyWithImpl<$Res>;
  $Res call({String userId});
}

/// @nodoc
class _$LoadedUserHistoryPaymentsCopyWithImpl<$Res>
    extends _$AdminPaymentsEventCopyWithImpl<$Res>
    implements $LoadedUserHistoryPaymentsCopyWith<$Res> {
  _$LoadedUserHistoryPaymentsCopyWithImpl(LoadedUserHistoryPayments _value,
      $Res Function(LoadedUserHistoryPayments) _then)
      : super(_value, (v) => _then(v as LoadedUserHistoryPayments));

  @override
  LoadedUserHistoryPayments get _value =>
      super._value as LoadedUserHistoryPayments;

  @override
  $Res call({
    Object userId = freezed,
  }) {
    return _then(LoadedUserHistoryPayments(
      userId: userId == freezed ? _value.userId : userId as String,
    ));
  }
}

/// @nodoc
class _$LoadedUserHistoryPayments implements LoadedUserHistoryPayments {
  const _$LoadedUserHistoryPayments({@required this.userId})
      : assert(userId != null);

  @override
  final String userId;

  @override
  String toString() {
    return 'AdminPaymentsEvent.loadedUserHistoryPayments(userId: $userId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoadedUserHistoryPayments &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(userId);

  @JsonKey(ignore: true)
  @override
  $LoadedUserHistoryPaymentsCopyWith<LoadedUserHistoryPayments> get copyWith =>
      _$LoadedUserHistoryPaymentsCopyWithImpl<LoadedUserHistoryPayments>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadedPayments(),
    @required TResult loadedGroups(),
    @required TResult loadedUserPayments(String userId),
    @required TResult loadedUserHistoryPayments(String userId),
    @required TResult loadedGroupPayments(String groupId),
    @required
        TResult addedUserPayment(
            List<String> userIds,
            DateTime startDueDate,
            DateTime endDueDate,
            String paymentName,
            int amount,
            RepeatMode repeatMode),
    @required
        TResult addedGroupPayment(
            List<String> groupIds,
            DateTime startDueDate,
            DateTime endDueDate,
            String paymentName,
            int amount,
            RepeatMode repeatMode),
    @required
        TResult updatedPayment(String paymentId, DateTime newDueDate,
            String newName, int newAmount, bool isAccounted),
    @required TResult paymentAccounted(Payment payment, DateTime dateAccounted),
  }) {
    assert(loadedPayments != null);
    assert(loadedGroups != null);
    assert(loadedUserPayments != null);
    assert(loadedUserHistoryPayments != null);
    assert(loadedGroupPayments != null);
    assert(addedUserPayment != null);
    assert(addedGroupPayment != null);
    assert(updatedPayment != null);
    assert(paymentAccounted != null);
    return loadedUserHistoryPayments(userId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadedPayments(),
    TResult loadedGroups(),
    TResult loadedUserPayments(String userId),
    TResult loadedUserHistoryPayments(String userId),
    TResult loadedGroupPayments(String groupId),
    TResult addedUserPayment(
        List<String> userIds,
        DateTime startDueDate,
        DateTime endDueDate,
        String paymentName,
        int amount,
        RepeatMode repeatMode),
    TResult addedGroupPayment(
        List<String> groupIds,
        DateTime startDueDate,
        DateTime endDueDate,
        String paymentName,
        int amount,
        RepeatMode repeatMode),
    TResult updatedPayment(String paymentId, DateTime newDueDate,
        String newName, int newAmount, bool isAccounted),
    TResult paymentAccounted(Payment payment, DateTime dateAccounted),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedUserHistoryPayments != null) {
      return loadedUserHistoryPayments(userId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadedPayments(LoadedPayments value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUserPayments(LoadedUserPayments value),
    @required
        TResult loadedUserHistoryPayments(LoadedUserHistoryPayments value),
    @required TResult loadedGroupPayments(LoadedGroupPayments value),
    @required TResult addedUserPayment(AddedUserPayment value),
    @required TResult addedGroupPayment(AddedGroupPayment value),
    @required TResult updatedPayment(UpdatedPayment value),
    @required TResult paymentAccounted(PaymentAccounted value),
  }) {
    assert(loadedPayments != null);
    assert(loadedGroups != null);
    assert(loadedUserPayments != null);
    assert(loadedUserHistoryPayments != null);
    assert(loadedGroupPayments != null);
    assert(addedUserPayment != null);
    assert(addedGroupPayment != null);
    assert(updatedPayment != null);
    assert(paymentAccounted != null);
    return loadedUserHistoryPayments(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadedPayments(LoadedPayments value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUserPayments(LoadedUserPayments value),
    TResult loadedUserHistoryPayments(LoadedUserHistoryPayments value),
    TResult loadedGroupPayments(LoadedGroupPayments value),
    TResult addedUserPayment(AddedUserPayment value),
    TResult addedGroupPayment(AddedGroupPayment value),
    TResult updatedPayment(UpdatedPayment value),
    TResult paymentAccounted(PaymentAccounted value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedUserHistoryPayments != null) {
      return loadedUserHistoryPayments(this);
    }
    return orElse();
  }
}

abstract class LoadedUserHistoryPayments implements AdminPaymentsEvent {
  const factory LoadedUserHistoryPayments({@required String userId}) =
      _$LoadedUserHistoryPayments;

  String get userId;
  @JsonKey(ignore: true)
  $LoadedUserHistoryPaymentsCopyWith<LoadedUserHistoryPayments> get copyWith;
}

/// @nodoc
abstract class $LoadedGroupPaymentsCopyWith<$Res> {
  factory $LoadedGroupPaymentsCopyWith(
          LoadedGroupPayments value, $Res Function(LoadedGroupPayments) then) =
      _$LoadedGroupPaymentsCopyWithImpl<$Res>;
  $Res call({String groupId});
}

/// @nodoc
class _$LoadedGroupPaymentsCopyWithImpl<$Res>
    extends _$AdminPaymentsEventCopyWithImpl<$Res>
    implements $LoadedGroupPaymentsCopyWith<$Res> {
  _$LoadedGroupPaymentsCopyWithImpl(
      LoadedGroupPayments _value, $Res Function(LoadedGroupPayments) _then)
      : super(_value, (v) => _then(v as LoadedGroupPayments));

  @override
  LoadedGroupPayments get _value => super._value as LoadedGroupPayments;

  @override
  $Res call({
    Object groupId = freezed,
  }) {
    return _then(LoadedGroupPayments(
      groupId: groupId == freezed ? _value.groupId : groupId as String,
    ));
  }
}

/// @nodoc
class _$LoadedGroupPayments implements LoadedGroupPayments {
  const _$LoadedGroupPayments({@required this.groupId})
      : assert(groupId != null);

  @override
  final String groupId;

  @override
  String toString() {
    return 'AdminPaymentsEvent.loadedGroupPayments(groupId: $groupId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoadedGroupPayments &&
            (identical(other.groupId, groupId) ||
                const DeepCollectionEquality().equals(other.groupId, groupId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(groupId);

  @JsonKey(ignore: true)
  @override
  $LoadedGroupPaymentsCopyWith<LoadedGroupPayments> get copyWith =>
      _$LoadedGroupPaymentsCopyWithImpl<LoadedGroupPayments>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadedPayments(),
    @required TResult loadedGroups(),
    @required TResult loadedUserPayments(String userId),
    @required TResult loadedUserHistoryPayments(String userId),
    @required TResult loadedGroupPayments(String groupId),
    @required
        TResult addedUserPayment(
            List<String> userIds,
            DateTime startDueDate,
            DateTime endDueDate,
            String paymentName,
            int amount,
            RepeatMode repeatMode),
    @required
        TResult addedGroupPayment(
            List<String> groupIds,
            DateTime startDueDate,
            DateTime endDueDate,
            String paymentName,
            int amount,
            RepeatMode repeatMode),
    @required
        TResult updatedPayment(String paymentId, DateTime newDueDate,
            String newName, int newAmount, bool isAccounted),
    @required TResult paymentAccounted(Payment payment, DateTime dateAccounted),
  }) {
    assert(loadedPayments != null);
    assert(loadedGroups != null);
    assert(loadedUserPayments != null);
    assert(loadedUserHistoryPayments != null);
    assert(loadedGroupPayments != null);
    assert(addedUserPayment != null);
    assert(addedGroupPayment != null);
    assert(updatedPayment != null);
    assert(paymentAccounted != null);
    return loadedGroupPayments(groupId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadedPayments(),
    TResult loadedGroups(),
    TResult loadedUserPayments(String userId),
    TResult loadedUserHistoryPayments(String userId),
    TResult loadedGroupPayments(String groupId),
    TResult addedUserPayment(
        List<String> userIds,
        DateTime startDueDate,
        DateTime endDueDate,
        String paymentName,
        int amount,
        RepeatMode repeatMode),
    TResult addedGroupPayment(
        List<String> groupIds,
        DateTime startDueDate,
        DateTime endDueDate,
        String paymentName,
        int amount,
        RepeatMode repeatMode),
    TResult updatedPayment(String paymentId, DateTime newDueDate,
        String newName, int newAmount, bool isAccounted),
    TResult paymentAccounted(Payment payment, DateTime dateAccounted),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedGroupPayments != null) {
      return loadedGroupPayments(groupId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadedPayments(LoadedPayments value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUserPayments(LoadedUserPayments value),
    @required
        TResult loadedUserHistoryPayments(LoadedUserHistoryPayments value),
    @required TResult loadedGroupPayments(LoadedGroupPayments value),
    @required TResult addedUserPayment(AddedUserPayment value),
    @required TResult addedGroupPayment(AddedGroupPayment value),
    @required TResult updatedPayment(UpdatedPayment value),
    @required TResult paymentAccounted(PaymentAccounted value),
  }) {
    assert(loadedPayments != null);
    assert(loadedGroups != null);
    assert(loadedUserPayments != null);
    assert(loadedUserHistoryPayments != null);
    assert(loadedGroupPayments != null);
    assert(addedUserPayment != null);
    assert(addedGroupPayment != null);
    assert(updatedPayment != null);
    assert(paymentAccounted != null);
    return loadedGroupPayments(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadedPayments(LoadedPayments value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUserPayments(LoadedUserPayments value),
    TResult loadedUserHistoryPayments(LoadedUserHistoryPayments value),
    TResult loadedGroupPayments(LoadedGroupPayments value),
    TResult addedUserPayment(AddedUserPayment value),
    TResult addedGroupPayment(AddedGroupPayment value),
    TResult updatedPayment(UpdatedPayment value),
    TResult paymentAccounted(PaymentAccounted value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedGroupPayments != null) {
      return loadedGroupPayments(this);
    }
    return orElse();
  }
}

abstract class LoadedGroupPayments implements AdminPaymentsEvent {
  const factory LoadedGroupPayments({@required String groupId}) =
      _$LoadedGroupPayments;

  String get groupId;
  @JsonKey(ignore: true)
  $LoadedGroupPaymentsCopyWith<LoadedGroupPayments> get copyWith;
}

/// @nodoc
abstract class $AddedUserPaymentCopyWith<$Res> {
  factory $AddedUserPaymentCopyWith(
          AddedUserPayment value, $Res Function(AddedUserPayment) then) =
      _$AddedUserPaymentCopyWithImpl<$Res>;
  $Res call(
      {List<String> userIds,
      DateTime startDueDate,
      DateTime endDueDate,
      String paymentName,
      int amount,
      RepeatMode repeatMode});
}

/// @nodoc
class _$AddedUserPaymentCopyWithImpl<$Res>
    extends _$AdminPaymentsEventCopyWithImpl<$Res>
    implements $AddedUserPaymentCopyWith<$Res> {
  _$AddedUserPaymentCopyWithImpl(
      AddedUserPayment _value, $Res Function(AddedUserPayment) _then)
      : super(_value, (v) => _then(v as AddedUserPayment));

  @override
  AddedUserPayment get _value => super._value as AddedUserPayment;

  @override
  $Res call({
    Object userIds = freezed,
    Object startDueDate = freezed,
    Object endDueDate = freezed,
    Object paymentName = freezed,
    Object amount = freezed,
    Object repeatMode = freezed,
  }) {
    return _then(AddedUserPayment(
      userIds: userIds == freezed ? _value.userIds : userIds as List<String>,
      startDueDate: startDueDate == freezed
          ? _value.startDueDate
          : startDueDate as DateTime,
      endDueDate:
          endDueDate == freezed ? _value.endDueDate : endDueDate as DateTime,
      paymentName:
          paymentName == freezed ? _value.paymentName : paymentName as String,
      amount: amount == freezed ? _value.amount : amount as int,
      repeatMode:
          repeatMode == freezed ? _value.repeatMode : repeatMode as RepeatMode,
    ));
  }
}

/// @nodoc
class _$AddedUserPayment implements AddedUserPayment {
  const _$AddedUserPayment(
      {@required this.userIds,
      @required this.startDueDate,
      this.endDueDate,
      @required this.paymentName,
      @required this.amount,
      @required this.repeatMode})
      : assert(userIds != null),
        assert(startDueDate != null),
        assert(paymentName != null),
        assert(amount != null),
        assert(repeatMode != null);

  @override
  final List<String> userIds;
  @override
  final DateTime startDueDate;
  @override
  final DateTime endDueDate;
  @override
  final String paymentName;
  @override
  final int amount;
  @override
  final RepeatMode repeatMode;

  @override
  String toString() {
    return 'AdminPaymentsEvent.addedUserPayment(userIds: $userIds, startDueDate: $startDueDate, endDueDate: $endDueDate, paymentName: $paymentName, amount: $amount, repeatMode: $repeatMode)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AddedUserPayment &&
            (identical(other.userIds, userIds) ||
                const DeepCollectionEquality()
                    .equals(other.userIds, userIds)) &&
            (identical(other.startDueDate, startDueDate) ||
                const DeepCollectionEquality()
                    .equals(other.startDueDate, startDueDate)) &&
            (identical(other.endDueDate, endDueDate) ||
                const DeepCollectionEquality()
                    .equals(other.endDueDate, endDueDate)) &&
            (identical(other.paymentName, paymentName) ||
                const DeepCollectionEquality()
                    .equals(other.paymentName, paymentName)) &&
            (identical(other.amount, amount) ||
                const DeepCollectionEquality().equals(other.amount, amount)) &&
            (identical(other.repeatMode, repeatMode) ||
                const DeepCollectionEquality()
                    .equals(other.repeatMode, repeatMode)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(userIds) ^
      const DeepCollectionEquality().hash(startDueDate) ^
      const DeepCollectionEquality().hash(endDueDate) ^
      const DeepCollectionEquality().hash(paymentName) ^
      const DeepCollectionEquality().hash(amount) ^
      const DeepCollectionEquality().hash(repeatMode);

  @JsonKey(ignore: true)
  @override
  $AddedUserPaymentCopyWith<AddedUserPayment> get copyWith =>
      _$AddedUserPaymentCopyWithImpl<AddedUserPayment>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadedPayments(),
    @required TResult loadedGroups(),
    @required TResult loadedUserPayments(String userId),
    @required TResult loadedUserHistoryPayments(String userId),
    @required TResult loadedGroupPayments(String groupId),
    @required
        TResult addedUserPayment(
            List<String> userIds,
            DateTime startDueDate,
            DateTime endDueDate,
            String paymentName,
            int amount,
            RepeatMode repeatMode),
    @required
        TResult addedGroupPayment(
            List<String> groupIds,
            DateTime startDueDate,
            DateTime endDueDate,
            String paymentName,
            int amount,
            RepeatMode repeatMode),
    @required
        TResult updatedPayment(String paymentId, DateTime newDueDate,
            String newName, int newAmount, bool isAccounted),
    @required TResult paymentAccounted(Payment payment, DateTime dateAccounted),
  }) {
    assert(loadedPayments != null);
    assert(loadedGroups != null);
    assert(loadedUserPayments != null);
    assert(loadedUserHistoryPayments != null);
    assert(loadedGroupPayments != null);
    assert(addedUserPayment != null);
    assert(addedGroupPayment != null);
    assert(updatedPayment != null);
    assert(paymentAccounted != null);
    return addedUserPayment(
        userIds, startDueDate, endDueDate, paymentName, amount, repeatMode);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadedPayments(),
    TResult loadedGroups(),
    TResult loadedUserPayments(String userId),
    TResult loadedUserHistoryPayments(String userId),
    TResult loadedGroupPayments(String groupId),
    TResult addedUserPayment(
        List<String> userIds,
        DateTime startDueDate,
        DateTime endDueDate,
        String paymentName,
        int amount,
        RepeatMode repeatMode),
    TResult addedGroupPayment(
        List<String> groupIds,
        DateTime startDueDate,
        DateTime endDueDate,
        String paymentName,
        int amount,
        RepeatMode repeatMode),
    TResult updatedPayment(String paymentId, DateTime newDueDate,
        String newName, int newAmount, bool isAccounted),
    TResult paymentAccounted(Payment payment, DateTime dateAccounted),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addedUserPayment != null) {
      return addedUserPayment(
          userIds, startDueDate, endDueDate, paymentName, amount, repeatMode);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadedPayments(LoadedPayments value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUserPayments(LoadedUserPayments value),
    @required
        TResult loadedUserHistoryPayments(LoadedUserHistoryPayments value),
    @required TResult loadedGroupPayments(LoadedGroupPayments value),
    @required TResult addedUserPayment(AddedUserPayment value),
    @required TResult addedGroupPayment(AddedGroupPayment value),
    @required TResult updatedPayment(UpdatedPayment value),
    @required TResult paymentAccounted(PaymentAccounted value),
  }) {
    assert(loadedPayments != null);
    assert(loadedGroups != null);
    assert(loadedUserPayments != null);
    assert(loadedUserHistoryPayments != null);
    assert(loadedGroupPayments != null);
    assert(addedUserPayment != null);
    assert(addedGroupPayment != null);
    assert(updatedPayment != null);
    assert(paymentAccounted != null);
    return addedUserPayment(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadedPayments(LoadedPayments value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUserPayments(LoadedUserPayments value),
    TResult loadedUserHistoryPayments(LoadedUserHistoryPayments value),
    TResult loadedGroupPayments(LoadedGroupPayments value),
    TResult addedUserPayment(AddedUserPayment value),
    TResult addedGroupPayment(AddedGroupPayment value),
    TResult updatedPayment(UpdatedPayment value),
    TResult paymentAccounted(PaymentAccounted value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addedUserPayment != null) {
      return addedUserPayment(this);
    }
    return orElse();
  }
}

abstract class AddedUserPayment implements AdminPaymentsEvent {
  const factory AddedUserPayment(
      {@required List<String> userIds,
      @required DateTime startDueDate,
      DateTime endDueDate,
      @required String paymentName,
      @required int amount,
      @required RepeatMode repeatMode}) = _$AddedUserPayment;

  List<String> get userIds;
  DateTime get startDueDate;
  DateTime get endDueDate;
  String get paymentName;
  int get amount;
  RepeatMode get repeatMode;
  @JsonKey(ignore: true)
  $AddedUserPaymentCopyWith<AddedUserPayment> get copyWith;
}

/// @nodoc
abstract class $AddedGroupPaymentCopyWith<$Res> {
  factory $AddedGroupPaymentCopyWith(
          AddedGroupPayment value, $Res Function(AddedGroupPayment) then) =
      _$AddedGroupPaymentCopyWithImpl<$Res>;
  $Res call(
      {List<String> groupIds,
      DateTime startDueDate,
      DateTime endDueDate,
      String paymentName,
      int amount,
      RepeatMode repeatMode});
}

/// @nodoc
class _$AddedGroupPaymentCopyWithImpl<$Res>
    extends _$AdminPaymentsEventCopyWithImpl<$Res>
    implements $AddedGroupPaymentCopyWith<$Res> {
  _$AddedGroupPaymentCopyWithImpl(
      AddedGroupPayment _value, $Res Function(AddedGroupPayment) _then)
      : super(_value, (v) => _then(v as AddedGroupPayment));

  @override
  AddedGroupPayment get _value => super._value as AddedGroupPayment;

  @override
  $Res call({
    Object groupIds = freezed,
    Object startDueDate = freezed,
    Object endDueDate = freezed,
    Object paymentName = freezed,
    Object amount = freezed,
    Object repeatMode = freezed,
  }) {
    return _then(AddedGroupPayment(
      groupIds:
          groupIds == freezed ? _value.groupIds : groupIds as List<String>,
      startDueDate: startDueDate == freezed
          ? _value.startDueDate
          : startDueDate as DateTime,
      endDueDate:
          endDueDate == freezed ? _value.endDueDate : endDueDate as DateTime,
      paymentName:
          paymentName == freezed ? _value.paymentName : paymentName as String,
      amount: amount == freezed ? _value.amount : amount as int,
      repeatMode:
          repeatMode == freezed ? _value.repeatMode : repeatMode as RepeatMode,
    ));
  }
}

/// @nodoc
class _$AddedGroupPayment implements AddedGroupPayment {
  const _$AddedGroupPayment(
      {@required this.groupIds,
      @required this.startDueDate,
      this.endDueDate,
      @required this.paymentName,
      @required this.amount,
      @required this.repeatMode})
      : assert(groupIds != null),
        assert(startDueDate != null),
        assert(paymentName != null),
        assert(amount != null),
        assert(repeatMode != null);

  @override
  final List<String> groupIds;
  @override
  final DateTime startDueDate;
  @override
  final DateTime endDueDate;
  @override
  final String paymentName;
  @override
  final int amount;
  @override
  final RepeatMode repeatMode;

  @override
  String toString() {
    return 'AdminPaymentsEvent.addedGroupPayment(groupIds: $groupIds, startDueDate: $startDueDate, endDueDate: $endDueDate, paymentName: $paymentName, amount: $amount, repeatMode: $repeatMode)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AddedGroupPayment &&
            (identical(other.groupIds, groupIds) ||
                const DeepCollectionEquality()
                    .equals(other.groupIds, groupIds)) &&
            (identical(other.startDueDate, startDueDate) ||
                const DeepCollectionEquality()
                    .equals(other.startDueDate, startDueDate)) &&
            (identical(other.endDueDate, endDueDate) ||
                const DeepCollectionEquality()
                    .equals(other.endDueDate, endDueDate)) &&
            (identical(other.paymentName, paymentName) ||
                const DeepCollectionEquality()
                    .equals(other.paymentName, paymentName)) &&
            (identical(other.amount, amount) ||
                const DeepCollectionEquality().equals(other.amount, amount)) &&
            (identical(other.repeatMode, repeatMode) ||
                const DeepCollectionEquality()
                    .equals(other.repeatMode, repeatMode)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(groupIds) ^
      const DeepCollectionEquality().hash(startDueDate) ^
      const DeepCollectionEquality().hash(endDueDate) ^
      const DeepCollectionEquality().hash(paymentName) ^
      const DeepCollectionEquality().hash(amount) ^
      const DeepCollectionEquality().hash(repeatMode);

  @JsonKey(ignore: true)
  @override
  $AddedGroupPaymentCopyWith<AddedGroupPayment> get copyWith =>
      _$AddedGroupPaymentCopyWithImpl<AddedGroupPayment>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadedPayments(),
    @required TResult loadedGroups(),
    @required TResult loadedUserPayments(String userId),
    @required TResult loadedUserHistoryPayments(String userId),
    @required TResult loadedGroupPayments(String groupId),
    @required
        TResult addedUserPayment(
            List<String> userIds,
            DateTime startDueDate,
            DateTime endDueDate,
            String paymentName,
            int amount,
            RepeatMode repeatMode),
    @required
        TResult addedGroupPayment(
            List<String> groupIds,
            DateTime startDueDate,
            DateTime endDueDate,
            String paymentName,
            int amount,
            RepeatMode repeatMode),
    @required
        TResult updatedPayment(String paymentId, DateTime newDueDate,
            String newName, int newAmount, bool isAccounted),
    @required TResult paymentAccounted(Payment payment, DateTime dateAccounted),
  }) {
    assert(loadedPayments != null);
    assert(loadedGroups != null);
    assert(loadedUserPayments != null);
    assert(loadedUserHistoryPayments != null);
    assert(loadedGroupPayments != null);
    assert(addedUserPayment != null);
    assert(addedGroupPayment != null);
    assert(updatedPayment != null);
    assert(paymentAccounted != null);
    return addedGroupPayment(
        groupIds, startDueDate, endDueDate, paymentName, amount, repeatMode);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadedPayments(),
    TResult loadedGroups(),
    TResult loadedUserPayments(String userId),
    TResult loadedUserHistoryPayments(String userId),
    TResult loadedGroupPayments(String groupId),
    TResult addedUserPayment(
        List<String> userIds,
        DateTime startDueDate,
        DateTime endDueDate,
        String paymentName,
        int amount,
        RepeatMode repeatMode),
    TResult addedGroupPayment(
        List<String> groupIds,
        DateTime startDueDate,
        DateTime endDueDate,
        String paymentName,
        int amount,
        RepeatMode repeatMode),
    TResult updatedPayment(String paymentId, DateTime newDueDate,
        String newName, int newAmount, bool isAccounted),
    TResult paymentAccounted(Payment payment, DateTime dateAccounted),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addedGroupPayment != null) {
      return addedGroupPayment(
          groupIds, startDueDate, endDueDate, paymentName, amount, repeatMode);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadedPayments(LoadedPayments value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUserPayments(LoadedUserPayments value),
    @required
        TResult loadedUserHistoryPayments(LoadedUserHistoryPayments value),
    @required TResult loadedGroupPayments(LoadedGroupPayments value),
    @required TResult addedUserPayment(AddedUserPayment value),
    @required TResult addedGroupPayment(AddedGroupPayment value),
    @required TResult updatedPayment(UpdatedPayment value),
    @required TResult paymentAccounted(PaymentAccounted value),
  }) {
    assert(loadedPayments != null);
    assert(loadedGroups != null);
    assert(loadedUserPayments != null);
    assert(loadedUserHistoryPayments != null);
    assert(loadedGroupPayments != null);
    assert(addedUserPayment != null);
    assert(addedGroupPayment != null);
    assert(updatedPayment != null);
    assert(paymentAccounted != null);
    return addedGroupPayment(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadedPayments(LoadedPayments value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUserPayments(LoadedUserPayments value),
    TResult loadedUserHistoryPayments(LoadedUserHistoryPayments value),
    TResult loadedGroupPayments(LoadedGroupPayments value),
    TResult addedUserPayment(AddedUserPayment value),
    TResult addedGroupPayment(AddedGroupPayment value),
    TResult updatedPayment(UpdatedPayment value),
    TResult paymentAccounted(PaymentAccounted value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addedGroupPayment != null) {
      return addedGroupPayment(this);
    }
    return orElse();
  }
}

abstract class AddedGroupPayment implements AdminPaymentsEvent {
  const factory AddedGroupPayment(
      {@required List<String> groupIds,
      @required DateTime startDueDate,
      DateTime endDueDate,
      @required String paymentName,
      @required int amount,
      @required RepeatMode repeatMode}) = _$AddedGroupPayment;

  List<String> get groupIds;
  DateTime get startDueDate;
  DateTime get endDueDate;
  String get paymentName;
  int get amount;
  RepeatMode get repeatMode;
  @JsonKey(ignore: true)
  $AddedGroupPaymentCopyWith<AddedGroupPayment> get copyWith;
}

/// @nodoc
abstract class $UpdatedPaymentCopyWith<$Res> {
  factory $UpdatedPaymentCopyWith(
          UpdatedPayment value, $Res Function(UpdatedPayment) then) =
      _$UpdatedPaymentCopyWithImpl<$Res>;
  $Res call(
      {String paymentId,
      DateTime newDueDate,
      String newName,
      int newAmount,
      bool isAccounted});
}

/// @nodoc
class _$UpdatedPaymentCopyWithImpl<$Res>
    extends _$AdminPaymentsEventCopyWithImpl<$Res>
    implements $UpdatedPaymentCopyWith<$Res> {
  _$UpdatedPaymentCopyWithImpl(
      UpdatedPayment _value, $Res Function(UpdatedPayment) _then)
      : super(_value, (v) => _then(v as UpdatedPayment));

  @override
  UpdatedPayment get _value => super._value as UpdatedPayment;

  @override
  $Res call({
    Object paymentId = freezed,
    Object newDueDate = freezed,
    Object newName = freezed,
    Object newAmount = freezed,
    Object isAccounted = freezed,
  }) {
    return _then(UpdatedPayment(
      paymentId: paymentId == freezed ? _value.paymentId : paymentId as String,
      newDueDate:
          newDueDate == freezed ? _value.newDueDate : newDueDate as DateTime,
      newName: newName == freezed ? _value.newName : newName as String,
      newAmount: newAmount == freezed ? _value.newAmount : newAmount as int,
      isAccounted:
          isAccounted == freezed ? _value.isAccounted : isAccounted as bool,
    ));
  }
}

/// @nodoc
class _$UpdatedPayment implements UpdatedPayment {
  const _$UpdatedPayment(
      {@required this.paymentId,
      this.newDueDate,
      this.newName,
      this.newAmount,
      this.isAccounted})
      : assert(paymentId != null);

  @override
  final String paymentId;
  @override
  final DateTime newDueDate;
  @override
  final String newName;
  @override
  final int newAmount;
  @override
  final bool isAccounted;

  @override
  String toString() {
    return 'AdminPaymentsEvent.updatedPayment(paymentId: $paymentId, newDueDate: $newDueDate, newName: $newName, newAmount: $newAmount, isAccounted: $isAccounted)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UpdatedPayment &&
            (identical(other.paymentId, paymentId) ||
                const DeepCollectionEquality()
                    .equals(other.paymentId, paymentId)) &&
            (identical(other.newDueDate, newDueDate) ||
                const DeepCollectionEquality()
                    .equals(other.newDueDate, newDueDate)) &&
            (identical(other.newName, newName) ||
                const DeepCollectionEquality()
                    .equals(other.newName, newName)) &&
            (identical(other.newAmount, newAmount) ||
                const DeepCollectionEquality()
                    .equals(other.newAmount, newAmount)) &&
            (identical(other.isAccounted, isAccounted) ||
                const DeepCollectionEquality()
                    .equals(other.isAccounted, isAccounted)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(paymentId) ^
      const DeepCollectionEquality().hash(newDueDate) ^
      const DeepCollectionEquality().hash(newName) ^
      const DeepCollectionEquality().hash(newAmount) ^
      const DeepCollectionEquality().hash(isAccounted);

  @JsonKey(ignore: true)
  @override
  $UpdatedPaymentCopyWith<UpdatedPayment> get copyWith =>
      _$UpdatedPaymentCopyWithImpl<UpdatedPayment>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadedPayments(),
    @required TResult loadedGroups(),
    @required TResult loadedUserPayments(String userId),
    @required TResult loadedUserHistoryPayments(String userId),
    @required TResult loadedGroupPayments(String groupId),
    @required
        TResult addedUserPayment(
            List<String> userIds,
            DateTime startDueDate,
            DateTime endDueDate,
            String paymentName,
            int amount,
            RepeatMode repeatMode),
    @required
        TResult addedGroupPayment(
            List<String> groupIds,
            DateTime startDueDate,
            DateTime endDueDate,
            String paymentName,
            int amount,
            RepeatMode repeatMode),
    @required
        TResult updatedPayment(String paymentId, DateTime newDueDate,
            String newName, int newAmount, bool isAccounted),
    @required TResult paymentAccounted(Payment payment, DateTime dateAccounted),
  }) {
    assert(loadedPayments != null);
    assert(loadedGroups != null);
    assert(loadedUserPayments != null);
    assert(loadedUserHistoryPayments != null);
    assert(loadedGroupPayments != null);
    assert(addedUserPayment != null);
    assert(addedGroupPayment != null);
    assert(updatedPayment != null);
    assert(paymentAccounted != null);
    return updatedPayment(
        paymentId, newDueDate, newName, newAmount, isAccounted);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadedPayments(),
    TResult loadedGroups(),
    TResult loadedUserPayments(String userId),
    TResult loadedUserHistoryPayments(String userId),
    TResult loadedGroupPayments(String groupId),
    TResult addedUserPayment(
        List<String> userIds,
        DateTime startDueDate,
        DateTime endDueDate,
        String paymentName,
        int amount,
        RepeatMode repeatMode),
    TResult addedGroupPayment(
        List<String> groupIds,
        DateTime startDueDate,
        DateTime endDueDate,
        String paymentName,
        int amount,
        RepeatMode repeatMode),
    TResult updatedPayment(String paymentId, DateTime newDueDate,
        String newName, int newAmount, bool isAccounted),
    TResult paymentAccounted(Payment payment, DateTime dateAccounted),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedPayment != null) {
      return updatedPayment(
          paymentId, newDueDate, newName, newAmount, isAccounted);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadedPayments(LoadedPayments value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUserPayments(LoadedUserPayments value),
    @required
        TResult loadedUserHistoryPayments(LoadedUserHistoryPayments value),
    @required TResult loadedGroupPayments(LoadedGroupPayments value),
    @required TResult addedUserPayment(AddedUserPayment value),
    @required TResult addedGroupPayment(AddedGroupPayment value),
    @required TResult updatedPayment(UpdatedPayment value),
    @required TResult paymentAccounted(PaymentAccounted value),
  }) {
    assert(loadedPayments != null);
    assert(loadedGroups != null);
    assert(loadedUserPayments != null);
    assert(loadedUserHistoryPayments != null);
    assert(loadedGroupPayments != null);
    assert(addedUserPayment != null);
    assert(addedGroupPayment != null);
    assert(updatedPayment != null);
    assert(paymentAccounted != null);
    return updatedPayment(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadedPayments(LoadedPayments value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUserPayments(LoadedUserPayments value),
    TResult loadedUserHistoryPayments(LoadedUserHistoryPayments value),
    TResult loadedGroupPayments(LoadedGroupPayments value),
    TResult addedUserPayment(AddedUserPayment value),
    TResult addedGroupPayment(AddedGroupPayment value),
    TResult updatedPayment(UpdatedPayment value),
    TResult paymentAccounted(PaymentAccounted value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedPayment != null) {
      return updatedPayment(this);
    }
    return orElse();
  }
}

abstract class UpdatedPayment implements AdminPaymentsEvent {
  const factory UpdatedPayment(
      {@required String paymentId,
      DateTime newDueDate,
      String newName,
      int newAmount,
      bool isAccounted}) = _$UpdatedPayment;

  String get paymentId;
  DateTime get newDueDate;
  String get newName;
  int get newAmount;
  bool get isAccounted;
  @JsonKey(ignore: true)
  $UpdatedPaymentCopyWith<UpdatedPayment> get copyWith;
}

/// @nodoc
abstract class $PaymentAccountedCopyWith<$Res> {
  factory $PaymentAccountedCopyWith(
          PaymentAccounted value, $Res Function(PaymentAccounted) then) =
      _$PaymentAccountedCopyWithImpl<$Res>;
  $Res call({Payment payment, DateTime dateAccounted});
}

/// @nodoc
class _$PaymentAccountedCopyWithImpl<$Res>
    extends _$AdminPaymentsEventCopyWithImpl<$Res>
    implements $PaymentAccountedCopyWith<$Res> {
  _$PaymentAccountedCopyWithImpl(
      PaymentAccounted _value, $Res Function(PaymentAccounted) _then)
      : super(_value, (v) => _then(v as PaymentAccounted));

  @override
  PaymentAccounted get _value => super._value as PaymentAccounted;

  @override
  $Res call({
    Object payment = freezed,
    Object dateAccounted = freezed,
  }) {
    return _then(PaymentAccounted(
      payment: payment == freezed ? _value.payment : payment as Payment,
      dateAccounted: dateAccounted == freezed
          ? _value.dateAccounted
          : dateAccounted as DateTime,
    ));
  }
}

/// @nodoc
class _$PaymentAccounted implements PaymentAccounted {
  const _$PaymentAccounted(
      {@required this.payment, @required this.dateAccounted})
      : assert(payment != null),
        assert(dateAccounted != null);

  @override
  final Payment payment;
  @override
  final DateTime dateAccounted;

  @override
  String toString() {
    return 'AdminPaymentsEvent.paymentAccounted(payment: $payment, dateAccounted: $dateAccounted)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is PaymentAccounted &&
            (identical(other.payment, payment) ||
                const DeepCollectionEquality()
                    .equals(other.payment, payment)) &&
            (identical(other.dateAccounted, dateAccounted) ||
                const DeepCollectionEquality()
                    .equals(other.dateAccounted, dateAccounted)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(payment) ^
      const DeepCollectionEquality().hash(dateAccounted);

  @JsonKey(ignore: true)
  @override
  $PaymentAccountedCopyWith<PaymentAccounted> get copyWith =>
      _$PaymentAccountedCopyWithImpl<PaymentAccounted>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadedPayments(),
    @required TResult loadedGroups(),
    @required TResult loadedUserPayments(String userId),
    @required TResult loadedUserHistoryPayments(String userId),
    @required TResult loadedGroupPayments(String groupId),
    @required
        TResult addedUserPayment(
            List<String> userIds,
            DateTime startDueDate,
            DateTime endDueDate,
            String paymentName,
            int amount,
            RepeatMode repeatMode),
    @required
        TResult addedGroupPayment(
            List<String> groupIds,
            DateTime startDueDate,
            DateTime endDueDate,
            String paymentName,
            int amount,
            RepeatMode repeatMode),
    @required
        TResult updatedPayment(String paymentId, DateTime newDueDate,
            String newName, int newAmount, bool isAccounted),
    @required TResult paymentAccounted(Payment payment, DateTime dateAccounted),
  }) {
    assert(loadedPayments != null);
    assert(loadedGroups != null);
    assert(loadedUserPayments != null);
    assert(loadedUserHistoryPayments != null);
    assert(loadedGroupPayments != null);
    assert(addedUserPayment != null);
    assert(addedGroupPayment != null);
    assert(updatedPayment != null);
    assert(paymentAccounted != null);
    return paymentAccounted(payment, dateAccounted);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadedPayments(),
    TResult loadedGroups(),
    TResult loadedUserPayments(String userId),
    TResult loadedUserHistoryPayments(String userId),
    TResult loadedGroupPayments(String groupId),
    TResult addedUserPayment(
        List<String> userIds,
        DateTime startDueDate,
        DateTime endDueDate,
        String paymentName,
        int amount,
        RepeatMode repeatMode),
    TResult addedGroupPayment(
        List<String> groupIds,
        DateTime startDueDate,
        DateTime endDueDate,
        String paymentName,
        int amount,
        RepeatMode repeatMode),
    TResult updatedPayment(String paymentId, DateTime newDueDate,
        String newName, int newAmount, bool isAccounted),
    TResult paymentAccounted(Payment payment, DateTime dateAccounted),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (paymentAccounted != null) {
      return paymentAccounted(payment, dateAccounted);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadedPayments(LoadedPayments value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUserPayments(LoadedUserPayments value),
    @required
        TResult loadedUserHistoryPayments(LoadedUserHistoryPayments value),
    @required TResult loadedGroupPayments(LoadedGroupPayments value),
    @required TResult addedUserPayment(AddedUserPayment value),
    @required TResult addedGroupPayment(AddedGroupPayment value),
    @required TResult updatedPayment(UpdatedPayment value),
    @required TResult paymentAccounted(PaymentAccounted value),
  }) {
    assert(loadedPayments != null);
    assert(loadedGroups != null);
    assert(loadedUserPayments != null);
    assert(loadedUserHistoryPayments != null);
    assert(loadedGroupPayments != null);
    assert(addedUserPayment != null);
    assert(addedGroupPayment != null);
    assert(updatedPayment != null);
    assert(paymentAccounted != null);
    return paymentAccounted(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadedPayments(LoadedPayments value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUserPayments(LoadedUserPayments value),
    TResult loadedUserHistoryPayments(LoadedUserHistoryPayments value),
    TResult loadedGroupPayments(LoadedGroupPayments value),
    TResult addedUserPayment(AddedUserPayment value),
    TResult addedGroupPayment(AddedGroupPayment value),
    TResult updatedPayment(UpdatedPayment value),
    TResult paymentAccounted(PaymentAccounted value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (paymentAccounted != null) {
      return paymentAccounted(this);
    }
    return orElse();
  }
}

abstract class PaymentAccounted implements AdminPaymentsEvent {
  const factory PaymentAccounted(
      {@required Payment payment,
      @required DateTime dateAccounted}) = _$PaymentAccounted;

  Payment get payment;
  DateTime get dateAccounted;
  @JsonKey(ignore: true)
  $PaymentAccountedCopyWith<PaymentAccounted> get copyWith;
}

/// @nodoc
class _$AdminPaymentsStateTearOff {
  const _$AdminPaymentsStateTearOff();

// ignore: unused_element
  PaymentsInitial initial() {
    return PaymentsInitial();
  }

// ignore: unused_element
  PaymentsInvalid paymentsInvalid() {
    return PaymentsInvalid();
  }

// ignore: unused_element
  PaymentsStatusInvalid paymentsStatusInvalid() {
    return PaymentsStatusInvalid();
  }

// ignore: unused_element
  UserPaymentsUpdated userPaymentsUpdated(
      {@required String userId, @required List<Payment> payments}) {
    return UserPaymentsUpdated(
      userId: userId,
      payments: payments,
    );
  }

// ignore: unused_element
  UserPaymentsHistoryUpdated userPaymentsHistoryUpdated(
      {@required String userId, @required List<Payment> payments}) {
    return UserPaymentsHistoryUpdated(
      userId: userId,
      payments: payments,
    );
  }

// ignore: unused_element
  OverallPaymentsStatusUpdated overallPaymentsStatusUpdated(
      {@required int paymentsPastDue,
      @required int paymentsPaid,
      @required int paymentsApproachingDue}) {
    return OverallPaymentsStatusUpdated(
      paymentsPastDue: paymentsPastDue,
      paymentsPaid: paymentsPaid,
      paymentsApproachingDue: paymentsApproachingDue,
    );
  }

// ignore: unused_element
  GroupPaymentsStatusUpdated groupPaymentsStatusUpdated(
      {@required String groupId,
      @required int paymentsPastDue,
      @required int paymentsPaid}) {
    return GroupPaymentsStatusUpdated(
      groupId: groupId,
      paymentsPastDue: paymentsPastDue,
      paymentsPaid: paymentsPaid,
    );
  }

// ignore: unused_element
  UserPaymentsStatusUpdated userPaymentsStatusUpdated(
      {@required String userId,
      @required int paymentsPastDue,
      @required int paymentsPaid}) {
    return UserPaymentsStatusUpdated(
      userId: userId,
      paymentsPastDue: paymentsPastDue,
      paymentsPaid: paymentsPaid,
    );
  }

// ignore: unused_element
  UserListUpdated userListUpdated(
      {@required String groupId, @required List<User> users}) {
    return UserListUpdated(
      groupId: groupId,
      users: users,
    );
  }

// ignore: unused_element
  GroupListUpdated groupListUpdated({@required List<Group> groups}) {
    return GroupListUpdated(
      groups: groups,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $AdminPaymentsState = _$AdminPaymentsStateTearOff();

/// @nodoc
mixin _$AdminPaymentsState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult paymentsInvalid(),
    @required TResult paymentsStatusInvalid(),
    @required
        TResult userPaymentsUpdated(String userId, List<Payment> payments),
    @required
        TResult userPaymentsHistoryUpdated(
            String userId, List<Payment> payments),
    @required
        TResult overallPaymentsStatusUpdated(
            int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    @required
        TResult groupPaymentsStatusUpdated(
            String groupId, int paymentsPastDue, int paymentsPaid),
    @required
        TResult userPaymentsStatusUpdated(
            String userId, int paymentsPastDue, int paymentsPaid),
    @required TResult userListUpdated(String groupId, List<User> users),
    @required TResult groupListUpdated(List<Group> groups),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult paymentsInvalid(),
    TResult paymentsStatusInvalid(),
    TResult userPaymentsUpdated(String userId, List<Payment> payments),
    TResult userPaymentsHistoryUpdated(String userId, List<Payment> payments),
    TResult overallPaymentsStatusUpdated(
        int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    TResult groupPaymentsStatusUpdated(
        String groupId, int paymentsPastDue, int paymentsPaid),
    TResult userPaymentsStatusUpdated(
        String userId, int paymentsPastDue, int paymentsPaid),
    TResult userListUpdated(String groupId, List<User> users),
    TResult groupListUpdated(List<Group> groups),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(PaymentsInitial value),
    @required TResult paymentsInvalid(PaymentsInvalid value),
    @required TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    @required TResult userPaymentsUpdated(UserPaymentsUpdated value),
    @required
        TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    @required
        TResult overallPaymentsStatusUpdated(
            OverallPaymentsStatusUpdated value),
    @required
        TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    @required
        TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    @required TResult userListUpdated(UserListUpdated value),
    @required TResult groupListUpdated(GroupListUpdated value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(PaymentsInitial value),
    TResult paymentsInvalid(PaymentsInvalid value),
    TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    TResult userPaymentsUpdated(UserPaymentsUpdated value),
    TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    TResult overallPaymentsStatusUpdated(OverallPaymentsStatusUpdated value),
    TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    TResult userListUpdated(UserListUpdated value),
    TResult groupListUpdated(GroupListUpdated value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $AdminPaymentsStateCopyWith<$Res> {
  factory $AdminPaymentsStateCopyWith(
          AdminPaymentsState value, $Res Function(AdminPaymentsState) then) =
      _$AdminPaymentsStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$AdminPaymentsStateCopyWithImpl<$Res>
    implements $AdminPaymentsStateCopyWith<$Res> {
  _$AdminPaymentsStateCopyWithImpl(this._value, this._then);

  final AdminPaymentsState _value;
  // ignore: unused_field
  final $Res Function(AdminPaymentsState) _then;
}

/// @nodoc
abstract class $PaymentsInitialCopyWith<$Res> {
  factory $PaymentsInitialCopyWith(
          PaymentsInitial value, $Res Function(PaymentsInitial) then) =
      _$PaymentsInitialCopyWithImpl<$Res>;
}

/// @nodoc
class _$PaymentsInitialCopyWithImpl<$Res>
    extends _$AdminPaymentsStateCopyWithImpl<$Res>
    implements $PaymentsInitialCopyWith<$Res> {
  _$PaymentsInitialCopyWithImpl(
      PaymentsInitial _value, $Res Function(PaymentsInitial) _then)
      : super(_value, (v) => _then(v as PaymentsInitial));

  @override
  PaymentsInitial get _value => super._value as PaymentsInitial;
}

/// @nodoc
class _$PaymentsInitial implements PaymentsInitial {
  _$PaymentsInitial();

  @override
  String toString() {
    return 'AdminPaymentsState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is PaymentsInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult paymentsInvalid(),
    @required TResult paymentsStatusInvalid(),
    @required
        TResult userPaymentsUpdated(String userId, List<Payment> payments),
    @required
        TResult userPaymentsHistoryUpdated(
            String userId, List<Payment> payments),
    @required
        TResult overallPaymentsStatusUpdated(
            int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    @required
        TResult groupPaymentsStatusUpdated(
            String groupId, int paymentsPastDue, int paymentsPaid),
    @required
        TResult userPaymentsStatusUpdated(
            String userId, int paymentsPastDue, int paymentsPaid),
    @required TResult userListUpdated(String groupId, List<User> users),
    @required TResult groupListUpdated(List<Group> groups),
  }) {
    assert(initial != null);
    assert(paymentsInvalid != null);
    assert(paymentsStatusInvalid != null);
    assert(userPaymentsUpdated != null);
    assert(userPaymentsHistoryUpdated != null);
    assert(overallPaymentsStatusUpdated != null);
    assert(groupPaymentsStatusUpdated != null);
    assert(userPaymentsStatusUpdated != null);
    assert(userListUpdated != null);
    assert(groupListUpdated != null);
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult paymentsInvalid(),
    TResult paymentsStatusInvalid(),
    TResult userPaymentsUpdated(String userId, List<Payment> payments),
    TResult userPaymentsHistoryUpdated(String userId, List<Payment> payments),
    TResult overallPaymentsStatusUpdated(
        int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    TResult groupPaymentsStatusUpdated(
        String groupId, int paymentsPastDue, int paymentsPaid),
    TResult userPaymentsStatusUpdated(
        String userId, int paymentsPastDue, int paymentsPaid),
    TResult userListUpdated(String groupId, List<User> users),
    TResult groupListUpdated(List<Group> groups),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(PaymentsInitial value),
    @required TResult paymentsInvalid(PaymentsInvalid value),
    @required TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    @required TResult userPaymentsUpdated(UserPaymentsUpdated value),
    @required
        TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    @required
        TResult overallPaymentsStatusUpdated(
            OverallPaymentsStatusUpdated value),
    @required
        TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    @required
        TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    @required TResult userListUpdated(UserListUpdated value),
    @required TResult groupListUpdated(GroupListUpdated value),
  }) {
    assert(initial != null);
    assert(paymentsInvalid != null);
    assert(paymentsStatusInvalid != null);
    assert(userPaymentsUpdated != null);
    assert(userPaymentsHistoryUpdated != null);
    assert(overallPaymentsStatusUpdated != null);
    assert(groupPaymentsStatusUpdated != null);
    assert(userPaymentsStatusUpdated != null);
    assert(userListUpdated != null);
    assert(groupListUpdated != null);
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(PaymentsInitial value),
    TResult paymentsInvalid(PaymentsInvalid value),
    TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    TResult userPaymentsUpdated(UserPaymentsUpdated value),
    TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    TResult overallPaymentsStatusUpdated(OverallPaymentsStatusUpdated value),
    TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    TResult userListUpdated(UserListUpdated value),
    TResult groupListUpdated(GroupListUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class PaymentsInitial implements AdminPaymentsState {
  factory PaymentsInitial() = _$PaymentsInitial;
}

/// @nodoc
abstract class $PaymentsInvalidCopyWith<$Res> {
  factory $PaymentsInvalidCopyWith(
          PaymentsInvalid value, $Res Function(PaymentsInvalid) then) =
      _$PaymentsInvalidCopyWithImpl<$Res>;
}

/// @nodoc
class _$PaymentsInvalidCopyWithImpl<$Res>
    extends _$AdminPaymentsStateCopyWithImpl<$Res>
    implements $PaymentsInvalidCopyWith<$Res> {
  _$PaymentsInvalidCopyWithImpl(
      PaymentsInvalid _value, $Res Function(PaymentsInvalid) _then)
      : super(_value, (v) => _then(v as PaymentsInvalid));

  @override
  PaymentsInvalid get _value => super._value as PaymentsInvalid;
}

/// @nodoc
class _$PaymentsInvalid implements PaymentsInvalid {
  _$PaymentsInvalid();

  @override
  String toString() {
    return 'AdminPaymentsState.paymentsInvalid()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is PaymentsInvalid);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult paymentsInvalid(),
    @required TResult paymentsStatusInvalid(),
    @required
        TResult userPaymentsUpdated(String userId, List<Payment> payments),
    @required
        TResult userPaymentsHistoryUpdated(
            String userId, List<Payment> payments),
    @required
        TResult overallPaymentsStatusUpdated(
            int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    @required
        TResult groupPaymentsStatusUpdated(
            String groupId, int paymentsPastDue, int paymentsPaid),
    @required
        TResult userPaymentsStatusUpdated(
            String userId, int paymentsPastDue, int paymentsPaid),
    @required TResult userListUpdated(String groupId, List<User> users),
    @required TResult groupListUpdated(List<Group> groups),
  }) {
    assert(initial != null);
    assert(paymentsInvalid != null);
    assert(paymentsStatusInvalid != null);
    assert(userPaymentsUpdated != null);
    assert(userPaymentsHistoryUpdated != null);
    assert(overallPaymentsStatusUpdated != null);
    assert(groupPaymentsStatusUpdated != null);
    assert(userPaymentsStatusUpdated != null);
    assert(userListUpdated != null);
    assert(groupListUpdated != null);
    return paymentsInvalid();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult paymentsInvalid(),
    TResult paymentsStatusInvalid(),
    TResult userPaymentsUpdated(String userId, List<Payment> payments),
    TResult userPaymentsHistoryUpdated(String userId, List<Payment> payments),
    TResult overallPaymentsStatusUpdated(
        int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    TResult groupPaymentsStatusUpdated(
        String groupId, int paymentsPastDue, int paymentsPaid),
    TResult userPaymentsStatusUpdated(
        String userId, int paymentsPastDue, int paymentsPaid),
    TResult userListUpdated(String groupId, List<User> users),
    TResult groupListUpdated(List<Group> groups),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (paymentsInvalid != null) {
      return paymentsInvalid();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(PaymentsInitial value),
    @required TResult paymentsInvalid(PaymentsInvalid value),
    @required TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    @required TResult userPaymentsUpdated(UserPaymentsUpdated value),
    @required
        TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    @required
        TResult overallPaymentsStatusUpdated(
            OverallPaymentsStatusUpdated value),
    @required
        TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    @required
        TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    @required TResult userListUpdated(UserListUpdated value),
    @required TResult groupListUpdated(GroupListUpdated value),
  }) {
    assert(initial != null);
    assert(paymentsInvalid != null);
    assert(paymentsStatusInvalid != null);
    assert(userPaymentsUpdated != null);
    assert(userPaymentsHistoryUpdated != null);
    assert(overallPaymentsStatusUpdated != null);
    assert(groupPaymentsStatusUpdated != null);
    assert(userPaymentsStatusUpdated != null);
    assert(userListUpdated != null);
    assert(groupListUpdated != null);
    return paymentsInvalid(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(PaymentsInitial value),
    TResult paymentsInvalid(PaymentsInvalid value),
    TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    TResult userPaymentsUpdated(UserPaymentsUpdated value),
    TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    TResult overallPaymentsStatusUpdated(OverallPaymentsStatusUpdated value),
    TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    TResult userListUpdated(UserListUpdated value),
    TResult groupListUpdated(GroupListUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (paymentsInvalid != null) {
      return paymentsInvalid(this);
    }
    return orElse();
  }
}

abstract class PaymentsInvalid implements AdminPaymentsState {
  factory PaymentsInvalid() = _$PaymentsInvalid;
}

/// @nodoc
abstract class $PaymentsStatusInvalidCopyWith<$Res> {
  factory $PaymentsStatusInvalidCopyWith(PaymentsStatusInvalid value,
          $Res Function(PaymentsStatusInvalid) then) =
      _$PaymentsStatusInvalidCopyWithImpl<$Res>;
}

/// @nodoc
class _$PaymentsStatusInvalidCopyWithImpl<$Res>
    extends _$AdminPaymentsStateCopyWithImpl<$Res>
    implements $PaymentsStatusInvalidCopyWith<$Res> {
  _$PaymentsStatusInvalidCopyWithImpl(
      PaymentsStatusInvalid _value, $Res Function(PaymentsStatusInvalid) _then)
      : super(_value, (v) => _then(v as PaymentsStatusInvalid));

  @override
  PaymentsStatusInvalid get _value => super._value as PaymentsStatusInvalid;
}

/// @nodoc
class _$PaymentsStatusInvalid implements PaymentsStatusInvalid {
  _$PaymentsStatusInvalid();

  @override
  String toString() {
    return 'AdminPaymentsState.paymentsStatusInvalid()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is PaymentsStatusInvalid);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult paymentsInvalid(),
    @required TResult paymentsStatusInvalid(),
    @required
        TResult userPaymentsUpdated(String userId, List<Payment> payments),
    @required
        TResult userPaymentsHistoryUpdated(
            String userId, List<Payment> payments),
    @required
        TResult overallPaymentsStatusUpdated(
            int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    @required
        TResult groupPaymentsStatusUpdated(
            String groupId, int paymentsPastDue, int paymentsPaid),
    @required
        TResult userPaymentsStatusUpdated(
            String userId, int paymentsPastDue, int paymentsPaid),
    @required TResult userListUpdated(String groupId, List<User> users),
    @required TResult groupListUpdated(List<Group> groups),
  }) {
    assert(initial != null);
    assert(paymentsInvalid != null);
    assert(paymentsStatusInvalid != null);
    assert(userPaymentsUpdated != null);
    assert(userPaymentsHistoryUpdated != null);
    assert(overallPaymentsStatusUpdated != null);
    assert(groupPaymentsStatusUpdated != null);
    assert(userPaymentsStatusUpdated != null);
    assert(userListUpdated != null);
    assert(groupListUpdated != null);
    return paymentsStatusInvalid();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult paymentsInvalid(),
    TResult paymentsStatusInvalid(),
    TResult userPaymentsUpdated(String userId, List<Payment> payments),
    TResult userPaymentsHistoryUpdated(String userId, List<Payment> payments),
    TResult overallPaymentsStatusUpdated(
        int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    TResult groupPaymentsStatusUpdated(
        String groupId, int paymentsPastDue, int paymentsPaid),
    TResult userPaymentsStatusUpdated(
        String userId, int paymentsPastDue, int paymentsPaid),
    TResult userListUpdated(String groupId, List<User> users),
    TResult groupListUpdated(List<Group> groups),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (paymentsStatusInvalid != null) {
      return paymentsStatusInvalid();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(PaymentsInitial value),
    @required TResult paymentsInvalid(PaymentsInvalid value),
    @required TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    @required TResult userPaymentsUpdated(UserPaymentsUpdated value),
    @required
        TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    @required
        TResult overallPaymentsStatusUpdated(
            OverallPaymentsStatusUpdated value),
    @required
        TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    @required
        TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    @required TResult userListUpdated(UserListUpdated value),
    @required TResult groupListUpdated(GroupListUpdated value),
  }) {
    assert(initial != null);
    assert(paymentsInvalid != null);
    assert(paymentsStatusInvalid != null);
    assert(userPaymentsUpdated != null);
    assert(userPaymentsHistoryUpdated != null);
    assert(overallPaymentsStatusUpdated != null);
    assert(groupPaymentsStatusUpdated != null);
    assert(userPaymentsStatusUpdated != null);
    assert(userListUpdated != null);
    assert(groupListUpdated != null);
    return paymentsStatusInvalid(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(PaymentsInitial value),
    TResult paymentsInvalid(PaymentsInvalid value),
    TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    TResult userPaymentsUpdated(UserPaymentsUpdated value),
    TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    TResult overallPaymentsStatusUpdated(OverallPaymentsStatusUpdated value),
    TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    TResult userListUpdated(UserListUpdated value),
    TResult groupListUpdated(GroupListUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (paymentsStatusInvalid != null) {
      return paymentsStatusInvalid(this);
    }
    return orElse();
  }
}

abstract class PaymentsStatusInvalid implements AdminPaymentsState {
  factory PaymentsStatusInvalid() = _$PaymentsStatusInvalid;
}

/// @nodoc
abstract class $UserPaymentsUpdatedCopyWith<$Res> {
  factory $UserPaymentsUpdatedCopyWith(
          UserPaymentsUpdated value, $Res Function(UserPaymentsUpdated) then) =
      _$UserPaymentsUpdatedCopyWithImpl<$Res>;
  $Res call({String userId, List<Payment> payments});
}

/// @nodoc
class _$UserPaymentsUpdatedCopyWithImpl<$Res>
    extends _$AdminPaymentsStateCopyWithImpl<$Res>
    implements $UserPaymentsUpdatedCopyWith<$Res> {
  _$UserPaymentsUpdatedCopyWithImpl(
      UserPaymentsUpdated _value, $Res Function(UserPaymentsUpdated) _then)
      : super(_value, (v) => _then(v as UserPaymentsUpdated));

  @override
  UserPaymentsUpdated get _value => super._value as UserPaymentsUpdated;

  @override
  $Res call({
    Object userId = freezed,
    Object payments = freezed,
  }) {
    return _then(UserPaymentsUpdated(
      userId: userId == freezed ? _value.userId : userId as String,
      payments:
          payments == freezed ? _value.payments : payments as List<Payment>,
    ));
  }
}

/// @nodoc
class _$UserPaymentsUpdated implements UserPaymentsUpdated {
  _$UserPaymentsUpdated({@required this.userId, @required this.payments})
      : assert(userId != null),
        assert(payments != null);

  @override
  final String userId;
  @override
  final List<Payment> payments;

  @override
  String toString() {
    return 'AdminPaymentsState.userPaymentsUpdated(userId: $userId, payments: $payments)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UserPaymentsUpdated &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)) &&
            (identical(other.payments, payments) ||
                const DeepCollectionEquality()
                    .equals(other.payments, payments)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(userId) ^
      const DeepCollectionEquality().hash(payments);

  @JsonKey(ignore: true)
  @override
  $UserPaymentsUpdatedCopyWith<UserPaymentsUpdated> get copyWith =>
      _$UserPaymentsUpdatedCopyWithImpl<UserPaymentsUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult paymentsInvalid(),
    @required TResult paymentsStatusInvalid(),
    @required
        TResult userPaymentsUpdated(String userId, List<Payment> payments),
    @required
        TResult userPaymentsHistoryUpdated(
            String userId, List<Payment> payments),
    @required
        TResult overallPaymentsStatusUpdated(
            int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    @required
        TResult groupPaymentsStatusUpdated(
            String groupId, int paymentsPastDue, int paymentsPaid),
    @required
        TResult userPaymentsStatusUpdated(
            String userId, int paymentsPastDue, int paymentsPaid),
    @required TResult userListUpdated(String groupId, List<User> users),
    @required TResult groupListUpdated(List<Group> groups),
  }) {
    assert(initial != null);
    assert(paymentsInvalid != null);
    assert(paymentsStatusInvalid != null);
    assert(userPaymentsUpdated != null);
    assert(userPaymentsHistoryUpdated != null);
    assert(overallPaymentsStatusUpdated != null);
    assert(groupPaymentsStatusUpdated != null);
    assert(userPaymentsStatusUpdated != null);
    assert(userListUpdated != null);
    assert(groupListUpdated != null);
    return userPaymentsUpdated(userId, payments);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult paymentsInvalid(),
    TResult paymentsStatusInvalid(),
    TResult userPaymentsUpdated(String userId, List<Payment> payments),
    TResult userPaymentsHistoryUpdated(String userId, List<Payment> payments),
    TResult overallPaymentsStatusUpdated(
        int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    TResult groupPaymentsStatusUpdated(
        String groupId, int paymentsPastDue, int paymentsPaid),
    TResult userPaymentsStatusUpdated(
        String userId, int paymentsPastDue, int paymentsPaid),
    TResult userListUpdated(String groupId, List<User> users),
    TResult groupListUpdated(List<Group> groups),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (userPaymentsUpdated != null) {
      return userPaymentsUpdated(userId, payments);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(PaymentsInitial value),
    @required TResult paymentsInvalid(PaymentsInvalid value),
    @required TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    @required TResult userPaymentsUpdated(UserPaymentsUpdated value),
    @required
        TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    @required
        TResult overallPaymentsStatusUpdated(
            OverallPaymentsStatusUpdated value),
    @required
        TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    @required
        TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    @required TResult userListUpdated(UserListUpdated value),
    @required TResult groupListUpdated(GroupListUpdated value),
  }) {
    assert(initial != null);
    assert(paymentsInvalid != null);
    assert(paymentsStatusInvalid != null);
    assert(userPaymentsUpdated != null);
    assert(userPaymentsHistoryUpdated != null);
    assert(overallPaymentsStatusUpdated != null);
    assert(groupPaymentsStatusUpdated != null);
    assert(userPaymentsStatusUpdated != null);
    assert(userListUpdated != null);
    assert(groupListUpdated != null);
    return userPaymentsUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(PaymentsInitial value),
    TResult paymentsInvalid(PaymentsInvalid value),
    TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    TResult userPaymentsUpdated(UserPaymentsUpdated value),
    TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    TResult overallPaymentsStatusUpdated(OverallPaymentsStatusUpdated value),
    TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    TResult userListUpdated(UserListUpdated value),
    TResult groupListUpdated(GroupListUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (userPaymentsUpdated != null) {
      return userPaymentsUpdated(this);
    }
    return orElse();
  }
}

abstract class UserPaymentsUpdated implements AdminPaymentsState {
  factory UserPaymentsUpdated(
      {@required String userId,
      @required List<Payment> payments}) = _$UserPaymentsUpdated;

  String get userId;
  List<Payment> get payments;
  @JsonKey(ignore: true)
  $UserPaymentsUpdatedCopyWith<UserPaymentsUpdated> get copyWith;
}

/// @nodoc
abstract class $UserPaymentsHistoryUpdatedCopyWith<$Res> {
  factory $UserPaymentsHistoryUpdatedCopyWith(UserPaymentsHistoryUpdated value,
          $Res Function(UserPaymentsHistoryUpdated) then) =
      _$UserPaymentsHistoryUpdatedCopyWithImpl<$Res>;
  $Res call({String userId, List<Payment> payments});
}

/// @nodoc
class _$UserPaymentsHistoryUpdatedCopyWithImpl<$Res>
    extends _$AdminPaymentsStateCopyWithImpl<$Res>
    implements $UserPaymentsHistoryUpdatedCopyWith<$Res> {
  _$UserPaymentsHistoryUpdatedCopyWithImpl(UserPaymentsHistoryUpdated _value,
      $Res Function(UserPaymentsHistoryUpdated) _then)
      : super(_value, (v) => _then(v as UserPaymentsHistoryUpdated));

  @override
  UserPaymentsHistoryUpdated get _value =>
      super._value as UserPaymentsHistoryUpdated;

  @override
  $Res call({
    Object userId = freezed,
    Object payments = freezed,
  }) {
    return _then(UserPaymentsHistoryUpdated(
      userId: userId == freezed ? _value.userId : userId as String,
      payments:
          payments == freezed ? _value.payments : payments as List<Payment>,
    ));
  }
}

/// @nodoc
class _$UserPaymentsHistoryUpdated implements UserPaymentsHistoryUpdated {
  _$UserPaymentsHistoryUpdated({@required this.userId, @required this.payments})
      : assert(userId != null),
        assert(payments != null);

  @override
  final String userId;
  @override
  final List<Payment> payments;

  @override
  String toString() {
    return 'AdminPaymentsState.userPaymentsHistoryUpdated(userId: $userId, payments: $payments)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UserPaymentsHistoryUpdated &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)) &&
            (identical(other.payments, payments) ||
                const DeepCollectionEquality()
                    .equals(other.payments, payments)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(userId) ^
      const DeepCollectionEquality().hash(payments);

  @JsonKey(ignore: true)
  @override
  $UserPaymentsHistoryUpdatedCopyWith<UserPaymentsHistoryUpdated>
      get copyWith =>
          _$UserPaymentsHistoryUpdatedCopyWithImpl<UserPaymentsHistoryUpdated>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult paymentsInvalid(),
    @required TResult paymentsStatusInvalid(),
    @required
        TResult userPaymentsUpdated(String userId, List<Payment> payments),
    @required
        TResult userPaymentsHistoryUpdated(
            String userId, List<Payment> payments),
    @required
        TResult overallPaymentsStatusUpdated(
            int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    @required
        TResult groupPaymentsStatusUpdated(
            String groupId, int paymentsPastDue, int paymentsPaid),
    @required
        TResult userPaymentsStatusUpdated(
            String userId, int paymentsPastDue, int paymentsPaid),
    @required TResult userListUpdated(String groupId, List<User> users),
    @required TResult groupListUpdated(List<Group> groups),
  }) {
    assert(initial != null);
    assert(paymentsInvalid != null);
    assert(paymentsStatusInvalid != null);
    assert(userPaymentsUpdated != null);
    assert(userPaymentsHistoryUpdated != null);
    assert(overallPaymentsStatusUpdated != null);
    assert(groupPaymentsStatusUpdated != null);
    assert(userPaymentsStatusUpdated != null);
    assert(userListUpdated != null);
    assert(groupListUpdated != null);
    return userPaymentsHistoryUpdated(userId, payments);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult paymentsInvalid(),
    TResult paymentsStatusInvalid(),
    TResult userPaymentsUpdated(String userId, List<Payment> payments),
    TResult userPaymentsHistoryUpdated(String userId, List<Payment> payments),
    TResult overallPaymentsStatusUpdated(
        int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    TResult groupPaymentsStatusUpdated(
        String groupId, int paymentsPastDue, int paymentsPaid),
    TResult userPaymentsStatusUpdated(
        String userId, int paymentsPastDue, int paymentsPaid),
    TResult userListUpdated(String groupId, List<User> users),
    TResult groupListUpdated(List<Group> groups),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (userPaymentsHistoryUpdated != null) {
      return userPaymentsHistoryUpdated(userId, payments);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(PaymentsInitial value),
    @required TResult paymentsInvalid(PaymentsInvalid value),
    @required TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    @required TResult userPaymentsUpdated(UserPaymentsUpdated value),
    @required
        TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    @required
        TResult overallPaymentsStatusUpdated(
            OverallPaymentsStatusUpdated value),
    @required
        TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    @required
        TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    @required TResult userListUpdated(UserListUpdated value),
    @required TResult groupListUpdated(GroupListUpdated value),
  }) {
    assert(initial != null);
    assert(paymentsInvalid != null);
    assert(paymentsStatusInvalid != null);
    assert(userPaymentsUpdated != null);
    assert(userPaymentsHistoryUpdated != null);
    assert(overallPaymentsStatusUpdated != null);
    assert(groupPaymentsStatusUpdated != null);
    assert(userPaymentsStatusUpdated != null);
    assert(userListUpdated != null);
    assert(groupListUpdated != null);
    return userPaymentsHistoryUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(PaymentsInitial value),
    TResult paymentsInvalid(PaymentsInvalid value),
    TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    TResult userPaymentsUpdated(UserPaymentsUpdated value),
    TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    TResult overallPaymentsStatusUpdated(OverallPaymentsStatusUpdated value),
    TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    TResult userListUpdated(UserListUpdated value),
    TResult groupListUpdated(GroupListUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (userPaymentsHistoryUpdated != null) {
      return userPaymentsHistoryUpdated(this);
    }
    return orElse();
  }
}

abstract class UserPaymentsHistoryUpdated implements AdminPaymentsState {
  factory UserPaymentsHistoryUpdated(
      {@required String userId,
      @required List<Payment> payments}) = _$UserPaymentsHistoryUpdated;

  String get userId;
  List<Payment> get payments;
  @JsonKey(ignore: true)
  $UserPaymentsHistoryUpdatedCopyWith<UserPaymentsHistoryUpdated> get copyWith;
}

/// @nodoc
abstract class $OverallPaymentsStatusUpdatedCopyWith<$Res> {
  factory $OverallPaymentsStatusUpdatedCopyWith(
          OverallPaymentsStatusUpdated value,
          $Res Function(OverallPaymentsStatusUpdated) then) =
      _$OverallPaymentsStatusUpdatedCopyWithImpl<$Res>;
  $Res call(
      {int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue});
}

/// @nodoc
class _$OverallPaymentsStatusUpdatedCopyWithImpl<$Res>
    extends _$AdminPaymentsStateCopyWithImpl<$Res>
    implements $OverallPaymentsStatusUpdatedCopyWith<$Res> {
  _$OverallPaymentsStatusUpdatedCopyWithImpl(
      OverallPaymentsStatusUpdated _value,
      $Res Function(OverallPaymentsStatusUpdated) _then)
      : super(_value, (v) => _then(v as OverallPaymentsStatusUpdated));

  @override
  OverallPaymentsStatusUpdated get _value =>
      super._value as OverallPaymentsStatusUpdated;

  @override
  $Res call({
    Object paymentsPastDue = freezed,
    Object paymentsPaid = freezed,
    Object paymentsApproachingDue = freezed,
  }) {
    return _then(OverallPaymentsStatusUpdated(
      paymentsPastDue: paymentsPastDue == freezed
          ? _value.paymentsPastDue
          : paymentsPastDue as int,
      paymentsPaid:
          paymentsPaid == freezed ? _value.paymentsPaid : paymentsPaid as int,
      paymentsApproachingDue: paymentsApproachingDue == freezed
          ? _value.paymentsApproachingDue
          : paymentsApproachingDue as int,
    ));
  }
}

/// @nodoc
class _$OverallPaymentsStatusUpdated implements OverallPaymentsStatusUpdated {
  _$OverallPaymentsStatusUpdated(
      {@required this.paymentsPastDue,
      @required this.paymentsPaid,
      @required this.paymentsApproachingDue})
      : assert(paymentsPastDue != null),
        assert(paymentsPaid != null),
        assert(paymentsApproachingDue != null);

  @override
  final int paymentsPastDue;
  @override
  final int paymentsPaid;
  @override
  final int paymentsApproachingDue;

  @override
  String toString() {
    return 'AdminPaymentsState.overallPaymentsStatusUpdated(paymentsPastDue: $paymentsPastDue, paymentsPaid: $paymentsPaid, paymentsApproachingDue: $paymentsApproachingDue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is OverallPaymentsStatusUpdated &&
            (identical(other.paymentsPastDue, paymentsPastDue) ||
                const DeepCollectionEquality()
                    .equals(other.paymentsPastDue, paymentsPastDue)) &&
            (identical(other.paymentsPaid, paymentsPaid) ||
                const DeepCollectionEquality()
                    .equals(other.paymentsPaid, paymentsPaid)) &&
            (identical(other.paymentsApproachingDue, paymentsApproachingDue) ||
                const DeepCollectionEquality().equals(
                    other.paymentsApproachingDue, paymentsApproachingDue)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(paymentsPastDue) ^
      const DeepCollectionEquality().hash(paymentsPaid) ^
      const DeepCollectionEquality().hash(paymentsApproachingDue);

  @JsonKey(ignore: true)
  @override
  $OverallPaymentsStatusUpdatedCopyWith<OverallPaymentsStatusUpdated>
      get copyWith => _$OverallPaymentsStatusUpdatedCopyWithImpl<
          OverallPaymentsStatusUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult paymentsInvalid(),
    @required TResult paymentsStatusInvalid(),
    @required
        TResult userPaymentsUpdated(String userId, List<Payment> payments),
    @required
        TResult userPaymentsHistoryUpdated(
            String userId, List<Payment> payments),
    @required
        TResult overallPaymentsStatusUpdated(
            int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    @required
        TResult groupPaymentsStatusUpdated(
            String groupId, int paymentsPastDue, int paymentsPaid),
    @required
        TResult userPaymentsStatusUpdated(
            String userId, int paymentsPastDue, int paymentsPaid),
    @required TResult userListUpdated(String groupId, List<User> users),
    @required TResult groupListUpdated(List<Group> groups),
  }) {
    assert(initial != null);
    assert(paymentsInvalid != null);
    assert(paymentsStatusInvalid != null);
    assert(userPaymentsUpdated != null);
    assert(userPaymentsHistoryUpdated != null);
    assert(overallPaymentsStatusUpdated != null);
    assert(groupPaymentsStatusUpdated != null);
    assert(userPaymentsStatusUpdated != null);
    assert(userListUpdated != null);
    assert(groupListUpdated != null);
    return overallPaymentsStatusUpdated(
        paymentsPastDue, paymentsPaid, paymentsApproachingDue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult paymentsInvalid(),
    TResult paymentsStatusInvalid(),
    TResult userPaymentsUpdated(String userId, List<Payment> payments),
    TResult userPaymentsHistoryUpdated(String userId, List<Payment> payments),
    TResult overallPaymentsStatusUpdated(
        int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    TResult groupPaymentsStatusUpdated(
        String groupId, int paymentsPastDue, int paymentsPaid),
    TResult userPaymentsStatusUpdated(
        String userId, int paymentsPastDue, int paymentsPaid),
    TResult userListUpdated(String groupId, List<User> users),
    TResult groupListUpdated(List<Group> groups),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (overallPaymentsStatusUpdated != null) {
      return overallPaymentsStatusUpdated(
          paymentsPastDue, paymentsPaid, paymentsApproachingDue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(PaymentsInitial value),
    @required TResult paymentsInvalid(PaymentsInvalid value),
    @required TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    @required TResult userPaymentsUpdated(UserPaymentsUpdated value),
    @required
        TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    @required
        TResult overallPaymentsStatusUpdated(
            OverallPaymentsStatusUpdated value),
    @required
        TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    @required
        TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    @required TResult userListUpdated(UserListUpdated value),
    @required TResult groupListUpdated(GroupListUpdated value),
  }) {
    assert(initial != null);
    assert(paymentsInvalid != null);
    assert(paymentsStatusInvalid != null);
    assert(userPaymentsUpdated != null);
    assert(userPaymentsHistoryUpdated != null);
    assert(overallPaymentsStatusUpdated != null);
    assert(groupPaymentsStatusUpdated != null);
    assert(userPaymentsStatusUpdated != null);
    assert(userListUpdated != null);
    assert(groupListUpdated != null);
    return overallPaymentsStatusUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(PaymentsInitial value),
    TResult paymentsInvalid(PaymentsInvalid value),
    TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    TResult userPaymentsUpdated(UserPaymentsUpdated value),
    TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    TResult overallPaymentsStatusUpdated(OverallPaymentsStatusUpdated value),
    TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    TResult userListUpdated(UserListUpdated value),
    TResult groupListUpdated(GroupListUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (overallPaymentsStatusUpdated != null) {
      return overallPaymentsStatusUpdated(this);
    }
    return orElse();
  }
}

abstract class OverallPaymentsStatusUpdated implements AdminPaymentsState {
  factory OverallPaymentsStatusUpdated(
      {@required int paymentsPastDue,
      @required int paymentsPaid,
      @required int paymentsApproachingDue}) = _$OverallPaymentsStatusUpdated;

  int get paymentsPastDue;
  int get paymentsPaid;
  int get paymentsApproachingDue;
  @JsonKey(ignore: true)
  $OverallPaymentsStatusUpdatedCopyWith<OverallPaymentsStatusUpdated>
      get copyWith;
}

/// @nodoc
abstract class $GroupPaymentsStatusUpdatedCopyWith<$Res> {
  factory $GroupPaymentsStatusUpdatedCopyWith(GroupPaymentsStatusUpdated value,
          $Res Function(GroupPaymentsStatusUpdated) then) =
      _$GroupPaymentsStatusUpdatedCopyWithImpl<$Res>;
  $Res call({String groupId, int paymentsPastDue, int paymentsPaid});
}

/// @nodoc
class _$GroupPaymentsStatusUpdatedCopyWithImpl<$Res>
    extends _$AdminPaymentsStateCopyWithImpl<$Res>
    implements $GroupPaymentsStatusUpdatedCopyWith<$Res> {
  _$GroupPaymentsStatusUpdatedCopyWithImpl(GroupPaymentsStatusUpdated _value,
      $Res Function(GroupPaymentsStatusUpdated) _then)
      : super(_value, (v) => _then(v as GroupPaymentsStatusUpdated));

  @override
  GroupPaymentsStatusUpdated get _value =>
      super._value as GroupPaymentsStatusUpdated;

  @override
  $Res call({
    Object groupId = freezed,
    Object paymentsPastDue = freezed,
    Object paymentsPaid = freezed,
  }) {
    return _then(GroupPaymentsStatusUpdated(
      groupId: groupId == freezed ? _value.groupId : groupId as String,
      paymentsPastDue: paymentsPastDue == freezed
          ? _value.paymentsPastDue
          : paymentsPastDue as int,
      paymentsPaid:
          paymentsPaid == freezed ? _value.paymentsPaid : paymentsPaid as int,
    ));
  }
}

/// @nodoc
class _$GroupPaymentsStatusUpdated implements GroupPaymentsStatusUpdated {
  _$GroupPaymentsStatusUpdated(
      {@required this.groupId,
      @required this.paymentsPastDue,
      @required this.paymentsPaid})
      : assert(groupId != null),
        assert(paymentsPastDue != null),
        assert(paymentsPaid != null);

  @override
  final String groupId;
  @override
  final int paymentsPastDue;
  @override
  final int paymentsPaid;

  @override
  String toString() {
    return 'AdminPaymentsState.groupPaymentsStatusUpdated(groupId: $groupId, paymentsPastDue: $paymentsPastDue, paymentsPaid: $paymentsPaid)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is GroupPaymentsStatusUpdated &&
            (identical(other.groupId, groupId) ||
                const DeepCollectionEquality()
                    .equals(other.groupId, groupId)) &&
            (identical(other.paymentsPastDue, paymentsPastDue) ||
                const DeepCollectionEquality()
                    .equals(other.paymentsPastDue, paymentsPastDue)) &&
            (identical(other.paymentsPaid, paymentsPaid) ||
                const DeepCollectionEquality()
                    .equals(other.paymentsPaid, paymentsPaid)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(groupId) ^
      const DeepCollectionEquality().hash(paymentsPastDue) ^
      const DeepCollectionEquality().hash(paymentsPaid);

  @JsonKey(ignore: true)
  @override
  $GroupPaymentsStatusUpdatedCopyWith<GroupPaymentsStatusUpdated>
      get copyWith =>
          _$GroupPaymentsStatusUpdatedCopyWithImpl<GroupPaymentsStatusUpdated>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult paymentsInvalid(),
    @required TResult paymentsStatusInvalid(),
    @required
        TResult userPaymentsUpdated(String userId, List<Payment> payments),
    @required
        TResult userPaymentsHistoryUpdated(
            String userId, List<Payment> payments),
    @required
        TResult overallPaymentsStatusUpdated(
            int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    @required
        TResult groupPaymentsStatusUpdated(
            String groupId, int paymentsPastDue, int paymentsPaid),
    @required
        TResult userPaymentsStatusUpdated(
            String userId, int paymentsPastDue, int paymentsPaid),
    @required TResult userListUpdated(String groupId, List<User> users),
    @required TResult groupListUpdated(List<Group> groups),
  }) {
    assert(initial != null);
    assert(paymentsInvalid != null);
    assert(paymentsStatusInvalid != null);
    assert(userPaymentsUpdated != null);
    assert(userPaymentsHistoryUpdated != null);
    assert(overallPaymentsStatusUpdated != null);
    assert(groupPaymentsStatusUpdated != null);
    assert(userPaymentsStatusUpdated != null);
    assert(userListUpdated != null);
    assert(groupListUpdated != null);
    return groupPaymentsStatusUpdated(groupId, paymentsPastDue, paymentsPaid);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult paymentsInvalid(),
    TResult paymentsStatusInvalid(),
    TResult userPaymentsUpdated(String userId, List<Payment> payments),
    TResult userPaymentsHistoryUpdated(String userId, List<Payment> payments),
    TResult overallPaymentsStatusUpdated(
        int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    TResult groupPaymentsStatusUpdated(
        String groupId, int paymentsPastDue, int paymentsPaid),
    TResult userPaymentsStatusUpdated(
        String userId, int paymentsPastDue, int paymentsPaid),
    TResult userListUpdated(String groupId, List<User> users),
    TResult groupListUpdated(List<Group> groups),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (groupPaymentsStatusUpdated != null) {
      return groupPaymentsStatusUpdated(groupId, paymentsPastDue, paymentsPaid);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(PaymentsInitial value),
    @required TResult paymentsInvalid(PaymentsInvalid value),
    @required TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    @required TResult userPaymentsUpdated(UserPaymentsUpdated value),
    @required
        TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    @required
        TResult overallPaymentsStatusUpdated(
            OverallPaymentsStatusUpdated value),
    @required
        TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    @required
        TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    @required TResult userListUpdated(UserListUpdated value),
    @required TResult groupListUpdated(GroupListUpdated value),
  }) {
    assert(initial != null);
    assert(paymentsInvalid != null);
    assert(paymentsStatusInvalid != null);
    assert(userPaymentsUpdated != null);
    assert(userPaymentsHistoryUpdated != null);
    assert(overallPaymentsStatusUpdated != null);
    assert(groupPaymentsStatusUpdated != null);
    assert(userPaymentsStatusUpdated != null);
    assert(userListUpdated != null);
    assert(groupListUpdated != null);
    return groupPaymentsStatusUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(PaymentsInitial value),
    TResult paymentsInvalid(PaymentsInvalid value),
    TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    TResult userPaymentsUpdated(UserPaymentsUpdated value),
    TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    TResult overallPaymentsStatusUpdated(OverallPaymentsStatusUpdated value),
    TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    TResult userListUpdated(UserListUpdated value),
    TResult groupListUpdated(GroupListUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (groupPaymentsStatusUpdated != null) {
      return groupPaymentsStatusUpdated(this);
    }
    return orElse();
  }
}

abstract class GroupPaymentsStatusUpdated implements AdminPaymentsState {
  factory GroupPaymentsStatusUpdated(
      {@required String groupId,
      @required int paymentsPastDue,
      @required int paymentsPaid}) = _$GroupPaymentsStatusUpdated;

  String get groupId;
  int get paymentsPastDue;
  int get paymentsPaid;
  @JsonKey(ignore: true)
  $GroupPaymentsStatusUpdatedCopyWith<GroupPaymentsStatusUpdated> get copyWith;
}

/// @nodoc
abstract class $UserPaymentsStatusUpdatedCopyWith<$Res> {
  factory $UserPaymentsStatusUpdatedCopyWith(UserPaymentsStatusUpdated value,
          $Res Function(UserPaymentsStatusUpdated) then) =
      _$UserPaymentsStatusUpdatedCopyWithImpl<$Res>;
  $Res call({String userId, int paymentsPastDue, int paymentsPaid});
}

/// @nodoc
class _$UserPaymentsStatusUpdatedCopyWithImpl<$Res>
    extends _$AdminPaymentsStateCopyWithImpl<$Res>
    implements $UserPaymentsStatusUpdatedCopyWith<$Res> {
  _$UserPaymentsStatusUpdatedCopyWithImpl(UserPaymentsStatusUpdated _value,
      $Res Function(UserPaymentsStatusUpdated) _then)
      : super(_value, (v) => _then(v as UserPaymentsStatusUpdated));

  @override
  UserPaymentsStatusUpdated get _value =>
      super._value as UserPaymentsStatusUpdated;

  @override
  $Res call({
    Object userId = freezed,
    Object paymentsPastDue = freezed,
    Object paymentsPaid = freezed,
  }) {
    return _then(UserPaymentsStatusUpdated(
      userId: userId == freezed ? _value.userId : userId as String,
      paymentsPastDue: paymentsPastDue == freezed
          ? _value.paymentsPastDue
          : paymentsPastDue as int,
      paymentsPaid:
          paymentsPaid == freezed ? _value.paymentsPaid : paymentsPaid as int,
    ));
  }
}

/// @nodoc
class _$UserPaymentsStatusUpdated implements UserPaymentsStatusUpdated {
  _$UserPaymentsStatusUpdated(
      {@required this.userId,
      @required this.paymentsPastDue,
      @required this.paymentsPaid})
      : assert(userId != null),
        assert(paymentsPastDue != null),
        assert(paymentsPaid != null);

  @override
  final String userId;
  @override
  final int paymentsPastDue;
  @override
  final int paymentsPaid;

  @override
  String toString() {
    return 'AdminPaymentsState.userPaymentsStatusUpdated(userId: $userId, paymentsPastDue: $paymentsPastDue, paymentsPaid: $paymentsPaid)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UserPaymentsStatusUpdated &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)) &&
            (identical(other.paymentsPastDue, paymentsPastDue) ||
                const DeepCollectionEquality()
                    .equals(other.paymentsPastDue, paymentsPastDue)) &&
            (identical(other.paymentsPaid, paymentsPaid) ||
                const DeepCollectionEquality()
                    .equals(other.paymentsPaid, paymentsPaid)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(userId) ^
      const DeepCollectionEquality().hash(paymentsPastDue) ^
      const DeepCollectionEquality().hash(paymentsPaid);

  @JsonKey(ignore: true)
  @override
  $UserPaymentsStatusUpdatedCopyWith<UserPaymentsStatusUpdated> get copyWith =>
      _$UserPaymentsStatusUpdatedCopyWithImpl<UserPaymentsStatusUpdated>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult paymentsInvalid(),
    @required TResult paymentsStatusInvalid(),
    @required
        TResult userPaymentsUpdated(String userId, List<Payment> payments),
    @required
        TResult userPaymentsHistoryUpdated(
            String userId, List<Payment> payments),
    @required
        TResult overallPaymentsStatusUpdated(
            int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    @required
        TResult groupPaymentsStatusUpdated(
            String groupId, int paymentsPastDue, int paymentsPaid),
    @required
        TResult userPaymentsStatusUpdated(
            String userId, int paymentsPastDue, int paymentsPaid),
    @required TResult userListUpdated(String groupId, List<User> users),
    @required TResult groupListUpdated(List<Group> groups),
  }) {
    assert(initial != null);
    assert(paymentsInvalid != null);
    assert(paymentsStatusInvalid != null);
    assert(userPaymentsUpdated != null);
    assert(userPaymentsHistoryUpdated != null);
    assert(overallPaymentsStatusUpdated != null);
    assert(groupPaymentsStatusUpdated != null);
    assert(userPaymentsStatusUpdated != null);
    assert(userListUpdated != null);
    assert(groupListUpdated != null);
    return userPaymentsStatusUpdated(userId, paymentsPastDue, paymentsPaid);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult paymentsInvalid(),
    TResult paymentsStatusInvalid(),
    TResult userPaymentsUpdated(String userId, List<Payment> payments),
    TResult userPaymentsHistoryUpdated(String userId, List<Payment> payments),
    TResult overallPaymentsStatusUpdated(
        int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    TResult groupPaymentsStatusUpdated(
        String groupId, int paymentsPastDue, int paymentsPaid),
    TResult userPaymentsStatusUpdated(
        String userId, int paymentsPastDue, int paymentsPaid),
    TResult userListUpdated(String groupId, List<User> users),
    TResult groupListUpdated(List<Group> groups),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (userPaymentsStatusUpdated != null) {
      return userPaymentsStatusUpdated(userId, paymentsPastDue, paymentsPaid);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(PaymentsInitial value),
    @required TResult paymentsInvalid(PaymentsInvalid value),
    @required TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    @required TResult userPaymentsUpdated(UserPaymentsUpdated value),
    @required
        TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    @required
        TResult overallPaymentsStatusUpdated(
            OverallPaymentsStatusUpdated value),
    @required
        TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    @required
        TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    @required TResult userListUpdated(UserListUpdated value),
    @required TResult groupListUpdated(GroupListUpdated value),
  }) {
    assert(initial != null);
    assert(paymentsInvalid != null);
    assert(paymentsStatusInvalid != null);
    assert(userPaymentsUpdated != null);
    assert(userPaymentsHistoryUpdated != null);
    assert(overallPaymentsStatusUpdated != null);
    assert(groupPaymentsStatusUpdated != null);
    assert(userPaymentsStatusUpdated != null);
    assert(userListUpdated != null);
    assert(groupListUpdated != null);
    return userPaymentsStatusUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(PaymentsInitial value),
    TResult paymentsInvalid(PaymentsInvalid value),
    TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    TResult userPaymentsUpdated(UserPaymentsUpdated value),
    TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    TResult overallPaymentsStatusUpdated(OverallPaymentsStatusUpdated value),
    TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    TResult userListUpdated(UserListUpdated value),
    TResult groupListUpdated(GroupListUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (userPaymentsStatusUpdated != null) {
      return userPaymentsStatusUpdated(this);
    }
    return orElse();
  }
}

abstract class UserPaymentsStatusUpdated implements AdminPaymentsState {
  factory UserPaymentsStatusUpdated(
      {@required String userId,
      @required int paymentsPastDue,
      @required int paymentsPaid}) = _$UserPaymentsStatusUpdated;

  String get userId;
  int get paymentsPastDue;
  int get paymentsPaid;
  @JsonKey(ignore: true)
  $UserPaymentsStatusUpdatedCopyWith<UserPaymentsStatusUpdated> get copyWith;
}

/// @nodoc
abstract class $UserListUpdatedCopyWith<$Res> {
  factory $UserListUpdatedCopyWith(
          UserListUpdated value, $Res Function(UserListUpdated) then) =
      _$UserListUpdatedCopyWithImpl<$Res>;
  $Res call({String groupId, List<User> users});
}

/// @nodoc
class _$UserListUpdatedCopyWithImpl<$Res>
    extends _$AdminPaymentsStateCopyWithImpl<$Res>
    implements $UserListUpdatedCopyWith<$Res> {
  _$UserListUpdatedCopyWithImpl(
      UserListUpdated _value, $Res Function(UserListUpdated) _then)
      : super(_value, (v) => _then(v as UserListUpdated));

  @override
  UserListUpdated get _value => super._value as UserListUpdated;

  @override
  $Res call({
    Object groupId = freezed,
    Object users = freezed,
  }) {
    return _then(UserListUpdated(
      groupId: groupId == freezed ? _value.groupId : groupId as String,
      users: users == freezed ? _value.users : users as List<User>,
    ));
  }
}

/// @nodoc
class _$UserListUpdated implements UserListUpdated {
  _$UserListUpdated({@required this.groupId, @required this.users})
      : assert(groupId != null),
        assert(users != null);

  @override
  final String groupId;
  @override
  final List<User> users;

  @override
  String toString() {
    return 'AdminPaymentsState.userListUpdated(groupId: $groupId, users: $users)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UserListUpdated &&
            (identical(other.groupId, groupId) ||
                const DeepCollectionEquality()
                    .equals(other.groupId, groupId)) &&
            (identical(other.users, users) ||
                const DeepCollectionEquality().equals(other.users, users)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(groupId) ^
      const DeepCollectionEquality().hash(users);

  @JsonKey(ignore: true)
  @override
  $UserListUpdatedCopyWith<UserListUpdated> get copyWith =>
      _$UserListUpdatedCopyWithImpl<UserListUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult paymentsInvalid(),
    @required TResult paymentsStatusInvalid(),
    @required
        TResult userPaymentsUpdated(String userId, List<Payment> payments),
    @required
        TResult userPaymentsHistoryUpdated(
            String userId, List<Payment> payments),
    @required
        TResult overallPaymentsStatusUpdated(
            int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    @required
        TResult groupPaymentsStatusUpdated(
            String groupId, int paymentsPastDue, int paymentsPaid),
    @required
        TResult userPaymentsStatusUpdated(
            String userId, int paymentsPastDue, int paymentsPaid),
    @required TResult userListUpdated(String groupId, List<User> users),
    @required TResult groupListUpdated(List<Group> groups),
  }) {
    assert(initial != null);
    assert(paymentsInvalid != null);
    assert(paymentsStatusInvalid != null);
    assert(userPaymentsUpdated != null);
    assert(userPaymentsHistoryUpdated != null);
    assert(overallPaymentsStatusUpdated != null);
    assert(groupPaymentsStatusUpdated != null);
    assert(userPaymentsStatusUpdated != null);
    assert(userListUpdated != null);
    assert(groupListUpdated != null);
    return userListUpdated(groupId, users);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult paymentsInvalid(),
    TResult paymentsStatusInvalid(),
    TResult userPaymentsUpdated(String userId, List<Payment> payments),
    TResult userPaymentsHistoryUpdated(String userId, List<Payment> payments),
    TResult overallPaymentsStatusUpdated(
        int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    TResult groupPaymentsStatusUpdated(
        String groupId, int paymentsPastDue, int paymentsPaid),
    TResult userPaymentsStatusUpdated(
        String userId, int paymentsPastDue, int paymentsPaid),
    TResult userListUpdated(String groupId, List<User> users),
    TResult groupListUpdated(List<Group> groups),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (userListUpdated != null) {
      return userListUpdated(groupId, users);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(PaymentsInitial value),
    @required TResult paymentsInvalid(PaymentsInvalid value),
    @required TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    @required TResult userPaymentsUpdated(UserPaymentsUpdated value),
    @required
        TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    @required
        TResult overallPaymentsStatusUpdated(
            OverallPaymentsStatusUpdated value),
    @required
        TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    @required
        TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    @required TResult userListUpdated(UserListUpdated value),
    @required TResult groupListUpdated(GroupListUpdated value),
  }) {
    assert(initial != null);
    assert(paymentsInvalid != null);
    assert(paymentsStatusInvalid != null);
    assert(userPaymentsUpdated != null);
    assert(userPaymentsHistoryUpdated != null);
    assert(overallPaymentsStatusUpdated != null);
    assert(groupPaymentsStatusUpdated != null);
    assert(userPaymentsStatusUpdated != null);
    assert(userListUpdated != null);
    assert(groupListUpdated != null);
    return userListUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(PaymentsInitial value),
    TResult paymentsInvalid(PaymentsInvalid value),
    TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    TResult userPaymentsUpdated(UserPaymentsUpdated value),
    TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    TResult overallPaymentsStatusUpdated(OverallPaymentsStatusUpdated value),
    TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    TResult userListUpdated(UserListUpdated value),
    TResult groupListUpdated(GroupListUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (userListUpdated != null) {
      return userListUpdated(this);
    }
    return orElse();
  }
}

abstract class UserListUpdated implements AdminPaymentsState {
  factory UserListUpdated(
      {@required String groupId,
      @required List<User> users}) = _$UserListUpdated;

  String get groupId;
  List<User> get users;
  @JsonKey(ignore: true)
  $UserListUpdatedCopyWith<UserListUpdated> get copyWith;
}

/// @nodoc
abstract class $GroupListUpdatedCopyWith<$Res> {
  factory $GroupListUpdatedCopyWith(
          GroupListUpdated value, $Res Function(GroupListUpdated) then) =
      _$GroupListUpdatedCopyWithImpl<$Res>;
  $Res call({List<Group> groups});
}

/// @nodoc
class _$GroupListUpdatedCopyWithImpl<$Res>
    extends _$AdminPaymentsStateCopyWithImpl<$Res>
    implements $GroupListUpdatedCopyWith<$Res> {
  _$GroupListUpdatedCopyWithImpl(
      GroupListUpdated _value, $Res Function(GroupListUpdated) _then)
      : super(_value, (v) => _then(v as GroupListUpdated));

  @override
  GroupListUpdated get _value => super._value as GroupListUpdated;

  @override
  $Res call({
    Object groups = freezed,
  }) {
    return _then(GroupListUpdated(
      groups: groups == freezed ? _value.groups : groups as List<Group>,
    ));
  }
}

/// @nodoc
class _$GroupListUpdated implements GroupListUpdated {
  _$GroupListUpdated({@required this.groups}) : assert(groups != null);

  @override
  final List<Group> groups;

  @override
  String toString() {
    return 'AdminPaymentsState.groupListUpdated(groups: $groups)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is GroupListUpdated &&
            (identical(other.groups, groups) ||
                const DeepCollectionEquality().equals(other.groups, groups)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(groups);

  @JsonKey(ignore: true)
  @override
  $GroupListUpdatedCopyWith<GroupListUpdated> get copyWith =>
      _$GroupListUpdatedCopyWithImpl<GroupListUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult paymentsInvalid(),
    @required TResult paymentsStatusInvalid(),
    @required
        TResult userPaymentsUpdated(String userId, List<Payment> payments),
    @required
        TResult userPaymentsHistoryUpdated(
            String userId, List<Payment> payments),
    @required
        TResult overallPaymentsStatusUpdated(
            int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    @required
        TResult groupPaymentsStatusUpdated(
            String groupId, int paymentsPastDue, int paymentsPaid),
    @required
        TResult userPaymentsStatusUpdated(
            String userId, int paymentsPastDue, int paymentsPaid),
    @required TResult userListUpdated(String groupId, List<User> users),
    @required TResult groupListUpdated(List<Group> groups),
  }) {
    assert(initial != null);
    assert(paymentsInvalid != null);
    assert(paymentsStatusInvalid != null);
    assert(userPaymentsUpdated != null);
    assert(userPaymentsHistoryUpdated != null);
    assert(overallPaymentsStatusUpdated != null);
    assert(groupPaymentsStatusUpdated != null);
    assert(userPaymentsStatusUpdated != null);
    assert(userListUpdated != null);
    assert(groupListUpdated != null);
    return groupListUpdated(groups);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult paymentsInvalid(),
    TResult paymentsStatusInvalid(),
    TResult userPaymentsUpdated(String userId, List<Payment> payments),
    TResult userPaymentsHistoryUpdated(String userId, List<Payment> payments),
    TResult overallPaymentsStatusUpdated(
        int paymentsPastDue, int paymentsPaid, int paymentsApproachingDue),
    TResult groupPaymentsStatusUpdated(
        String groupId, int paymentsPastDue, int paymentsPaid),
    TResult userPaymentsStatusUpdated(
        String userId, int paymentsPastDue, int paymentsPaid),
    TResult userListUpdated(String groupId, List<User> users),
    TResult groupListUpdated(List<Group> groups),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (groupListUpdated != null) {
      return groupListUpdated(groups);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(PaymentsInitial value),
    @required TResult paymentsInvalid(PaymentsInvalid value),
    @required TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    @required TResult userPaymentsUpdated(UserPaymentsUpdated value),
    @required
        TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    @required
        TResult overallPaymentsStatusUpdated(
            OverallPaymentsStatusUpdated value),
    @required
        TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    @required
        TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    @required TResult userListUpdated(UserListUpdated value),
    @required TResult groupListUpdated(GroupListUpdated value),
  }) {
    assert(initial != null);
    assert(paymentsInvalid != null);
    assert(paymentsStatusInvalid != null);
    assert(userPaymentsUpdated != null);
    assert(userPaymentsHistoryUpdated != null);
    assert(overallPaymentsStatusUpdated != null);
    assert(groupPaymentsStatusUpdated != null);
    assert(userPaymentsStatusUpdated != null);
    assert(userListUpdated != null);
    assert(groupListUpdated != null);
    return groupListUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(PaymentsInitial value),
    TResult paymentsInvalid(PaymentsInvalid value),
    TResult paymentsStatusInvalid(PaymentsStatusInvalid value),
    TResult userPaymentsUpdated(UserPaymentsUpdated value),
    TResult userPaymentsHistoryUpdated(UserPaymentsHistoryUpdated value),
    TResult overallPaymentsStatusUpdated(OverallPaymentsStatusUpdated value),
    TResult groupPaymentsStatusUpdated(GroupPaymentsStatusUpdated value),
    TResult userPaymentsStatusUpdated(UserPaymentsStatusUpdated value),
    TResult userListUpdated(UserListUpdated value),
    TResult groupListUpdated(GroupListUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (groupListUpdated != null) {
      return groupListUpdated(this);
    }
    return orElse();
  }
}

abstract class GroupListUpdated implements AdminPaymentsState {
  factory GroupListUpdated({@required List<Group> groups}) = _$GroupListUpdated;

  List<Group> get groups;
  @JsonKey(ignore: true)
  $GroupListUpdatedCopyWith<GroupListUpdated> get copyWith;
}
