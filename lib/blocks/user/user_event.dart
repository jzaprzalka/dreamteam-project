part of 'user_bloc.dart';

@freezed
abstract class UserEvent with _$UserEvent {
  const factory UserEvent.updatedEmail(
      {@required String email,
      @required String password,
      @required String newEmail}) = UpdatedEmail;
  const factory UserEvent.updatedPassword(
      {@required String email,
      @required String password,
      @required String newPassword}) = UpdatedPassword;
  const factory UserEvent.updatedName(
      {@required String userId, @required String name}) = UpdatedName;
  const factory UserEvent.updatedSurname(
      {@required String userId, @required String surname}) = UpdatedSurname;
  const factory UserEvent.updatedPhone(
      {@required String userId,
      @required String phoneNumber}) = UpdatedPhoneNumber;
}
