import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/repositories/authentication_repository.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase;
import 'package:dreamteam_project/repositories/user_data_repository.dart';

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';

part 'user_event.dart';
part 'user_state.dart';

part 'user_bloc.freezed.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  UserDataRepository userDataRepository = UserDataRepository();
  AuthenticationRepository authenticationRepository =
      AuthenticationRepository();

  UserBloc() : super(UserState.userInitial());

  Future<User> _getUser() async {
    User user;
    try {
      firebase.User firebaseUser =
          await authenticationRepository.getCurrentUser();
      user = await userDataRepository.getUser(firebaseUser.uid);
    } catch (e) {
      logger.e("Error occurred in loading user from server: $e");
      return null;
    }
    return user;
  }

  @override
  Stream<UserState> mapEventToState(UserEvent event) async* {
    User user;
    try {
      event.map(
        updatedEmail: (e) =>
            userDataRepository.updateEmail(e.email, e.password, e.newEmail),
        updatedPassword: (e) => userDataRepository.updatePassword(
            e.email, e.password, e.newPassword),
        updatedName: (e) => userDataRepository.updateName(e.userId, e.name),
        updatedSurname: (e) =>
            userDataRepository.updateSurname(e.userId, e.surname),
        updatedPhone: (e) =>
            userDataRepository.updatePhone(e.userId, e.phoneNumber),
      );
      user = await _getUser();
      yield UserState.userUpdated(user: user, isUpdated: true);
    } catch (e) {
      logger.e("Error occurred in mapping User event to user state: $e");
    }
  }
}
