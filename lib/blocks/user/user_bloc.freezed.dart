// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'user_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$UserEventTearOff {
  const _$UserEventTearOff();

// ignore: unused_element
  UpdatedEmail updatedEmail(
      {@required String email,
      @required String password,
      @required String newEmail}) {
    return UpdatedEmail(
      email: email,
      password: password,
      newEmail: newEmail,
    );
  }

// ignore: unused_element
  UpdatedPassword updatedPassword(
      {@required String email,
      @required String password,
      @required String newPassword}) {
    return UpdatedPassword(
      email: email,
      password: password,
      newPassword: newPassword,
    );
  }

// ignore: unused_element
  UpdatedName updatedName({@required String userId, @required String name}) {
    return UpdatedName(
      userId: userId,
      name: name,
    );
  }

// ignore: unused_element
  UpdatedSurname updatedSurname(
      {@required String userId, @required String surname}) {
    return UpdatedSurname(
      userId: userId,
      surname: surname,
    );
  }

// ignore: unused_element
  UpdatedPhoneNumber updatedPhone(
      {@required String userId, @required String phoneNumber}) {
    return UpdatedPhoneNumber(
      userId: userId,
      phoneNumber: phoneNumber,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $UserEvent = _$UserEventTearOff();

/// @nodoc
mixin _$UserEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required
        TResult updatedEmail(String email, String password, String newEmail),
    @required
        TResult updatedPassword(
            String email, String password, String newPassword),
    @required TResult updatedName(String userId, String name),
    @required TResult updatedSurname(String userId, String surname),
    @required TResult updatedPhone(String userId, String phoneNumber),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updatedEmail(String email, String password, String newEmail),
    TResult updatedPassword(String email, String password, String newPassword),
    TResult updatedName(String userId, String name),
    TResult updatedSurname(String userId, String surname),
    TResult updatedPhone(String userId, String phoneNumber),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updatedEmail(UpdatedEmail value),
    @required TResult updatedPassword(UpdatedPassword value),
    @required TResult updatedName(UpdatedName value),
    @required TResult updatedSurname(UpdatedSurname value),
    @required TResult updatedPhone(UpdatedPhoneNumber value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updatedEmail(UpdatedEmail value),
    TResult updatedPassword(UpdatedPassword value),
    TResult updatedName(UpdatedName value),
    TResult updatedSurname(UpdatedSurname value),
    TResult updatedPhone(UpdatedPhoneNumber value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $UserEventCopyWith<$Res> {
  factory $UserEventCopyWith(UserEvent value, $Res Function(UserEvent) then) =
      _$UserEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$UserEventCopyWithImpl<$Res> implements $UserEventCopyWith<$Res> {
  _$UserEventCopyWithImpl(this._value, this._then);

  final UserEvent _value;
  // ignore: unused_field
  final $Res Function(UserEvent) _then;
}

/// @nodoc
abstract class $UpdatedEmailCopyWith<$Res> {
  factory $UpdatedEmailCopyWith(
          UpdatedEmail value, $Res Function(UpdatedEmail) then) =
      _$UpdatedEmailCopyWithImpl<$Res>;
  $Res call({String email, String password, String newEmail});
}

/// @nodoc
class _$UpdatedEmailCopyWithImpl<$Res> extends _$UserEventCopyWithImpl<$Res>
    implements $UpdatedEmailCopyWith<$Res> {
  _$UpdatedEmailCopyWithImpl(
      UpdatedEmail _value, $Res Function(UpdatedEmail) _then)
      : super(_value, (v) => _then(v as UpdatedEmail));

  @override
  UpdatedEmail get _value => super._value as UpdatedEmail;

  @override
  $Res call({
    Object email = freezed,
    Object password = freezed,
    Object newEmail = freezed,
  }) {
    return _then(UpdatedEmail(
      email: email == freezed ? _value.email : email as String,
      password: password == freezed ? _value.password : password as String,
      newEmail: newEmail == freezed ? _value.newEmail : newEmail as String,
    ));
  }
}

/// @nodoc
class _$UpdatedEmail implements UpdatedEmail {
  const _$UpdatedEmail(
      {@required this.email, @required this.password, @required this.newEmail})
      : assert(email != null),
        assert(password != null),
        assert(newEmail != null);

  @override
  final String email;
  @override
  final String password;
  @override
  final String newEmail;

  @override
  String toString() {
    return 'UserEvent.updatedEmail(email: $email, password: $password, newEmail: $newEmail)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UpdatedEmail &&
            (identical(other.email, email) ||
                const DeepCollectionEquality().equals(other.email, email)) &&
            (identical(other.password, password) ||
                const DeepCollectionEquality()
                    .equals(other.password, password)) &&
            (identical(other.newEmail, newEmail) ||
                const DeepCollectionEquality()
                    .equals(other.newEmail, newEmail)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(email) ^
      const DeepCollectionEquality().hash(password) ^
      const DeepCollectionEquality().hash(newEmail);

  @JsonKey(ignore: true)
  @override
  $UpdatedEmailCopyWith<UpdatedEmail> get copyWith =>
      _$UpdatedEmailCopyWithImpl<UpdatedEmail>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required
        TResult updatedEmail(String email, String password, String newEmail),
    @required
        TResult updatedPassword(
            String email, String password, String newPassword),
    @required TResult updatedName(String userId, String name),
    @required TResult updatedSurname(String userId, String surname),
    @required TResult updatedPhone(String userId, String phoneNumber),
  }) {
    assert(updatedEmail != null);
    assert(updatedPassword != null);
    assert(updatedName != null);
    assert(updatedSurname != null);
    assert(updatedPhone != null);
    return updatedEmail(email, password, newEmail);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updatedEmail(String email, String password, String newEmail),
    TResult updatedPassword(String email, String password, String newPassword),
    TResult updatedName(String userId, String name),
    TResult updatedSurname(String userId, String surname),
    TResult updatedPhone(String userId, String phoneNumber),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedEmail != null) {
      return updatedEmail(email, password, newEmail);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updatedEmail(UpdatedEmail value),
    @required TResult updatedPassword(UpdatedPassword value),
    @required TResult updatedName(UpdatedName value),
    @required TResult updatedSurname(UpdatedSurname value),
    @required TResult updatedPhone(UpdatedPhoneNumber value),
  }) {
    assert(updatedEmail != null);
    assert(updatedPassword != null);
    assert(updatedName != null);
    assert(updatedSurname != null);
    assert(updatedPhone != null);
    return updatedEmail(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updatedEmail(UpdatedEmail value),
    TResult updatedPassword(UpdatedPassword value),
    TResult updatedName(UpdatedName value),
    TResult updatedSurname(UpdatedSurname value),
    TResult updatedPhone(UpdatedPhoneNumber value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedEmail != null) {
      return updatedEmail(this);
    }
    return orElse();
  }
}

abstract class UpdatedEmail implements UserEvent {
  const factory UpdatedEmail(
      {@required String email,
      @required String password,
      @required String newEmail}) = _$UpdatedEmail;

  String get email;
  String get password;
  String get newEmail;
  @JsonKey(ignore: true)
  $UpdatedEmailCopyWith<UpdatedEmail> get copyWith;
}

/// @nodoc
abstract class $UpdatedPasswordCopyWith<$Res> {
  factory $UpdatedPasswordCopyWith(
          UpdatedPassword value, $Res Function(UpdatedPassword) then) =
      _$UpdatedPasswordCopyWithImpl<$Res>;
  $Res call({String email, String password, String newPassword});
}

/// @nodoc
class _$UpdatedPasswordCopyWithImpl<$Res> extends _$UserEventCopyWithImpl<$Res>
    implements $UpdatedPasswordCopyWith<$Res> {
  _$UpdatedPasswordCopyWithImpl(
      UpdatedPassword _value, $Res Function(UpdatedPassword) _then)
      : super(_value, (v) => _then(v as UpdatedPassword));

  @override
  UpdatedPassword get _value => super._value as UpdatedPassword;

  @override
  $Res call({
    Object email = freezed,
    Object password = freezed,
    Object newPassword = freezed,
  }) {
    return _then(UpdatedPassword(
      email: email == freezed ? _value.email : email as String,
      password: password == freezed ? _value.password : password as String,
      newPassword:
          newPassword == freezed ? _value.newPassword : newPassword as String,
    ));
  }
}

/// @nodoc
class _$UpdatedPassword implements UpdatedPassword {
  const _$UpdatedPassword(
      {@required this.email,
      @required this.password,
      @required this.newPassword})
      : assert(email != null),
        assert(password != null),
        assert(newPassword != null);

  @override
  final String email;
  @override
  final String password;
  @override
  final String newPassword;

  @override
  String toString() {
    return 'UserEvent.updatedPassword(email: $email, password: $password, newPassword: $newPassword)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UpdatedPassword &&
            (identical(other.email, email) ||
                const DeepCollectionEquality().equals(other.email, email)) &&
            (identical(other.password, password) ||
                const DeepCollectionEquality()
                    .equals(other.password, password)) &&
            (identical(other.newPassword, newPassword) ||
                const DeepCollectionEquality()
                    .equals(other.newPassword, newPassword)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(email) ^
      const DeepCollectionEquality().hash(password) ^
      const DeepCollectionEquality().hash(newPassword);

  @JsonKey(ignore: true)
  @override
  $UpdatedPasswordCopyWith<UpdatedPassword> get copyWith =>
      _$UpdatedPasswordCopyWithImpl<UpdatedPassword>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required
        TResult updatedEmail(String email, String password, String newEmail),
    @required
        TResult updatedPassword(
            String email, String password, String newPassword),
    @required TResult updatedName(String userId, String name),
    @required TResult updatedSurname(String userId, String surname),
    @required TResult updatedPhone(String userId, String phoneNumber),
  }) {
    assert(updatedEmail != null);
    assert(updatedPassword != null);
    assert(updatedName != null);
    assert(updatedSurname != null);
    assert(updatedPhone != null);
    return updatedPassword(email, password, newPassword);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updatedEmail(String email, String password, String newEmail),
    TResult updatedPassword(String email, String password, String newPassword),
    TResult updatedName(String userId, String name),
    TResult updatedSurname(String userId, String surname),
    TResult updatedPhone(String userId, String phoneNumber),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedPassword != null) {
      return updatedPassword(email, password, newPassword);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updatedEmail(UpdatedEmail value),
    @required TResult updatedPassword(UpdatedPassword value),
    @required TResult updatedName(UpdatedName value),
    @required TResult updatedSurname(UpdatedSurname value),
    @required TResult updatedPhone(UpdatedPhoneNumber value),
  }) {
    assert(updatedEmail != null);
    assert(updatedPassword != null);
    assert(updatedName != null);
    assert(updatedSurname != null);
    assert(updatedPhone != null);
    return updatedPassword(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updatedEmail(UpdatedEmail value),
    TResult updatedPassword(UpdatedPassword value),
    TResult updatedName(UpdatedName value),
    TResult updatedSurname(UpdatedSurname value),
    TResult updatedPhone(UpdatedPhoneNumber value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedPassword != null) {
      return updatedPassword(this);
    }
    return orElse();
  }
}

abstract class UpdatedPassword implements UserEvent {
  const factory UpdatedPassword(
      {@required String email,
      @required String password,
      @required String newPassword}) = _$UpdatedPassword;

  String get email;
  String get password;
  String get newPassword;
  @JsonKey(ignore: true)
  $UpdatedPasswordCopyWith<UpdatedPassword> get copyWith;
}

/// @nodoc
abstract class $UpdatedNameCopyWith<$Res> {
  factory $UpdatedNameCopyWith(
          UpdatedName value, $Res Function(UpdatedName) then) =
      _$UpdatedNameCopyWithImpl<$Res>;
  $Res call({String userId, String name});
}

/// @nodoc
class _$UpdatedNameCopyWithImpl<$Res> extends _$UserEventCopyWithImpl<$Res>
    implements $UpdatedNameCopyWith<$Res> {
  _$UpdatedNameCopyWithImpl(
      UpdatedName _value, $Res Function(UpdatedName) _then)
      : super(_value, (v) => _then(v as UpdatedName));

  @override
  UpdatedName get _value => super._value as UpdatedName;

  @override
  $Res call({
    Object userId = freezed,
    Object name = freezed,
  }) {
    return _then(UpdatedName(
      userId: userId == freezed ? _value.userId : userId as String,
      name: name == freezed ? _value.name : name as String,
    ));
  }
}

/// @nodoc
class _$UpdatedName implements UpdatedName {
  const _$UpdatedName({@required this.userId, @required this.name})
      : assert(userId != null),
        assert(name != null);

  @override
  final String userId;
  @override
  final String name;

  @override
  String toString() {
    return 'UserEvent.updatedName(userId: $userId, name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UpdatedName &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(userId) ^
      const DeepCollectionEquality().hash(name);

  @JsonKey(ignore: true)
  @override
  $UpdatedNameCopyWith<UpdatedName> get copyWith =>
      _$UpdatedNameCopyWithImpl<UpdatedName>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required
        TResult updatedEmail(String email, String password, String newEmail),
    @required
        TResult updatedPassword(
            String email, String password, String newPassword),
    @required TResult updatedName(String userId, String name),
    @required TResult updatedSurname(String userId, String surname),
    @required TResult updatedPhone(String userId, String phoneNumber),
  }) {
    assert(updatedEmail != null);
    assert(updatedPassword != null);
    assert(updatedName != null);
    assert(updatedSurname != null);
    assert(updatedPhone != null);
    return updatedName(userId, name);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updatedEmail(String email, String password, String newEmail),
    TResult updatedPassword(String email, String password, String newPassword),
    TResult updatedName(String userId, String name),
    TResult updatedSurname(String userId, String surname),
    TResult updatedPhone(String userId, String phoneNumber),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedName != null) {
      return updatedName(userId, name);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updatedEmail(UpdatedEmail value),
    @required TResult updatedPassword(UpdatedPassword value),
    @required TResult updatedName(UpdatedName value),
    @required TResult updatedSurname(UpdatedSurname value),
    @required TResult updatedPhone(UpdatedPhoneNumber value),
  }) {
    assert(updatedEmail != null);
    assert(updatedPassword != null);
    assert(updatedName != null);
    assert(updatedSurname != null);
    assert(updatedPhone != null);
    return updatedName(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updatedEmail(UpdatedEmail value),
    TResult updatedPassword(UpdatedPassword value),
    TResult updatedName(UpdatedName value),
    TResult updatedSurname(UpdatedSurname value),
    TResult updatedPhone(UpdatedPhoneNumber value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedName != null) {
      return updatedName(this);
    }
    return orElse();
  }
}

abstract class UpdatedName implements UserEvent {
  const factory UpdatedName({@required String userId, @required String name}) =
      _$UpdatedName;

  String get userId;
  String get name;
  @JsonKey(ignore: true)
  $UpdatedNameCopyWith<UpdatedName> get copyWith;
}

/// @nodoc
abstract class $UpdatedSurnameCopyWith<$Res> {
  factory $UpdatedSurnameCopyWith(
          UpdatedSurname value, $Res Function(UpdatedSurname) then) =
      _$UpdatedSurnameCopyWithImpl<$Res>;
  $Res call({String userId, String surname});
}

/// @nodoc
class _$UpdatedSurnameCopyWithImpl<$Res> extends _$UserEventCopyWithImpl<$Res>
    implements $UpdatedSurnameCopyWith<$Res> {
  _$UpdatedSurnameCopyWithImpl(
      UpdatedSurname _value, $Res Function(UpdatedSurname) _then)
      : super(_value, (v) => _then(v as UpdatedSurname));

  @override
  UpdatedSurname get _value => super._value as UpdatedSurname;

  @override
  $Res call({
    Object userId = freezed,
    Object surname = freezed,
  }) {
    return _then(UpdatedSurname(
      userId: userId == freezed ? _value.userId : userId as String,
      surname: surname == freezed ? _value.surname : surname as String,
    ));
  }
}

/// @nodoc
class _$UpdatedSurname implements UpdatedSurname {
  const _$UpdatedSurname({@required this.userId, @required this.surname})
      : assert(userId != null),
        assert(surname != null);

  @override
  final String userId;
  @override
  final String surname;

  @override
  String toString() {
    return 'UserEvent.updatedSurname(userId: $userId, surname: $surname)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UpdatedSurname &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)) &&
            (identical(other.surname, surname) ||
                const DeepCollectionEquality().equals(other.surname, surname)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(userId) ^
      const DeepCollectionEquality().hash(surname);

  @JsonKey(ignore: true)
  @override
  $UpdatedSurnameCopyWith<UpdatedSurname> get copyWith =>
      _$UpdatedSurnameCopyWithImpl<UpdatedSurname>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required
        TResult updatedEmail(String email, String password, String newEmail),
    @required
        TResult updatedPassword(
            String email, String password, String newPassword),
    @required TResult updatedName(String userId, String name),
    @required TResult updatedSurname(String userId, String surname),
    @required TResult updatedPhone(String userId, String phoneNumber),
  }) {
    assert(updatedEmail != null);
    assert(updatedPassword != null);
    assert(updatedName != null);
    assert(updatedSurname != null);
    assert(updatedPhone != null);
    return updatedSurname(userId, surname);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updatedEmail(String email, String password, String newEmail),
    TResult updatedPassword(String email, String password, String newPassword),
    TResult updatedName(String userId, String name),
    TResult updatedSurname(String userId, String surname),
    TResult updatedPhone(String userId, String phoneNumber),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedSurname != null) {
      return updatedSurname(userId, surname);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updatedEmail(UpdatedEmail value),
    @required TResult updatedPassword(UpdatedPassword value),
    @required TResult updatedName(UpdatedName value),
    @required TResult updatedSurname(UpdatedSurname value),
    @required TResult updatedPhone(UpdatedPhoneNumber value),
  }) {
    assert(updatedEmail != null);
    assert(updatedPassword != null);
    assert(updatedName != null);
    assert(updatedSurname != null);
    assert(updatedPhone != null);
    return updatedSurname(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updatedEmail(UpdatedEmail value),
    TResult updatedPassword(UpdatedPassword value),
    TResult updatedName(UpdatedName value),
    TResult updatedSurname(UpdatedSurname value),
    TResult updatedPhone(UpdatedPhoneNumber value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedSurname != null) {
      return updatedSurname(this);
    }
    return orElse();
  }
}

abstract class UpdatedSurname implements UserEvent {
  const factory UpdatedSurname(
      {@required String userId, @required String surname}) = _$UpdatedSurname;

  String get userId;
  String get surname;
  @JsonKey(ignore: true)
  $UpdatedSurnameCopyWith<UpdatedSurname> get copyWith;
}

/// @nodoc
abstract class $UpdatedPhoneNumberCopyWith<$Res> {
  factory $UpdatedPhoneNumberCopyWith(
          UpdatedPhoneNumber value, $Res Function(UpdatedPhoneNumber) then) =
      _$UpdatedPhoneNumberCopyWithImpl<$Res>;
  $Res call({String userId, String phoneNumber});
}

/// @nodoc
class _$UpdatedPhoneNumberCopyWithImpl<$Res>
    extends _$UserEventCopyWithImpl<$Res>
    implements $UpdatedPhoneNumberCopyWith<$Res> {
  _$UpdatedPhoneNumberCopyWithImpl(
      UpdatedPhoneNumber _value, $Res Function(UpdatedPhoneNumber) _then)
      : super(_value, (v) => _then(v as UpdatedPhoneNumber));

  @override
  UpdatedPhoneNumber get _value => super._value as UpdatedPhoneNumber;

  @override
  $Res call({
    Object userId = freezed,
    Object phoneNumber = freezed,
  }) {
    return _then(UpdatedPhoneNumber(
      userId: userId == freezed ? _value.userId : userId as String,
      phoneNumber:
          phoneNumber == freezed ? _value.phoneNumber : phoneNumber as String,
    ));
  }
}

/// @nodoc
class _$UpdatedPhoneNumber implements UpdatedPhoneNumber {
  const _$UpdatedPhoneNumber(
      {@required this.userId, @required this.phoneNumber})
      : assert(userId != null),
        assert(phoneNumber != null);

  @override
  final String userId;
  @override
  final String phoneNumber;

  @override
  String toString() {
    return 'UserEvent.updatedPhone(userId: $userId, phoneNumber: $phoneNumber)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UpdatedPhoneNumber &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)) &&
            (identical(other.phoneNumber, phoneNumber) ||
                const DeepCollectionEquality()
                    .equals(other.phoneNumber, phoneNumber)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(userId) ^
      const DeepCollectionEquality().hash(phoneNumber);

  @JsonKey(ignore: true)
  @override
  $UpdatedPhoneNumberCopyWith<UpdatedPhoneNumber> get copyWith =>
      _$UpdatedPhoneNumberCopyWithImpl<UpdatedPhoneNumber>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required
        TResult updatedEmail(String email, String password, String newEmail),
    @required
        TResult updatedPassword(
            String email, String password, String newPassword),
    @required TResult updatedName(String userId, String name),
    @required TResult updatedSurname(String userId, String surname),
    @required TResult updatedPhone(String userId, String phoneNumber),
  }) {
    assert(updatedEmail != null);
    assert(updatedPassword != null);
    assert(updatedName != null);
    assert(updatedSurname != null);
    assert(updatedPhone != null);
    return updatedPhone(userId, phoneNumber);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updatedEmail(String email, String password, String newEmail),
    TResult updatedPassword(String email, String password, String newPassword),
    TResult updatedName(String userId, String name),
    TResult updatedSurname(String userId, String surname),
    TResult updatedPhone(String userId, String phoneNumber),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedPhone != null) {
      return updatedPhone(userId, phoneNumber);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updatedEmail(UpdatedEmail value),
    @required TResult updatedPassword(UpdatedPassword value),
    @required TResult updatedName(UpdatedName value),
    @required TResult updatedSurname(UpdatedSurname value),
    @required TResult updatedPhone(UpdatedPhoneNumber value),
  }) {
    assert(updatedEmail != null);
    assert(updatedPassword != null);
    assert(updatedName != null);
    assert(updatedSurname != null);
    assert(updatedPhone != null);
    return updatedPhone(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updatedEmail(UpdatedEmail value),
    TResult updatedPassword(UpdatedPassword value),
    TResult updatedName(UpdatedName value),
    TResult updatedSurname(UpdatedSurname value),
    TResult updatedPhone(UpdatedPhoneNumber value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedPhone != null) {
      return updatedPhone(this);
    }
    return orElse();
  }
}

abstract class UpdatedPhoneNumber implements UserEvent {
  const factory UpdatedPhoneNumber(
      {@required String userId,
      @required String phoneNumber}) = _$UpdatedPhoneNumber;

  String get userId;
  String get phoneNumber;
  @JsonKey(ignore: true)
  $UpdatedPhoneNumberCopyWith<UpdatedPhoneNumber> get copyWith;
}

/// @nodoc
class _$UserStateTearOff {
  const _$UserStateTearOff();

// ignore: unused_element
  UserInitial userInitial() {
    return UserInitial();
  }

// ignore: unused_element
  UserUpdated userUpdated({@required User user, @required bool isUpdated}) {
    return UserUpdated(
      user: user,
      isUpdated: isUpdated,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $UserState = _$UserStateTearOff();

/// @nodoc
mixin _$UserState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult userInitial(),
    @required TResult userUpdated(User user, bool isUpdated),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult userInitial(),
    TResult userUpdated(User user, bool isUpdated),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult userInitial(UserInitial value),
    @required TResult userUpdated(UserUpdated value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult userInitial(UserInitial value),
    TResult userUpdated(UserUpdated value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $UserStateCopyWith<$Res> {
  factory $UserStateCopyWith(UserState value, $Res Function(UserState) then) =
      _$UserStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$UserStateCopyWithImpl<$Res> implements $UserStateCopyWith<$Res> {
  _$UserStateCopyWithImpl(this._value, this._then);

  final UserState _value;
  // ignore: unused_field
  final $Res Function(UserState) _then;
}

/// @nodoc
abstract class $UserInitialCopyWith<$Res> {
  factory $UserInitialCopyWith(
          UserInitial value, $Res Function(UserInitial) then) =
      _$UserInitialCopyWithImpl<$Res>;
}

/// @nodoc
class _$UserInitialCopyWithImpl<$Res> extends _$UserStateCopyWithImpl<$Res>
    implements $UserInitialCopyWith<$Res> {
  _$UserInitialCopyWithImpl(
      UserInitial _value, $Res Function(UserInitial) _then)
      : super(_value, (v) => _then(v as UserInitial));

  @override
  UserInitial get _value => super._value as UserInitial;
}

/// @nodoc
class _$UserInitial implements UserInitial {
  _$UserInitial();

  @override
  String toString() {
    return 'UserState.userInitial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is UserInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult userInitial(),
    @required TResult userUpdated(User user, bool isUpdated),
  }) {
    assert(userInitial != null);
    assert(userUpdated != null);
    return userInitial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult userInitial(),
    TResult userUpdated(User user, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (userInitial != null) {
      return userInitial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult userInitial(UserInitial value),
    @required TResult userUpdated(UserUpdated value),
  }) {
    assert(userInitial != null);
    assert(userUpdated != null);
    return userInitial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult userInitial(UserInitial value),
    TResult userUpdated(UserUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (userInitial != null) {
      return userInitial(this);
    }
    return orElse();
  }
}

abstract class UserInitial implements UserState {
  factory UserInitial() = _$UserInitial;
}

/// @nodoc
abstract class $UserUpdatedCopyWith<$Res> {
  factory $UserUpdatedCopyWith(
          UserUpdated value, $Res Function(UserUpdated) then) =
      _$UserUpdatedCopyWithImpl<$Res>;
  $Res call({User user, bool isUpdated});
}

/// @nodoc
class _$UserUpdatedCopyWithImpl<$Res> extends _$UserStateCopyWithImpl<$Res>
    implements $UserUpdatedCopyWith<$Res> {
  _$UserUpdatedCopyWithImpl(
      UserUpdated _value, $Res Function(UserUpdated) _then)
      : super(_value, (v) => _then(v as UserUpdated));

  @override
  UserUpdated get _value => super._value as UserUpdated;

  @override
  $Res call({
    Object user = freezed,
    Object isUpdated = freezed,
  }) {
    return _then(UserUpdated(
      user: user == freezed ? _value.user : user as User,
      isUpdated: isUpdated == freezed ? _value.isUpdated : isUpdated as bool,
    ));
  }
}

/// @nodoc
class _$UserUpdated implements UserUpdated {
  _$UserUpdated({@required this.user, @required this.isUpdated})
      : assert(user != null),
        assert(isUpdated != null);

  @override
  final User user;
  @override
  final bool isUpdated;

  @override
  String toString() {
    return 'UserState.userUpdated(user: $user, isUpdated: $isUpdated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UserUpdated &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)) &&
            (identical(other.isUpdated, isUpdated) ||
                const DeepCollectionEquality()
                    .equals(other.isUpdated, isUpdated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(user) ^
      const DeepCollectionEquality().hash(isUpdated);

  @JsonKey(ignore: true)
  @override
  $UserUpdatedCopyWith<UserUpdated> get copyWith =>
      _$UserUpdatedCopyWithImpl<UserUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult userInitial(),
    @required TResult userUpdated(User user, bool isUpdated),
  }) {
    assert(userInitial != null);
    assert(userUpdated != null);
    return userUpdated(user, isUpdated);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult userInitial(),
    TResult userUpdated(User user, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (userUpdated != null) {
      return userUpdated(user, isUpdated);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult userInitial(UserInitial value),
    @required TResult userUpdated(UserUpdated value),
  }) {
    assert(userInitial != null);
    assert(userUpdated != null);
    return userUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult userInitial(UserInitial value),
    TResult userUpdated(UserUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (userUpdated != null) {
      return userUpdated(this);
    }
    return orElse();
  }
}

abstract class UserUpdated implements UserState {
  factory UserUpdated({@required User user, @required bool isUpdated}) =
      _$UserUpdated;

  User get user;
  bool get isUpdated;
  @JsonKey(ignore: true)
  $UserUpdatedCopyWith<UserUpdated> get copyWith;
}
