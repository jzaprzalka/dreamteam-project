part of 'user_bloc.dart';

@freezed
abstract class UserState with _$UserState {
  factory UserState.userInitial() = UserInitial;
  factory UserState.userUpdated(
      {@required User user, @required bool isUpdated}) = UserUpdated;
}
