part of 'announcement_bloc.dart';

@freezed
abstract class AnnouncementState with _$AnnouncementState {
  factory AnnouncementState.announcementInitial() = AnnouncementInitial;
  factory AnnouncementState.announcementsUpdated(
      {@required List<Announcement> announcements,
      @required bool isUpdated}) = AnnouncementsUpdated;
  factory AnnouncementState.announcementUpdated(
      {@required Announcement announcement,
      @required bool isUpdated}) = AnnouncementUpdated;
  factory AnnouncementState.loadedGroups(
      {@required List<Group> groups, @required bool isUpdated}) = GroupsLoaded;
}
