// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'announcement_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$AnnouncementEventTearOff {
  const _$AnnouncementEventTearOff();

// ignore: unused_element
  _Started started() {
    return const _Started();
  }

// ignore: unused_element
  AddedAnnouncement addedAnnouncement(
      {@required DateTime announcementDate,
      @required String announcement,
      @required List<String> groupIds}) {
    return AddedAnnouncement(
      announcementDate: announcementDate,
      announcement: announcement,
      groupIds: groupIds,
    );
  }

// ignore: unused_element
  DeletedAnnouncement deletedAnnouncement({@required String announcementId}) {
    return DeletedAnnouncement(
      announcementId: announcementId,
    );
  }

// ignore: unused_element
  LoadedAnnouncementList loadedAnnouncementList() {
    return const LoadedAnnouncementList();
  }

// ignore: unused_element
  LoadedAnnouncement loadedAnnouncement({@required String announcementId}) {
    return LoadedAnnouncement(
      announcementId: announcementId,
    );
  }

// ignore: unused_element
  LoadedGroups loadedGroups() {
    return const LoadedGroups();
  }
}

/// @nodoc
// ignore: unused_element
const $AnnouncementEvent = _$AnnouncementEventTearOff();

/// @nodoc
mixin _$AnnouncementEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult started(),
    @required
        TResult addedAnnouncement(DateTime announcementDate,
            String announcement, List<String> groupIds),
    @required TResult deletedAnnouncement(String announcementId),
    @required TResult loadedAnnouncementList(),
    @required TResult loadedAnnouncement(String announcementId),
    @required TResult loadedGroups(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult started(),
    TResult addedAnnouncement(
        DateTime announcementDate, String announcement, List<String> groupIds),
    TResult deletedAnnouncement(String announcementId),
    TResult loadedAnnouncementList(),
    TResult loadedAnnouncement(String announcementId),
    TResult loadedGroups(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult started(_Started value),
    @required TResult addedAnnouncement(AddedAnnouncement value),
    @required TResult deletedAnnouncement(DeletedAnnouncement value),
    @required TResult loadedAnnouncementList(LoadedAnnouncementList value),
    @required TResult loadedAnnouncement(LoadedAnnouncement value),
    @required TResult loadedGroups(LoadedGroups value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult started(_Started value),
    TResult addedAnnouncement(AddedAnnouncement value),
    TResult deletedAnnouncement(DeletedAnnouncement value),
    TResult loadedAnnouncementList(LoadedAnnouncementList value),
    TResult loadedAnnouncement(LoadedAnnouncement value),
    TResult loadedGroups(LoadedGroups value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $AnnouncementEventCopyWith<$Res> {
  factory $AnnouncementEventCopyWith(
          AnnouncementEvent value, $Res Function(AnnouncementEvent) then) =
      _$AnnouncementEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$AnnouncementEventCopyWithImpl<$Res>
    implements $AnnouncementEventCopyWith<$Res> {
  _$AnnouncementEventCopyWithImpl(this._value, this._then);

  final AnnouncementEvent _value;
  // ignore: unused_field
  final $Res Function(AnnouncementEvent) _then;
}

/// @nodoc
abstract class _$StartedCopyWith<$Res> {
  factory _$StartedCopyWith(_Started value, $Res Function(_Started) then) =
      __$StartedCopyWithImpl<$Res>;
}

/// @nodoc
class __$StartedCopyWithImpl<$Res> extends _$AnnouncementEventCopyWithImpl<$Res>
    implements _$StartedCopyWith<$Res> {
  __$StartedCopyWithImpl(_Started _value, $Res Function(_Started) _then)
      : super(_value, (v) => _then(v as _Started));

  @override
  _Started get _value => super._value as _Started;
}

/// @nodoc
class _$_Started implements _Started {
  const _$_Started();

  @override
  String toString() {
    return 'AnnouncementEvent.started()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Started);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult started(),
    @required
        TResult addedAnnouncement(DateTime announcementDate,
            String announcement, List<String> groupIds),
    @required TResult deletedAnnouncement(String announcementId),
    @required TResult loadedAnnouncementList(),
    @required TResult loadedAnnouncement(String announcementId),
    @required TResult loadedGroups(),
  }) {
    assert(started != null);
    assert(addedAnnouncement != null);
    assert(deletedAnnouncement != null);
    assert(loadedAnnouncementList != null);
    assert(loadedAnnouncement != null);
    assert(loadedGroups != null);
    return started();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult started(),
    TResult addedAnnouncement(
        DateTime announcementDate, String announcement, List<String> groupIds),
    TResult deletedAnnouncement(String announcementId),
    TResult loadedAnnouncementList(),
    TResult loadedAnnouncement(String announcementId),
    TResult loadedGroups(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (started != null) {
      return started();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult started(_Started value),
    @required TResult addedAnnouncement(AddedAnnouncement value),
    @required TResult deletedAnnouncement(DeletedAnnouncement value),
    @required TResult loadedAnnouncementList(LoadedAnnouncementList value),
    @required TResult loadedAnnouncement(LoadedAnnouncement value),
    @required TResult loadedGroups(LoadedGroups value),
  }) {
    assert(started != null);
    assert(addedAnnouncement != null);
    assert(deletedAnnouncement != null);
    assert(loadedAnnouncementList != null);
    assert(loadedAnnouncement != null);
    assert(loadedGroups != null);
    return started(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult started(_Started value),
    TResult addedAnnouncement(AddedAnnouncement value),
    TResult deletedAnnouncement(DeletedAnnouncement value),
    TResult loadedAnnouncementList(LoadedAnnouncementList value),
    TResult loadedAnnouncement(LoadedAnnouncement value),
    TResult loadedGroups(LoadedGroups value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (started != null) {
      return started(this);
    }
    return orElse();
  }
}

abstract class _Started implements AnnouncementEvent {
  const factory _Started() = _$_Started;
}

/// @nodoc
abstract class $AddedAnnouncementCopyWith<$Res> {
  factory $AddedAnnouncementCopyWith(
          AddedAnnouncement value, $Res Function(AddedAnnouncement) then) =
      _$AddedAnnouncementCopyWithImpl<$Res>;
  $Res call(
      {DateTime announcementDate, String announcement, List<String> groupIds});
}

/// @nodoc
class _$AddedAnnouncementCopyWithImpl<$Res>
    extends _$AnnouncementEventCopyWithImpl<$Res>
    implements $AddedAnnouncementCopyWith<$Res> {
  _$AddedAnnouncementCopyWithImpl(
      AddedAnnouncement _value, $Res Function(AddedAnnouncement) _then)
      : super(_value, (v) => _then(v as AddedAnnouncement));

  @override
  AddedAnnouncement get _value => super._value as AddedAnnouncement;

  @override
  $Res call({
    Object announcementDate = freezed,
    Object announcement = freezed,
    Object groupIds = freezed,
  }) {
    return _then(AddedAnnouncement(
      announcementDate: announcementDate == freezed
          ? _value.announcementDate
          : announcementDate as DateTime,
      announcement: announcement == freezed
          ? _value.announcement
          : announcement as String,
      groupIds:
          groupIds == freezed ? _value.groupIds : groupIds as List<String>,
    ));
  }
}

/// @nodoc
class _$AddedAnnouncement implements AddedAnnouncement {
  const _$AddedAnnouncement(
      {@required this.announcementDate,
      @required this.announcement,
      @required this.groupIds})
      : assert(announcementDate != null),
        assert(announcement != null),
        assert(groupIds != null);

  @override
  final DateTime announcementDate;
  @override
  final String announcement;
  @override
  final List<String> groupIds;

  @override
  String toString() {
    return 'AnnouncementEvent.addedAnnouncement(announcementDate: $announcementDate, announcement: $announcement, groupIds: $groupIds)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AddedAnnouncement &&
            (identical(other.announcementDate, announcementDate) ||
                const DeepCollectionEquality()
                    .equals(other.announcementDate, announcementDate)) &&
            (identical(other.announcement, announcement) ||
                const DeepCollectionEquality()
                    .equals(other.announcement, announcement)) &&
            (identical(other.groupIds, groupIds) ||
                const DeepCollectionEquality()
                    .equals(other.groupIds, groupIds)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(announcementDate) ^
      const DeepCollectionEquality().hash(announcement) ^
      const DeepCollectionEquality().hash(groupIds);

  @JsonKey(ignore: true)
  @override
  $AddedAnnouncementCopyWith<AddedAnnouncement> get copyWith =>
      _$AddedAnnouncementCopyWithImpl<AddedAnnouncement>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult started(),
    @required
        TResult addedAnnouncement(DateTime announcementDate,
            String announcement, List<String> groupIds),
    @required TResult deletedAnnouncement(String announcementId),
    @required TResult loadedAnnouncementList(),
    @required TResult loadedAnnouncement(String announcementId),
    @required TResult loadedGroups(),
  }) {
    assert(started != null);
    assert(addedAnnouncement != null);
    assert(deletedAnnouncement != null);
    assert(loadedAnnouncementList != null);
    assert(loadedAnnouncement != null);
    assert(loadedGroups != null);
    return addedAnnouncement(announcementDate, announcement, groupIds);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult started(),
    TResult addedAnnouncement(
        DateTime announcementDate, String announcement, List<String> groupIds),
    TResult deletedAnnouncement(String announcementId),
    TResult loadedAnnouncementList(),
    TResult loadedAnnouncement(String announcementId),
    TResult loadedGroups(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addedAnnouncement != null) {
      return addedAnnouncement(announcementDate, announcement, groupIds);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult started(_Started value),
    @required TResult addedAnnouncement(AddedAnnouncement value),
    @required TResult deletedAnnouncement(DeletedAnnouncement value),
    @required TResult loadedAnnouncementList(LoadedAnnouncementList value),
    @required TResult loadedAnnouncement(LoadedAnnouncement value),
    @required TResult loadedGroups(LoadedGroups value),
  }) {
    assert(started != null);
    assert(addedAnnouncement != null);
    assert(deletedAnnouncement != null);
    assert(loadedAnnouncementList != null);
    assert(loadedAnnouncement != null);
    assert(loadedGroups != null);
    return addedAnnouncement(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult started(_Started value),
    TResult addedAnnouncement(AddedAnnouncement value),
    TResult deletedAnnouncement(DeletedAnnouncement value),
    TResult loadedAnnouncementList(LoadedAnnouncementList value),
    TResult loadedAnnouncement(LoadedAnnouncement value),
    TResult loadedGroups(LoadedGroups value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addedAnnouncement != null) {
      return addedAnnouncement(this);
    }
    return orElse();
  }
}

abstract class AddedAnnouncement implements AnnouncementEvent {
  const factory AddedAnnouncement(
      {@required DateTime announcementDate,
      @required String announcement,
      @required List<String> groupIds}) = _$AddedAnnouncement;

  DateTime get announcementDate;
  String get announcement;
  List<String> get groupIds;
  @JsonKey(ignore: true)
  $AddedAnnouncementCopyWith<AddedAnnouncement> get copyWith;
}

/// @nodoc
abstract class $DeletedAnnouncementCopyWith<$Res> {
  factory $DeletedAnnouncementCopyWith(
          DeletedAnnouncement value, $Res Function(DeletedAnnouncement) then) =
      _$DeletedAnnouncementCopyWithImpl<$Res>;
  $Res call({String announcementId});
}

/// @nodoc
class _$DeletedAnnouncementCopyWithImpl<$Res>
    extends _$AnnouncementEventCopyWithImpl<$Res>
    implements $DeletedAnnouncementCopyWith<$Res> {
  _$DeletedAnnouncementCopyWithImpl(
      DeletedAnnouncement _value, $Res Function(DeletedAnnouncement) _then)
      : super(_value, (v) => _then(v as DeletedAnnouncement));

  @override
  DeletedAnnouncement get _value => super._value as DeletedAnnouncement;

  @override
  $Res call({
    Object announcementId = freezed,
  }) {
    return _then(DeletedAnnouncement(
      announcementId: announcementId == freezed
          ? _value.announcementId
          : announcementId as String,
    ));
  }
}

/// @nodoc
class _$DeletedAnnouncement implements DeletedAnnouncement {
  const _$DeletedAnnouncement({@required this.announcementId})
      : assert(announcementId != null);

  @override
  final String announcementId;

  @override
  String toString() {
    return 'AnnouncementEvent.deletedAnnouncement(announcementId: $announcementId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is DeletedAnnouncement &&
            (identical(other.announcementId, announcementId) ||
                const DeepCollectionEquality()
                    .equals(other.announcementId, announcementId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(announcementId);

  @JsonKey(ignore: true)
  @override
  $DeletedAnnouncementCopyWith<DeletedAnnouncement> get copyWith =>
      _$DeletedAnnouncementCopyWithImpl<DeletedAnnouncement>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult started(),
    @required
        TResult addedAnnouncement(DateTime announcementDate,
            String announcement, List<String> groupIds),
    @required TResult deletedAnnouncement(String announcementId),
    @required TResult loadedAnnouncementList(),
    @required TResult loadedAnnouncement(String announcementId),
    @required TResult loadedGroups(),
  }) {
    assert(started != null);
    assert(addedAnnouncement != null);
    assert(deletedAnnouncement != null);
    assert(loadedAnnouncementList != null);
    assert(loadedAnnouncement != null);
    assert(loadedGroups != null);
    return deletedAnnouncement(announcementId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult started(),
    TResult addedAnnouncement(
        DateTime announcementDate, String announcement, List<String> groupIds),
    TResult deletedAnnouncement(String announcementId),
    TResult loadedAnnouncementList(),
    TResult loadedAnnouncement(String announcementId),
    TResult loadedGroups(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (deletedAnnouncement != null) {
      return deletedAnnouncement(announcementId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult started(_Started value),
    @required TResult addedAnnouncement(AddedAnnouncement value),
    @required TResult deletedAnnouncement(DeletedAnnouncement value),
    @required TResult loadedAnnouncementList(LoadedAnnouncementList value),
    @required TResult loadedAnnouncement(LoadedAnnouncement value),
    @required TResult loadedGroups(LoadedGroups value),
  }) {
    assert(started != null);
    assert(addedAnnouncement != null);
    assert(deletedAnnouncement != null);
    assert(loadedAnnouncementList != null);
    assert(loadedAnnouncement != null);
    assert(loadedGroups != null);
    return deletedAnnouncement(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult started(_Started value),
    TResult addedAnnouncement(AddedAnnouncement value),
    TResult deletedAnnouncement(DeletedAnnouncement value),
    TResult loadedAnnouncementList(LoadedAnnouncementList value),
    TResult loadedAnnouncement(LoadedAnnouncement value),
    TResult loadedGroups(LoadedGroups value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (deletedAnnouncement != null) {
      return deletedAnnouncement(this);
    }
    return orElse();
  }
}

abstract class DeletedAnnouncement implements AnnouncementEvent {
  const factory DeletedAnnouncement({@required String announcementId}) =
      _$DeletedAnnouncement;

  String get announcementId;
  @JsonKey(ignore: true)
  $DeletedAnnouncementCopyWith<DeletedAnnouncement> get copyWith;
}

/// @nodoc
abstract class $LoadedAnnouncementListCopyWith<$Res> {
  factory $LoadedAnnouncementListCopyWith(LoadedAnnouncementList value,
          $Res Function(LoadedAnnouncementList) then) =
      _$LoadedAnnouncementListCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoadedAnnouncementListCopyWithImpl<$Res>
    extends _$AnnouncementEventCopyWithImpl<$Res>
    implements $LoadedAnnouncementListCopyWith<$Res> {
  _$LoadedAnnouncementListCopyWithImpl(LoadedAnnouncementList _value,
      $Res Function(LoadedAnnouncementList) _then)
      : super(_value, (v) => _then(v as LoadedAnnouncementList));

  @override
  LoadedAnnouncementList get _value => super._value as LoadedAnnouncementList;
}

/// @nodoc
class _$LoadedAnnouncementList implements LoadedAnnouncementList {
  const _$LoadedAnnouncementList();

  @override
  String toString() {
    return 'AnnouncementEvent.loadedAnnouncementList()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoadedAnnouncementList);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult started(),
    @required
        TResult addedAnnouncement(DateTime announcementDate,
            String announcement, List<String> groupIds),
    @required TResult deletedAnnouncement(String announcementId),
    @required TResult loadedAnnouncementList(),
    @required TResult loadedAnnouncement(String announcementId),
    @required TResult loadedGroups(),
  }) {
    assert(started != null);
    assert(addedAnnouncement != null);
    assert(deletedAnnouncement != null);
    assert(loadedAnnouncementList != null);
    assert(loadedAnnouncement != null);
    assert(loadedGroups != null);
    return loadedAnnouncementList();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult started(),
    TResult addedAnnouncement(
        DateTime announcementDate, String announcement, List<String> groupIds),
    TResult deletedAnnouncement(String announcementId),
    TResult loadedAnnouncementList(),
    TResult loadedAnnouncement(String announcementId),
    TResult loadedGroups(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedAnnouncementList != null) {
      return loadedAnnouncementList();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult started(_Started value),
    @required TResult addedAnnouncement(AddedAnnouncement value),
    @required TResult deletedAnnouncement(DeletedAnnouncement value),
    @required TResult loadedAnnouncementList(LoadedAnnouncementList value),
    @required TResult loadedAnnouncement(LoadedAnnouncement value),
    @required TResult loadedGroups(LoadedGroups value),
  }) {
    assert(started != null);
    assert(addedAnnouncement != null);
    assert(deletedAnnouncement != null);
    assert(loadedAnnouncementList != null);
    assert(loadedAnnouncement != null);
    assert(loadedGroups != null);
    return loadedAnnouncementList(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult started(_Started value),
    TResult addedAnnouncement(AddedAnnouncement value),
    TResult deletedAnnouncement(DeletedAnnouncement value),
    TResult loadedAnnouncementList(LoadedAnnouncementList value),
    TResult loadedAnnouncement(LoadedAnnouncement value),
    TResult loadedGroups(LoadedGroups value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedAnnouncementList != null) {
      return loadedAnnouncementList(this);
    }
    return orElse();
  }
}

abstract class LoadedAnnouncementList implements AnnouncementEvent {
  const factory LoadedAnnouncementList() = _$LoadedAnnouncementList;
}

/// @nodoc
abstract class $LoadedAnnouncementCopyWith<$Res> {
  factory $LoadedAnnouncementCopyWith(
          LoadedAnnouncement value, $Res Function(LoadedAnnouncement) then) =
      _$LoadedAnnouncementCopyWithImpl<$Res>;
  $Res call({String announcementId});
}

/// @nodoc
class _$LoadedAnnouncementCopyWithImpl<$Res>
    extends _$AnnouncementEventCopyWithImpl<$Res>
    implements $LoadedAnnouncementCopyWith<$Res> {
  _$LoadedAnnouncementCopyWithImpl(
      LoadedAnnouncement _value, $Res Function(LoadedAnnouncement) _then)
      : super(_value, (v) => _then(v as LoadedAnnouncement));

  @override
  LoadedAnnouncement get _value => super._value as LoadedAnnouncement;

  @override
  $Res call({
    Object announcementId = freezed,
  }) {
    return _then(LoadedAnnouncement(
      announcementId: announcementId == freezed
          ? _value.announcementId
          : announcementId as String,
    ));
  }
}

/// @nodoc
class _$LoadedAnnouncement implements LoadedAnnouncement {
  const _$LoadedAnnouncement({@required this.announcementId})
      : assert(announcementId != null);

  @override
  final String announcementId;

  @override
  String toString() {
    return 'AnnouncementEvent.loadedAnnouncement(announcementId: $announcementId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoadedAnnouncement &&
            (identical(other.announcementId, announcementId) ||
                const DeepCollectionEquality()
                    .equals(other.announcementId, announcementId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(announcementId);

  @JsonKey(ignore: true)
  @override
  $LoadedAnnouncementCopyWith<LoadedAnnouncement> get copyWith =>
      _$LoadedAnnouncementCopyWithImpl<LoadedAnnouncement>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult started(),
    @required
        TResult addedAnnouncement(DateTime announcementDate,
            String announcement, List<String> groupIds),
    @required TResult deletedAnnouncement(String announcementId),
    @required TResult loadedAnnouncementList(),
    @required TResult loadedAnnouncement(String announcementId),
    @required TResult loadedGroups(),
  }) {
    assert(started != null);
    assert(addedAnnouncement != null);
    assert(deletedAnnouncement != null);
    assert(loadedAnnouncementList != null);
    assert(loadedAnnouncement != null);
    assert(loadedGroups != null);
    return loadedAnnouncement(announcementId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult started(),
    TResult addedAnnouncement(
        DateTime announcementDate, String announcement, List<String> groupIds),
    TResult deletedAnnouncement(String announcementId),
    TResult loadedAnnouncementList(),
    TResult loadedAnnouncement(String announcementId),
    TResult loadedGroups(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedAnnouncement != null) {
      return loadedAnnouncement(announcementId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult started(_Started value),
    @required TResult addedAnnouncement(AddedAnnouncement value),
    @required TResult deletedAnnouncement(DeletedAnnouncement value),
    @required TResult loadedAnnouncementList(LoadedAnnouncementList value),
    @required TResult loadedAnnouncement(LoadedAnnouncement value),
    @required TResult loadedGroups(LoadedGroups value),
  }) {
    assert(started != null);
    assert(addedAnnouncement != null);
    assert(deletedAnnouncement != null);
    assert(loadedAnnouncementList != null);
    assert(loadedAnnouncement != null);
    assert(loadedGroups != null);
    return loadedAnnouncement(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult started(_Started value),
    TResult addedAnnouncement(AddedAnnouncement value),
    TResult deletedAnnouncement(DeletedAnnouncement value),
    TResult loadedAnnouncementList(LoadedAnnouncementList value),
    TResult loadedAnnouncement(LoadedAnnouncement value),
    TResult loadedGroups(LoadedGroups value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedAnnouncement != null) {
      return loadedAnnouncement(this);
    }
    return orElse();
  }
}

abstract class LoadedAnnouncement implements AnnouncementEvent {
  const factory LoadedAnnouncement({@required String announcementId}) =
      _$LoadedAnnouncement;

  String get announcementId;
  @JsonKey(ignore: true)
  $LoadedAnnouncementCopyWith<LoadedAnnouncement> get copyWith;
}

/// @nodoc
abstract class $LoadedGroupsCopyWith<$Res> {
  factory $LoadedGroupsCopyWith(
          LoadedGroups value, $Res Function(LoadedGroups) then) =
      _$LoadedGroupsCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoadedGroupsCopyWithImpl<$Res>
    extends _$AnnouncementEventCopyWithImpl<$Res>
    implements $LoadedGroupsCopyWith<$Res> {
  _$LoadedGroupsCopyWithImpl(
      LoadedGroups _value, $Res Function(LoadedGroups) _then)
      : super(_value, (v) => _then(v as LoadedGroups));

  @override
  LoadedGroups get _value => super._value as LoadedGroups;
}

/// @nodoc
class _$LoadedGroups implements LoadedGroups {
  const _$LoadedGroups();

  @override
  String toString() {
    return 'AnnouncementEvent.loadedGroups()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoadedGroups);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult started(),
    @required
        TResult addedAnnouncement(DateTime announcementDate,
            String announcement, List<String> groupIds),
    @required TResult deletedAnnouncement(String announcementId),
    @required TResult loadedAnnouncementList(),
    @required TResult loadedAnnouncement(String announcementId),
    @required TResult loadedGroups(),
  }) {
    assert(started != null);
    assert(addedAnnouncement != null);
    assert(deletedAnnouncement != null);
    assert(loadedAnnouncementList != null);
    assert(loadedAnnouncement != null);
    assert(loadedGroups != null);
    return loadedGroups();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult started(),
    TResult addedAnnouncement(
        DateTime announcementDate, String announcement, List<String> groupIds),
    TResult deletedAnnouncement(String announcementId),
    TResult loadedAnnouncementList(),
    TResult loadedAnnouncement(String announcementId),
    TResult loadedGroups(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedGroups != null) {
      return loadedGroups();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult started(_Started value),
    @required TResult addedAnnouncement(AddedAnnouncement value),
    @required TResult deletedAnnouncement(DeletedAnnouncement value),
    @required TResult loadedAnnouncementList(LoadedAnnouncementList value),
    @required TResult loadedAnnouncement(LoadedAnnouncement value),
    @required TResult loadedGroups(LoadedGroups value),
  }) {
    assert(started != null);
    assert(addedAnnouncement != null);
    assert(deletedAnnouncement != null);
    assert(loadedAnnouncementList != null);
    assert(loadedAnnouncement != null);
    assert(loadedGroups != null);
    return loadedGroups(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult started(_Started value),
    TResult addedAnnouncement(AddedAnnouncement value),
    TResult deletedAnnouncement(DeletedAnnouncement value),
    TResult loadedAnnouncementList(LoadedAnnouncementList value),
    TResult loadedAnnouncement(LoadedAnnouncement value),
    TResult loadedGroups(LoadedGroups value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedGroups != null) {
      return loadedGroups(this);
    }
    return orElse();
  }
}

abstract class LoadedGroups implements AnnouncementEvent {
  const factory LoadedGroups() = _$LoadedGroups;
}

/// @nodoc
class _$AnnouncementStateTearOff {
  const _$AnnouncementStateTearOff();

// ignore: unused_element
  AnnouncementInitial announcementInitial() {
    return AnnouncementInitial();
  }

// ignore: unused_element
  AnnouncementsUpdated announcementsUpdated(
      {@required List<Announcement> announcements, @required bool isUpdated}) {
    return AnnouncementsUpdated(
      announcements: announcements,
      isUpdated: isUpdated,
    );
  }

// ignore: unused_element
  AnnouncementUpdated announcementUpdated(
      {@required Announcement announcement, @required bool isUpdated}) {
    return AnnouncementUpdated(
      announcement: announcement,
      isUpdated: isUpdated,
    );
  }

// ignore: unused_element
  GroupsLoaded loadedGroups(
      {@required List<Group> groups, @required bool isUpdated}) {
    return GroupsLoaded(
      groups: groups,
      isUpdated: isUpdated,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $AnnouncementState = _$AnnouncementStateTearOff();

/// @nodoc
mixin _$AnnouncementState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult announcementInitial(),
    @required
        TResult announcementsUpdated(
            List<Announcement> announcements, bool isUpdated),
    @required
        TResult announcementUpdated(Announcement announcement, bool isUpdated),
    @required TResult loadedGroups(List<Group> groups, bool isUpdated),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult announcementInitial(),
    TResult announcementsUpdated(
        List<Announcement> announcements, bool isUpdated),
    TResult announcementUpdated(Announcement announcement, bool isUpdated),
    TResult loadedGroups(List<Group> groups, bool isUpdated),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult announcementInitial(AnnouncementInitial value),
    @required TResult announcementsUpdated(AnnouncementsUpdated value),
    @required TResult announcementUpdated(AnnouncementUpdated value),
    @required TResult loadedGroups(GroupsLoaded value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult announcementInitial(AnnouncementInitial value),
    TResult announcementsUpdated(AnnouncementsUpdated value),
    TResult announcementUpdated(AnnouncementUpdated value),
    TResult loadedGroups(GroupsLoaded value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $AnnouncementStateCopyWith<$Res> {
  factory $AnnouncementStateCopyWith(
          AnnouncementState value, $Res Function(AnnouncementState) then) =
      _$AnnouncementStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$AnnouncementStateCopyWithImpl<$Res>
    implements $AnnouncementStateCopyWith<$Res> {
  _$AnnouncementStateCopyWithImpl(this._value, this._then);

  final AnnouncementState _value;
  // ignore: unused_field
  final $Res Function(AnnouncementState) _then;
}

/// @nodoc
abstract class $AnnouncementInitialCopyWith<$Res> {
  factory $AnnouncementInitialCopyWith(
          AnnouncementInitial value, $Res Function(AnnouncementInitial) then) =
      _$AnnouncementInitialCopyWithImpl<$Res>;
}

/// @nodoc
class _$AnnouncementInitialCopyWithImpl<$Res>
    extends _$AnnouncementStateCopyWithImpl<$Res>
    implements $AnnouncementInitialCopyWith<$Res> {
  _$AnnouncementInitialCopyWithImpl(
      AnnouncementInitial _value, $Res Function(AnnouncementInitial) _then)
      : super(_value, (v) => _then(v as AnnouncementInitial));

  @override
  AnnouncementInitial get _value => super._value as AnnouncementInitial;
}

/// @nodoc
class _$AnnouncementInitial implements AnnouncementInitial {
  _$AnnouncementInitial();

  @override
  String toString() {
    return 'AnnouncementState.announcementInitial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is AnnouncementInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult announcementInitial(),
    @required
        TResult announcementsUpdated(
            List<Announcement> announcements, bool isUpdated),
    @required
        TResult announcementUpdated(Announcement announcement, bool isUpdated),
    @required TResult loadedGroups(List<Group> groups, bool isUpdated),
  }) {
    assert(announcementInitial != null);
    assert(announcementsUpdated != null);
    assert(announcementUpdated != null);
    assert(loadedGroups != null);
    return announcementInitial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult announcementInitial(),
    TResult announcementsUpdated(
        List<Announcement> announcements, bool isUpdated),
    TResult announcementUpdated(Announcement announcement, bool isUpdated),
    TResult loadedGroups(List<Group> groups, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (announcementInitial != null) {
      return announcementInitial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult announcementInitial(AnnouncementInitial value),
    @required TResult announcementsUpdated(AnnouncementsUpdated value),
    @required TResult announcementUpdated(AnnouncementUpdated value),
    @required TResult loadedGroups(GroupsLoaded value),
  }) {
    assert(announcementInitial != null);
    assert(announcementsUpdated != null);
    assert(announcementUpdated != null);
    assert(loadedGroups != null);
    return announcementInitial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult announcementInitial(AnnouncementInitial value),
    TResult announcementsUpdated(AnnouncementsUpdated value),
    TResult announcementUpdated(AnnouncementUpdated value),
    TResult loadedGroups(GroupsLoaded value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (announcementInitial != null) {
      return announcementInitial(this);
    }
    return orElse();
  }
}

abstract class AnnouncementInitial implements AnnouncementState {
  factory AnnouncementInitial() = _$AnnouncementInitial;
}

/// @nodoc
abstract class $AnnouncementsUpdatedCopyWith<$Res> {
  factory $AnnouncementsUpdatedCopyWith(AnnouncementsUpdated value,
          $Res Function(AnnouncementsUpdated) then) =
      _$AnnouncementsUpdatedCopyWithImpl<$Res>;
  $Res call({List<Announcement> announcements, bool isUpdated});
}

/// @nodoc
class _$AnnouncementsUpdatedCopyWithImpl<$Res>
    extends _$AnnouncementStateCopyWithImpl<$Res>
    implements $AnnouncementsUpdatedCopyWith<$Res> {
  _$AnnouncementsUpdatedCopyWithImpl(
      AnnouncementsUpdated _value, $Res Function(AnnouncementsUpdated) _then)
      : super(_value, (v) => _then(v as AnnouncementsUpdated));

  @override
  AnnouncementsUpdated get _value => super._value as AnnouncementsUpdated;

  @override
  $Res call({
    Object announcements = freezed,
    Object isUpdated = freezed,
  }) {
    return _then(AnnouncementsUpdated(
      announcements: announcements == freezed
          ? _value.announcements
          : announcements as List<Announcement>,
      isUpdated: isUpdated == freezed ? _value.isUpdated : isUpdated as bool,
    ));
  }
}

/// @nodoc
class _$AnnouncementsUpdated implements AnnouncementsUpdated {
  _$AnnouncementsUpdated(
      {@required this.announcements, @required this.isUpdated})
      : assert(announcements != null),
        assert(isUpdated != null);

  @override
  final List<Announcement> announcements;
  @override
  final bool isUpdated;

  @override
  String toString() {
    return 'AnnouncementState.announcementsUpdated(announcements: $announcements, isUpdated: $isUpdated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AnnouncementsUpdated &&
            (identical(other.announcements, announcements) ||
                const DeepCollectionEquality()
                    .equals(other.announcements, announcements)) &&
            (identical(other.isUpdated, isUpdated) ||
                const DeepCollectionEquality()
                    .equals(other.isUpdated, isUpdated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(announcements) ^
      const DeepCollectionEquality().hash(isUpdated);

  @JsonKey(ignore: true)
  @override
  $AnnouncementsUpdatedCopyWith<AnnouncementsUpdated> get copyWith =>
      _$AnnouncementsUpdatedCopyWithImpl<AnnouncementsUpdated>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult announcementInitial(),
    @required
        TResult announcementsUpdated(
            List<Announcement> announcements, bool isUpdated),
    @required
        TResult announcementUpdated(Announcement announcement, bool isUpdated),
    @required TResult loadedGroups(List<Group> groups, bool isUpdated),
  }) {
    assert(announcementInitial != null);
    assert(announcementsUpdated != null);
    assert(announcementUpdated != null);
    assert(loadedGroups != null);
    return announcementsUpdated(announcements, isUpdated);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult announcementInitial(),
    TResult announcementsUpdated(
        List<Announcement> announcements, bool isUpdated),
    TResult announcementUpdated(Announcement announcement, bool isUpdated),
    TResult loadedGroups(List<Group> groups, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (announcementsUpdated != null) {
      return announcementsUpdated(announcements, isUpdated);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult announcementInitial(AnnouncementInitial value),
    @required TResult announcementsUpdated(AnnouncementsUpdated value),
    @required TResult announcementUpdated(AnnouncementUpdated value),
    @required TResult loadedGroups(GroupsLoaded value),
  }) {
    assert(announcementInitial != null);
    assert(announcementsUpdated != null);
    assert(announcementUpdated != null);
    assert(loadedGroups != null);
    return announcementsUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult announcementInitial(AnnouncementInitial value),
    TResult announcementsUpdated(AnnouncementsUpdated value),
    TResult announcementUpdated(AnnouncementUpdated value),
    TResult loadedGroups(GroupsLoaded value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (announcementsUpdated != null) {
      return announcementsUpdated(this);
    }
    return orElse();
  }
}

abstract class AnnouncementsUpdated implements AnnouncementState {
  factory AnnouncementsUpdated(
      {@required List<Announcement> announcements,
      @required bool isUpdated}) = _$AnnouncementsUpdated;

  List<Announcement> get announcements;
  bool get isUpdated;
  @JsonKey(ignore: true)
  $AnnouncementsUpdatedCopyWith<AnnouncementsUpdated> get copyWith;
}

/// @nodoc
abstract class $AnnouncementUpdatedCopyWith<$Res> {
  factory $AnnouncementUpdatedCopyWith(
          AnnouncementUpdated value, $Res Function(AnnouncementUpdated) then) =
      _$AnnouncementUpdatedCopyWithImpl<$Res>;
  $Res call({Announcement announcement, bool isUpdated});
}

/// @nodoc
class _$AnnouncementUpdatedCopyWithImpl<$Res>
    extends _$AnnouncementStateCopyWithImpl<$Res>
    implements $AnnouncementUpdatedCopyWith<$Res> {
  _$AnnouncementUpdatedCopyWithImpl(
      AnnouncementUpdated _value, $Res Function(AnnouncementUpdated) _then)
      : super(_value, (v) => _then(v as AnnouncementUpdated));

  @override
  AnnouncementUpdated get _value => super._value as AnnouncementUpdated;

  @override
  $Res call({
    Object announcement = freezed,
    Object isUpdated = freezed,
  }) {
    return _then(AnnouncementUpdated(
      announcement: announcement == freezed
          ? _value.announcement
          : announcement as Announcement,
      isUpdated: isUpdated == freezed ? _value.isUpdated : isUpdated as bool,
    ));
  }
}

/// @nodoc
class _$AnnouncementUpdated implements AnnouncementUpdated {
  _$AnnouncementUpdated({@required this.announcement, @required this.isUpdated})
      : assert(announcement != null),
        assert(isUpdated != null);

  @override
  final Announcement announcement;
  @override
  final bool isUpdated;

  @override
  String toString() {
    return 'AnnouncementState.announcementUpdated(announcement: $announcement, isUpdated: $isUpdated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AnnouncementUpdated &&
            (identical(other.announcement, announcement) ||
                const DeepCollectionEquality()
                    .equals(other.announcement, announcement)) &&
            (identical(other.isUpdated, isUpdated) ||
                const DeepCollectionEquality()
                    .equals(other.isUpdated, isUpdated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(announcement) ^
      const DeepCollectionEquality().hash(isUpdated);

  @JsonKey(ignore: true)
  @override
  $AnnouncementUpdatedCopyWith<AnnouncementUpdated> get copyWith =>
      _$AnnouncementUpdatedCopyWithImpl<AnnouncementUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult announcementInitial(),
    @required
        TResult announcementsUpdated(
            List<Announcement> announcements, bool isUpdated),
    @required
        TResult announcementUpdated(Announcement announcement, bool isUpdated),
    @required TResult loadedGroups(List<Group> groups, bool isUpdated),
  }) {
    assert(announcementInitial != null);
    assert(announcementsUpdated != null);
    assert(announcementUpdated != null);
    assert(loadedGroups != null);
    return announcementUpdated(announcement, isUpdated);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult announcementInitial(),
    TResult announcementsUpdated(
        List<Announcement> announcements, bool isUpdated),
    TResult announcementUpdated(Announcement announcement, bool isUpdated),
    TResult loadedGroups(List<Group> groups, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (announcementUpdated != null) {
      return announcementUpdated(announcement, isUpdated);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult announcementInitial(AnnouncementInitial value),
    @required TResult announcementsUpdated(AnnouncementsUpdated value),
    @required TResult announcementUpdated(AnnouncementUpdated value),
    @required TResult loadedGroups(GroupsLoaded value),
  }) {
    assert(announcementInitial != null);
    assert(announcementsUpdated != null);
    assert(announcementUpdated != null);
    assert(loadedGroups != null);
    return announcementUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult announcementInitial(AnnouncementInitial value),
    TResult announcementsUpdated(AnnouncementsUpdated value),
    TResult announcementUpdated(AnnouncementUpdated value),
    TResult loadedGroups(GroupsLoaded value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (announcementUpdated != null) {
      return announcementUpdated(this);
    }
    return orElse();
  }
}

abstract class AnnouncementUpdated implements AnnouncementState {
  factory AnnouncementUpdated(
      {@required Announcement announcement,
      @required bool isUpdated}) = _$AnnouncementUpdated;

  Announcement get announcement;
  bool get isUpdated;
  @JsonKey(ignore: true)
  $AnnouncementUpdatedCopyWith<AnnouncementUpdated> get copyWith;
}

/// @nodoc
abstract class $GroupsLoadedCopyWith<$Res> {
  factory $GroupsLoadedCopyWith(
          GroupsLoaded value, $Res Function(GroupsLoaded) then) =
      _$GroupsLoadedCopyWithImpl<$Res>;
  $Res call({List<Group> groups, bool isUpdated});
}

/// @nodoc
class _$GroupsLoadedCopyWithImpl<$Res>
    extends _$AnnouncementStateCopyWithImpl<$Res>
    implements $GroupsLoadedCopyWith<$Res> {
  _$GroupsLoadedCopyWithImpl(
      GroupsLoaded _value, $Res Function(GroupsLoaded) _then)
      : super(_value, (v) => _then(v as GroupsLoaded));

  @override
  GroupsLoaded get _value => super._value as GroupsLoaded;

  @override
  $Res call({
    Object groups = freezed,
    Object isUpdated = freezed,
  }) {
    return _then(GroupsLoaded(
      groups: groups == freezed ? _value.groups : groups as List<Group>,
      isUpdated: isUpdated == freezed ? _value.isUpdated : isUpdated as bool,
    ));
  }
}

/// @nodoc
class _$GroupsLoaded implements GroupsLoaded {
  _$GroupsLoaded({@required this.groups, @required this.isUpdated})
      : assert(groups != null),
        assert(isUpdated != null);

  @override
  final List<Group> groups;
  @override
  final bool isUpdated;

  @override
  String toString() {
    return 'AnnouncementState.loadedGroups(groups: $groups, isUpdated: $isUpdated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is GroupsLoaded &&
            (identical(other.groups, groups) ||
                const DeepCollectionEquality().equals(other.groups, groups)) &&
            (identical(other.isUpdated, isUpdated) ||
                const DeepCollectionEquality()
                    .equals(other.isUpdated, isUpdated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(groups) ^
      const DeepCollectionEquality().hash(isUpdated);

  @JsonKey(ignore: true)
  @override
  $GroupsLoadedCopyWith<GroupsLoaded> get copyWith =>
      _$GroupsLoadedCopyWithImpl<GroupsLoaded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult announcementInitial(),
    @required
        TResult announcementsUpdated(
            List<Announcement> announcements, bool isUpdated),
    @required
        TResult announcementUpdated(Announcement announcement, bool isUpdated),
    @required TResult loadedGroups(List<Group> groups, bool isUpdated),
  }) {
    assert(announcementInitial != null);
    assert(announcementsUpdated != null);
    assert(announcementUpdated != null);
    assert(loadedGroups != null);
    return loadedGroups(groups, isUpdated);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult announcementInitial(),
    TResult announcementsUpdated(
        List<Announcement> announcements, bool isUpdated),
    TResult announcementUpdated(Announcement announcement, bool isUpdated),
    TResult loadedGroups(List<Group> groups, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedGroups != null) {
      return loadedGroups(groups, isUpdated);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult announcementInitial(AnnouncementInitial value),
    @required TResult announcementsUpdated(AnnouncementsUpdated value),
    @required TResult announcementUpdated(AnnouncementUpdated value),
    @required TResult loadedGroups(GroupsLoaded value),
  }) {
    assert(announcementInitial != null);
    assert(announcementsUpdated != null);
    assert(announcementUpdated != null);
    assert(loadedGroups != null);
    return loadedGroups(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult announcementInitial(AnnouncementInitial value),
    TResult announcementsUpdated(AnnouncementsUpdated value),
    TResult announcementUpdated(AnnouncementUpdated value),
    TResult loadedGroups(GroupsLoaded value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedGroups != null) {
      return loadedGroups(this);
    }
    return orElse();
  }
}

abstract class GroupsLoaded implements AnnouncementState {
  factory GroupsLoaded(
      {@required List<Group> groups,
      @required bool isUpdated}) = _$GroupsLoaded;

  List<Group> get groups;
  bool get isUpdated;
  @JsonKey(ignore: true)
  $GroupsLoadedCopyWith<GroupsLoaded> get copyWith;
}
