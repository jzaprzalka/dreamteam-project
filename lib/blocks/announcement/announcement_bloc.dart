import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/Announcement.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/repositories/announcement_repository.dart';
import 'package:dreamteam_project/repositories/groups_data_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';

part 'announcement_event.dart';
part 'announcement_state.dart';
part 'announcement_bloc.freezed.dart';

class AnnouncementBloc extends Bloc<AnnouncementEvent, AnnouncementState> {
  GroupsDataRepository groupsDataRepository = GroupsDataRepository();
  AnnouncementRepository announcementRepository = AnnouncementRepository();

  AnnouncementBloc() : super(AnnouncementState.announcementInitial());

  Future<List<Group>> _getGroups() async {
    List<Group> groups, coachGroups, studentGroups;
    try {
      logger.i("Loading student groups");
      studentGroups = await groupsDataRepository.getStudentGroups();
      logger.i("Loading coach groups");
      coachGroups = await groupsDataRepository.getCoachGroups();
      logger.i("Joining coach and student groups");
      groups = List.from(coachGroups);
      groups.addAll(
        studentGroups.whereNot((studentGroup) =>
            coachGroups.map((g) => g.groupId).contains(studentGroup.groupId)),
      );
    } catch (e) {
      logger.e("Exception thrown when loading groups List: $e");
      return List.empty();
    }
    return groups ?? List.empty();
  }

  Future<List<Announcement>> _getAnnouncements() async {
    List<Announcement> announcements;
    try {
      logger.i("Loading announcements");
      List<String> groupIds =
          (await _getGroups()).map((g) => g.groupId).toList();
      announcements =
          await announcementRepository.getGroupsAnnouncements(groupIds);
    } catch (e) {
      logger.e("Exception thrown when loading groups List: $e");
      return List.empty();
    }
    return announcements ?? List.empty();
  }

  Future<Announcement> _getAnnouncement(String announcementId) async {
    Announcement announcement;
    try {
      logger.i("Loading announcement");
      announcement =
          await announcementRepository.getAnnouncement(announcementId);
      announcement.groups =
          await groupsDataRepository.getGroups(announcement.groupIds);
    } catch (e) {
      logger.e("Exception thrown when loading groups List: $e");
      return null;
    }
    return announcement ?? null;
  }

  @override
  Stream<AnnouncementState> mapEventToState(AnnouncementEvent event) async* {
    try {
      yield* event.map(
        started: (e) => null,
        addedAnnouncement: (e) async* {
          Announcement announcement =
              await announcementRepository.addAnnouncement(
            e.announcementDate,
            e.announcement,
            e.groupIds,
          );
          yield AnnouncementState.announcementUpdated(
              announcement: await _getAnnouncement(announcement.announcementId),
              isUpdated: true);
          yield AnnouncementState.announcementsUpdated(
              announcements: await _getAnnouncements(), isUpdated: true);
        },
        deletedAnnouncement: (e) async* {
          await announcementRepository.deleteAnnouncement(e.announcementId);
          yield AnnouncementState.announcementsUpdated(
              announcements: await _getAnnouncements(), isUpdated: true);
        },
        loadedAnnouncementList: (e) async* {
          yield AnnouncementState.announcementsUpdated(
              announcements: await _getAnnouncements(), isUpdated: true);
        },
        loadedAnnouncement: (e) async* {
          yield AnnouncementState.announcementUpdated(
              announcement: await _getAnnouncement(e.announcementId),
              isUpdated: true);
        },
        loadedGroups: (e) async* {
          yield AnnouncementState.loadedGroups(
              groups: await _getGroups(), isUpdated: true);
        },
      );
    } catch (e) {
      logger.e(
          "Exception thrown mapping Announcement Event to Announcement State: $e");
    }
  }
}
