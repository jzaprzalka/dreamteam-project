part of 'announcement_bloc.dart';

@freezed
abstract class AnnouncementEvent with _$AnnouncementEvent {
  const factory AnnouncementEvent.started() = _Started;
  const factory AnnouncementEvent.addedAnnouncement({
    @required DateTime announcementDate,
    @required String announcement,
    @required List<String> groupIds,
  }) = AddedAnnouncement;
  const factory AnnouncementEvent.deletedAnnouncement({
    @required String announcementId,
  }) = DeletedAnnouncement;
  const factory AnnouncementEvent.loadedAnnouncementList() =
      LoadedAnnouncementList;
  const factory AnnouncementEvent.loadedAnnouncement({
    @required String announcementId,
  }) = LoadedAnnouncement;
  const factory AnnouncementEvent.loadedGroups() = LoadedGroups;
}
