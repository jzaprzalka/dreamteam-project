import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/repositories/groups_data_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'admin_user_token_list_event.dart';
part 'admin_user_token_list_state.dart';
part 'admin_user_token_list_bloc.freezed.dart';

class AdminUserTokenListBloc
    extends Bloc<AdminUserTokenListEvent, AdminUserTokenListState> {
  GroupsDataRepository groupsDataRepository = GroupsDataRepository();

  AdminUserTokenListBloc() : super(AdminUserTokenListState.initial());

  @override
  Stream<AdminUserTokenListState> mapEventToState(
    AdminUserTokenListEvent event,
  ) async* {
    yield* event.map(loadUsers: (e) async* {
      final users = await groupsDataRepository.getAllUsers();
      yield state.copyWith(users: users);
    });
  }
}
