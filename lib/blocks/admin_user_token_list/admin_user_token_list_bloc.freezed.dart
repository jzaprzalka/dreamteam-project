// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'admin_user_token_list_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$AdminUserTokenListEventTearOff {
  const _$AdminUserTokenListEventTearOff();

// ignore: unused_element
  _LoadUsers loadUsers() {
    return const _LoadUsers();
  }
}

/// @nodoc
// ignore: unused_element
const $AdminUserTokenListEvent = _$AdminUserTokenListEventTearOff();

/// @nodoc
mixin _$AdminUserTokenListEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadUsers(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadUsers(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadUsers(_LoadUsers value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadUsers(_LoadUsers value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $AdminUserTokenListEventCopyWith<$Res> {
  factory $AdminUserTokenListEventCopyWith(AdminUserTokenListEvent value,
          $Res Function(AdminUserTokenListEvent) then) =
      _$AdminUserTokenListEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$AdminUserTokenListEventCopyWithImpl<$Res>
    implements $AdminUserTokenListEventCopyWith<$Res> {
  _$AdminUserTokenListEventCopyWithImpl(this._value, this._then);

  final AdminUserTokenListEvent _value;
  // ignore: unused_field
  final $Res Function(AdminUserTokenListEvent) _then;
}

/// @nodoc
abstract class _$LoadUsersCopyWith<$Res> {
  factory _$LoadUsersCopyWith(
          _LoadUsers value, $Res Function(_LoadUsers) then) =
      __$LoadUsersCopyWithImpl<$Res>;
}

/// @nodoc
class __$LoadUsersCopyWithImpl<$Res>
    extends _$AdminUserTokenListEventCopyWithImpl<$Res>
    implements _$LoadUsersCopyWith<$Res> {
  __$LoadUsersCopyWithImpl(_LoadUsers _value, $Res Function(_LoadUsers) _then)
      : super(_value, (v) => _then(v as _LoadUsers));

  @override
  _LoadUsers get _value => super._value as _LoadUsers;
}

/// @nodoc
class _$_LoadUsers implements _LoadUsers {
  const _$_LoadUsers();

  @override
  String toString() {
    return 'AdminUserTokenListEvent.loadUsers()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _LoadUsers);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadUsers(),
  }) {
    assert(loadUsers != null);
    return loadUsers();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadUsers(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadUsers != null) {
      return loadUsers();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadUsers(_LoadUsers value),
  }) {
    assert(loadUsers != null);
    return loadUsers(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadUsers(_LoadUsers value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadUsers != null) {
      return loadUsers(this);
    }
    return orElse();
  }
}

abstract class _LoadUsers implements AdminUserTokenListEvent {
  const factory _LoadUsers() = _$_LoadUsers;
}

/// @nodoc
class _$AdminUserTokenListStateTearOff {
  const _$AdminUserTokenListStateTearOff();

// ignore: unused_element
  _AdminUserTokenListState call({@required List<User> users}) {
    return _AdminUserTokenListState(
      users: users,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $AdminUserTokenListState = _$AdminUserTokenListStateTearOff();

/// @nodoc
mixin _$AdminUserTokenListState {
  List<User> get users;

  @JsonKey(ignore: true)
  $AdminUserTokenListStateCopyWith<AdminUserTokenListState> get copyWith;
}

/// @nodoc
abstract class $AdminUserTokenListStateCopyWith<$Res> {
  factory $AdminUserTokenListStateCopyWith(AdminUserTokenListState value,
          $Res Function(AdminUserTokenListState) then) =
      _$AdminUserTokenListStateCopyWithImpl<$Res>;
  $Res call({List<User> users});
}

/// @nodoc
class _$AdminUserTokenListStateCopyWithImpl<$Res>
    implements $AdminUserTokenListStateCopyWith<$Res> {
  _$AdminUserTokenListStateCopyWithImpl(this._value, this._then);

  final AdminUserTokenListState _value;
  // ignore: unused_field
  final $Res Function(AdminUserTokenListState) _then;

  @override
  $Res call({
    Object users = freezed,
  }) {
    return _then(_value.copyWith(
      users: users == freezed ? _value.users : users as List<User>,
    ));
  }
}

/// @nodoc
abstract class _$AdminUserTokenListStateCopyWith<$Res>
    implements $AdminUserTokenListStateCopyWith<$Res> {
  factory _$AdminUserTokenListStateCopyWith(_AdminUserTokenListState value,
          $Res Function(_AdminUserTokenListState) then) =
      __$AdminUserTokenListStateCopyWithImpl<$Res>;
  @override
  $Res call({List<User> users});
}

/// @nodoc
class __$AdminUserTokenListStateCopyWithImpl<$Res>
    extends _$AdminUserTokenListStateCopyWithImpl<$Res>
    implements _$AdminUserTokenListStateCopyWith<$Res> {
  __$AdminUserTokenListStateCopyWithImpl(_AdminUserTokenListState _value,
      $Res Function(_AdminUserTokenListState) _then)
      : super(_value, (v) => _then(v as _AdminUserTokenListState));

  @override
  _AdminUserTokenListState get _value =>
      super._value as _AdminUserTokenListState;

  @override
  $Res call({
    Object users = freezed,
  }) {
    return _then(_AdminUserTokenListState(
      users: users == freezed ? _value.users : users as List<User>,
    ));
  }
}

/// @nodoc
class _$_AdminUserTokenListState implements _AdminUserTokenListState {
  const _$_AdminUserTokenListState({@required this.users})
      : assert(users != null);

  @override
  final List<User> users;

  @override
  String toString() {
    return 'AdminUserTokenListState(users: $users)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _AdminUserTokenListState &&
            (identical(other.users, users) ||
                const DeepCollectionEquality().equals(other.users, users)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(users);

  @JsonKey(ignore: true)
  @override
  _$AdminUserTokenListStateCopyWith<_AdminUserTokenListState> get copyWith =>
      __$AdminUserTokenListStateCopyWithImpl<_AdminUserTokenListState>(
          this, _$identity);
}

abstract class _AdminUserTokenListState implements AdminUserTokenListState {
  const factory _AdminUserTokenListState({@required List<User> users}) =
      _$_AdminUserTokenListState;

  @override
  List<User> get users;
  @override
  @JsonKey(ignore: true)
  _$AdminUserTokenListStateCopyWith<_AdminUserTokenListState> get copyWith;
}
