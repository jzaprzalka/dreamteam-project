part of 'admin_user_token_list_bloc.dart';

@freezed
abstract class AdminUserTokenListEvent with _$AdminUserTokenListEvent {
  const factory AdminUserTokenListEvent.loadUsers() = _LoadUsers;
}