part of 'admin_user_token_list_bloc.dart';

@freezed
abstract class AdminUserTokenListState with _$AdminUserTokenListState {
  const factory AdminUserTokenListState({@required List<User> users}) =
      _AdminUserTokenListState;

  factory AdminUserTokenListState.initial() => _AdminUserTokenListState(users: List.empty());
}
