part of 'groups_bloc.dart';

@freezed
abstract class GroupsEvent with _$GroupsEvent {
  const factory GroupsEvent.updatedGroupName(
      {@required String groupId, @required String newName}) = UpdatedGroupName;
  const factory GroupsEvent.addedUserToGroup(
      {@required String groupId, @required String userId}) = AddedUserToGroup;
  const factory GroupsEvent.deletedUserFromGroup(
      {@required String groupId, @required String userId}) = DeletedUser;
  const factory GroupsEvent.deletedGroup({@required String groupId}) =
      DeletedGroup;
  const factory GroupsEvent.addedCoach(
      {@required String groupId, @required String coachId}) = AddedCoach;
  const factory GroupsEvent.removedCoach(
      {@required String groupId, @required String coachId}) = RemovedCoach;
  const factory GroupsEvent.updatedCoaches(
      {@required String groupId,
      @required List<String> coachIds}) = UpdatedCoaches;
  const factory GroupsEvent.addedGroupCoach(
      {@required String name, @required List<String> userIds}) = AddedGroupCoach;
  const factory GroupsEvent.addedGroupAdmin(
      {@required String name,@required List<String> coachIds, @required List<String> userIds}) = AddedGroupAdmin;
  const factory GroupsEvent.updatedGroup(
      {@required String groupId,
      @required String name,
      @required List<String> coachIds,
      @required List<String> participantIds}) = UpdatedGroup;
  // const factory GroupsEvent.updatedAttendance(
  //     {@required String scheduleId,
  //     @required List<Attendance> attendance}) = UpdatedAttendance;
  const factory GroupsEvent.loadedGroups({bool isCoach}) = LoadedGroups;
  const factory GroupsEvent.loadedUsers() = LoadedUsers;
  const factory GroupsEvent.loadedGroup({@required String groupId}) =
      LoadedGroup;
}
