import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/models/User.dart';
// import 'package:dreamteam_project/models/Attendance.dart';
import 'package:dreamteam_project/repositories/groups_data_repository.dart';
import 'package:dreamteam_project/repositories/user_data_repository.dart';
// import 'package:dreamteam_project/providers/schedule_provider.dart';

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';

part 'groups_event.dart';
part 'groups_state.dart';

part 'groups_bloc.freezed.dart';

class GroupsBloc extends Bloc<GroupsEvent, GroupsState> {
  GroupsDataRepository groupsDataRepository = GroupsDataRepository();
  UserDataRepository userDataRepository = UserDataRepository();
  // ScheduleProvider scheduleProvider = ScheduleProvider();

  GroupsBloc() : super(GroupsState.groupsInitial());

  bool isCoach = false;

  Future<Group> _getGroup(String groupId) async {
    Group group;
    try {
      group = await groupsDataRepository.getGroup(groupId);
      if (group != null && group.participants == null)
        group.participants = List.empty(growable: true);
      for (String studentId in group?.participantIds ?? []) {
        try {
          group.participants.add(await userDataRepository.getUser(studentId));
        } catch (e) {
          logger.e("Error loading users to group: $e");
        }
      }
    } catch (e) {
      logger.e("Exception thrown when loading group [$groupId]: $e");
      return null;
    }
    return group;
  }

  Future<List<User>> _getUsers() async {
    List<User> users;
    try {
      users = await groupsDataRepository.getAllUsers();
    } catch (e) {
      logger.e("Exception thrown when loading users: $e");
      return List.empty();
    }
    return users ?? List.empty();
  }

  Future<List<Group>> _getGroups() async {
    List<Group> groups;
    try {
      logger.i("Loading coach groups");
      groups = isCoach
          ? await groupsDataRepository.getAllGroups()
          : await groupsDataRepository.getCoachGroups();
    } catch (e) {
      logger.e("Exception thrown when loading groups List: $e");
      return List.empty();
    }
    return groups ?? List.empty();
  }

  @override
  Stream<GroupsState> mapEventToState(GroupsEvent event) async* {
    List<Group> groups;
    // Schedule schedule;
    try {
      event.map(
        updatedGroupName: (e) =>
            groupsDataRepository.updateName(e.groupId, e.newName),
        addedUserToGroup: (e) =>
            groupsDataRepository.addUserToGroup(e.groupId, e.userId),
        deletedUserFromGroup: (e) =>
            groupsDataRepository.deleteUserFromGroup(e.groupId, e.userId),
        deletedGroup: (e) => groupsDataRepository.deleteGroup(e.groupId),
        addedCoach: (e) => groupsDataRepository.addCoach(e.groupId, e.coachId),
        removedCoach: (e) =>
            groupsDataRepository.removeCoach(e.groupId, e.coachId),
        updatedCoaches: (e) =>
            groupsDataRepository.updateCoaches(e.groupId, e.coachIds),
        addedGroupCoach: (e) =>
            groupsDataRepository.addGroupAsCoach(e.name, e.userIds),
        // updatedAttendance: (e) =>
        //     groupsDataRepository.updateAttendance(e.scheduleId, e.attendance),
        loadedGroups: (e) {
          if (e.isCoach != null && e.isCoach) {
            isCoach = true;
          }
        },
        updatedGroup: (e) => groupsDataRepository.updateGroup(
            e.groupId, e.name, e.coachIds, e.participantIds),
        loadedUsers: (e) => null,
        loadedGroup: (e) => null,
        addedGroupAdmin: (e) =>
            groupsDataRepository.addGroupAsAdmin(e.name, e.coachIds, e.userIds),
      );
      groups = await _getGroups();
      yield GroupsState.groupsUpdated(groups: groups, isUpdated: true);
      if (event is LoadedUsers) {
        List<User> users = await _getUsers();
        yield GroupsState.usersUpdated(users: users, isUpdated: true);
      }
      if (event is LoadedGroup) {
        Group group = await _getGroup(event.groupId);
        yield GroupsState.groupUpdated(group: group, isUpdated: true);
      }
    } catch (e) {
      logger.e("Exception thrown mapping Group Event to Group State: $e");
    }
  }
}
