// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'groups_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$GroupsEventTearOff {
  const _$GroupsEventTearOff();

// ignore: unused_element
  UpdatedGroupName updatedGroupName(
      {@required String groupId, @required String newName}) {
    return UpdatedGroupName(
      groupId: groupId,
      newName: newName,
    );
  }

// ignore: unused_element
  AddedUserToGroup addedUserToGroup(
      {@required String groupId, @required String userId}) {
    return AddedUserToGroup(
      groupId: groupId,
      userId: userId,
    );
  }

// ignore: unused_element
  DeletedUser deletedUserFromGroup(
      {@required String groupId, @required String userId}) {
    return DeletedUser(
      groupId: groupId,
      userId: userId,
    );
  }

// ignore: unused_element
  DeletedGroup deletedGroup({@required String groupId}) {
    return DeletedGroup(
      groupId: groupId,
    );
  }

// ignore: unused_element
  AddedCoach addedCoach({@required String groupId, @required String coachId}) {
    return AddedCoach(
      groupId: groupId,
      coachId: coachId,
    );
  }

// ignore: unused_element
  RemovedCoach removedCoach(
      {@required String groupId, @required String coachId}) {
    return RemovedCoach(
      groupId: groupId,
      coachId: coachId,
    );
  }

// ignore: unused_element
  UpdatedCoaches updatedCoaches(
      {@required String groupId, @required List<String> coachIds}) {
    return UpdatedCoaches(
      groupId: groupId,
      coachIds: coachIds,
    );
  }

// ignore: unused_element
  AddedGroupCoach addedGroupCoach(
      {@required String name, @required List<String> userIds}) {
    return AddedGroupCoach(
      name: name,
      userIds: userIds,
    );
  }

// ignore: unused_element
  AddedGroupAdmin addedGroupAdmin(
      {@required String name,
      @required List<String> coachIds,
      @required List<String> userIds}) {
    return AddedGroupAdmin(
      name: name,
      coachIds: coachIds,
      userIds: userIds,
    );
  }

// ignore: unused_element
  UpdatedGroup updatedGroup(
      {@required String groupId,
      @required String name,
      @required List<String> coachIds,
      @required List<String> participantIds}) {
    return UpdatedGroup(
      groupId: groupId,
      name: name,
      coachIds: coachIds,
      participantIds: participantIds,
    );
  }

// ignore: unused_element
  LoadedGroups loadedGroups({bool isCoach}) {
    return LoadedGroups(
      isCoach: isCoach,
    );
  }

// ignore: unused_element
  LoadedUsers loadedUsers() {
    return const LoadedUsers();
  }

// ignore: unused_element
  LoadedGroup loadedGroup({@required String groupId}) {
    return LoadedGroup(
      groupId: groupId,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $GroupsEvent = _$GroupsEventTearOff();

/// @nodoc
mixin _$GroupsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult updatedGroupName(String groupId, String newName),
    @required TResult addedUserToGroup(String groupId, String userId),
    @required TResult deletedUserFromGroup(String groupId, String userId),
    @required TResult deletedGroup(String groupId),
    @required TResult addedCoach(String groupId, String coachId),
    @required TResult removedCoach(String groupId, String coachId),
    @required TResult updatedCoaches(String groupId, List<String> coachIds),
    @required TResult addedGroupCoach(String name, List<String> userIds),
    @required
        TResult addedGroupAdmin(
            String name, List<String> coachIds, List<String> userIds),
    @required
        TResult updatedGroup(String groupId, String name, List<String> coachIds,
            List<String> participantIds),
    @required TResult loadedGroups(bool isCoach),
    @required TResult loadedUsers(),
    @required TResult loadedGroup(String groupId),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updatedGroupName(String groupId, String newName),
    TResult addedUserToGroup(String groupId, String userId),
    TResult deletedUserFromGroup(String groupId, String userId),
    TResult deletedGroup(String groupId),
    TResult addedCoach(String groupId, String coachId),
    TResult removedCoach(String groupId, String coachId),
    TResult updatedCoaches(String groupId, List<String> coachIds),
    TResult addedGroupCoach(String name, List<String> userIds),
    TResult addedGroupAdmin(
        String name, List<String> coachIds, List<String> userIds),
    TResult updatedGroup(String groupId, String name, List<String> coachIds,
        List<String> participantIds),
    TResult loadedGroups(bool isCoach),
    TResult loadedUsers(),
    TResult loadedGroup(String groupId),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updatedGroupName(UpdatedGroupName value),
    @required TResult addedUserToGroup(AddedUserToGroup value),
    @required TResult deletedUserFromGroup(DeletedUser value),
    @required TResult deletedGroup(DeletedGroup value),
    @required TResult addedCoach(AddedCoach value),
    @required TResult removedCoach(RemovedCoach value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult addedGroupCoach(AddedGroupCoach value),
    @required TResult addedGroupAdmin(AddedGroupAdmin value),
    @required TResult updatedGroup(UpdatedGroup value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUsers(LoadedUsers value),
    @required TResult loadedGroup(LoadedGroup value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updatedGroupName(UpdatedGroupName value),
    TResult addedUserToGroup(AddedUserToGroup value),
    TResult deletedUserFromGroup(DeletedUser value),
    TResult deletedGroup(DeletedGroup value),
    TResult addedCoach(AddedCoach value),
    TResult removedCoach(RemovedCoach value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult addedGroupCoach(AddedGroupCoach value),
    TResult addedGroupAdmin(AddedGroupAdmin value),
    TResult updatedGroup(UpdatedGroup value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUsers(LoadedUsers value),
    TResult loadedGroup(LoadedGroup value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $GroupsEventCopyWith<$Res> {
  factory $GroupsEventCopyWith(
          GroupsEvent value, $Res Function(GroupsEvent) then) =
      _$GroupsEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$GroupsEventCopyWithImpl<$Res> implements $GroupsEventCopyWith<$Res> {
  _$GroupsEventCopyWithImpl(this._value, this._then);

  final GroupsEvent _value;
  // ignore: unused_field
  final $Res Function(GroupsEvent) _then;
}

/// @nodoc
abstract class $UpdatedGroupNameCopyWith<$Res> {
  factory $UpdatedGroupNameCopyWith(
          UpdatedGroupName value, $Res Function(UpdatedGroupName) then) =
      _$UpdatedGroupNameCopyWithImpl<$Res>;
  $Res call({String groupId, String newName});
}

/// @nodoc
class _$UpdatedGroupNameCopyWithImpl<$Res>
    extends _$GroupsEventCopyWithImpl<$Res>
    implements $UpdatedGroupNameCopyWith<$Res> {
  _$UpdatedGroupNameCopyWithImpl(
      UpdatedGroupName _value, $Res Function(UpdatedGroupName) _then)
      : super(_value, (v) => _then(v as UpdatedGroupName));

  @override
  UpdatedGroupName get _value => super._value as UpdatedGroupName;

  @override
  $Res call({
    Object groupId = freezed,
    Object newName = freezed,
  }) {
    return _then(UpdatedGroupName(
      groupId: groupId == freezed ? _value.groupId : groupId as String,
      newName: newName == freezed ? _value.newName : newName as String,
    ));
  }
}

/// @nodoc
class _$UpdatedGroupName implements UpdatedGroupName {
  const _$UpdatedGroupName({@required this.groupId, @required this.newName})
      : assert(groupId != null),
        assert(newName != null);

  @override
  final String groupId;
  @override
  final String newName;

  @override
  String toString() {
    return 'GroupsEvent.updatedGroupName(groupId: $groupId, newName: $newName)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UpdatedGroupName &&
            (identical(other.groupId, groupId) ||
                const DeepCollectionEquality()
                    .equals(other.groupId, groupId)) &&
            (identical(other.newName, newName) ||
                const DeepCollectionEquality().equals(other.newName, newName)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(groupId) ^
      const DeepCollectionEquality().hash(newName);

  @JsonKey(ignore: true)
  @override
  $UpdatedGroupNameCopyWith<UpdatedGroupName> get copyWith =>
      _$UpdatedGroupNameCopyWithImpl<UpdatedGroupName>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult updatedGroupName(String groupId, String newName),
    @required TResult addedUserToGroup(String groupId, String userId),
    @required TResult deletedUserFromGroup(String groupId, String userId),
    @required TResult deletedGroup(String groupId),
    @required TResult addedCoach(String groupId, String coachId),
    @required TResult removedCoach(String groupId, String coachId),
    @required TResult updatedCoaches(String groupId, List<String> coachIds),
    @required TResult addedGroupCoach(String name, List<String> userIds),
    @required
        TResult addedGroupAdmin(
            String name, List<String> coachIds, List<String> userIds),
    @required
        TResult updatedGroup(String groupId, String name, List<String> coachIds,
            List<String> participantIds),
    @required TResult loadedGroups(bool isCoach),
    @required TResult loadedUsers(),
    @required TResult loadedGroup(String groupId),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return updatedGroupName(groupId, newName);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updatedGroupName(String groupId, String newName),
    TResult addedUserToGroup(String groupId, String userId),
    TResult deletedUserFromGroup(String groupId, String userId),
    TResult deletedGroup(String groupId),
    TResult addedCoach(String groupId, String coachId),
    TResult removedCoach(String groupId, String coachId),
    TResult updatedCoaches(String groupId, List<String> coachIds),
    TResult addedGroupCoach(String name, List<String> userIds),
    TResult addedGroupAdmin(
        String name, List<String> coachIds, List<String> userIds),
    TResult updatedGroup(String groupId, String name, List<String> coachIds,
        List<String> participantIds),
    TResult loadedGroups(bool isCoach),
    TResult loadedUsers(),
    TResult loadedGroup(String groupId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedGroupName != null) {
      return updatedGroupName(groupId, newName);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updatedGroupName(UpdatedGroupName value),
    @required TResult addedUserToGroup(AddedUserToGroup value),
    @required TResult deletedUserFromGroup(DeletedUser value),
    @required TResult deletedGroup(DeletedGroup value),
    @required TResult addedCoach(AddedCoach value),
    @required TResult removedCoach(RemovedCoach value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult addedGroupCoach(AddedGroupCoach value),
    @required TResult addedGroupAdmin(AddedGroupAdmin value),
    @required TResult updatedGroup(UpdatedGroup value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUsers(LoadedUsers value),
    @required TResult loadedGroup(LoadedGroup value),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return updatedGroupName(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updatedGroupName(UpdatedGroupName value),
    TResult addedUserToGroup(AddedUserToGroup value),
    TResult deletedUserFromGroup(DeletedUser value),
    TResult deletedGroup(DeletedGroup value),
    TResult addedCoach(AddedCoach value),
    TResult removedCoach(RemovedCoach value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult addedGroupCoach(AddedGroupCoach value),
    TResult addedGroupAdmin(AddedGroupAdmin value),
    TResult updatedGroup(UpdatedGroup value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUsers(LoadedUsers value),
    TResult loadedGroup(LoadedGroup value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedGroupName != null) {
      return updatedGroupName(this);
    }
    return orElse();
  }
}

abstract class UpdatedGroupName implements GroupsEvent {
  const factory UpdatedGroupName(
      {@required String groupId,
      @required String newName}) = _$UpdatedGroupName;

  String get groupId;
  String get newName;
  @JsonKey(ignore: true)
  $UpdatedGroupNameCopyWith<UpdatedGroupName> get copyWith;
}

/// @nodoc
abstract class $AddedUserToGroupCopyWith<$Res> {
  factory $AddedUserToGroupCopyWith(
          AddedUserToGroup value, $Res Function(AddedUserToGroup) then) =
      _$AddedUserToGroupCopyWithImpl<$Res>;
  $Res call({String groupId, String userId});
}

/// @nodoc
class _$AddedUserToGroupCopyWithImpl<$Res>
    extends _$GroupsEventCopyWithImpl<$Res>
    implements $AddedUserToGroupCopyWith<$Res> {
  _$AddedUserToGroupCopyWithImpl(
      AddedUserToGroup _value, $Res Function(AddedUserToGroup) _then)
      : super(_value, (v) => _then(v as AddedUserToGroup));

  @override
  AddedUserToGroup get _value => super._value as AddedUserToGroup;

  @override
  $Res call({
    Object groupId = freezed,
    Object userId = freezed,
  }) {
    return _then(AddedUserToGroup(
      groupId: groupId == freezed ? _value.groupId : groupId as String,
      userId: userId == freezed ? _value.userId : userId as String,
    ));
  }
}

/// @nodoc
class _$AddedUserToGroup implements AddedUserToGroup {
  const _$AddedUserToGroup({@required this.groupId, @required this.userId})
      : assert(groupId != null),
        assert(userId != null);

  @override
  final String groupId;
  @override
  final String userId;

  @override
  String toString() {
    return 'GroupsEvent.addedUserToGroup(groupId: $groupId, userId: $userId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AddedUserToGroup &&
            (identical(other.groupId, groupId) ||
                const DeepCollectionEquality()
                    .equals(other.groupId, groupId)) &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(groupId) ^
      const DeepCollectionEquality().hash(userId);

  @JsonKey(ignore: true)
  @override
  $AddedUserToGroupCopyWith<AddedUserToGroup> get copyWith =>
      _$AddedUserToGroupCopyWithImpl<AddedUserToGroup>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult updatedGroupName(String groupId, String newName),
    @required TResult addedUserToGroup(String groupId, String userId),
    @required TResult deletedUserFromGroup(String groupId, String userId),
    @required TResult deletedGroup(String groupId),
    @required TResult addedCoach(String groupId, String coachId),
    @required TResult removedCoach(String groupId, String coachId),
    @required TResult updatedCoaches(String groupId, List<String> coachIds),
    @required TResult addedGroupCoach(String name, List<String> userIds),
    @required
        TResult addedGroupAdmin(
            String name, List<String> coachIds, List<String> userIds),
    @required
        TResult updatedGroup(String groupId, String name, List<String> coachIds,
            List<String> participantIds),
    @required TResult loadedGroups(bool isCoach),
    @required TResult loadedUsers(),
    @required TResult loadedGroup(String groupId),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return addedUserToGroup(groupId, userId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updatedGroupName(String groupId, String newName),
    TResult addedUserToGroup(String groupId, String userId),
    TResult deletedUserFromGroup(String groupId, String userId),
    TResult deletedGroup(String groupId),
    TResult addedCoach(String groupId, String coachId),
    TResult removedCoach(String groupId, String coachId),
    TResult updatedCoaches(String groupId, List<String> coachIds),
    TResult addedGroupCoach(String name, List<String> userIds),
    TResult addedGroupAdmin(
        String name, List<String> coachIds, List<String> userIds),
    TResult updatedGroup(String groupId, String name, List<String> coachIds,
        List<String> participantIds),
    TResult loadedGroups(bool isCoach),
    TResult loadedUsers(),
    TResult loadedGroup(String groupId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addedUserToGroup != null) {
      return addedUserToGroup(groupId, userId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updatedGroupName(UpdatedGroupName value),
    @required TResult addedUserToGroup(AddedUserToGroup value),
    @required TResult deletedUserFromGroup(DeletedUser value),
    @required TResult deletedGroup(DeletedGroup value),
    @required TResult addedCoach(AddedCoach value),
    @required TResult removedCoach(RemovedCoach value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult addedGroupCoach(AddedGroupCoach value),
    @required TResult addedGroupAdmin(AddedGroupAdmin value),
    @required TResult updatedGroup(UpdatedGroup value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUsers(LoadedUsers value),
    @required TResult loadedGroup(LoadedGroup value),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return addedUserToGroup(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updatedGroupName(UpdatedGroupName value),
    TResult addedUserToGroup(AddedUserToGroup value),
    TResult deletedUserFromGroup(DeletedUser value),
    TResult deletedGroup(DeletedGroup value),
    TResult addedCoach(AddedCoach value),
    TResult removedCoach(RemovedCoach value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult addedGroupCoach(AddedGroupCoach value),
    TResult addedGroupAdmin(AddedGroupAdmin value),
    TResult updatedGroup(UpdatedGroup value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUsers(LoadedUsers value),
    TResult loadedGroup(LoadedGroup value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addedUserToGroup != null) {
      return addedUserToGroup(this);
    }
    return orElse();
  }
}

abstract class AddedUserToGroup implements GroupsEvent {
  const factory AddedUserToGroup(
      {@required String groupId, @required String userId}) = _$AddedUserToGroup;

  String get groupId;
  String get userId;
  @JsonKey(ignore: true)
  $AddedUserToGroupCopyWith<AddedUserToGroup> get copyWith;
}

/// @nodoc
abstract class $DeletedUserCopyWith<$Res> {
  factory $DeletedUserCopyWith(
          DeletedUser value, $Res Function(DeletedUser) then) =
      _$DeletedUserCopyWithImpl<$Res>;
  $Res call({String groupId, String userId});
}

/// @nodoc
class _$DeletedUserCopyWithImpl<$Res> extends _$GroupsEventCopyWithImpl<$Res>
    implements $DeletedUserCopyWith<$Res> {
  _$DeletedUserCopyWithImpl(
      DeletedUser _value, $Res Function(DeletedUser) _then)
      : super(_value, (v) => _then(v as DeletedUser));

  @override
  DeletedUser get _value => super._value as DeletedUser;

  @override
  $Res call({
    Object groupId = freezed,
    Object userId = freezed,
  }) {
    return _then(DeletedUser(
      groupId: groupId == freezed ? _value.groupId : groupId as String,
      userId: userId == freezed ? _value.userId : userId as String,
    ));
  }
}

/// @nodoc
class _$DeletedUser implements DeletedUser {
  const _$DeletedUser({@required this.groupId, @required this.userId})
      : assert(groupId != null),
        assert(userId != null);

  @override
  final String groupId;
  @override
  final String userId;

  @override
  String toString() {
    return 'GroupsEvent.deletedUserFromGroup(groupId: $groupId, userId: $userId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is DeletedUser &&
            (identical(other.groupId, groupId) ||
                const DeepCollectionEquality()
                    .equals(other.groupId, groupId)) &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(groupId) ^
      const DeepCollectionEquality().hash(userId);

  @JsonKey(ignore: true)
  @override
  $DeletedUserCopyWith<DeletedUser> get copyWith =>
      _$DeletedUserCopyWithImpl<DeletedUser>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult updatedGroupName(String groupId, String newName),
    @required TResult addedUserToGroup(String groupId, String userId),
    @required TResult deletedUserFromGroup(String groupId, String userId),
    @required TResult deletedGroup(String groupId),
    @required TResult addedCoach(String groupId, String coachId),
    @required TResult removedCoach(String groupId, String coachId),
    @required TResult updatedCoaches(String groupId, List<String> coachIds),
    @required TResult addedGroupCoach(String name, List<String> userIds),
    @required
        TResult addedGroupAdmin(
            String name, List<String> coachIds, List<String> userIds),
    @required
        TResult updatedGroup(String groupId, String name, List<String> coachIds,
            List<String> participantIds),
    @required TResult loadedGroups(bool isCoach),
    @required TResult loadedUsers(),
    @required TResult loadedGroup(String groupId),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return deletedUserFromGroup(groupId, userId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updatedGroupName(String groupId, String newName),
    TResult addedUserToGroup(String groupId, String userId),
    TResult deletedUserFromGroup(String groupId, String userId),
    TResult deletedGroup(String groupId),
    TResult addedCoach(String groupId, String coachId),
    TResult removedCoach(String groupId, String coachId),
    TResult updatedCoaches(String groupId, List<String> coachIds),
    TResult addedGroupCoach(String name, List<String> userIds),
    TResult addedGroupAdmin(
        String name, List<String> coachIds, List<String> userIds),
    TResult updatedGroup(String groupId, String name, List<String> coachIds,
        List<String> participantIds),
    TResult loadedGroups(bool isCoach),
    TResult loadedUsers(),
    TResult loadedGroup(String groupId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (deletedUserFromGroup != null) {
      return deletedUserFromGroup(groupId, userId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updatedGroupName(UpdatedGroupName value),
    @required TResult addedUserToGroup(AddedUserToGroup value),
    @required TResult deletedUserFromGroup(DeletedUser value),
    @required TResult deletedGroup(DeletedGroup value),
    @required TResult addedCoach(AddedCoach value),
    @required TResult removedCoach(RemovedCoach value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult addedGroupCoach(AddedGroupCoach value),
    @required TResult addedGroupAdmin(AddedGroupAdmin value),
    @required TResult updatedGroup(UpdatedGroup value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUsers(LoadedUsers value),
    @required TResult loadedGroup(LoadedGroup value),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return deletedUserFromGroup(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updatedGroupName(UpdatedGroupName value),
    TResult addedUserToGroup(AddedUserToGroup value),
    TResult deletedUserFromGroup(DeletedUser value),
    TResult deletedGroup(DeletedGroup value),
    TResult addedCoach(AddedCoach value),
    TResult removedCoach(RemovedCoach value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult addedGroupCoach(AddedGroupCoach value),
    TResult addedGroupAdmin(AddedGroupAdmin value),
    TResult updatedGroup(UpdatedGroup value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUsers(LoadedUsers value),
    TResult loadedGroup(LoadedGroup value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (deletedUserFromGroup != null) {
      return deletedUserFromGroup(this);
    }
    return orElse();
  }
}

abstract class DeletedUser implements GroupsEvent {
  const factory DeletedUser(
      {@required String groupId, @required String userId}) = _$DeletedUser;

  String get groupId;
  String get userId;
  @JsonKey(ignore: true)
  $DeletedUserCopyWith<DeletedUser> get copyWith;
}

/// @nodoc
abstract class $DeletedGroupCopyWith<$Res> {
  factory $DeletedGroupCopyWith(
          DeletedGroup value, $Res Function(DeletedGroup) then) =
      _$DeletedGroupCopyWithImpl<$Res>;
  $Res call({String groupId});
}

/// @nodoc
class _$DeletedGroupCopyWithImpl<$Res> extends _$GroupsEventCopyWithImpl<$Res>
    implements $DeletedGroupCopyWith<$Res> {
  _$DeletedGroupCopyWithImpl(
      DeletedGroup _value, $Res Function(DeletedGroup) _then)
      : super(_value, (v) => _then(v as DeletedGroup));

  @override
  DeletedGroup get _value => super._value as DeletedGroup;

  @override
  $Res call({
    Object groupId = freezed,
  }) {
    return _then(DeletedGroup(
      groupId: groupId == freezed ? _value.groupId : groupId as String,
    ));
  }
}

/// @nodoc
class _$DeletedGroup implements DeletedGroup {
  const _$DeletedGroup({@required this.groupId}) : assert(groupId != null);

  @override
  final String groupId;

  @override
  String toString() {
    return 'GroupsEvent.deletedGroup(groupId: $groupId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is DeletedGroup &&
            (identical(other.groupId, groupId) ||
                const DeepCollectionEquality().equals(other.groupId, groupId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(groupId);

  @JsonKey(ignore: true)
  @override
  $DeletedGroupCopyWith<DeletedGroup> get copyWith =>
      _$DeletedGroupCopyWithImpl<DeletedGroup>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult updatedGroupName(String groupId, String newName),
    @required TResult addedUserToGroup(String groupId, String userId),
    @required TResult deletedUserFromGroup(String groupId, String userId),
    @required TResult deletedGroup(String groupId),
    @required TResult addedCoach(String groupId, String coachId),
    @required TResult removedCoach(String groupId, String coachId),
    @required TResult updatedCoaches(String groupId, List<String> coachIds),
    @required TResult addedGroupCoach(String name, List<String> userIds),
    @required
        TResult addedGroupAdmin(
            String name, List<String> coachIds, List<String> userIds),
    @required
        TResult updatedGroup(String groupId, String name, List<String> coachIds,
            List<String> participantIds),
    @required TResult loadedGroups(bool isCoach),
    @required TResult loadedUsers(),
    @required TResult loadedGroup(String groupId),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return deletedGroup(groupId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updatedGroupName(String groupId, String newName),
    TResult addedUserToGroup(String groupId, String userId),
    TResult deletedUserFromGroup(String groupId, String userId),
    TResult deletedGroup(String groupId),
    TResult addedCoach(String groupId, String coachId),
    TResult removedCoach(String groupId, String coachId),
    TResult updatedCoaches(String groupId, List<String> coachIds),
    TResult addedGroupCoach(String name, List<String> userIds),
    TResult addedGroupAdmin(
        String name, List<String> coachIds, List<String> userIds),
    TResult updatedGroup(String groupId, String name, List<String> coachIds,
        List<String> participantIds),
    TResult loadedGroups(bool isCoach),
    TResult loadedUsers(),
    TResult loadedGroup(String groupId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (deletedGroup != null) {
      return deletedGroup(groupId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updatedGroupName(UpdatedGroupName value),
    @required TResult addedUserToGroup(AddedUserToGroup value),
    @required TResult deletedUserFromGroup(DeletedUser value),
    @required TResult deletedGroup(DeletedGroup value),
    @required TResult addedCoach(AddedCoach value),
    @required TResult removedCoach(RemovedCoach value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult addedGroupCoach(AddedGroupCoach value),
    @required TResult addedGroupAdmin(AddedGroupAdmin value),
    @required TResult updatedGroup(UpdatedGroup value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUsers(LoadedUsers value),
    @required TResult loadedGroup(LoadedGroup value),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return deletedGroup(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updatedGroupName(UpdatedGroupName value),
    TResult addedUserToGroup(AddedUserToGroup value),
    TResult deletedUserFromGroup(DeletedUser value),
    TResult deletedGroup(DeletedGroup value),
    TResult addedCoach(AddedCoach value),
    TResult removedCoach(RemovedCoach value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult addedGroupCoach(AddedGroupCoach value),
    TResult addedGroupAdmin(AddedGroupAdmin value),
    TResult updatedGroup(UpdatedGroup value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUsers(LoadedUsers value),
    TResult loadedGroup(LoadedGroup value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (deletedGroup != null) {
      return deletedGroup(this);
    }
    return orElse();
  }
}

abstract class DeletedGroup implements GroupsEvent {
  const factory DeletedGroup({@required String groupId}) = _$DeletedGroup;

  String get groupId;
  @JsonKey(ignore: true)
  $DeletedGroupCopyWith<DeletedGroup> get copyWith;
}

/// @nodoc
abstract class $AddedCoachCopyWith<$Res> {
  factory $AddedCoachCopyWith(
          AddedCoach value, $Res Function(AddedCoach) then) =
      _$AddedCoachCopyWithImpl<$Res>;
  $Res call({String groupId, String coachId});
}

/// @nodoc
class _$AddedCoachCopyWithImpl<$Res> extends _$GroupsEventCopyWithImpl<$Res>
    implements $AddedCoachCopyWith<$Res> {
  _$AddedCoachCopyWithImpl(AddedCoach _value, $Res Function(AddedCoach) _then)
      : super(_value, (v) => _then(v as AddedCoach));

  @override
  AddedCoach get _value => super._value as AddedCoach;

  @override
  $Res call({
    Object groupId = freezed,
    Object coachId = freezed,
  }) {
    return _then(AddedCoach(
      groupId: groupId == freezed ? _value.groupId : groupId as String,
      coachId: coachId == freezed ? _value.coachId : coachId as String,
    ));
  }
}

/// @nodoc
class _$AddedCoach implements AddedCoach {
  const _$AddedCoach({@required this.groupId, @required this.coachId})
      : assert(groupId != null),
        assert(coachId != null);

  @override
  final String groupId;
  @override
  final String coachId;

  @override
  String toString() {
    return 'GroupsEvent.addedCoach(groupId: $groupId, coachId: $coachId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AddedCoach &&
            (identical(other.groupId, groupId) ||
                const DeepCollectionEquality()
                    .equals(other.groupId, groupId)) &&
            (identical(other.coachId, coachId) ||
                const DeepCollectionEquality().equals(other.coachId, coachId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(groupId) ^
      const DeepCollectionEquality().hash(coachId);

  @JsonKey(ignore: true)
  @override
  $AddedCoachCopyWith<AddedCoach> get copyWith =>
      _$AddedCoachCopyWithImpl<AddedCoach>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult updatedGroupName(String groupId, String newName),
    @required TResult addedUserToGroup(String groupId, String userId),
    @required TResult deletedUserFromGroup(String groupId, String userId),
    @required TResult deletedGroup(String groupId),
    @required TResult addedCoach(String groupId, String coachId),
    @required TResult removedCoach(String groupId, String coachId),
    @required TResult updatedCoaches(String groupId, List<String> coachIds),
    @required TResult addedGroupCoach(String name, List<String> userIds),
    @required
        TResult addedGroupAdmin(
            String name, List<String> coachIds, List<String> userIds),
    @required
        TResult updatedGroup(String groupId, String name, List<String> coachIds,
            List<String> participantIds),
    @required TResult loadedGroups(bool isCoach),
    @required TResult loadedUsers(),
    @required TResult loadedGroup(String groupId),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return addedCoach(groupId, coachId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updatedGroupName(String groupId, String newName),
    TResult addedUserToGroup(String groupId, String userId),
    TResult deletedUserFromGroup(String groupId, String userId),
    TResult deletedGroup(String groupId),
    TResult addedCoach(String groupId, String coachId),
    TResult removedCoach(String groupId, String coachId),
    TResult updatedCoaches(String groupId, List<String> coachIds),
    TResult addedGroupCoach(String name, List<String> userIds),
    TResult addedGroupAdmin(
        String name, List<String> coachIds, List<String> userIds),
    TResult updatedGroup(String groupId, String name, List<String> coachIds,
        List<String> participantIds),
    TResult loadedGroups(bool isCoach),
    TResult loadedUsers(),
    TResult loadedGroup(String groupId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addedCoach != null) {
      return addedCoach(groupId, coachId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updatedGroupName(UpdatedGroupName value),
    @required TResult addedUserToGroup(AddedUserToGroup value),
    @required TResult deletedUserFromGroup(DeletedUser value),
    @required TResult deletedGroup(DeletedGroup value),
    @required TResult addedCoach(AddedCoach value),
    @required TResult removedCoach(RemovedCoach value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult addedGroupCoach(AddedGroupCoach value),
    @required TResult addedGroupAdmin(AddedGroupAdmin value),
    @required TResult updatedGroup(UpdatedGroup value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUsers(LoadedUsers value),
    @required TResult loadedGroup(LoadedGroup value),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return addedCoach(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updatedGroupName(UpdatedGroupName value),
    TResult addedUserToGroup(AddedUserToGroup value),
    TResult deletedUserFromGroup(DeletedUser value),
    TResult deletedGroup(DeletedGroup value),
    TResult addedCoach(AddedCoach value),
    TResult removedCoach(RemovedCoach value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult addedGroupCoach(AddedGroupCoach value),
    TResult addedGroupAdmin(AddedGroupAdmin value),
    TResult updatedGroup(UpdatedGroup value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUsers(LoadedUsers value),
    TResult loadedGroup(LoadedGroup value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addedCoach != null) {
      return addedCoach(this);
    }
    return orElse();
  }
}

abstract class AddedCoach implements GroupsEvent {
  const factory AddedCoach(
      {@required String groupId, @required String coachId}) = _$AddedCoach;

  String get groupId;
  String get coachId;
  @JsonKey(ignore: true)
  $AddedCoachCopyWith<AddedCoach> get copyWith;
}

/// @nodoc
abstract class $RemovedCoachCopyWith<$Res> {
  factory $RemovedCoachCopyWith(
          RemovedCoach value, $Res Function(RemovedCoach) then) =
      _$RemovedCoachCopyWithImpl<$Res>;
  $Res call({String groupId, String coachId});
}

/// @nodoc
class _$RemovedCoachCopyWithImpl<$Res> extends _$GroupsEventCopyWithImpl<$Res>
    implements $RemovedCoachCopyWith<$Res> {
  _$RemovedCoachCopyWithImpl(
      RemovedCoach _value, $Res Function(RemovedCoach) _then)
      : super(_value, (v) => _then(v as RemovedCoach));

  @override
  RemovedCoach get _value => super._value as RemovedCoach;

  @override
  $Res call({
    Object groupId = freezed,
    Object coachId = freezed,
  }) {
    return _then(RemovedCoach(
      groupId: groupId == freezed ? _value.groupId : groupId as String,
      coachId: coachId == freezed ? _value.coachId : coachId as String,
    ));
  }
}

/// @nodoc
class _$RemovedCoach implements RemovedCoach {
  const _$RemovedCoach({@required this.groupId, @required this.coachId})
      : assert(groupId != null),
        assert(coachId != null);

  @override
  final String groupId;
  @override
  final String coachId;

  @override
  String toString() {
    return 'GroupsEvent.removedCoach(groupId: $groupId, coachId: $coachId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is RemovedCoach &&
            (identical(other.groupId, groupId) ||
                const DeepCollectionEquality()
                    .equals(other.groupId, groupId)) &&
            (identical(other.coachId, coachId) ||
                const DeepCollectionEquality().equals(other.coachId, coachId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(groupId) ^
      const DeepCollectionEquality().hash(coachId);

  @JsonKey(ignore: true)
  @override
  $RemovedCoachCopyWith<RemovedCoach> get copyWith =>
      _$RemovedCoachCopyWithImpl<RemovedCoach>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult updatedGroupName(String groupId, String newName),
    @required TResult addedUserToGroup(String groupId, String userId),
    @required TResult deletedUserFromGroup(String groupId, String userId),
    @required TResult deletedGroup(String groupId),
    @required TResult addedCoach(String groupId, String coachId),
    @required TResult removedCoach(String groupId, String coachId),
    @required TResult updatedCoaches(String groupId, List<String> coachIds),
    @required TResult addedGroupCoach(String name, List<String> userIds),
    @required
        TResult addedGroupAdmin(
            String name, List<String> coachIds, List<String> userIds),
    @required
        TResult updatedGroup(String groupId, String name, List<String> coachIds,
            List<String> participantIds),
    @required TResult loadedGroups(bool isCoach),
    @required TResult loadedUsers(),
    @required TResult loadedGroup(String groupId),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return removedCoach(groupId, coachId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updatedGroupName(String groupId, String newName),
    TResult addedUserToGroup(String groupId, String userId),
    TResult deletedUserFromGroup(String groupId, String userId),
    TResult deletedGroup(String groupId),
    TResult addedCoach(String groupId, String coachId),
    TResult removedCoach(String groupId, String coachId),
    TResult updatedCoaches(String groupId, List<String> coachIds),
    TResult addedGroupCoach(String name, List<String> userIds),
    TResult addedGroupAdmin(
        String name, List<String> coachIds, List<String> userIds),
    TResult updatedGroup(String groupId, String name, List<String> coachIds,
        List<String> participantIds),
    TResult loadedGroups(bool isCoach),
    TResult loadedUsers(),
    TResult loadedGroup(String groupId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (removedCoach != null) {
      return removedCoach(groupId, coachId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updatedGroupName(UpdatedGroupName value),
    @required TResult addedUserToGroup(AddedUserToGroup value),
    @required TResult deletedUserFromGroup(DeletedUser value),
    @required TResult deletedGroup(DeletedGroup value),
    @required TResult addedCoach(AddedCoach value),
    @required TResult removedCoach(RemovedCoach value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult addedGroupCoach(AddedGroupCoach value),
    @required TResult addedGroupAdmin(AddedGroupAdmin value),
    @required TResult updatedGroup(UpdatedGroup value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUsers(LoadedUsers value),
    @required TResult loadedGroup(LoadedGroup value),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return removedCoach(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updatedGroupName(UpdatedGroupName value),
    TResult addedUserToGroup(AddedUserToGroup value),
    TResult deletedUserFromGroup(DeletedUser value),
    TResult deletedGroup(DeletedGroup value),
    TResult addedCoach(AddedCoach value),
    TResult removedCoach(RemovedCoach value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult addedGroupCoach(AddedGroupCoach value),
    TResult addedGroupAdmin(AddedGroupAdmin value),
    TResult updatedGroup(UpdatedGroup value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUsers(LoadedUsers value),
    TResult loadedGroup(LoadedGroup value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (removedCoach != null) {
      return removedCoach(this);
    }
    return orElse();
  }
}

abstract class RemovedCoach implements GroupsEvent {
  const factory RemovedCoach(
      {@required String groupId, @required String coachId}) = _$RemovedCoach;

  String get groupId;
  String get coachId;
  @JsonKey(ignore: true)
  $RemovedCoachCopyWith<RemovedCoach> get copyWith;
}

/// @nodoc
abstract class $UpdatedCoachesCopyWith<$Res> {
  factory $UpdatedCoachesCopyWith(
          UpdatedCoaches value, $Res Function(UpdatedCoaches) then) =
      _$UpdatedCoachesCopyWithImpl<$Res>;
  $Res call({String groupId, List<String> coachIds});
}

/// @nodoc
class _$UpdatedCoachesCopyWithImpl<$Res> extends _$GroupsEventCopyWithImpl<$Res>
    implements $UpdatedCoachesCopyWith<$Res> {
  _$UpdatedCoachesCopyWithImpl(
      UpdatedCoaches _value, $Res Function(UpdatedCoaches) _then)
      : super(_value, (v) => _then(v as UpdatedCoaches));

  @override
  UpdatedCoaches get _value => super._value as UpdatedCoaches;

  @override
  $Res call({
    Object groupId = freezed,
    Object coachIds = freezed,
  }) {
    return _then(UpdatedCoaches(
      groupId: groupId == freezed ? _value.groupId : groupId as String,
      coachIds:
          coachIds == freezed ? _value.coachIds : coachIds as List<String>,
    ));
  }
}

/// @nodoc
class _$UpdatedCoaches implements UpdatedCoaches {
  const _$UpdatedCoaches({@required this.groupId, @required this.coachIds})
      : assert(groupId != null),
        assert(coachIds != null);

  @override
  final String groupId;
  @override
  final List<String> coachIds;

  @override
  String toString() {
    return 'GroupsEvent.updatedCoaches(groupId: $groupId, coachIds: $coachIds)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UpdatedCoaches &&
            (identical(other.groupId, groupId) ||
                const DeepCollectionEquality()
                    .equals(other.groupId, groupId)) &&
            (identical(other.coachIds, coachIds) ||
                const DeepCollectionEquality()
                    .equals(other.coachIds, coachIds)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(groupId) ^
      const DeepCollectionEquality().hash(coachIds);

  @JsonKey(ignore: true)
  @override
  $UpdatedCoachesCopyWith<UpdatedCoaches> get copyWith =>
      _$UpdatedCoachesCopyWithImpl<UpdatedCoaches>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult updatedGroupName(String groupId, String newName),
    @required TResult addedUserToGroup(String groupId, String userId),
    @required TResult deletedUserFromGroup(String groupId, String userId),
    @required TResult deletedGroup(String groupId),
    @required TResult addedCoach(String groupId, String coachId),
    @required TResult removedCoach(String groupId, String coachId),
    @required TResult updatedCoaches(String groupId, List<String> coachIds),
    @required TResult addedGroupCoach(String name, List<String> userIds),
    @required
        TResult addedGroupAdmin(
            String name, List<String> coachIds, List<String> userIds),
    @required
        TResult updatedGroup(String groupId, String name, List<String> coachIds,
            List<String> participantIds),
    @required TResult loadedGroups(bool isCoach),
    @required TResult loadedUsers(),
    @required TResult loadedGroup(String groupId),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return updatedCoaches(groupId, coachIds);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updatedGroupName(String groupId, String newName),
    TResult addedUserToGroup(String groupId, String userId),
    TResult deletedUserFromGroup(String groupId, String userId),
    TResult deletedGroup(String groupId),
    TResult addedCoach(String groupId, String coachId),
    TResult removedCoach(String groupId, String coachId),
    TResult updatedCoaches(String groupId, List<String> coachIds),
    TResult addedGroupCoach(String name, List<String> userIds),
    TResult addedGroupAdmin(
        String name, List<String> coachIds, List<String> userIds),
    TResult updatedGroup(String groupId, String name, List<String> coachIds,
        List<String> participantIds),
    TResult loadedGroups(bool isCoach),
    TResult loadedUsers(),
    TResult loadedGroup(String groupId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedCoaches != null) {
      return updatedCoaches(groupId, coachIds);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updatedGroupName(UpdatedGroupName value),
    @required TResult addedUserToGroup(AddedUserToGroup value),
    @required TResult deletedUserFromGroup(DeletedUser value),
    @required TResult deletedGroup(DeletedGroup value),
    @required TResult addedCoach(AddedCoach value),
    @required TResult removedCoach(RemovedCoach value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult addedGroupCoach(AddedGroupCoach value),
    @required TResult addedGroupAdmin(AddedGroupAdmin value),
    @required TResult updatedGroup(UpdatedGroup value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUsers(LoadedUsers value),
    @required TResult loadedGroup(LoadedGroup value),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return updatedCoaches(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updatedGroupName(UpdatedGroupName value),
    TResult addedUserToGroup(AddedUserToGroup value),
    TResult deletedUserFromGroup(DeletedUser value),
    TResult deletedGroup(DeletedGroup value),
    TResult addedCoach(AddedCoach value),
    TResult removedCoach(RemovedCoach value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult addedGroupCoach(AddedGroupCoach value),
    TResult addedGroupAdmin(AddedGroupAdmin value),
    TResult updatedGroup(UpdatedGroup value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUsers(LoadedUsers value),
    TResult loadedGroup(LoadedGroup value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedCoaches != null) {
      return updatedCoaches(this);
    }
    return orElse();
  }
}

abstract class UpdatedCoaches implements GroupsEvent {
  const factory UpdatedCoaches(
      {@required String groupId,
      @required List<String> coachIds}) = _$UpdatedCoaches;

  String get groupId;
  List<String> get coachIds;
  @JsonKey(ignore: true)
  $UpdatedCoachesCopyWith<UpdatedCoaches> get copyWith;
}

/// @nodoc
abstract class $AddedGroupCoachCopyWith<$Res> {
  factory $AddedGroupCoachCopyWith(
          AddedGroupCoach value, $Res Function(AddedGroupCoach) then) =
      _$AddedGroupCoachCopyWithImpl<$Res>;
  $Res call({String name, List<String> userIds});
}

/// @nodoc
class _$AddedGroupCoachCopyWithImpl<$Res>
    extends _$GroupsEventCopyWithImpl<$Res>
    implements $AddedGroupCoachCopyWith<$Res> {
  _$AddedGroupCoachCopyWithImpl(
      AddedGroupCoach _value, $Res Function(AddedGroupCoach) _then)
      : super(_value, (v) => _then(v as AddedGroupCoach));

  @override
  AddedGroupCoach get _value => super._value as AddedGroupCoach;

  @override
  $Res call({
    Object name = freezed,
    Object userIds = freezed,
  }) {
    return _then(AddedGroupCoach(
      name: name == freezed ? _value.name : name as String,
      userIds: userIds == freezed ? _value.userIds : userIds as List<String>,
    ));
  }
}

/// @nodoc
class _$AddedGroupCoach implements AddedGroupCoach {
  const _$AddedGroupCoach({@required this.name, @required this.userIds})
      : assert(name != null),
        assert(userIds != null);

  @override
  final String name;
  @override
  final List<String> userIds;

  @override
  String toString() {
    return 'GroupsEvent.addedGroupCoach(name: $name, userIds: $userIds)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AddedGroupCoach &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.userIds, userIds) ||
                const DeepCollectionEquality().equals(other.userIds, userIds)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(userIds);

  @JsonKey(ignore: true)
  @override
  $AddedGroupCoachCopyWith<AddedGroupCoach> get copyWith =>
      _$AddedGroupCoachCopyWithImpl<AddedGroupCoach>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult updatedGroupName(String groupId, String newName),
    @required TResult addedUserToGroup(String groupId, String userId),
    @required TResult deletedUserFromGroup(String groupId, String userId),
    @required TResult deletedGroup(String groupId),
    @required TResult addedCoach(String groupId, String coachId),
    @required TResult removedCoach(String groupId, String coachId),
    @required TResult updatedCoaches(String groupId, List<String> coachIds),
    @required TResult addedGroupCoach(String name, List<String> userIds),
    @required
        TResult addedGroupAdmin(
            String name, List<String> coachIds, List<String> userIds),
    @required
        TResult updatedGroup(String groupId, String name, List<String> coachIds,
            List<String> participantIds),
    @required TResult loadedGroups(bool isCoach),
    @required TResult loadedUsers(),
    @required TResult loadedGroup(String groupId),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return addedGroupCoach(name, userIds);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updatedGroupName(String groupId, String newName),
    TResult addedUserToGroup(String groupId, String userId),
    TResult deletedUserFromGroup(String groupId, String userId),
    TResult deletedGroup(String groupId),
    TResult addedCoach(String groupId, String coachId),
    TResult removedCoach(String groupId, String coachId),
    TResult updatedCoaches(String groupId, List<String> coachIds),
    TResult addedGroupCoach(String name, List<String> userIds),
    TResult addedGroupAdmin(
        String name, List<String> coachIds, List<String> userIds),
    TResult updatedGroup(String groupId, String name, List<String> coachIds,
        List<String> participantIds),
    TResult loadedGroups(bool isCoach),
    TResult loadedUsers(),
    TResult loadedGroup(String groupId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addedGroupCoach != null) {
      return addedGroupCoach(name, userIds);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updatedGroupName(UpdatedGroupName value),
    @required TResult addedUserToGroup(AddedUserToGroup value),
    @required TResult deletedUserFromGroup(DeletedUser value),
    @required TResult deletedGroup(DeletedGroup value),
    @required TResult addedCoach(AddedCoach value),
    @required TResult removedCoach(RemovedCoach value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult addedGroupCoach(AddedGroupCoach value),
    @required TResult addedGroupAdmin(AddedGroupAdmin value),
    @required TResult updatedGroup(UpdatedGroup value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUsers(LoadedUsers value),
    @required TResult loadedGroup(LoadedGroup value),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return addedGroupCoach(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updatedGroupName(UpdatedGroupName value),
    TResult addedUserToGroup(AddedUserToGroup value),
    TResult deletedUserFromGroup(DeletedUser value),
    TResult deletedGroup(DeletedGroup value),
    TResult addedCoach(AddedCoach value),
    TResult removedCoach(RemovedCoach value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult addedGroupCoach(AddedGroupCoach value),
    TResult addedGroupAdmin(AddedGroupAdmin value),
    TResult updatedGroup(UpdatedGroup value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUsers(LoadedUsers value),
    TResult loadedGroup(LoadedGroup value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addedGroupCoach != null) {
      return addedGroupCoach(this);
    }
    return orElse();
  }
}

abstract class AddedGroupCoach implements GroupsEvent {
  const factory AddedGroupCoach(
      {@required String name,
      @required List<String> userIds}) = _$AddedGroupCoach;

  String get name;
  List<String> get userIds;
  @JsonKey(ignore: true)
  $AddedGroupCoachCopyWith<AddedGroupCoach> get copyWith;
}

/// @nodoc
abstract class $AddedGroupAdminCopyWith<$Res> {
  factory $AddedGroupAdminCopyWith(
          AddedGroupAdmin value, $Res Function(AddedGroupAdmin) then) =
      _$AddedGroupAdminCopyWithImpl<$Res>;
  $Res call({String name, List<String> coachIds, List<String> userIds});
}

/// @nodoc
class _$AddedGroupAdminCopyWithImpl<$Res>
    extends _$GroupsEventCopyWithImpl<$Res>
    implements $AddedGroupAdminCopyWith<$Res> {
  _$AddedGroupAdminCopyWithImpl(
      AddedGroupAdmin _value, $Res Function(AddedGroupAdmin) _then)
      : super(_value, (v) => _then(v as AddedGroupAdmin));

  @override
  AddedGroupAdmin get _value => super._value as AddedGroupAdmin;

  @override
  $Res call({
    Object name = freezed,
    Object coachIds = freezed,
    Object userIds = freezed,
  }) {
    return _then(AddedGroupAdmin(
      name: name == freezed ? _value.name : name as String,
      coachIds:
          coachIds == freezed ? _value.coachIds : coachIds as List<String>,
      userIds: userIds == freezed ? _value.userIds : userIds as List<String>,
    ));
  }
}

/// @nodoc
class _$AddedGroupAdmin implements AddedGroupAdmin {
  const _$AddedGroupAdmin(
      {@required this.name, @required this.coachIds, @required this.userIds})
      : assert(name != null),
        assert(coachIds != null),
        assert(userIds != null);

  @override
  final String name;
  @override
  final List<String> coachIds;
  @override
  final List<String> userIds;

  @override
  String toString() {
    return 'GroupsEvent.addedGroupAdmin(name: $name, coachIds: $coachIds, userIds: $userIds)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AddedGroupAdmin &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.coachIds, coachIds) ||
                const DeepCollectionEquality()
                    .equals(other.coachIds, coachIds)) &&
            (identical(other.userIds, userIds) ||
                const DeepCollectionEquality().equals(other.userIds, userIds)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(coachIds) ^
      const DeepCollectionEquality().hash(userIds);

  @JsonKey(ignore: true)
  @override
  $AddedGroupAdminCopyWith<AddedGroupAdmin> get copyWith =>
      _$AddedGroupAdminCopyWithImpl<AddedGroupAdmin>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult updatedGroupName(String groupId, String newName),
    @required TResult addedUserToGroup(String groupId, String userId),
    @required TResult deletedUserFromGroup(String groupId, String userId),
    @required TResult deletedGroup(String groupId),
    @required TResult addedCoach(String groupId, String coachId),
    @required TResult removedCoach(String groupId, String coachId),
    @required TResult updatedCoaches(String groupId, List<String> coachIds),
    @required TResult addedGroupCoach(String name, List<String> userIds),
    @required
        TResult addedGroupAdmin(
            String name, List<String> coachIds, List<String> userIds),
    @required
        TResult updatedGroup(String groupId, String name, List<String> coachIds,
            List<String> participantIds),
    @required TResult loadedGroups(bool isCoach),
    @required TResult loadedUsers(),
    @required TResult loadedGroup(String groupId),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return addedGroupAdmin(name, coachIds, userIds);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updatedGroupName(String groupId, String newName),
    TResult addedUserToGroup(String groupId, String userId),
    TResult deletedUserFromGroup(String groupId, String userId),
    TResult deletedGroup(String groupId),
    TResult addedCoach(String groupId, String coachId),
    TResult removedCoach(String groupId, String coachId),
    TResult updatedCoaches(String groupId, List<String> coachIds),
    TResult addedGroupCoach(String name, List<String> userIds),
    TResult addedGroupAdmin(
        String name, List<String> coachIds, List<String> userIds),
    TResult updatedGroup(String groupId, String name, List<String> coachIds,
        List<String> participantIds),
    TResult loadedGroups(bool isCoach),
    TResult loadedUsers(),
    TResult loadedGroup(String groupId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addedGroupAdmin != null) {
      return addedGroupAdmin(name, coachIds, userIds);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updatedGroupName(UpdatedGroupName value),
    @required TResult addedUserToGroup(AddedUserToGroup value),
    @required TResult deletedUserFromGroup(DeletedUser value),
    @required TResult deletedGroup(DeletedGroup value),
    @required TResult addedCoach(AddedCoach value),
    @required TResult removedCoach(RemovedCoach value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult addedGroupCoach(AddedGroupCoach value),
    @required TResult addedGroupAdmin(AddedGroupAdmin value),
    @required TResult updatedGroup(UpdatedGroup value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUsers(LoadedUsers value),
    @required TResult loadedGroup(LoadedGroup value),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return addedGroupAdmin(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updatedGroupName(UpdatedGroupName value),
    TResult addedUserToGroup(AddedUserToGroup value),
    TResult deletedUserFromGroup(DeletedUser value),
    TResult deletedGroup(DeletedGroup value),
    TResult addedCoach(AddedCoach value),
    TResult removedCoach(RemovedCoach value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult addedGroupCoach(AddedGroupCoach value),
    TResult addedGroupAdmin(AddedGroupAdmin value),
    TResult updatedGroup(UpdatedGroup value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUsers(LoadedUsers value),
    TResult loadedGroup(LoadedGroup value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addedGroupAdmin != null) {
      return addedGroupAdmin(this);
    }
    return orElse();
  }
}

abstract class AddedGroupAdmin implements GroupsEvent {
  const factory AddedGroupAdmin(
      {@required String name,
      @required List<String> coachIds,
      @required List<String> userIds}) = _$AddedGroupAdmin;

  String get name;
  List<String> get coachIds;
  List<String> get userIds;
  @JsonKey(ignore: true)
  $AddedGroupAdminCopyWith<AddedGroupAdmin> get copyWith;
}

/// @nodoc
abstract class $UpdatedGroupCopyWith<$Res> {
  factory $UpdatedGroupCopyWith(
          UpdatedGroup value, $Res Function(UpdatedGroup) then) =
      _$UpdatedGroupCopyWithImpl<$Res>;
  $Res call(
      {String groupId,
      String name,
      List<String> coachIds,
      List<String> participantIds});
}

/// @nodoc
class _$UpdatedGroupCopyWithImpl<$Res> extends _$GroupsEventCopyWithImpl<$Res>
    implements $UpdatedGroupCopyWith<$Res> {
  _$UpdatedGroupCopyWithImpl(
      UpdatedGroup _value, $Res Function(UpdatedGroup) _then)
      : super(_value, (v) => _then(v as UpdatedGroup));

  @override
  UpdatedGroup get _value => super._value as UpdatedGroup;

  @override
  $Res call({
    Object groupId = freezed,
    Object name = freezed,
    Object coachIds = freezed,
    Object participantIds = freezed,
  }) {
    return _then(UpdatedGroup(
      groupId: groupId == freezed ? _value.groupId : groupId as String,
      name: name == freezed ? _value.name : name as String,
      coachIds:
          coachIds == freezed ? _value.coachIds : coachIds as List<String>,
      participantIds: participantIds == freezed
          ? _value.participantIds
          : participantIds as List<String>,
    ));
  }
}

/// @nodoc
class _$UpdatedGroup implements UpdatedGroup {
  const _$UpdatedGroup(
      {@required this.groupId,
      @required this.name,
      @required this.coachIds,
      @required this.participantIds})
      : assert(groupId != null),
        assert(name != null),
        assert(coachIds != null),
        assert(participantIds != null);

  @override
  final String groupId;
  @override
  final String name;
  @override
  final List<String> coachIds;
  @override
  final List<String> participantIds;

  @override
  String toString() {
    return 'GroupsEvent.updatedGroup(groupId: $groupId, name: $name, coachIds: $coachIds, participantIds: $participantIds)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UpdatedGroup &&
            (identical(other.groupId, groupId) ||
                const DeepCollectionEquality()
                    .equals(other.groupId, groupId)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.coachIds, coachIds) ||
                const DeepCollectionEquality()
                    .equals(other.coachIds, coachIds)) &&
            (identical(other.participantIds, participantIds) ||
                const DeepCollectionEquality()
                    .equals(other.participantIds, participantIds)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(groupId) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(coachIds) ^
      const DeepCollectionEquality().hash(participantIds);

  @JsonKey(ignore: true)
  @override
  $UpdatedGroupCopyWith<UpdatedGroup> get copyWith =>
      _$UpdatedGroupCopyWithImpl<UpdatedGroup>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult updatedGroupName(String groupId, String newName),
    @required TResult addedUserToGroup(String groupId, String userId),
    @required TResult deletedUserFromGroup(String groupId, String userId),
    @required TResult deletedGroup(String groupId),
    @required TResult addedCoach(String groupId, String coachId),
    @required TResult removedCoach(String groupId, String coachId),
    @required TResult updatedCoaches(String groupId, List<String> coachIds),
    @required TResult addedGroupCoach(String name, List<String> userIds),
    @required
        TResult addedGroupAdmin(
            String name, List<String> coachIds, List<String> userIds),
    @required
        TResult updatedGroup(String groupId, String name, List<String> coachIds,
            List<String> participantIds),
    @required TResult loadedGroups(bool isCoach),
    @required TResult loadedUsers(),
    @required TResult loadedGroup(String groupId),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return updatedGroup(groupId, name, coachIds, participantIds);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updatedGroupName(String groupId, String newName),
    TResult addedUserToGroup(String groupId, String userId),
    TResult deletedUserFromGroup(String groupId, String userId),
    TResult deletedGroup(String groupId),
    TResult addedCoach(String groupId, String coachId),
    TResult removedCoach(String groupId, String coachId),
    TResult updatedCoaches(String groupId, List<String> coachIds),
    TResult addedGroupCoach(String name, List<String> userIds),
    TResult addedGroupAdmin(
        String name, List<String> coachIds, List<String> userIds),
    TResult updatedGroup(String groupId, String name, List<String> coachIds,
        List<String> participantIds),
    TResult loadedGroups(bool isCoach),
    TResult loadedUsers(),
    TResult loadedGroup(String groupId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedGroup != null) {
      return updatedGroup(groupId, name, coachIds, participantIds);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updatedGroupName(UpdatedGroupName value),
    @required TResult addedUserToGroup(AddedUserToGroup value),
    @required TResult deletedUserFromGroup(DeletedUser value),
    @required TResult deletedGroup(DeletedGroup value),
    @required TResult addedCoach(AddedCoach value),
    @required TResult removedCoach(RemovedCoach value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult addedGroupCoach(AddedGroupCoach value),
    @required TResult addedGroupAdmin(AddedGroupAdmin value),
    @required TResult updatedGroup(UpdatedGroup value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUsers(LoadedUsers value),
    @required TResult loadedGroup(LoadedGroup value),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return updatedGroup(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updatedGroupName(UpdatedGroupName value),
    TResult addedUserToGroup(AddedUserToGroup value),
    TResult deletedUserFromGroup(DeletedUser value),
    TResult deletedGroup(DeletedGroup value),
    TResult addedCoach(AddedCoach value),
    TResult removedCoach(RemovedCoach value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult addedGroupCoach(AddedGroupCoach value),
    TResult addedGroupAdmin(AddedGroupAdmin value),
    TResult updatedGroup(UpdatedGroup value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUsers(LoadedUsers value),
    TResult loadedGroup(LoadedGroup value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedGroup != null) {
      return updatedGroup(this);
    }
    return orElse();
  }
}

abstract class UpdatedGroup implements GroupsEvent {
  const factory UpdatedGroup(
      {@required String groupId,
      @required String name,
      @required List<String> coachIds,
      @required List<String> participantIds}) = _$UpdatedGroup;

  String get groupId;
  String get name;
  List<String> get coachIds;
  List<String> get participantIds;
  @JsonKey(ignore: true)
  $UpdatedGroupCopyWith<UpdatedGroup> get copyWith;
}

/// @nodoc
abstract class $LoadedGroupsCopyWith<$Res> {
  factory $LoadedGroupsCopyWith(
          LoadedGroups value, $Res Function(LoadedGroups) then) =
      _$LoadedGroupsCopyWithImpl<$Res>;
  $Res call({bool isCoach});
}

/// @nodoc
class _$LoadedGroupsCopyWithImpl<$Res> extends _$GroupsEventCopyWithImpl<$Res>
    implements $LoadedGroupsCopyWith<$Res> {
  _$LoadedGroupsCopyWithImpl(
      LoadedGroups _value, $Res Function(LoadedGroups) _then)
      : super(_value, (v) => _then(v as LoadedGroups));

  @override
  LoadedGroups get _value => super._value as LoadedGroups;

  @override
  $Res call({
    Object isCoach = freezed,
  }) {
    return _then(LoadedGroups(
      isCoach: isCoach == freezed ? _value.isCoach : isCoach as bool,
    ));
  }
}

/// @nodoc
class _$LoadedGroups implements LoadedGroups {
  const _$LoadedGroups({this.isCoach});

  @override
  final bool isCoach;

  @override
  String toString() {
    return 'GroupsEvent.loadedGroups(isCoach: $isCoach)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoadedGroups &&
            (identical(other.isCoach, isCoach) ||
                const DeepCollectionEquality().equals(other.isCoach, isCoach)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(isCoach);

  @JsonKey(ignore: true)
  @override
  $LoadedGroupsCopyWith<LoadedGroups> get copyWith =>
      _$LoadedGroupsCopyWithImpl<LoadedGroups>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult updatedGroupName(String groupId, String newName),
    @required TResult addedUserToGroup(String groupId, String userId),
    @required TResult deletedUserFromGroup(String groupId, String userId),
    @required TResult deletedGroup(String groupId),
    @required TResult addedCoach(String groupId, String coachId),
    @required TResult removedCoach(String groupId, String coachId),
    @required TResult updatedCoaches(String groupId, List<String> coachIds),
    @required TResult addedGroupCoach(String name, List<String> userIds),
    @required
        TResult addedGroupAdmin(
            String name, List<String> coachIds, List<String> userIds),
    @required
        TResult updatedGroup(String groupId, String name, List<String> coachIds,
            List<String> participantIds),
    @required TResult loadedGroups(bool isCoach),
    @required TResult loadedUsers(),
    @required TResult loadedGroup(String groupId),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return loadedGroups(isCoach);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updatedGroupName(String groupId, String newName),
    TResult addedUserToGroup(String groupId, String userId),
    TResult deletedUserFromGroup(String groupId, String userId),
    TResult deletedGroup(String groupId),
    TResult addedCoach(String groupId, String coachId),
    TResult removedCoach(String groupId, String coachId),
    TResult updatedCoaches(String groupId, List<String> coachIds),
    TResult addedGroupCoach(String name, List<String> userIds),
    TResult addedGroupAdmin(
        String name, List<String> coachIds, List<String> userIds),
    TResult updatedGroup(String groupId, String name, List<String> coachIds,
        List<String> participantIds),
    TResult loadedGroups(bool isCoach),
    TResult loadedUsers(),
    TResult loadedGroup(String groupId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedGroups != null) {
      return loadedGroups(isCoach);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updatedGroupName(UpdatedGroupName value),
    @required TResult addedUserToGroup(AddedUserToGroup value),
    @required TResult deletedUserFromGroup(DeletedUser value),
    @required TResult deletedGroup(DeletedGroup value),
    @required TResult addedCoach(AddedCoach value),
    @required TResult removedCoach(RemovedCoach value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult addedGroupCoach(AddedGroupCoach value),
    @required TResult addedGroupAdmin(AddedGroupAdmin value),
    @required TResult updatedGroup(UpdatedGroup value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUsers(LoadedUsers value),
    @required TResult loadedGroup(LoadedGroup value),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return loadedGroups(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updatedGroupName(UpdatedGroupName value),
    TResult addedUserToGroup(AddedUserToGroup value),
    TResult deletedUserFromGroup(DeletedUser value),
    TResult deletedGroup(DeletedGroup value),
    TResult addedCoach(AddedCoach value),
    TResult removedCoach(RemovedCoach value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult addedGroupCoach(AddedGroupCoach value),
    TResult addedGroupAdmin(AddedGroupAdmin value),
    TResult updatedGroup(UpdatedGroup value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUsers(LoadedUsers value),
    TResult loadedGroup(LoadedGroup value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedGroups != null) {
      return loadedGroups(this);
    }
    return orElse();
  }
}

abstract class LoadedGroups implements GroupsEvent {
  const factory LoadedGroups({bool isCoach}) = _$LoadedGroups;

  bool get isCoach;
  @JsonKey(ignore: true)
  $LoadedGroupsCopyWith<LoadedGroups> get copyWith;
}

/// @nodoc
abstract class $LoadedUsersCopyWith<$Res> {
  factory $LoadedUsersCopyWith(
          LoadedUsers value, $Res Function(LoadedUsers) then) =
      _$LoadedUsersCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoadedUsersCopyWithImpl<$Res> extends _$GroupsEventCopyWithImpl<$Res>
    implements $LoadedUsersCopyWith<$Res> {
  _$LoadedUsersCopyWithImpl(
      LoadedUsers _value, $Res Function(LoadedUsers) _then)
      : super(_value, (v) => _then(v as LoadedUsers));

  @override
  LoadedUsers get _value => super._value as LoadedUsers;
}

/// @nodoc
class _$LoadedUsers implements LoadedUsers {
  const _$LoadedUsers();

  @override
  String toString() {
    return 'GroupsEvent.loadedUsers()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoadedUsers);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult updatedGroupName(String groupId, String newName),
    @required TResult addedUserToGroup(String groupId, String userId),
    @required TResult deletedUserFromGroup(String groupId, String userId),
    @required TResult deletedGroup(String groupId),
    @required TResult addedCoach(String groupId, String coachId),
    @required TResult removedCoach(String groupId, String coachId),
    @required TResult updatedCoaches(String groupId, List<String> coachIds),
    @required TResult addedGroupCoach(String name, List<String> userIds),
    @required
        TResult addedGroupAdmin(
            String name, List<String> coachIds, List<String> userIds),
    @required
        TResult updatedGroup(String groupId, String name, List<String> coachIds,
            List<String> participantIds),
    @required TResult loadedGroups(bool isCoach),
    @required TResult loadedUsers(),
    @required TResult loadedGroup(String groupId),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return loadedUsers();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updatedGroupName(String groupId, String newName),
    TResult addedUserToGroup(String groupId, String userId),
    TResult deletedUserFromGroup(String groupId, String userId),
    TResult deletedGroup(String groupId),
    TResult addedCoach(String groupId, String coachId),
    TResult removedCoach(String groupId, String coachId),
    TResult updatedCoaches(String groupId, List<String> coachIds),
    TResult addedGroupCoach(String name, List<String> userIds),
    TResult addedGroupAdmin(
        String name, List<String> coachIds, List<String> userIds),
    TResult updatedGroup(String groupId, String name, List<String> coachIds,
        List<String> participantIds),
    TResult loadedGroups(bool isCoach),
    TResult loadedUsers(),
    TResult loadedGroup(String groupId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedUsers != null) {
      return loadedUsers();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updatedGroupName(UpdatedGroupName value),
    @required TResult addedUserToGroup(AddedUserToGroup value),
    @required TResult deletedUserFromGroup(DeletedUser value),
    @required TResult deletedGroup(DeletedGroup value),
    @required TResult addedCoach(AddedCoach value),
    @required TResult removedCoach(RemovedCoach value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult addedGroupCoach(AddedGroupCoach value),
    @required TResult addedGroupAdmin(AddedGroupAdmin value),
    @required TResult updatedGroup(UpdatedGroup value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUsers(LoadedUsers value),
    @required TResult loadedGroup(LoadedGroup value),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return loadedUsers(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updatedGroupName(UpdatedGroupName value),
    TResult addedUserToGroup(AddedUserToGroup value),
    TResult deletedUserFromGroup(DeletedUser value),
    TResult deletedGroup(DeletedGroup value),
    TResult addedCoach(AddedCoach value),
    TResult removedCoach(RemovedCoach value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult addedGroupCoach(AddedGroupCoach value),
    TResult addedGroupAdmin(AddedGroupAdmin value),
    TResult updatedGroup(UpdatedGroup value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUsers(LoadedUsers value),
    TResult loadedGroup(LoadedGroup value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedUsers != null) {
      return loadedUsers(this);
    }
    return orElse();
  }
}

abstract class LoadedUsers implements GroupsEvent {
  const factory LoadedUsers() = _$LoadedUsers;
}

/// @nodoc
abstract class $LoadedGroupCopyWith<$Res> {
  factory $LoadedGroupCopyWith(
          LoadedGroup value, $Res Function(LoadedGroup) then) =
      _$LoadedGroupCopyWithImpl<$Res>;
  $Res call({String groupId});
}

/// @nodoc
class _$LoadedGroupCopyWithImpl<$Res> extends _$GroupsEventCopyWithImpl<$Res>
    implements $LoadedGroupCopyWith<$Res> {
  _$LoadedGroupCopyWithImpl(
      LoadedGroup _value, $Res Function(LoadedGroup) _then)
      : super(_value, (v) => _then(v as LoadedGroup));

  @override
  LoadedGroup get _value => super._value as LoadedGroup;

  @override
  $Res call({
    Object groupId = freezed,
  }) {
    return _then(LoadedGroup(
      groupId: groupId == freezed ? _value.groupId : groupId as String,
    ));
  }
}

/// @nodoc
class _$LoadedGroup implements LoadedGroup {
  const _$LoadedGroup({@required this.groupId}) : assert(groupId != null);

  @override
  final String groupId;

  @override
  String toString() {
    return 'GroupsEvent.loadedGroup(groupId: $groupId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoadedGroup &&
            (identical(other.groupId, groupId) ||
                const DeepCollectionEquality().equals(other.groupId, groupId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(groupId);

  @JsonKey(ignore: true)
  @override
  $LoadedGroupCopyWith<LoadedGroup> get copyWith =>
      _$LoadedGroupCopyWithImpl<LoadedGroup>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult updatedGroupName(String groupId, String newName),
    @required TResult addedUserToGroup(String groupId, String userId),
    @required TResult deletedUserFromGroup(String groupId, String userId),
    @required TResult deletedGroup(String groupId),
    @required TResult addedCoach(String groupId, String coachId),
    @required TResult removedCoach(String groupId, String coachId),
    @required TResult updatedCoaches(String groupId, List<String> coachIds),
    @required TResult addedGroupCoach(String name, List<String> userIds),
    @required
        TResult addedGroupAdmin(
            String name, List<String> coachIds, List<String> userIds),
    @required
        TResult updatedGroup(String groupId, String name, List<String> coachIds,
            List<String> participantIds),
    @required TResult loadedGroups(bool isCoach),
    @required TResult loadedUsers(),
    @required TResult loadedGroup(String groupId),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return loadedGroup(groupId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updatedGroupName(String groupId, String newName),
    TResult addedUserToGroup(String groupId, String userId),
    TResult deletedUserFromGroup(String groupId, String userId),
    TResult deletedGroup(String groupId),
    TResult addedCoach(String groupId, String coachId),
    TResult removedCoach(String groupId, String coachId),
    TResult updatedCoaches(String groupId, List<String> coachIds),
    TResult addedGroupCoach(String name, List<String> userIds),
    TResult addedGroupAdmin(
        String name, List<String> coachIds, List<String> userIds),
    TResult updatedGroup(String groupId, String name, List<String> coachIds,
        List<String> participantIds),
    TResult loadedGroups(bool isCoach),
    TResult loadedUsers(),
    TResult loadedGroup(String groupId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedGroup != null) {
      return loadedGroup(groupId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updatedGroupName(UpdatedGroupName value),
    @required TResult addedUserToGroup(AddedUserToGroup value),
    @required TResult deletedUserFromGroup(DeletedUser value),
    @required TResult deletedGroup(DeletedGroup value),
    @required TResult addedCoach(AddedCoach value),
    @required TResult removedCoach(RemovedCoach value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult addedGroupCoach(AddedGroupCoach value),
    @required TResult addedGroupAdmin(AddedGroupAdmin value),
    @required TResult updatedGroup(UpdatedGroup value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedUsers(LoadedUsers value),
    @required TResult loadedGroup(LoadedGroup value),
  }) {
    assert(updatedGroupName != null);
    assert(addedUserToGroup != null);
    assert(deletedUserFromGroup != null);
    assert(deletedGroup != null);
    assert(addedCoach != null);
    assert(removedCoach != null);
    assert(updatedCoaches != null);
    assert(addedGroupCoach != null);
    assert(addedGroupAdmin != null);
    assert(updatedGroup != null);
    assert(loadedGroups != null);
    assert(loadedUsers != null);
    assert(loadedGroup != null);
    return loadedGroup(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updatedGroupName(UpdatedGroupName value),
    TResult addedUserToGroup(AddedUserToGroup value),
    TResult deletedUserFromGroup(DeletedUser value),
    TResult deletedGroup(DeletedGroup value),
    TResult addedCoach(AddedCoach value),
    TResult removedCoach(RemovedCoach value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult addedGroupCoach(AddedGroupCoach value),
    TResult addedGroupAdmin(AddedGroupAdmin value),
    TResult updatedGroup(UpdatedGroup value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedUsers(LoadedUsers value),
    TResult loadedGroup(LoadedGroup value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedGroup != null) {
      return loadedGroup(this);
    }
    return orElse();
  }
}

abstract class LoadedGroup implements GroupsEvent {
  const factory LoadedGroup({@required String groupId}) = _$LoadedGroup;

  String get groupId;
  @JsonKey(ignore: true)
  $LoadedGroupCopyWith<LoadedGroup> get copyWith;
}

/// @nodoc
class _$GroupsStateTearOff {
  const _$GroupsStateTearOff();

// ignore: unused_element
  GroupsInitial groupsInitial() {
    return GroupsInitial();
  }

// ignore: unused_element
  GroupUpdated groupUpdated({@required Group group, @required bool isUpdated}) {
    return GroupUpdated(
      group: group,
      isUpdated: isUpdated,
    );
  }

// ignore: unused_element
  GroupsUpdated groupsUpdated(
      {@required List<Group> groups, @required bool isUpdated}) {
    return GroupsUpdated(
      groups: groups,
      isUpdated: isUpdated,
    );
  }

// ignore: unused_element
  UsersUpdated usersUpdated(
      {@required List<User> users, @required bool isUpdated}) {
    return UsersUpdated(
      users: users,
      isUpdated: isUpdated,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $GroupsState = _$GroupsStateTearOff();

/// @nodoc
mixin _$GroupsState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult groupsInitial(),
    @required TResult groupUpdated(Group group, bool isUpdated),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required TResult usersUpdated(List<User> users, bool isUpdated),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult groupsInitial(),
    TResult groupUpdated(Group group, bool isUpdated),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    TResult usersUpdated(List<User> users, bool isUpdated),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult groupsInitial(GroupsInitial value),
    @required TResult groupUpdated(GroupUpdated value),
    @required TResult groupsUpdated(GroupsUpdated value),
    @required TResult usersUpdated(UsersUpdated value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult groupsInitial(GroupsInitial value),
    TResult groupUpdated(GroupUpdated value),
    TResult groupsUpdated(GroupsUpdated value),
    TResult usersUpdated(UsersUpdated value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $GroupsStateCopyWith<$Res> {
  factory $GroupsStateCopyWith(
          GroupsState value, $Res Function(GroupsState) then) =
      _$GroupsStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$GroupsStateCopyWithImpl<$Res> implements $GroupsStateCopyWith<$Res> {
  _$GroupsStateCopyWithImpl(this._value, this._then);

  final GroupsState _value;
  // ignore: unused_field
  final $Res Function(GroupsState) _then;
}

/// @nodoc
abstract class $GroupsInitialCopyWith<$Res> {
  factory $GroupsInitialCopyWith(
          GroupsInitial value, $Res Function(GroupsInitial) then) =
      _$GroupsInitialCopyWithImpl<$Res>;
}

/// @nodoc
class _$GroupsInitialCopyWithImpl<$Res> extends _$GroupsStateCopyWithImpl<$Res>
    implements $GroupsInitialCopyWith<$Res> {
  _$GroupsInitialCopyWithImpl(
      GroupsInitial _value, $Res Function(GroupsInitial) _then)
      : super(_value, (v) => _then(v as GroupsInitial));

  @override
  GroupsInitial get _value => super._value as GroupsInitial;
}

/// @nodoc
class _$GroupsInitial implements GroupsInitial {
  _$GroupsInitial();

  @override
  String toString() {
    return 'GroupsState.groupsInitial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is GroupsInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult groupsInitial(),
    @required TResult groupUpdated(Group group, bool isUpdated),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required TResult usersUpdated(List<User> users, bool isUpdated),
  }) {
    assert(groupsInitial != null);
    assert(groupUpdated != null);
    assert(groupsUpdated != null);
    assert(usersUpdated != null);
    return groupsInitial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult groupsInitial(),
    TResult groupUpdated(Group group, bool isUpdated),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    TResult usersUpdated(List<User> users, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (groupsInitial != null) {
      return groupsInitial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult groupsInitial(GroupsInitial value),
    @required TResult groupUpdated(GroupUpdated value),
    @required TResult groupsUpdated(GroupsUpdated value),
    @required TResult usersUpdated(UsersUpdated value),
  }) {
    assert(groupsInitial != null);
    assert(groupUpdated != null);
    assert(groupsUpdated != null);
    assert(usersUpdated != null);
    return groupsInitial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult groupsInitial(GroupsInitial value),
    TResult groupUpdated(GroupUpdated value),
    TResult groupsUpdated(GroupsUpdated value),
    TResult usersUpdated(UsersUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (groupsInitial != null) {
      return groupsInitial(this);
    }
    return orElse();
  }
}

abstract class GroupsInitial implements GroupsState {
  factory GroupsInitial() = _$GroupsInitial;
}

/// @nodoc
abstract class $GroupUpdatedCopyWith<$Res> {
  factory $GroupUpdatedCopyWith(
          GroupUpdated value, $Res Function(GroupUpdated) then) =
      _$GroupUpdatedCopyWithImpl<$Res>;
  $Res call({Group group, bool isUpdated});
}

/// @nodoc
class _$GroupUpdatedCopyWithImpl<$Res> extends _$GroupsStateCopyWithImpl<$Res>
    implements $GroupUpdatedCopyWith<$Res> {
  _$GroupUpdatedCopyWithImpl(
      GroupUpdated _value, $Res Function(GroupUpdated) _then)
      : super(_value, (v) => _then(v as GroupUpdated));

  @override
  GroupUpdated get _value => super._value as GroupUpdated;

  @override
  $Res call({
    Object group = freezed,
    Object isUpdated = freezed,
  }) {
    return _then(GroupUpdated(
      group: group == freezed ? _value.group : group as Group,
      isUpdated: isUpdated == freezed ? _value.isUpdated : isUpdated as bool,
    ));
  }
}

/// @nodoc
class _$GroupUpdated implements GroupUpdated {
  _$GroupUpdated({@required this.group, @required this.isUpdated})
      : assert(group != null),
        assert(isUpdated != null);

  @override
  final Group group;
  @override
  final bool isUpdated;

  @override
  String toString() {
    return 'GroupsState.groupUpdated(group: $group, isUpdated: $isUpdated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is GroupUpdated &&
            (identical(other.group, group) ||
                const DeepCollectionEquality().equals(other.group, group)) &&
            (identical(other.isUpdated, isUpdated) ||
                const DeepCollectionEquality()
                    .equals(other.isUpdated, isUpdated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(group) ^
      const DeepCollectionEquality().hash(isUpdated);

  @JsonKey(ignore: true)
  @override
  $GroupUpdatedCopyWith<GroupUpdated> get copyWith =>
      _$GroupUpdatedCopyWithImpl<GroupUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult groupsInitial(),
    @required TResult groupUpdated(Group group, bool isUpdated),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required TResult usersUpdated(List<User> users, bool isUpdated),
  }) {
    assert(groupsInitial != null);
    assert(groupUpdated != null);
    assert(groupsUpdated != null);
    assert(usersUpdated != null);
    return groupUpdated(group, isUpdated);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult groupsInitial(),
    TResult groupUpdated(Group group, bool isUpdated),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    TResult usersUpdated(List<User> users, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (groupUpdated != null) {
      return groupUpdated(group, isUpdated);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult groupsInitial(GroupsInitial value),
    @required TResult groupUpdated(GroupUpdated value),
    @required TResult groupsUpdated(GroupsUpdated value),
    @required TResult usersUpdated(UsersUpdated value),
  }) {
    assert(groupsInitial != null);
    assert(groupUpdated != null);
    assert(groupsUpdated != null);
    assert(usersUpdated != null);
    return groupUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult groupsInitial(GroupsInitial value),
    TResult groupUpdated(GroupUpdated value),
    TResult groupsUpdated(GroupsUpdated value),
    TResult usersUpdated(UsersUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (groupUpdated != null) {
      return groupUpdated(this);
    }
    return orElse();
  }
}

abstract class GroupUpdated implements GroupsState {
  factory GroupUpdated({@required Group group, @required bool isUpdated}) =
      _$GroupUpdated;

  Group get group;
  bool get isUpdated;
  @JsonKey(ignore: true)
  $GroupUpdatedCopyWith<GroupUpdated> get copyWith;
}

/// @nodoc
abstract class $GroupsUpdatedCopyWith<$Res> {
  factory $GroupsUpdatedCopyWith(
          GroupsUpdated value, $Res Function(GroupsUpdated) then) =
      _$GroupsUpdatedCopyWithImpl<$Res>;
  $Res call({List<Group> groups, bool isUpdated});
}

/// @nodoc
class _$GroupsUpdatedCopyWithImpl<$Res> extends _$GroupsStateCopyWithImpl<$Res>
    implements $GroupsUpdatedCopyWith<$Res> {
  _$GroupsUpdatedCopyWithImpl(
      GroupsUpdated _value, $Res Function(GroupsUpdated) _then)
      : super(_value, (v) => _then(v as GroupsUpdated));

  @override
  GroupsUpdated get _value => super._value as GroupsUpdated;

  @override
  $Res call({
    Object groups = freezed,
    Object isUpdated = freezed,
  }) {
    return _then(GroupsUpdated(
      groups: groups == freezed ? _value.groups : groups as List<Group>,
      isUpdated: isUpdated == freezed ? _value.isUpdated : isUpdated as bool,
    ));
  }
}

/// @nodoc
class _$GroupsUpdated implements GroupsUpdated {
  _$GroupsUpdated({@required this.groups, @required this.isUpdated})
      : assert(groups != null),
        assert(isUpdated != null);

  @override
  final List<Group> groups;
  @override
  final bool isUpdated;

  @override
  String toString() {
    return 'GroupsState.groupsUpdated(groups: $groups, isUpdated: $isUpdated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is GroupsUpdated &&
            (identical(other.groups, groups) ||
                const DeepCollectionEquality().equals(other.groups, groups)) &&
            (identical(other.isUpdated, isUpdated) ||
                const DeepCollectionEquality()
                    .equals(other.isUpdated, isUpdated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(groups) ^
      const DeepCollectionEquality().hash(isUpdated);

  @JsonKey(ignore: true)
  @override
  $GroupsUpdatedCopyWith<GroupsUpdated> get copyWith =>
      _$GroupsUpdatedCopyWithImpl<GroupsUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult groupsInitial(),
    @required TResult groupUpdated(Group group, bool isUpdated),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required TResult usersUpdated(List<User> users, bool isUpdated),
  }) {
    assert(groupsInitial != null);
    assert(groupUpdated != null);
    assert(groupsUpdated != null);
    assert(usersUpdated != null);
    return groupsUpdated(groups, isUpdated);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult groupsInitial(),
    TResult groupUpdated(Group group, bool isUpdated),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    TResult usersUpdated(List<User> users, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (groupsUpdated != null) {
      return groupsUpdated(groups, isUpdated);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult groupsInitial(GroupsInitial value),
    @required TResult groupUpdated(GroupUpdated value),
    @required TResult groupsUpdated(GroupsUpdated value),
    @required TResult usersUpdated(UsersUpdated value),
  }) {
    assert(groupsInitial != null);
    assert(groupUpdated != null);
    assert(groupsUpdated != null);
    assert(usersUpdated != null);
    return groupsUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult groupsInitial(GroupsInitial value),
    TResult groupUpdated(GroupUpdated value),
    TResult groupsUpdated(GroupsUpdated value),
    TResult usersUpdated(UsersUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (groupsUpdated != null) {
      return groupsUpdated(this);
    }
    return orElse();
  }
}

abstract class GroupsUpdated implements GroupsState {
  factory GroupsUpdated(
      {@required List<Group> groups,
      @required bool isUpdated}) = _$GroupsUpdated;

  List<Group> get groups;
  bool get isUpdated;
  @JsonKey(ignore: true)
  $GroupsUpdatedCopyWith<GroupsUpdated> get copyWith;
}

/// @nodoc
abstract class $UsersUpdatedCopyWith<$Res> {
  factory $UsersUpdatedCopyWith(
          UsersUpdated value, $Res Function(UsersUpdated) then) =
      _$UsersUpdatedCopyWithImpl<$Res>;
  $Res call({List<User> users, bool isUpdated});
}

/// @nodoc
class _$UsersUpdatedCopyWithImpl<$Res> extends _$GroupsStateCopyWithImpl<$Res>
    implements $UsersUpdatedCopyWith<$Res> {
  _$UsersUpdatedCopyWithImpl(
      UsersUpdated _value, $Res Function(UsersUpdated) _then)
      : super(_value, (v) => _then(v as UsersUpdated));

  @override
  UsersUpdated get _value => super._value as UsersUpdated;

  @override
  $Res call({
    Object users = freezed,
    Object isUpdated = freezed,
  }) {
    return _then(UsersUpdated(
      users: users == freezed ? _value.users : users as List<User>,
      isUpdated: isUpdated == freezed ? _value.isUpdated : isUpdated as bool,
    ));
  }
}

/// @nodoc
class _$UsersUpdated implements UsersUpdated {
  _$UsersUpdated({@required this.users, @required this.isUpdated})
      : assert(users != null),
        assert(isUpdated != null);

  @override
  final List<User> users;
  @override
  final bool isUpdated;

  @override
  String toString() {
    return 'GroupsState.usersUpdated(users: $users, isUpdated: $isUpdated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UsersUpdated &&
            (identical(other.users, users) ||
                const DeepCollectionEquality().equals(other.users, users)) &&
            (identical(other.isUpdated, isUpdated) ||
                const DeepCollectionEquality()
                    .equals(other.isUpdated, isUpdated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(users) ^
      const DeepCollectionEquality().hash(isUpdated);

  @JsonKey(ignore: true)
  @override
  $UsersUpdatedCopyWith<UsersUpdated> get copyWith =>
      _$UsersUpdatedCopyWithImpl<UsersUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult groupsInitial(),
    @required TResult groupUpdated(Group group, bool isUpdated),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required TResult usersUpdated(List<User> users, bool isUpdated),
  }) {
    assert(groupsInitial != null);
    assert(groupUpdated != null);
    assert(groupsUpdated != null);
    assert(usersUpdated != null);
    return usersUpdated(users, isUpdated);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult groupsInitial(),
    TResult groupUpdated(Group group, bool isUpdated),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    TResult usersUpdated(List<User> users, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (usersUpdated != null) {
      return usersUpdated(users, isUpdated);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult groupsInitial(GroupsInitial value),
    @required TResult groupUpdated(GroupUpdated value),
    @required TResult groupsUpdated(GroupsUpdated value),
    @required TResult usersUpdated(UsersUpdated value),
  }) {
    assert(groupsInitial != null);
    assert(groupUpdated != null);
    assert(groupsUpdated != null);
    assert(usersUpdated != null);
    return usersUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult groupsInitial(GroupsInitial value),
    TResult groupUpdated(GroupUpdated value),
    TResult groupsUpdated(GroupsUpdated value),
    TResult usersUpdated(UsersUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (usersUpdated != null) {
      return usersUpdated(this);
    }
    return orElse();
  }
}

abstract class UsersUpdated implements GroupsState {
  factory UsersUpdated({@required List<User> users, @required bool isUpdated}) =
      _$UsersUpdated;

  List<User> get users;
  bool get isUpdated;
  @JsonKey(ignore: true)
  $UsersUpdatedCopyWith<UsersUpdated> get copyWith;
}
