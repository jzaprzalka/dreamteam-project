part of 'groups_bloc.dart';

@freezed
abstract class GroupsState with _$GroupsState {
  factory GroupsState.groupsInitial() = GroupsInitial;
  factory GroupsState.groupUpdated(
      {@required Group group, @required bool isUpdated}) = GroupUpdated;
  factory GroupsState.groupsUpdated(
      {@required List<Group> groups, @required bool isUpdated}) = GroupsUpdated;
  factory GroupsState.usersUpdated(
      {@required List<User> users, @required bool isUpdated}) = UsersUpdated;
}
