// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'chatroom_add_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$ChatroomAddEventTearOff {
  const _$ChatroomAddEventTearOff();

// ignore: unused_element
  _LoadUsers loadUsers() {
    return const _LoadUsers();
  }

// ignore: unused_element
  _AddChatroom addChatroom({@required Chatroom chatroom}) {
    return _AddChatroom(
      chatroom: chatroom,
    );
  }

// ignore: unused_element
  _EditChatroom editChatroom(
      {@required String chatroomId, @required Chatroom chatroom}) {
    return _EditChatroom(
      chatroomId: chatroomId,
      chatroom: chatroom,
    );
  }

// ignore: unused_element
  _DeleteChatroom deleteChatroom({@required String chatroomId}) {
    return _DeleteChatroom(
      chatroomId: chatroomId,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $ChatroomAddEvent = _$ChatroomAddEventTearOff();

/// @nodoc
mixin _$ChatroomAddEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadUsers(),
    @required TResult addChatroom(Chatroom chatroom),
    @required TResult editChatroom(String chatroomId, Chatroom chatroom),
    @required TResult deleteChatroom(String chatroomId),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadUsers(),
    TResult addChatroom(Chatroom chatroom),
    TResult editChatroom(String chatroomId, Chatroom chatroom),
    TResult deleteChatroom(String chatroomId),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadUsers(_LoadUsers value),
    @required TResult addChatroom(_AddChatroom value),
    @required TResult editChatroom(_EditChatroom value),
    @required TResult deleteChatroom(_DeleteChatroom value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadUsers(_LoadUsers value),
    TResult addChatroom(_AddChatroom value),
    TResult editChatroom(_EditChatroom value),
    TResult deleteChatroom(_DeleteChatroom value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $ChatroomAddEventCopyWith<$Res> {
  factory $ChatroomAddEventCopyWith(
          ChatroomAddEvent value, $Res Function(ChatroomAddEvent) then) =
      _$ChatroomAddEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$ChatroomAddEventCopyWithImpl<$Res>
    implements $ChatroomAddEventCopyWith<$Res> {
  _$ChatroomAddEventCopyWithImpl(this._value, this._then);

  final ChatroomAddEvent _value;
  // ignore: unused_field
  final $Res Function(ChatroomAddEvent) _then;
}

/// @nodoc
abstract class _$LoadUsersCopyWith<$Res> {
  factory _$LoadUsersCopyWith(
          _LoadUsers value, $Res Function(_LoadUsers) then) =
      __$LoadUsersCopyWithImpl<$Res>;
}

/// @nodoc
class __$LoadUsersCopyWithImpl<$Res>
    extends _$ChatroomAddEventCopyWithImpl<$Res>
    implements _$LoadUsersCopyWith<$Res> {
  __$LoadUsersCopyWithImpl(_LoadUsers _value, $Res Function(_LoadUsers) _then)
      : super(_value, (v) => _then(v as _LoadUsers));

  @override
  _LoadUsers get _value => super._value as _LoadUsers;
}

/// @nodoc
class _$_LoadUsers implements _LoadUsers {
  const _$_LoadUsers();

  @override
  String toString() {
    return 'ChatroomAddEvent.loadUsers()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _LoadUsers);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadUsers(),
    @required TResult addChatroom(Chatroom chatroom),
    @required TResult editChatroom(String chatroomId, Chatroom chatroom),
    @required TResult deleteChatroom(String chatroomId),
  }) {
    assert(loadUsers != null);
    assert(addChatroom != null);
    assert(editChatroom != null);
    assert(deleteChatroom != null);
    return loadUsers();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadUsers(),
    TResult addChatroom(Chatroom chatroom),
    TResult editChatroom(String chatroomId, Chatroom chatroom),
    TResult deleteChatroom(String chatroomId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadUsers != null) {
      return loadUsers();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadUsers(_LoadUsers value),
    @required TResult addChatroom(_AddChatroom value),
    @required TResult editChatroom(_EditChatroom value),
    @required TResult deleteChatroom(_DeleteChatroom value),
  }) {
    assert(loadUsers != null);
    assert(addChatroom != null);
    assert(editChatroom != null);
    assert(deleteChatroom != null);
    return loadUsers(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadUsers(_LoadUsers value),
    TResult addChatroom(_AddChatroom value),
    TResult editChatroom(_EditChatroom value),
    TResult deleteChatroom(_DeleteChatroom value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadUsers != null) {
      return loadUsers(this);
    }
    return orElse();
  }
}

abstract class _LoadUsers implements ChatroomAddEvent {
  const factory _LoadUsers() = _$_LoadUsers;
}

/// @nodoc
abstract class _$AddChatroomCopyWith<$Res> {
  factory _$AddChatroomCopyWith(
          _AddChatroom value, $Res Function(_AddChatroom) then) =
      __$AddChatroomCopyWithImpl<$Res>;
  $Res call({Chatroom chatroom});
}

/// @nodoc
class __$AddChatroomCopyWithImpl<$Res>
    extends _$ChatroomAddEventCopyWithImpl<$Res>
    implements _$AddChatroomCopyWith<$Res> {
  __$AddChatroomCopyWithImpl(
      _AddChatroom _value, $Res Function(_AddChatroom) _then)
      : super(_value, (v) => _then(v as _AddChatroom));

  @override
  _AddChatroom get _value => super._value as _AddChatroom;

  @override
  $Res call({
    Object chatroom = freezed,
  }) {
    return _then(_AddChatroom(
      chatroom: chatroom == freezed ? _value.chatroom : chatroom as Chatroom,
    ));
  }
}

/// @nodoc
class _$_AddChatroom implements _AddChatroom {
  const _$_AddChatroom({@required this.chatroom}) : assert(chatroom != null);

  @override
  final Chatroom chatroom;

  @override
  String toString() {
    return 'ChatroomAddEvent.addChatroom(chatroom: $chatroom)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _AddChatroom &&
            (identical(other.chatroom, chatroom) ||
                const DeepCollectionEquality()
                    .equals(other.chatroom, chatroom)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(chatroom);

  @JsonKey(ignore: true)
  @override
  _$AddChatroomCopyWith<_AddChatroom> get copyWith =>
      __$AddChatroomCopyWithImpl<_AddChatroom>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadUsers(),
    @required TResult addChatroom(Chatroom chatroom),
    @required TResult editChatroom(String chatroomId, Chatroom chatroom),
    @required TResult deleteChatroom(String chatroomId),
  }) {
    assert(loadUsers != null);
    assert(addChatroom != null);
    assert(editChatroom != null);
    assert(deleteChatroom != null);
    return addChatroom(chatroom);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadUsers(),
    TResult addChatroom(Chatroom chatroom),
    TResult editChatroom(String chatroomId, Chatroom chatroom),
    TResult deleteChatroom(String chatroomId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addChatroom != null) {
      return addChatroom(chatroom);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadUsers(_LoadUsers value),
    @required TResult addChatroom(_AddChatroom value),
    @required TResult editChatroom(_EditChatroom value),
    @required TResult deleteChatroom(_DeleteChatroom value),
  }) {
    assert(loadUsers != null);
    assert(addChatroom != null);
    assert(editChatroom != null);
    assert(deleteChatroom != null);
    return addChatroom(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadUsers(_LoadUsers value),
    TResult addChatroom(_AddChatroom value),
    TResult editChatroom(_EditChatroom value),
    TResult deleteChatroom(_DeleteChatroom value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addChatroom != null) {
      return addChatroom(this);
    }
    return orElse();
  }
}

abstract class _AddChatroom implements ChatroomAddEvent {
  const factory _AddChatroom({@required Chatroom chatroom}) = _$_AddChatroom;

  Chatroom get chatroom;
  @JsonKey(ignore: true)
  _$AddChatroomCopyWith<_AddChatroom> get copyWith;
}

/// @nodoc
abstract class _$EditChatroomCopyWith<$Res> {
  factory _$EditChatroomCopyWith(
          _EditChatroom value, $Res Function(_EditChatroom) then) =
      __$EditChatroomCopyWithImpl<$Res>;
  $Res call({String chatroomId, Chatroom chatroom});
}

/// @nodoc
class __$EditChatroomCopyWithImpl<$Res>
    extends _$ChatroomAddEventCopyWithImpl<$Res>
    implements _$EditChatroomCopyWith<$Res> {
  __$EditChatroomCopyWithImpl(
      _EditChatroom _value, $Res Function(_EditChatroom) _then)
      : super(_value, (v) => _then(v as _EditChatroom));

  @override
  _EditChatroom get _value => super._value as _EditChatroom;

  @override
  $Res call({
    Object chatroomId = freezed,
    Object chatroom = freezed,
  }) {
    return _then(_EditChatroom(
      chatroomId:
          chatroomId == freezed ? _value.chatroomId : chatroomId as String,
      chatroom: chatroom == freezed ? _value.chatroom : chatroom as Chatroom,
    ));
  }
}

/// @nodoc
class _$_EditChatroom implements _EditChatroom {
  const _$_EditChatroom({@required this.chatroomId, @required this.chatroom})
      : assert(chatroomId != null),
        assert(chatroom != null);

  @override
  final String chatroomId;
  @override
  final Chatroom chatroom;

  @override
  String toString() {
    return 'ChatroomAddEvent.editChatroom(chatroomId: $chatroomId, chatroom: $chatroom)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EditChatroom &&
            (identical(other.chatroomId, chatroomId) ||
                const DeepCollectionEquality()
                    .equals(other.chatroomId, chatroomId)) &&
            (identical(other.chatroom, chatroom) ||
                const DeepCollectionEquality()
                    .equals(other.chatroom, chatroom)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(chatroomId) ^
      const DeepCollectionEquality().hash(chatroom);

  @JsonKey(ignore: true)
  @override
  _$EditChatroomCopyWith<_EditChatroom> get copyWith =>
      __$EditChatroomCopyWithImpl<_EditChatroom>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadUsers(),
    @required TResult addChatroom(Chatroom chatroom),
    @required TResult editChatroom(String chatroomId, Chatroom chatroom),
    @required TResult deleteChatroom(String chatroomId),
  }) {
    assert(loadUsers != null);
    assert(addChatroom != null);
    assert(editChatroom != null);
    assert(deleteChatroom != null);
    return editChatroom(chatroomId, chatroom);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadUsers(),
    TResult addChatroom(Chatroom chatroom),
    TResult editChatroom(String chatroomId, Chatroom chatroom),
    TResult deleteChatroom(String chatroomId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (editChatroom != null) {
      return editChatroom(chatroomId, chatroom);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadUsers(_LoadUsers value),
    @required TResult addChatroom(_AddChatroom value),
    @required TResult editChatroom(_EditChatroom value),
    @required TResult deleteChatroom(_DeleteChatroom value),
  }) {
    assert(loadUsers != null);
    assert(addChatroom != null);
    assert(editChatroom != null);
    assert(deleteChatroom != null);
    return editChatroom(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadUsers(_LoadUsers value),
    TResult addChatroom(_AddChatroom value),
    TResult editChatroom(_EditChatroom value),
    TResult deleteChatroom(_DeleteChatroom value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (editChatroom != null) {
      return editChatroom(this);
    }
    return orElse();
  }
}

abstract class _EditChatroom implements ChatroomAddEvent {
  const factory _EditChatroom(
      {@required String chatroomId,
      @required Chatroom chatroom}) = _$_EditChatroom;

  String get chatroomId;
  Chatroom get chatroom;
  @JsonKey(ignore: true)
  _$EditChatroomCopyWith<_EditChatroom> get copyWith;
}

/// @nodoc
abstract class _$DeleteChatroomCopyWith<$Res> {
  factory _$DeleteChatroomCopyWith(
          _DeleteChatroom value, $Res Function(_DeleteChatroom) then) =
      __$DeleteChatroomCopyWithImpl<$Res>;
  $Res call({String chatroomId});
}

/// @nodoc
class __$DeleteChatroomCopyWithImpl<$Res>
    extends _$ChatroomAddEventCopyWithImpl<$Res>
    implements _$DeleteChatroomCopyWith<$Res> {
  __$DeleteChatroomCopyWithImpl(
      _DeleteChatroom _value, $Res Function(_DeleteChatroom) _then)
      : super(_value, (v) => _then(v as _DeleteChatroom));

  @override
  _DeleteChatroom get _value => super._value as _DeleteChatroom;

  @override
  $Res call({
    Object chatroomId = freezed,
  }) {
    return _then(_DeleteChatroom(
      chatroomId:
          chatroomId == freezed ? _value.chatroomId : chatroomId as String,
    ));
  }
}

/// @nodoc
class _$_DeleteChatroom implements _DeleteChatroom {
  const _$_DeleteChatroom({@required this.chatroomId})
      : assert(chatroomId != null);

  @override
  final String chatroomId;

  @override
  String toString() {
    return 'ChatroomAddEvent.deleteChatroom(chatroomId: $chatroomId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _DeleteChatroom &&
            (identical(other.chatroomId, chatroomId) ||
                const DeepCollectionEquality()
                    .equals(other.chatroomId, chatroomId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(chatroomId);

  @JsonKey(ignore: true)
  @override
  _$DeleteChatroomCopyWith<_DeleteChatroom> get copyWith =>
      __$DeleteChatroomCopyWithImpl<_DeleteChatroom>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadUsers(),
    @required TResult addChatroom(Chatroom chatroom),
    @required TResult editChatroom(String chatroomId, Chatroom chatroom),
    @required TResult deleteChatroom(String chatroomId),
  }) {
    assert(loadUsers != null);
    assert(addChatroom != null);
    assert(editChatroom != null);
    assert(deleteChatroom != null);
    return deleteChatroom(chatroomId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadUsers(),
    TResult addChatroom(Chatroom chatroom),
    TResult editChatroom(String chatroomId, Chatroom chatroom),
    TResult deleteChatroom(String chatroomId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (deleteChatroom != null) {
      return deleteChatroom(chatroomId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadUsers(_LoadUsers value),
    @required TResult addChatroom(_AddChatroom value),
    @required TResult editChatroom(_EditChatroom value),
    @required TResult deleteChatroom(_DeleteChatroom value),
  }) {
    assert(loadUsers != null);
    assert(addChatroom != null);
    assert(editChatroom != null);
    assert(deleteChatroom != null);
    return deleteChatroom(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadUsers(_LoadUsers value),
    TResult addChatroom(_AddChatroom value),
    TResult editChatroom(_EditChatroom value),
    TResult deleteChatroom(_DeleteChatroom value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (deleteChatroom != null) {
      return deleteChatroom(this);
    }
    return orElse();
  }
}

abstract class _DeleteChatroom implements ChatroomAddEvent {
  const factory _DeleteChatroom({@required String chatroomId}) =
      _$_DeleteChatroom;

  String get chatroomId;
  @JsonKey(ignore: true)
  _$DeleteChatroomCopyWith<_DeleteChatroom> get copyWith;
}

/// @nodoc
class _$ChatroomAddStateTearOff {
  const _$ChatroomAddStateTearOff();

// ignore: unused_element
  _ChatroomAddState call({@required List<User> users}) {
    return _ChatroomAddState(
      users: users,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $ChatroomAddState = _$ChatroomAddStateTearOff();

/// @nodoc
mixin _$ChatroomAddState {
  List<User> get users;

  @JsonKey(ignore: true)
  $ChatroomAddStateCopyWith<ChatroomAddState> get copyWith;
}

/// @nodoc
abstract class $ChatroomAddStateCopyWith<$Res> {
  factory $ChatroomAddStateCopyWith(
          ChatroomAddState value, $Res Function(ChatroomAddState) then) =
      _$ChatroomAddStateCopyWithImpl<$Res>;
  $Res call({List<User> users});
}

/// @nodoc
class _$ChatroomAddStateCopyWithImpl<$Res>
    implements $ChatroomAddStateCopyWith<$Res> {
  _$ChatroomAddStateCopyWithImpl(this._value, this._then);

  final ChatroomAddState _value;
  // ignore: unused_field
  final $Res Function(ChatroomAddState) _then;

  @override
  $Res call({
    Object users = freezed,
  }) {
    return _then(_value.copyWith(
      users: users == freezed ? _value.users : users as List<User>,
    ));
  }
}

/// @nodoc
abstract class _$ChatroomAddStateCopyWith<$Res>
    implements $ChatroomAddStateCopyWith<$Res> {
  factory _$ChatroomAddStateCopyWith(
          _ChatroomAddState value, $Res Function(_ChatroomAddState) then) =
      __$ChatroomAddStateCopyWithImpl<$Res>;
  @override
  $Res call({List<User> users});
}

/// @nodoc
class __$ChatroomAddStateCopyWithImpl<$Res>
    extends _$ChatroomAddStateCopyWithImpl<$Res>
    implements _$ChatroomAddStateCopyWith<$Res> {
  __$ChatroomAddStateCopyWithImpl(
      _ChatroomAddState _value, $Res Function(_ChatroomAddState) _then)
      : super(_value, (v) => _then(v as _ChatroomAddState));

  @override
  _ChatroomAddState get _value => super._value as _ChatroomAddState;

  @override
  $Res call({
    Object users = freezed,
  }) {
    return _then(_ChatroomAddState(
      users: users == freezed ? _value.users : users as List<User>,
    ));
  }
}

/// @nodoc
class _$_ChatroomAddState implements _ChatroomAddState {
  const _$_ChatroomAddState({@required this.users}) : assert(users != null);

  @override
  final List<User> users;

  @override
  String toString() {
    return 'ChatroomAddState(users: $users)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ChatroomAddState &&
            (identical(other.users, users) ||
                const DeepCollectionEquality().equals(other.users, users)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(users);

  @JsonKey(ignore: true)
  @override
  _$ChatroomAddStateCopyWith<_ChatroomAddState> get copyWith =>
      __$ChatroomAddStateCopyWithImpl<_ChatroomAddState>(this, _$identity);
}

abstract class _ChatroomAddState implements ChatroomAddState {
  const factory _ChatroomAddState({@required List<User> users}) =
      _$_ChatroomAddState;

  @override
  List<User> get users;
  @override
  @JsonKey(ignore: true)
  _$ChatroomAddStateCopyWith<_ChatroomAddState> get copyWith;
}
