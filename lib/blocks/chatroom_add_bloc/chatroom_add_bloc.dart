import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dreamteam_project/models/Chatroom.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/repositories/authentication_repository.dart';
import 'package:dreamteam_project/repositories/groups_data_repository.dart';
import 'package:dreamteam_project/repositories/messages_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'chatroom_add_event.dart';
part 'chatroom_add_state.dart';
part 'chatroom_add_bloc.freezed.dart';

class ChatroomAddBloc extends Bloc<ChatroomAddEvent, ChatroomAddState> {
  MessagesRepository messagesRepository = MessagesRepository();
  GroupsDataRepository groupsDataRepository = GroupsDataRepository();
  AuthenticationRepository authenticationRepository =
      AuthenticationRepository();

  ChatroomAddBloc() : super(ChatroomAddState.initial());

  Future<String> get _userId async =>
      (await authenticationRepository.getCurrentUser()).uid;

  Future<List<User>> get _users async {
    String userId = await _userId;
    List<User> users = await groupsDataRepository.getAllUsers();
    users.removeWhere((u) => u.userId == userId);
    return users;
  }

  @override
  Stream<ChatroomAddState> mapEventToState(
    ChatroomAddEvent event,
  ) async* {
    yield* event.map(
      loadUsers: (e) async* {
        yield state.copyWith(users: (await _users));
      },
      addChatroom: (e) async* {
        String userId = await _userId;
        if (!e.chatroom.userIds.contains(userId))
          e.chatroom.userIds.add(userId);
        await messagesRepository.createChatroom(e.chatroom);
      },
      deleteChatroom: (e) async* {
        await messagesRepository.deleteChatroom(e.chatroomId);
      },
      editChatroom: (e) async* {
        String userId = await _userId;
        if (!e.chatroom.userIds.contains(userId))
          e.chatroom.userIds.add(userId);
        await messagesRepository.updateChatroomParticipants(
            e.chatroomId, e.chatroom.userIds);
      },
    );
  }
}
