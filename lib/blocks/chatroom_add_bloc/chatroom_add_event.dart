part of 'chatroom_add_bloc.dart';

@freezed
abstract class ChatroomAddEvent with _$ChatroomAddEvent {
  const factory ChatroomAddEvent.loadUsers() = _LoadUsers;
  const factory ChatroomAddEvent.addChatroom({@required Chatroom chatroom}) =
      _AddChatroom;
  const factory ChatroomAddEvent.editChatroom({
    @required String chatroomId,
    @required Chatroom chatroom,
  }) = _EditChatroom;
  const factory ChatroomAddEvent.deleteChatroom({@required String chatroomId}) =
      _DeleteChatroom;
}
