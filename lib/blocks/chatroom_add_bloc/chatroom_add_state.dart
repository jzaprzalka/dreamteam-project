part of 'chatroom_add_bloc.dart';

@freezed
abstract class ChatroomAddState with _$ChatroomAddState {
  const factory ChatroomAddState({@required List<User> users}) =
      _ChatroomAddState;

  factory ChatroomAddState.initial() => _ChatroomAddState(users: []);
}
