// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'messages_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$MessagesEventTearOff {
  const _$MessagesEventTearOff();

// ignore: unused_element
  _SendMessage sendMessage({@required String messageText}) {
    return _SendMessage(
      messageText: messageText,
    );
  }

// ignore: unused_element
  _LoadMessages loadMessages(
      {@required Chatroom chatroom, @required User user}) {
    return _LoadMessages(
      chatroom: chatroom,
      user: user,
    );
  }

// ignore: unused_element
  _ReceivedMessages receivedMessages({@required List<Message> messages}) {
    return _ReceivedMessages(
      messages: messages,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $MessagesEvent = _$MessagesEventTearOff();

/// @nodoc
mixin _$MessagesEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult sendMessage(String messageText),
    @required TResult loadMessages(Chatroom chatroom, User user),
    @required TResult receivedMessages(List<Message> messages),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult sendMessage(String messageText),
    TResult loadMessages(Chatroom chatroom, User user),
    TResult receivedMessages(List<Message> messages),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult sendMessage(_SendMessage value),
    @required TResult loadMessages(_LoadMessages value),
    @required TResult receivedMessages(_ReceivedMessages value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult sendMessage(_SendMessage value),
    TResult loadMessages(_LoadMessages value),
    TResult receivedMessages(_ReceivedMessages value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $MessagesEventCopyWith<$Res> {
  factory $MessagesEventCopyWith(
          MessagesEvent value, $Res Function(MessagesEvent) then) =
      _$MessagesEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$MessagesEventCopyWithImpl<$Res>
    implements $MessagesEventCopyWith<$Res> {
  _$MessagesEventCopyWithImpl(this._value, this._then);

  final MessagesEvent _value;
  // ignore: unused_field
  final $Res Function(MessagesEvent) _then;
}

/// @nodoc
abstract class _$SendMessageCopyWith<$Res> {
  factory _$SendMessageCopyWith(
          _SendMessage value, $Res Function(_SendMessage) then) =
      __$SendMessageCopyWithImpl<$Res>;
  $Res call({String messageText});
}

/// @nodoc
class __$SendMessageCopyWithImpl<$Res> extends _$MessagesEventCopyWithImpl<$Res>
    implements _$SendMessageCopyWith<$Res> {
  __$SendMessageCopyWithImpl(
      _SendMessage _value, $Res Function(_SendMessage) _then)
      : super(_value, (v) => _then(v as _SendMessage));

  @override
  _SendMessage get _value => super._value as _SendMessage;

  @override
  $Res call({
    Object messageText = freezed,
  }) {
    return _then(_SendMessage(
      messageText:
          messageText == freezed ? _value.messageText : messageText as String,
    ));
  }
}

/// @nodoc
class _$_SendMessage implements _SendMessage {
  const _$_SendMessage({@required this.messageText})
      : assert(messageText != null);

  @override
  final String messageText;

  @override
  String toString() {
    return 'MessagesEvent.sendMessage(messageText: $messageText)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SendMessage &&
            (identical(other.messageText, messageText) ||
                const DeepCollectionEquality()
                    .equals(other.messageText, messageText)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(messageText);

  @JsonKey(ignore: true)
  @override
  _$SendMessageCopyWith<_SendMessage> get copyWith =>
      __$SendMessageCopyWithImpl<_SendMessage>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult sendMessage(String messageText),
    @required TResult loadMessages(Chatroom chatroom, User user),
    @required TResult receivedMessages(List<Message> messages),
  }) {
    assert(sendMessage != null);
    assert(loadMessages != null);
    assert(receivedMessages != null);
    return sendMessage(messageText);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult sendMessage(String messageText),
    TResult loadMessages(Chatroom chatroom, User user),
    TResult receivedMessages(List<Message> messages),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (sendMessage != null) {
      return sendMessage(messageText);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult sendMessage(_SendMessage value),
    @required TResult loadMessages(_LoadMessages value),
    @required TResult receivedMessages(_ReceivedMessages value),
  }) {
    assert(sendMessage != null);
    assert(loadMessages != null);
    assert(receivedMessages != null);
    return sendMessage(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult sendMessage(_SendMessage value),
    TResult loadMessages(_LoadMessages value),
    TResult receivedMessages(_ReceivedMessages value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (sendMessage != null) {
      return sendMessage(this);
    }
    return orElse();
  }
}

abstract class _SendMessage implements MessagesEvent {
  const factory _SendMessage({@required String messageText}) = _$_SendMessage;

  String get messageText;
  @JsonKey(ignore: true)
  _$SendMessageCopyWith<_SendMessage> get copyWith;
}

/// @nodoc
abstract class _$LoadMessagesCopyWith<$Res> {
  factory _$LoadMessagesCopyWith(
          _LoadMessages value, $Res Function(_LoadMessages) then) =
      __$LoadMessagesCopyWithImpl<$Res>;
  $Res call({Chatroom chatroom, User user});
}

/// @nodoc
class __$LoadMessagesCopyWithImpl<$Res>
    extends _$MessagesEventCopyWithImpl<$Res>
    implements _$LoadMessagesCopyWith<$Res> {
  __$LoadMessagesCopyWithImpl(
      _LoadMessages _value, $Res Function(_LoadMessages) _then)
      : super(_value, (v) => _then(v as _LoadMessages));

  @override
  _LoadMessages get _value => super._value as _LoadMessages;

  @override
  $Res call({
    Object chatroom = freezed,
    Object user = freezed,
  }) {
    return _then(_LoadMessages(
      chatroom: chatroom == freezed ? _value.chatroom : chatroom as Chatroom,
      user: user == freezed ? _value.user : user as User,
    ));
  }
}

/// @nodoc
class _$_LoadMessages implements _LoadMessages {
  const _$_LoadMessages({@required this.chatroom, @required this.user})
      : assert(chatroom != null),
        assert(user != null);

  @override
  final Chatroom chatroom;
  @override
  final User user;

  @override
  String toString() {
    return 'MessagesEvent.loadMessages(chatroom: $chatroom, user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _LoadMessages &&
            (identical(other.chatroom, chatroom) ||
                const DeepCollectionEquality()
                    .equals(other.chatroom, chatroom)) &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(chatroom) ^
      const DeepCollectionEquality().hash(user);

  @JsonKey(ignore: true)
  @override
  _$LoadMessagesCopyWith<_LoadMessages> get copyWith =>
      __$LoadMessagesCopyWithImpl<_LoadMessages>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult sendMessage(String messageText),
    @required TResult loadMessages(Chatroom chatroom, User user),
    @required TResult receivedMessages(List<Message> messages),
  }) {
    assert(sendMessage != null);
    assert(loadMessages != null);
    assert(receivedMessages != null);
    return loadMessages(chatroom, user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult sendMessage(String messageText),
    TResult loadMessages(Chatroom chatroom, User user),
    TResult receivedMessages(List<Message> messages),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadMessages != null) {
      return loadMessages(chatroom, user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult sendMessage(_SendMessage value),
    @required TResult loadMessages(_LoadMessages value),
    @required TResult receivedMessages(_ReceivedMessages value),
  }) {
    assert(sendMessage != null);
    assert(loadMessages != null);
    assert(receivedMessages != null);
    return loadMessages(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult sendMessage(_SendMessage value),
    TResult loadMessages(_LoadMessages value),
    TResult receivedMessages(_ReceivedMessages value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadMessages != null) {
      return loadMessages(this);
    }
    return orElse();
  }
}

abstract class _LoadMessages implements MessagesEvent {
  const factory _LoadMessages(
      {@required Chatroom chatroom, @required User user}) = _$_LoadMessages;

  Chatroom get chatroom;
  User get user;
  @JsonKey(ignore: true)
  _$LoadMessagesCopyWith<_LoadMessages> get copyWith;
}

/// @nodoc
abstract class _$ReceivedMessagesCopyWith<$Res> {
  factory _$ReceivedMessagesCopyWith(
          _ReceivedMessages value, $Res Function(_ReceivedMessages) then) =
      __$ReceivedMessagesCopyWithImpl<$Res>;
  $Res call({List<Message> messages});
}

/// @nodoc
class __$ReceivedMessagesCopyWithImpl<$Res>
    extends _$MessagesEventCopyWithImpl<$Res>
    implements _$ReceivedMessagesCopyWith<$Res> {
  __$ReceivedMessagesCopyWithImpl(
      _ReceivedMessages _value, $Res Function(_ReceivedMessages) _then)
      : super(_value, (v) => _then(v as _ReceivedMessages));

  @override
  _ReceivedMessages get _value => super._value as _ReceivedMessages;

  @override
  $Res call({
    Object messages = freezed,
  }) {
    return _then(_ReceivedMessages(
      messages:
          messages == freezed ? _value.messages : messages as List<Message>,
    ));
  }
}

/// @nodoc
class _$_ReceivedMessages implements _ReceivedMessages {
  const _$_ReceivedMessages({@required this.messages})
      : assert(messages != null);

  @override
  final List<Message> messages;

  @override
  String toString() {
    return 'MessagesEvent.receivedMessages(messages: $messages)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ReceivedMessages &&
            (identical(other.messages, messages) ||
                const DeepCollectionEquality()
                    .equals(other.messages, messages)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(messages);

  @JsonKey(ignore: true)
  @override
  _$ReceivedMessagesCopyWith<_ReceivedMessages> get copyWith =>
      __$ReceivedMessagesCopyWithImpl<_ReceivedMessages>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult sendMessage(String messageText),
    @required TResult loadMessages(Chatroom chatroom, User user),
    @required TResult receivedMessages(List<Message> messages),
  }) {
    assert(sendMessage != null);
    assert(loadMessages != null);
    assert(receivedMessages != null);
    return receivedMessages(messages);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult sendMessage(String messageText),
    TResult loadMessages(Chatroom chatroom, User user),
    TResult receivedMessages(List<Message> messages),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (receivedMessages != null) {
      return receivedMessages(messages);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult sendMessage(_SendMessage value),
    @required TResult loadMessages(_LoadMessages value),
    @required TResult receivedMessages(_ReceivedMessages value),
  }) {
    assert(sendMessage != null);
    assert(loadMessages != null);
    assert(receivedMessages != null);
    return receivedMessages(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult sendMessage(_SendMessage value),
    TResult loadMessages(_LoadMessages value),
    TResult receivedMessages(_ReceivedMessages value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (receivedMessages != null) {
      return receivedMessages(this);
    }
    return orElse();
  }
}

abstract class _ReceivedMessages implements MessagesEvent {
  const factory _ReceivedMessages({@required List<Message> messages}) =
      _$_ReceivedMessages;

  List<Message> get messages;
  @JsonKey(ignore: true)
  _$ReceivedMessagesCopyWith<_ReceivedMessages> get copyWith;
}

/// @nodoc
class _$MessagesStateTearOff {
  const _$MessagesStateTearOff();

// ignore: unused_element
  _MessagesState call(
      {@required User user,
      @required Chatroom chatroom,
      @required List<Message> messages}) {
    return _MessagesState(
      user: user,
      chatroom: chatroom,
      messages: messages,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $MessagesState = _$MessagesStateTearOff();

/// @nodoc
mixin _$MessagesState {
  User get user;
  Chatroom get chatroom;
  List<Message> get messages;

  @JsonKey(ignore: true)
  $MessagesStateCopyWith<MessagesState> get copyWith;
}

/// @nodoc
abstract class $MessagesStateCopyWith<$Res> {
  factory $MessagesStateCopyWith(
          MessagesState value, $Res Function(MessagesState) then) =
      _$MessagesStateCopyWithImpl<$Res>;
  $Res call({User user, Chatroom chatroom, List<Message> messages});
}

/// @nodoc
class _$MessagesStateCopyWithImpl<$Res>
    implements $MessagesStateCopyWith<$Res> {
  _$MessagesStateCopyWithImpl(this._value, this._then);

  final MessagesState _value;
  // ignore: unused_field
  final $Res Function(MessagesState) _then;

  @override
  $Res call({
    Object user = freezed,
    Object chatroom = freezed,
    Object messages = freezed,
  }) {
    return _then(_value.copyWith(
      user: user == freezed ? _value.user : user as User,
      chatroom: chatroom == freezed ? _value.chatroom : chatroom as Chatroom,
      messages:
          messages == freezed ? _value.messages : messages as List<Message>,
    ));
  }
}

/// @nodoc
abstract class _$MessagesStateCopyWith<$Res>
    implements $MessagesStateCopyWith<$Res> {
  factory _$MessagesStateCopyWith(
          _MessagesState value, $Res Function(_MessagesState) then) =
      __$MessagesStateCopyWithImpl<$Res>;
  @override
  $Res call({User user, Chatroom chatroom, List<Message> messages});
}

/// @nodoc
class __$MessagesStateCopyWithImpl<$Res>
    extends _$MessagesStateCopyWithImpl<$Res>
    implements _$MessagesStateCopyWith<$Res> {
  __$MessagesStateCopyWithImpl(
      _MessagesState _value, $Res Function(_MessagesState) _then)
      : super(_value, (v) => _then(v as _MessagesState));

  @override
  _MessagesState get _value => super._value as _MessagesState;

  @override
  $Res call({
    Object user = freezed,
    Object chatroom = freezed,
    Object messages = freezed,
  }) {
    return _then(_MessagesState(
      user: user == freezed ? _value.user : user as User,
      chatroom: chatroom == freezed ? _value.chatroom : chatroom as Chatroom,
      messages:
          messages == freezed ? _value.messages : messages as List<Message>,
    ));
  }
}

/// @nodoc
class _$_MessagesState implements _MessagesState {
  const _$_MessagesState(
      {@required this.user, @required this.chatroom, @required this.messages})
      : assert(user != null),
        assert(chatroom != null),
        assert(messages != null);

  @override
  final User user;
  @override
  final Chatroom chatroom;
  @override
  final List<Message> messages;

  @override
  String toString() {
    return 'MessagesState(user: $user, chatroom: $chatroom, messages: $messages)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _MessagesState &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)) &&
            (identical(other.chatroom, chatroom) ||
                const DeepCollectionEquality()
                    .equals(other.chatroom, chatroom)) &&
            (identical(other.messages, messages) ||
                const DeepCollectionEquality()
                    .equals(other.messages, messages)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(user) ^
      const DeepCollectionEquality().hash(chatroom) ^
      const DeepCollectionEquality().hash(messages);

  @JsonKey(ignore: true)
  @override
  _$MessagesStateCopyWith<_MessagesState> get copyWith =>
      __$MessagesStateCopyWithImpl<_MessagesState>(this, _$identity);
}

abstract class _MessagesState implements MessagesState {
  const factory _MessagesState(
      {@required User user,
      @required Chatroom chatroom,
      @required List<Message> messages}) = _$_MessagesState;

  @override
  User get user;
  @override
  Chatroom get chatroom;
  @override
  List<Message> get messages;
  @override
  @JsonKey(ignore: true)
  _$MessagesStateCopyWith<_MessagesState> get copyWith;
}
