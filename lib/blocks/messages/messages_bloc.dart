import 'package:bloc/bloc.dart';
import 'package:dreamteam_project/generated/intl/messages_en.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/Chatroom.dart';
import 'package:dreamteam_project/models/Messages.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/repositories/messages_repository.dart';
import 'package:dreamteam_project/repositories/user_data_repository.dart';
import 'dart:async';
import 'package:meta/meta.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'messages_event.dart';
part 'messages_state.dart';
part 'messages_bloc.freezed.dart';

class MessagesBloc extends Bloc<MessagesEvent, MessagesState> {
  MessagesRepository messagesRepository = MessagesRepository();
  UserDataRepository userDataRepository = UserDataRepository();
  StreamSubscription messageSubscription;

  MessagesBloc() : super(MessagesState.initial());

  @override
  Stream<MessagesState> mapEventToState(MessagesEvent event) async* {
    yield* event.map(sendMessage: (e) async* {
      final mess =
          TextMessage(e.messageText, DateTime.now(), state.user.userId);
      messagesRepository.addMessage(mess, state.chatroom.chatroomId);
    }, loadMessages: (e) async* {
      e.chatroom.users = await userDataRepository.getUsers(e.chatroom.userIds);
      yield state.copyWith(user: e.user, chatroom: e.chatroom);
      messagesRepository.getMessagesForChatroomStream(e.chatroom.chatroomId);
      messageSubscription?.cancel();
      messageSubscription = messagesRepository
          .getMessagesForChatroomStream(e.chatroom.chatroomId)
          .listen((messages) =>
              add(MessagesEvent.receivedMessages(messages: messages)));
    }, receivedMessages: (e) async* {
      yield state.copyWith(messages: e.messages);
    });
  }
}
