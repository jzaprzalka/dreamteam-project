part of 'messages_bloc.dart';

@freezed
abstract class MessagesEvent with _$MessagesEvent {
  const factory MessagesEvent.sendMessage(
      {@required String messageText}) = _SendMessage;
  const factory MessagesEvent.loadMessages(
      {@required Chatroom chatroom, @required User user}) = _LoadMessages;
  const factory MessagesEvent.receivedMessages(
      {@required List<Message> messages}) = _ReceivedMessages;
}
