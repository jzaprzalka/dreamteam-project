part of 'messages_bloc.dart';

@freezed
abstract class MessagesState with _$MessagesState {
  const factory MessagesState(
      {@required User user,
      @required Chatroom chatroom,
      @required List<Message> messages}) = _MessagesState;

  factory MessagesState.initial() => _MessagesState(
        user: User(name: '', surname: '', userId: ''),
        chatroom: Chatroom(chatroomId: ""),
        messages: List.empty(),
      );
}
