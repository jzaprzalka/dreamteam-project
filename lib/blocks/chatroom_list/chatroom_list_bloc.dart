import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/Chatroom.dart';
import 'package:dreamteam_project/models/Messages.dart';
import 'package:dreamteam_project/repositories/messages_repository.dart';
import 'package:dreamteam_project/repositories/user_data_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'chatroom_list_event.dart';
part 'chatroom_list_state.dart';
part 'chatroom_list_bloc.freezed.dart';

class ChatroomListBloc extends Bloc<ChatroomListEvent, ChatroomListState> {
  MessagesRepository messagesRepository = MessagesRepository();
  UserDataRepository userDataRepository = UserDataRepository();

  ChatroomListBloc() : super(ChatroomListState.initial());

  @override
  Stream<ChatroomListState> mapEventToState(
    ChatroomListEvent event,
  ) async* {
    yield* event.map(
      loadChatroomForUser: (e) async* {
        List<Chatroom> chatrooms =
            await messagesRepository.getUsersChatrooms(e.userId);
        for (int i = 0; i < chatrooms.length; i++) {
          chatrooms[i].users =
              await userDataRepository.getUsers(chatrooms[i].userIds);
        }
        yield state.copyWith(userId: e.userId, chatrooms: chatrooms);
      },
    );
  }
}
