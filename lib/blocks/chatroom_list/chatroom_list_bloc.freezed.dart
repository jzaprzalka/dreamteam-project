// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'chatroom_list_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$ChatroomListEventTearOff {
  const _$ChatroomListEventTearOff();

// ignore: unused_element
  _LoadChatroomForUser loadChatroomForUser(String userId) {
    return _LoadChatroomForUser(
      userId,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $ChatroomListEvent = _$ChatroomListEventTearOff();

/// @nodoc
mixin _$ChatroomListEvent {
  String get userId;

  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadChatroomForUser(String userId),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadChatroomForUser(String userId),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadChatroomForUser(_LoadChatroomForUser value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadChatroomForUser(_LoadChatroomForUser value),
    @required TResult orElse(),
  });

  @JsonKey(ignore: true)
  $ChatroomListEventCopyWith<ChatroomListEvent> get copyWith;
}

/// @nodoc
abstract class $ChatroomListEventCopyWith<$Res> {
  factory $ChatroomListEventCopyWith(
          ChatroomListEvent value, $Res Function(ChatroomListEvent) then) =
      _$ChatroomListEventCopyWithImpl<$Res>;
  $Res call({String userId});
}

/// @nodoc
class _$ChatroomListEventCopyWithImpl<$Res>
    implements $ChatroomListEventCopyWith<$Res> {
  _$ChatroomListEventCopyWithImpl(this._value, this._then);

  final ChatroomListEvent _value;
  // ignore: unused_field
  final $Res Function(ChatroomListEvent) _then;

  @override
  $Res call({
    Object userId = freezed,
  }) {
    return _then(_value.copyWith(
      userId: userId == freezed ? _value.userId : userId as String,
    ));
  }
}

/// @nodoc
abstract class _$LoadChatroomForUserCopyWith<$Res>
    implements $ChatroomListEventCopyWith<$Res> {
  factory _$LoadChatroomForUserCopyWith(_LoadChatroomForUser value,
          $Res Function(_LoadChatroomForUser) then) =
      __$LoadChatroomForUserCopyWithImpl<$Res>;
  @override
  $Res call({String userId});
}

/// @nodoc
class __$LoadChatroomForUserCopyWithImpl<$Res>
    extends _$ChatroomListEventCopyWithImpl<$Res>
    implements _$LoadChatroomForUserCopyWith<$Res> {
  __$LoadChatroomForUserCopyWithImpl(
      _LoadChatroomForUser _value, $Res Function(_LoadChatroomForUser) _then)
      : super(_value, (v) => _then(v as _LoadChatroomForUser));

  @override
  _LoadChatroomForUser get _value => super._value as _LoadChatroomForUser;

  @override
  $Res call({
    Object userId = freezed,
  }) {
    return _then(_LoadChatroomForUser(
      userId == freezed ? _value.userId : userId as String,
    ));
  }
}

/// @nodoc
class _$_LoadChatroomForUser implements _LoadChatroomForUser {
  const _$_LoadChatroomForUser(this.userId) : assert(userId != null);

  @override
  final String userId;

  @override
  String toString() {
    return 'ChatroomListEvent.loadChatroomForUser(userId: $userId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _LoadChatroomForUser &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(userId);

  @JsonKey(ignore: true)
  @override
  _$LoadChatroomForUserCopyWith<_LoadChatroomForUser> get copyWith =>
      __$LoadChatroomForUserCopyWithImpl<_LoadChatroomForUser>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadChatroomForUser(String userId),
  }) {
    assert(loadChatroomForUser != null);
    return loadChatroomForUser(userId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadChatroomForUser(String userId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadChatroomForUser != null) {
      return loadChatroomForUser(userId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadChatroomForUser(_LoadChatroomForUser value),
  }) {
    assert(loadChatroomForUser != null);
    return loadChatroomForUser(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadChatroomForUser(_LoadChatroomForUser value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadChatroomForUser != null) {
      return loadChatroomForUser(this);
    }
    return orElse();
  }
}

abstract class _LoadChatroomForUser implements ChatroomListEvent {
  const factory _LoadChatroomForUser(String userId) = _$_LoadChatroomForUser;

  @override
  String get userId;
  @override
  @JsonKey(ignore: true)
  _$LoadChatroomForUserCopyWith<_LoadChatroomForUser> get copyWith;
}

/// @nodoc
class _$ChatroomListStateTearOff {
  const _$ChatroomListStateTearOff();

// ignore: unused_element
  _ChatroomListState call(
      {@required String userId, @required List<Chatroom> chatrooms}) {
    return _ChatroomListState(
      userId: userId,
      chatrooms: chatrooms,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $ChatroomListState = _$ChatroomListStateTearOff();

/// @nodoc
mixin _$ChatroomListState {
  String get userId;
  List<Chatroom> get chatrooms;

  @JsonKey(ignore: true)
  $ChatroomListStateCopyWith<ChatroomListState> get copyWith;
}

/// @nodoc
abstract class $ChatroomListStateCopyWith<$Res> {
  factory $ChatroomListStateCopyWith(
          ChatroomListState value, $Res Function(ChatroomListState) then) =
      _$ChatroomListStateCopyWithImpl<$Res>;
  $Res call({String userId, List<Chatroom> chatrooms});
}

/// @nodoc
class _$ChatroomListStateCopyWithImpl<$Res>
    implements $ChatroomListStateCopyWith<$Res> {
  _$ChatroomListStateCopyWithImpl(this._value, this._then);

  final ChatroomListState _value;
  // ignore: unused_field
  final $Res Function(ChatroomListState) _then;

  @override
  $Res call({
    Object userId = freezed,
    Object chatrooms = freezed,
  }) {
    return _then(_value.copyWith(
      userId: userId == freezed ? _value.userId : userId as String,
      chatrooms:
          chatrooms == freezed ? _value.chatrooms : chatrooms as List<Chatroom>,
    ));
  }
}

/// @nodoc
abstract class _$ChatroomListStateCopyWith<$Res>
    implements $ChatroomListStateCopyWith<$Res> {
  factory _$ChatroomListStateCopyWith(
          _ChatroomListState value, $Res Function(_ChatroomListState) then) =
      __$ChatroomListStateCopyWithImpl<$Res>;
  @override
  $Res call({String userId, List<Chatroom> chatrooms});
}

/// @nodoc
class __$ChatroomListStateCopyWithImpl<$Res>
    extends _$ChatroomListStateCopyWithImpl<$Res>
    implements _$ChatroomListStateCopyWith<$Res> {
  __$ChatroomListStateCopyWithImpl(
      _ChatroomListState _value, $Res Function(_ChatroomListState) _then)
      : super(_value, (v) => _then(v as _ChatroomListState));

  @override
  _ChatroomListState get _value => super._value as _ChatroomListState;

  @override
  $Res call({
    Object userId = freezed,
    Object chatrooms = freezed,
  }) {
    return _then(_ChatroomListState(
      userId: userId == freezed ? _value.userId : userId as String,
      chatrooms:
          chatrooms == freezed ? _value.chatrooms : chatrooms as List<Chatroom>,
    ));
  }
}

/// @nodoc
class _$_ChatroomListState implements _ChatroomListState {
  const _$_ChatroomListState({@required this.userId, @required this.chatrooms})
      : assert(userId != null),
        assert(chatrooms != null);

  @override
  final String userId;
  @override
  final List<Chatroom> chatrooms;

  @override
  String toString() {
    return 'ChatroomListState(userId: $userId, chatrooms: $chatrooms)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ChatroomListState &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)) &&
            (identical(other.chatrooms, chatrooms) ||
                const DeepCollectionEquality()
                    .equals(other.chatrooms, chatrooms)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(userId) ^
      const DeepCollectionEquality().hash(chatrooms);

  @JsonKey(ignore: true)
  @override
  _$ChatroomListStateCopyWith<_ChatroomListState> get copyWith =>
      __$ChatroomListStateCopyWithImpl<_ChatroomListState>(this, _$identity);
}

abstract class _ChatroomListState implements ChatroomListState {
  const factory _ChatroomListState(
      {@required String userId,
      @required List<Chatroom> chatrooms}) = _$_ChatroomListState;

  @override
  String get userId;
  @override
  List<Chatroom> get chatrooms;
  @override
  @JsonKey(ignore: true)
  _$ChatroomListStateCopyWith<_ChatroomListState> get copyWith;
}
