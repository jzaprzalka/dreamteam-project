part of 'chatroom_list_bloc.dart';

@freezed
abstract class ChatroomListEvent with _$ChatroomListEvent {
  const factory ChatroomListEvent.loadChatroomForUser(String userId) = _LoadChatroomForUser;
}