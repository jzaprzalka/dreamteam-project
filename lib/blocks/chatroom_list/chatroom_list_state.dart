part of 'chatroom_list_bloc.dart';

@freezed
abstract class ChatroomListState with _$ChatroomListState {
  const factory ChatroomListState({@required String userId, @required List<Chatroom> chatrooms}) =
      _ChatroomListState;

  factory ChatroomListState.initial() =>
      _ChatroomListState(userId: "", chatrooms: []);
}
