part of 'payments_bloc.dart';

@freezed
abstract class PaymentsState with _$PaymentsState {
  factory PaymentsState.initial() = PaymentsInitial;
  factory PaymentsState.listUpdated(
      {@required List<Payment> payments,
      @required bool isUpdated}) = PaymentsUpdated;
}
