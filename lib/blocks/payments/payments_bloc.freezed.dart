// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'payments_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$PaymentsEventTearOff {
  const _$PaymentsEventTearOff();

// ignore: unused_element
  PaymentAccounted paymentAccounted(
      {@required Payment payment,
      @required DateTime dateAccounted,
      @required String newEmail}) {
    return PaymentAccounted(
      payment: payment,
      dateAccounted: dateAccounted,
      newEmail: newEmail,
    );
  }

// ignore: unused_element
  PaymentAdded paymentAdded({@required Payment payment}) {
    return PaymentAdded(
      payment: payment,
    );
  }

// ignore: unused_element
  PaymentsLoaded loadedPayments() {
    return const PaymentsLoaded();
  }
}

/// @nodoc
// ignore: unused_element
const $PaymentsEvent = _$PaymentsEventTearOff();

/// @nodoc
mixin _$PaymentsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required
        TResult paymentAccounted(
            Payment payment, DateTime dateAccounted, String newEmail),
    @required TResult paymentAdded(Payment payment),
    @required TResult loadedPayments(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult paymentAccounted(
        Payment payment, DateTime dateAccounted, String newEmail),
    TResult paymentAdded(Payment payment),
    TResult loadedPayments(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult paymentAccounted(PaymentAccounted value),
    @required TResult paymentAdded(PaymentAdded value),
    @required TResult loadedPayments(PaymentsLoaded value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult paymentAccounted(PaymentAccounted value),
    TResult paymentAdded(PaymentAdded value),
    TResult loadedPayments(PaymentsLoaded value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $PaymentsEventCopyWith<$Res> {
  factory $PaymentsEventCopyWith(
          PaymentsEvent value, $Res Function(PaymentsEvent) then) =
      _$PaymentsEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$PaymentsEventCopyWithImpl<$Res>
    implements $PaymentsEventCopyWith<$Res> {
  _$PaymentsEventCopyWithImpl(this._value, this._then);

  final PaymentsEvent _value;
  // ignore: unused_field
  final $Res Function(PaymentsEvent) _then;
}

/// @nodoc
abstract class $PaymentAccountedCopyWith<$Res> {
  factory $PaymentAccountedCopyWith(
          PaymentAccounted value, $Res Function(PaymentAccounted) then) =
      _$PaymentAccountedCopyWithImpl<$Res>;
  $Res call({Payment payment, DateTime dateAccounted, String newEmail});
}

/// @nodoc
class _$PaymentAccountedCopyWithImpl<$Res>
    extends _$PaymentsEventCopyWithImpl<$Res>
    implements $PaymentAccountedCopyWith<$Res> {
  _$PaymentAccountedCopyWithImpl(
      PaymentAccounted _value, $Res Function(PaymentAccounted) _then)
      : super(_value, (v) => _then(v as PaymentAccounted));

  @override
  PaymentAccounted get _value => super._value as PaymentAccounted;

  @override
  $Res call({
    Object payment = freezed,
    Object dateAccounted = freezed,
    Object newEmail = freezed,
  }) {
    return _then(PaymentAccounted(
      payment: payment == freezed ? _value.payment : payment as Payment,
      dateAccounted: dateAccounted == freezed
          ? _value.dateAccounted
          : dateAccounted as DateTime,
      newEmail: newEmail == freezed ? _value.newEmail : newEmail as String,
    ));
  }
}

/// @nodoc
class _$PaymentAccounted implements PaymentAccounted {
  const _$PaymentAccounted(
      {@required this.payment,
      @required this.dateAccounted,
      @required this.newEmail})
      : assert(payment != null),
        assert(dateAccounted != null),
        assert(newEmail != null);

  @override
  final Payment payment;
  @override
  final DateTime dateAccounted;
  @override
  final String newEmail;

  @override
  String toString() {
    return 'PaymentsEvent.paymentAccounted(payment: $payment, dateAccounted: $dateAccounted, newEmail: $newEmail)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is PaymentAccounted &&
            (identical(other.payment, payment) ||
                const DeepCollectionEquality()
                    .equals(other.payment, payment)) &&
            (identical(other.dateAccounted, dateAccounted) ||
                const DeepCollectionEquality()
                    .equals(other.dateAccounted, dateAccounted)) &&
            (identical(other.newEmail, newEmail) ||
                const DeepCollectionEquality()
                    .equals(other.newEmail, newEmail)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(payment) ^
      const DeepCollectionEquality().hash(dateAccounted) ^
      const DeepCollectionEquality().hash(newEmail);

  @JsonKey(ignore: true)
  @override
  $PaymentAccountedCopyWith<PaymentAccounted> get copyWith =>
      _$PaymentAccountedCopyWithImpl<PaymentAccounted>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required
        TResult paymentAccounted(
            Payment payment, DateTime dateAccounted, String newEmail),
    @required TResult paymentAdded(Payment payment),
    @required TResult loadedPayments(),
  }) {
    assert(paymentAccounted != null);
    assert(paymentAdded != null);
    assert(loadedPayments != null);
    return paymentAccounted(payment, dateAccounted, newEmail);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult paymentAccounted(
        Payment payment, DateTime dateAccounted, String newEmail),
    TResult paymentAdded(Payment payment),
    TResult loadedPayments(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (paymentAccounted != null) {
      return paymentAccounted(payment, dateAccounted, newEmail);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult paymentAccounted(PaymentAccounted value),
    @required TResult paymentAdded(PaymentAdded value),
    @required TResult loadedPayments(PaymentsLoaded value),
  }) {
    assert(paymentAccounted != null);
    assert(paymentAdded != null);
    assert(loadedPayments != null);
    return paymentAccounted(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult paymentAccounted(PaymentAccounted value),
    TResult paymentAdded(PaymentAdded value),
    TResult loadedPayments(PaymentsLoaded value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (paymentAccounted != null) {
      return paymentAccounted(this);
    }
    return orElse();
  }
}

abstract class PaymentAccounted implements PaymentsEvent {
  const factory PaymentAccounted(
      {@required Payment payment,
      @required DateTime dateAccounted,
      @required String newEmail}) = _$PaymentAccounted;

  Payment get payment;
  DateTime get dateAccounted;
  String get newEmail;
  @JsonKey(ignore: true)
  $PaymentAccountedCopyWith<PaymentAccounted> get copyWith;
}

/// @nodoc
abstract class $PaymentAddedCopyWith<$Res> {
  factory $PaymentAddedCopyWith(
          PaymentAdded value, $Res Function(PaymentAdded) then) =
      _$PaymentAddedCopyWithImpl<$Res>;
  $Res call({Payment payment});
}

/// @nodoc
class _$PaymentAddedCopyWithImpl<$Res> extends _$PaymentsEventCopyWithImpl<$Res>
    implements $PaymentAddedCopyWith<$Res> {
  _$PaymentAddedCopyWithImpl(
      PaymentAdded _value, $Res Function(PaymentAdded) _then)
      : super(_value, (v) => _then(v as PaymentAdded));

  @override
  PaymentAdded get _value => super._value as PaymentAdded;

  @override
  $Res call({
    Object payment = freezed,
  }) {
    return _then(PaymentAdded(
      payment: payment == freezed ? _value.payment : payment as Payment,
    ));
  }
}

/// @nodoc
class _$PaymentAdded implements PaymentAdded {
  const _$PaymentAdded({@required this.payment}) : assert(payment != null);

  @override
  final Payment payment;

  @override
  String toString() {
    return 'PaymentsEvent.paymentAdded(payment: $payment)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is PaymentAdded &&
            (identical(other.payment, payment) ||
                const DeepCollectionEquality().equals(other.payment, payment)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(payment);

  @JsonKey(ignore: true)
  @override
  $PaymentAddedCopyWith<PaymentAdded> get copyWith =>
      _$PaymentAddedCopyWithImpl<PaymentAdded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required
        TResult paymentAccounted(
            Payment payment, DateTime dateAccounted, String newEmail),
    @required TResult paymentAdded(Payment payment),
    @required TResult loadedPayments(),
  }) {
    assert(paymentAccounted != null);
    assert(paymentAdded != null);
    assert(loadedPayments != null);
    return paymentAdded(payment);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult paymentAccounted(
        Payment payment, DateTime dateAccounted, String newEmail),
    TResult paymentAdded(Payment payment),
    TResult loadedPayments(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (paymentAdded != null) {
      return paymentAdded(payment);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult paymentAccounted(PaymentAccounted value),
    @required TResult paymentAdded(PaymentAdded value),
    @required TResult loadedPayments(PaymentsLoaded value),
  }) {
    assert(paymentAccounted != null);
    assert(paymentAdded != null);
    assert(loadedPayments != null);
    return paymentAdded(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult paymentAccounted(PaymentAccounted value),
    TResult paymentAdded(PaymentAdded value),
    TResult loadedPayments(PaymentsLoaded value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (paymentAdded != null) {
      return paymentAdded(this);
    }
    return orElse();
  }
}

abstract class PaymentAdded implements PaymentsEvent {
  const factory PaymentAdded({@required Payment payment}) = _$PaymentAdded;

  Payment get payment;
  @JsonKey(ignore: true)
  $PaymentAddedCopyWith<PaymentAdded> get copyWith;
}

/// @nodoc
abstract class $PaymentsLoadedCopyWith<$Res> {
  factory $PaymentsLoadedCopyWith(
          PaymentsLoaded value, $Res Function(PaymentsLoaded) then) =
      _$PaymentsLoadedCopyWithImpl<$Res>;
}

/// @nodoc
class _$PaymentsLoadedCopyWithImpl<$Res>
    extends _$PaymentsEventCopyWithImpl<$Res>
    implements $PaymentsLoadedCopyWith<$Res> {
  _$PaymentsLoadedCopyWithImpl(
      PaymentsLoaded _value, $Res Function(PaymentsLoaded) _then)
      : super(_value, (v) => _then(v as PaymentsLoaded));

  @override
  PaymentsLoaded get _value => super._value as PaymentsLoaded;
}

/// @nodoc
class _$PaymentsLoaded implements PaymentsLoaded {
  const _$PaymentsLoaded();

  @override
  String toString() {
    return 'PaymentsEvent.loadedPayments()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is PaymentsLoaded);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required
        TResult paymentAccounted(
            Payment payment, DateTime dateAccounted, String newEmail),
    @required TResult paymentAdded(Payment payment),
    @required TResult loadedPayments(),
  }) {
    assert(paymentAccounted != null);
    assert(paymentAdded != null);
    assert(loadedPayments != null);
    return loadedPayments();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult paymentAccounted(
        Payment payment, DateTime dateAccounted, String newEmail),
    TResult paymentAdded(Payment payment),
    TResult loadedPayments(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedPayments != null) {
      return loadedPayments();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult paymentAccounted(PaymentAccounted value),
    @required TResult paymentAdded(PaymentAdded value),
    @required TResult loadedPayments(PaymentsLoaded value),
  }) {
    assert(paymentAccounted != null);
    assert(paymentAdded != null);
    assert(loadedPayments != null);
    return loadedPayments(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult paymentAccounted(PaymentAccounted value),
    TResult paymentAdded(PaymentAdded value),
    TResult loadedPayments(PaymentsLoaded value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedPayments != null) {
      return loadedPayments(this);
    }
    return orElse();
  }
}

abstract class PaymentsLoaded implements PaymentsEvent {
  const factory PaymentsLoaded() = _$PaymentsLoaded;
}

/// @nodoc
class _$PaymentsStateTearOff {
  const _$PaymentsStateTearOff();

// ignore: unused_element
  PaymentsInitial initial() {
    return PaymentsInitial();
  }

// ignore: unused_element
  PaymentsUpdated listUpdated(
      {@required List<Payment> payments, @required bool isUpdated}) {
    return PaymentsUpdated(
      payments: payments,
      isUpdated: isUpdated,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $PaymentsState = _$PaymentsStateTearOff();

/// @nodoc
mixin _$PaymentsState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult listUpdated(List<Payment> payments, bool isUpdated),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult listUpdated(List<Payment> payments, bool isUpdated),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(PaymentsInitial value),
    @required TResult listUpdated(PaymentsUpdated value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(PaymentsInitial value),
    TResult listUpdated(PaymentsUpdated value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $PaymentsStateCopyWith<$Res> {
  factory $PaymentsStateCopyWith(
          PaymentsState value, $Res Function(PaymentsState) then) =
      _$PaymentsStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$PaymentsStateCopyWithImpl<$Res>
    implements $PaymentsStateCopyWith<$Res> {
  _$PaymentsStateCopyWithImpl(this._value, this._then);

  final PaymentsState _value;
  // ignore: unused_field
  final $Res Function(PaymentsState) _then;
}

/// @nodoc
abstract class $PaymentsInitialCopyWith<$Res> {
  factory $PaymentsInitialCopyWith(
          PaymentsInitial value, $Res Function(PaymentsInitial) then) =
      _$PaymentsInitialCopyWithImpl<$Res>;
}

/// @nodoc
class _$PaymentsInitialCopyWithImpl<$Res>
    extends _$PaymentsStateCopyWithImpl<$Res>
    implements $PaymentsInitialCopyWith<$Res> {
  _$PaymentsInitialCopyWithImpl(
      PaymentsInitial _value, $Res Function(PaymentsInitial) _then)
      : super(_value, (v) => _then(v as PaymentsInitial));

  @override
  PaymentsInitial get _value => super._value as PaymentsInitial;
}

/// @nodoc
class _$PaymentsInitial implements PaymentsInitial {
  _$PaymentsInitial();

  @override
  String toString() {
    return 'PaymentsState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is PaymentsInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult listUpdated(List<Payment> payments, bool isUpdated),
  }) {
    assert(initial != null);
    assert(listUpdated != null);
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult listUpdated(List<Payment> payments, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(PaymentsInitial value),
    @required TResult listUpdated(PaymentsUpdated value),
  }) {
    assert(initial != null);
    assert(listUpdated != null);
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(PaymentsInitial value),
    TResult listUpdated(PaymentsUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class PaymentsInitial implements PaymentsState {
  factory PaymentsInitial() = _$PaymentsInitial;
}

/// @nodoc
abstract class $PaymentsUpdatedCopyWith<$Res> {
  factory $PaymentsUpdatedCopyWith(
          PaymentsUpdated value, $Res Function(PaymentsUpdated) then) =
      _$PaymentsUpdatedCopyWithImpl<$Res>;
  $Res call({List<Payment> payments, bool isUpdated});
}

/// @nodoc
class _$PaymentsUpdatedCopyWithImpl<$Res>
    extends _$PaymentsStateCopyWithImpl<$Res>
    implements $PaymentsUpdatedCopyWith<$Res> {
  _$PaymentsUpdatedCopyWithImpl(
      PaymentsUpdated _value, $Res Function(PaymentsUpdated) _then)
      : super(_value, (v) => _then(v as PaymentsUpdated));

  @override
  PaymentsUpdated get _value => super._value as PaymentsUpdated;

  @override
  $Res call({
    Object payments = freezed,
    Object isUpdated = freezed,
  }) {
    return _then(PaymentsUpdated(
      payments:
          payments == freezed ? _value.payments : payments as List<Payment>,
      isUpdated: isUpdated == freezed ? _value.isUpdated : isUpdated as bool,
    ));
  }
}

/// @nodoc
class _$PaymentsUpdated implements PaymentsUpdated {
  _$PaymentsUpdated({@required this.payments, @required this.isUpdated})
      : assert(payments != null),
        assert(isUpdated != null);

  @override
  final List<Payment> payments;
  @override
  final bool isUpdated;

  @override
  String toString() {
    return 'PaymentsState.listUpdated(payments: $payments, isUpdated: $isUpdated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is PaymentsUpdated &&
            (identical(other.payments, payments) ||
                const DeepCollectionEquality()
                    .equals(other.payments, payments)) &&
            (identical(other.isUpdated, isUpdated) ||
                const DeepCollectionEquality()
                    .equals(other.isUpdated, isUpdated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(payments) ^
      const DeepCollectionEquality().hash(isUpdated);

  @JsonKey(ignore: true)
  @override
  $PaymentsUpdatedCopyWith<PaymentsUpdated> get copyWith =>
      _$PaymentsUpdatedCopyWithImpl<PaymentsUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult listUpdated(List<Payment> payments, bool isUpdated),
  }) {
    assert(initial != null);
    assert(listUpdated != null);
    return listUpdated(payments, isUpdated);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult listUpdated(List<Payment> payments, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (listUpdated != null) {
      return listUpdated(payments, isUpdated);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(PaymentsInitial value),
    @required TResult listUpdated(PaymentsUpdated value),
  }) {
    assert(initial != null);
    assert(listUpdated != null);
    return listUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(PaymentsInitial value),
    TResult listUpdated(PaymentsUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (listUpdated != null) {
      return listUpdated(this);
    }
    return orElse();
  }
}

abstract class PaymentsUpdated implements PaymentsState {
  factory PaymentsUpdated(
      {@required List<Payment> payments,
      @required bool isUpdated}) = _$PaymentsUpdated;

  List<Payment> get payments;
  bool get isUpdated;
  @JsonKey(ignore: true)
  $PaymentsUpdatedCopyWith<PaymentsUpdated> get copyWith;
}
