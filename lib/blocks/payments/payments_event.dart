part of 'payments_bloc.dart';

@freezed
abstract class PaymentsEvent with _$PaymentsEvent {
  const factory PaymentsEvent.paymentAccounted(
      {@required Payment payment,
      @required DateTime dateAccounted,
      @required String newEmail}) = PaymentAccounted;
  const factory PaymentsEvent.paymentAdded({@required Payment payment}) =
      PaymentAdded;
  const factory PaymentsEvent.loadedPayments() = PaymentsLoaded;
}
