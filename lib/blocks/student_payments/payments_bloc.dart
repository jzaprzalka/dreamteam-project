import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:dreamteam_project/repositories/authentication_repository.dart';
import 'package:dreamteam_project/repositories/payments_repository.dart';
import 'package:dreamteam_project/repositories/user_data_repository.dart';

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';

part 'payments_event.dart';
part 'payments_state.dart';

part 'payments_bloc.freezed.dart';

class PaymentsBloc extends Bloc<PaymentsEvent, PaymentsState> {
  PaymentsDataRepository paymentsDataRepository = PaymentsDataRepository();
  UserDataRepository userDataRepository = UserDataRepository();
  AuthenticationRepository authenticationRepository =
      AuthenticationRepository();

  PaymentsBloc() : super(PaymentsState.initial());

  Future<List<Payment>> _getPayments() async {
    List<Payment> payments;
    try {
      var fbuser = await authenticationRepository.getCurrentUser();
      // var user = await userDataRepository.getUser(fbuser.uid);
      String userId = fbuser.uid;
      payments = await paymentsDataRepository.getPaymentsFor(userId);
      logger.i("Fetched ${payments.length} payment(s) for user $userId");
      for (Payment p in payments) {
        try {
          p.user = await userDataRepository.getUser(p.userId);
        } catch (e) {
          logger.e("Failed to load user for payment ${p.paymentId}: $e");
        }
      }
    } catch (e) {
      logger.e("Error occurred in loading payments: $e");
      return null;
    }
    return payments;
  }

  @override
  Stream<PaymentsState> mapEventToState(PaymentsEvent event) async* {
    try {
      yield* event.map(
        loadedPayments: (e) async* {
          List<Payment> payments;
          payments = await _getPayments();
          yield PaymentsState.listUpdated(
            payments: payments ?? List.empty(),
            isUpdated: true,
          );
        },
      );
    } catch (e) {
      logger.e("Error occurred in mapping event to payments state: $e");
    }
  }
}
