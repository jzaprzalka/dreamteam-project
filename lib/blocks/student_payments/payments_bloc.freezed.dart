// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'payments_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$PaymentsEventTearOff {
  const _$PaymentsEventTearOff();

// ignore: unused_element
  PaymentsLoaded loadedPayments() {
    return const PaymentsLoaded();
  }
}

/// @nodoc
// ignore: unused_element
const $PaymentsEvent = _$PaymentsEventTearOff();

/// @nodoc
mixin _$PaymentsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadedPayments(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadedPayments(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadedPayments(PaymentsLoaded value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadedPayments(PaymentsLoaded value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $PaymentsEventCopyWith<$Res> {
  factory $PaymentsEventCopyWith(
          PaymentsEvent value, $Res Function(PaymentsEvent) then) =
      _$PaymentsEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$PaymentsEventCopyWithImpl<$Res>
    implements $PaymentsEventCopyWith<$Res> {
  _$PaymentsEventCopyWithImpl(this._value, this._then);

  final PaymentsEvent _value;
  // ignore: unused_field
  final $Res Function(PaymentsEvent) _then;
}

/// @nodoc
abstract class $PaymentsLoadedCopyWith<$Res> {
  factory $PaymentsLoadedCopyWith(
          PaymentsLoaded value, $Res Function(PaymentsLoaded) then) =
      _$PaymentsLoadedCopyWithImpl<$Res>;
}

/// @nodoc
class _$PaymentsLoadedCopyWithImpl<$Res>
    extends _$PaymentsEventCopyWithImpl<$Res>
    implements $PaymentsLoadedCopyWith<$Res> {
  _$PaymentsLoadedCopyWithImpl(
      PaymentsLoaded _value, $Res Function(PaymentsLoaded) _then)
      : super(_value, (v) => _then(v as PaymentsLoaded));

  @override
  PaymentsLoaded get _value => super._value as PaymentsLoaded;
}

/// @nodoc
class _$PaymentsLoaded implements PaymentsLoaded {
  const _$PaymentsLoaded();

  @override
  String toString() {
    return 'PaymentsEvent.loadedPayments()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is PaymentsLoaded);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadedPayments(),
  }) {
    assert(loadedPayments != null);
    return loadedPayments();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadedPayments(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedPayments != null) {
      return loadedPayments();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadedPayments(PaymentsLoaded value),
  }) {
    assert(loadedPayments != null);
    return loadedPayments(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadedPayments(PaymentsLoaded value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedPayments != null) {
      return loadedPayments(this);
    }
    return orElse();
  }
}

abstract class PaymentsLoaded implements PaymentsEvent {
  const factory PaymentsLoaded() = _$PaymentsLoaded;
}

/// @nodoc
class _$PaymentsStateTearOff {
  const _$PaymentsStateTearOff();

// ignore: unused_element
  PaymentsInitial initial() {
    return PaymentsInitial();
  }

// ignore: unused_element
  PaymentsUpdated listUpdated(
      {@required List<Payment> payments, @required bool isUpdated}) {
    return PaymentsUpdated(
      payments: payments,
      isUpdated: isUpdated,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $PaymentsState = _$PaymentsStateTearOff();

/// @nodoc
mixin _$PaymentsState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult listUpdated(List<Payment> payments, bool isUpdated),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult listUpdated(List<Payment> payments, bool isUpdated),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(PaymentsInitial value),
    @required TResult listUpdated(PaymentsUpdated value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(PaymentsInitial value),
    TResult listUpdated(PaymentsUpdated value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $PaymentsStateCopyWith<$Res> {
  factory $PaymentsStateCopyWith(
          PaymentsState value, $Res Function(PaymentsState) then) =
      _$PaymentsStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$PaymentsStateCopyWithImpl<$Res>
    implements $PaymentsStateCopyWith<$Res> {
  _$PaymentsStateCopyWithImpl(this._value, this._then);

  final PaymentsState _value;
  // ignore: unused_field
  final $Res Function(PaymentsState) _then;
}

/// @nodoc
abstract class $PaymentsInitialCopyWith<$Res> {
  factory $PaymentsInitialCopyWith(
          PaymentsInitial value, $Res Function(PaymentsInitial) then) =
      _$PaymentsInitialCopyWithImpl<$Res>;
}

/// @nodoc
class _$PaymentsInitialCopyWithImpl<$Res>
    extends _$PaymentsStateCopyWithImpl<$Res>
    implements $PaymentsInitialCopyWith<$Res> {
  _$PaymentsInitialCopyWithImpl(
      PaymentsInitial _value, $Res Function(PaymentsInitial) _then)
      : super(_value, (v) => _then(v as PaymentsInitial));

  @override
  PaymentsInitial get _value => super._value as PaymentsInitial;
}

/// @nodoc
class _$PaymentsInitial implements PaymentsInitial {
  _$PaymentsInitial();

  @override
  String toString() {
    return 'PaymentsState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is PaymentsInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult listUpdated(List<Payment> payments, bool isUpdated),
  }) {
    assert(initial != null);
    assert(listUpdated != null);
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult listUpdated(List<Payment> payments, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(PaymentsInitial value),
    @required TResult listUpdated(PaymentsUpdated value),
  }) {
    assert(initial != null);
    assert(listUpdated != null);
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(PaymentsInitial value),
    TResult listUpdated(PaymentsUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class PaymentsInitial implements PaymentsState {
  factory PaymentsInitial() = _$PaymentsInitial;
}

/// @nodoc
abstract class $PaymentsUpdatedCopyWith<$Res> {
  factory $PaymentsUpdatedCopyWith(
          PaymentsUpdated value, $Res Function(PaymentsUpdated) then) =
      _$PaymentsUpdatedCopyWithImpl<$Res>;
  $Res call({List<Payment> payments, bool isUpdated});
}

/// @nodoc
class _$PaymentsUpdatedCopyWithImpl<$Res>
    extends _$PaymentsStateCopyWithImpl<$Res>
    implements $PaymentsUpdatedCopyWith<$Res> {
  _$PaymentsUpdatedCopyWithImpl(
      PaymentsUpdated _value, $Res Function(PaymentsUpdated) _then)
      : super(_value, (v) => _then(v as PaymentsUpdated));

  @override
  PaymentsUpdated get _value => super._value as PaymentsUpdated;

  @override
  $Res call({
    Object payments = freezed,
    Object isUpdated = freezed,
  }) {
    return _then(PaymentsUpdated(
      payments:
          payments == freezed ? _value.payments : payments as List<Payment>,
      isUpdated: isUpdated == freezed ? _value.isUpdated : isUpdated as bool,
    ));
  }
}

/// @nodoc
class _$PaymentsUpdated implements PaymentsUpdated {
  _$PaymentsUpdated({@required this.payments, @required this.isUpdated})
      : assert(payments != null),
        assert(isUpdated != null);

  @override
  final List<Payment> payments;
  @override
  final bool isUpdated;

  @override
  String toString() {
    return 'PaymentsState.listUpdated(payments: $payments, isUpdated: $isUpdated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is PaymentsUpdated &&
            (identical(other.payments, payments) ||
                const DeepCollectionEquality()
                    .equals(other.payments, payments)) &&
            (identical(other.isUpdated, isUpdated) ||
                const DeepCollectionEquality()
                    .equals(other.isUpdated, isUpdated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(payments) ^
      const DeepCollectionEquality().hash(isUpdated);

  @JsonKey(ignore: true)
  @override
  $PaymentsUpdatedCopyWith<PaymentsUpdated> get copyWith =>
      _$PaymentsUpdatedCopyWithImpl<PaymentsUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult listUpdated(List<Payment> payments, bool isUpdated),
  }) {
    assert(initial != null);
    assert(listUpdated != null);
    return listUpdated(payments, isUpdated);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult listUpdated(List<Payment> payments, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (listUpdated != null) {
      return listUpdated(payments, isUpdated);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(PaymentsInitial value),
    @required TResult listUpdated(PaymentsUpdated value),
  }) {
    assert(initial != null);
    assert(listUpdated != null);
    return listUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(PaymentsInitial value),
    TResult listUpdated(PaymentsUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (listUpdated != null) {
      return listUpdated(this);
    }
    return orElse();
  }
}

abstract class PaymentsUpdated implements PaymentsState {
  factory PaymentsUpdated(
      {@required List<Payment> payments,
      @required bool isUpdated}) = _$PaymentsUpdated;

  List<Payment> get payments;
  bool get isUpdated;
  @JsonKey(ignore: true)
  $PaymentsUpdatedCopyWith<PaymentsUpdated> get copyWith;
}
