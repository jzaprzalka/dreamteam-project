part of 'payments_bloc.dart';

@freezed
abstract class PaymentsEvent with _$PaymentsEvent {
  const factory PaymentsEvent.loadedPayments() = PaymentsLoaded;
}
