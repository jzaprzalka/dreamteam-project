// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'admin_token_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$AdminTokenEventTearOff {
  const _$AdminTokenEventTearOff();

// ignore: unused_element
  LoadedTokens loadTokens() {
    return const LoadedTokens();
  }

// ignore: unused_element
  AddedToken addToken({@required String name}) {
    return AddedToken(
      name: name,
    );
  }

// ignore: unused_element
  EditToken editToken({@required Token token}) {
    return EditToken(
      token: token,
    );
  }

// ignore: unused_element
  DeleteToken deleteToken({@required Token token}) {
    return DeleteToken(
      token: token,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $AdminTokenEvent = _$AdminTokenEventTearOff();

/// @nodoc
mixin _$AdminTokenEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadTokens(),
    @required TResult addToken(String name),
    @required TResult editToken(Token token),
    @required TResult deleteToken(Token token),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadTokens(),
    TResult addToken(String name),
    TResult editToken(Token token),
    TResult deleteToken(Token token),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadTokens(LoadedTokens value),
    @required TResult addToken(AddedToken value),
    @required TResult editToken(EditToken value),
    @required TResult deleteToken(DeleteToken value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadTokens(LoadedTokens value),
    TResult addToken(AddedToken value),
    TResult editToken(EditToken value),
    TResult deleteToken(DeleteToken value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $AdminTokenEventCopyWith<$Res> {
  factory $AdminTokenEventCopyWith(
          AdminTokenEvent value, $Res Function(AdminTokenEvent) then) =
      _$AdminTokenEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$AdminTokenEventCopyWithImpl<$Res>
    implements $AdminTokenEventCopyWith<$Res> {
  _$AdminTokenEventCopyWithImpl(this._value, this._then);

  final AdminTokenEvent _value;
  // ignore: unused_field
  final $Res Function(AdminTokenEvent) _then;
}

/// @nodoc
abstract class $LoadedTokensCopyWith<$Res> {
  factory $LoadedTokensCopyWith(
          LoadedTokens value, $Res Function(LoadedTokens) then) =
      _$LoadedTokensCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoadedTokensCopyWithImpl<$Res>
    extends _$AdminTokenEventCopyWithImpl<$Res>
    implements $LoadedTokensCopyWith<$Res> {
  _$LoadedTokensCopyWithImpl(
      LoadedTokens _value, $Res Function(LoadedTokens) _then)
      : super(_value, (v) => _then(v as LoadedTokens));

  @override
  LoadedTokens get _value => super._value as LoadedTokens;
}

/// @nodoc
class _$LoadedTokens implements LoadedTokens {
  const _$LoadedTokens();

  @override
  String toString() {
    return 'AdminTokenEvent.loadTokens()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoadedTokens);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadTokens(),
    @required TResult addToken(String name),
    @required TResult editToken(Token token),
    @required TResult deleteToken(Token token),
  }) {
    assert(loadTokens != null);
    assert(addToken != null);
    assert(editToken != null);
    assert(deleteToken != null);
    return loadTokens();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadTokens(),
    TResult addToken(String name),
    TResult editToken(Token token),
    TResult deleteToken(Token token),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadTokens != null) {
      return loadTokens();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadTokens(LoadedTokens value),
    @required TResult addToken(AddedToken value),
    @required TResult editToken(EditToken value),
    @required TResult deleteToken(DeleteToken value),
  }) {
    assert(loadTokens != null);
    assert(addToken != null);
    assert(editToken != null);
    assert(deleteToken != null);
    return loadTokens(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadTokens(LoadedTokens value),
    TResult addToken(AddedToken value),
    TResult editToken(EditToken value),
    TResult deleteToken(DeleteToken value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadTokens != null) {
      return loadTokens(this);
    }
    return orElse();
  }
}

abstract class LoadedTokens implements AdminTokenEvent {
  const factory LoadedTokens() = _$LoadedTokens;
}

/// @nodoc
abstract class $AddedTokenCopyWith<$Res> {
  factory $AddedTokenCopyWith(
          AddedToken value, $Res Function(AddedToken) then) =
      _$AddedTokenCopyWithImpl<$Res>;
  $Res call({String name});
}

/// @nodoc
class _$AddedTokenCopyWithImpl<$Res> extends _$AdminTokenEventCopyWithImpl<$Res>
    implements $AddedTokenCopyWith<$Res> {
  _$AddedTokenCopyWithImpl(AddedToken _value, $Res Function(AddedToken) _then)
      : super(_value, (v) => _then(v as AddedToken));

  @override
  AddedToken get _value => super._value as AddedToken;

  @override
  $Res call({
    Object name = freezed,
  }) {
    return _then(AddedToken(
      name: name == freezed ? _value.name : name as String,
    ));
  }
}

/// @nodoc
class _$AddedToken implements AddedToken {
  const _$AddedToken({@required this.name}) : assert(name != null);

  @override
  final String name;

  @override
  String toString() {
    return 'AdminTokenEvent.addToken(name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AddedToken &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(name);

  @JsonKey(ignore: true)
  @override
  $AddedTokenCopyWith<AddedToken> get copyWith =>
      _$AddedTokenCopyWithImpl<AddedToken>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadTokens(),
    @required TResult addToken(String name),
    @required TResult editToken(Token token),
    @required TResult deleteToken(Token token),
  }) {
    assert(loadTokens != null);
    assert(addToken != null);
    assert(editToken != null);
    assert(deleteToken != null);
    return addToken(name);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadTokens(),
    TResult addToken(String name),
    TResult editToken(Token token),
    TResult deleteToken(Token token),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addToken != null) {
      return addToken(name);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadTokens(LoadedTokens value),
    @required TResult addToken(AddedToken value),
    @required TResult editToken(EditToken value),
    @required TResult deleteToken(DeleteToken value),
  }) {
    assert(loadTokens != null);
    assert(addToken != null);
    assert(editToken != null);
    assert(deleteToken != null);
    return addToken(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadTokens(LoadedTokens value),
    TResult addToken(AddedToken value),
    TResult editToken(EditToken value),
    TResult deleteToken(DeleteToken value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addToken != null) {
      return addToken(this);
    }
    return orElse();
  }
}

abstract class AddedToken implements AdminTokenEvent {
  const factory AddedToken({@required String name}) = _$AddedToken;

  String get name;
  @JsonKey(ignore: true)
  $AddedTokenCopyWith<AddedToken> get copyWith;
}

/// @nodoc
abstract class $EditTokenCopyWith<$Res> {
  factory $EditTokenCopyWith(EditToken value, $Res Function(EditToken) then) =
      _$EditTokenCopyWithImpl<$Res>;
  $Res call({Token token});
}

/// @nodoc
class _$EditTokenCopyWithImpl<$Res> extends _$AdminTokenEventCopyWithImpl<$Res>
    implements $EditTokenCopyWith<$Res> {
  _$EditTokenCopyWithImpl(EditToken _value, $Res Function(EditToken) _then)
      : super(_value, (v) => _then(v as EditToken));

  @override
  EditToken get _value => super._value as EditToken;

  @override
  $Res call({
    Object token = freezed,
  }) {
    return _then(EditToken(
      token: token == freezed ? _value.token : token as Token,
    ));
  }
}

/// @nodoc
class _$EditToken implements EditToken {
  const _$EditToken({@required this.token}) : assert(token != null);

  @override
  final Token token;

  @override
  String toString() {
    return 'AdminTokenEvent.editToken(token: $token)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is EditToken &&
            (identical(other.token, token) ||
                const DeepCollectionEquality().equals(other.token, token)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(token);

  @JsonKey(ignore: true)
  @override
  $EditTokenCopyWith<EditToken> get copyWith =>
      _$EditTokenCopyWithImpl<EditToken>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadTokens(),
    @required TResult addToken(String name),
    @required TResult editToken(Token token),
    @required TResult deleteToken(Token token),
  }) {
    assert(loadTokens != null);
    assert(addToken != null);
    assert(editToken != null);
    assert(deleteToken != null);
    return editToken(token);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadTokens(),
    TResult addToken(String name),
    TResult editToken(Token token),
    TResult deleteToken(Token token),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (editToken != null) {
      return editToken(token);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadTokens(LoadedTokens value),
    @required TResult addToken(AddedToken value),
    @required TResult editToken(EditToken value),
    @required TResult deleteToken(DeleteToken value),
  }) {
    assert(loadTokens != null);
    assert(addToken != null);
    assert(editToken != null);
    assert(deleteToken != null);
    return editToken(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadTokens(LoadedTokens value),
    TResult addToken(AddedToken value),
    TResult editToken(EditToken value),
    TResult deleteToken(DeleteToken value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (editToken != null) {
      return editToken(this);
    }
    return orElse();
  }
}

abstract class EditToken implements AdminTokenEvent {
  const factory EditToken({@required Token token}) = _$EditToken;

  Token get token;
  @JsonKey(ignore: true)
  $EditTokenCopyWith<EditToken> get copyWith;
}

/// @nodoc
abstract class $DeleteTokenCopyWith<$Res> {
  factory $DeleteTokenCopyWith(
          DeleteToken value, $Res Function(DeleteToken) then) =
      _$DeleteTokenCopyWithImpl<$Res>;
  $Res call({Token token});
}

/// @nodoc
class _$DeleteTokenCopyWithImpl<$Res>
    extends _$AdminTokenEventCopyWithImpl<$Res>
    implements $DeleteTokenCopyWith<$Res> {
  _$DeleteTokenCopyWithImpl(
      DeleteToken _value, $Res Function(DeleteToken) _then)
      : super(_value, (v) => _then(v as DeleteToken));

  @override
  DeleteToken get _value => super._value as DeleteToken;

  @override
  $Res call({
    Object token = freezed,
  }) {
    return _then(DeleteToken(
      token: token == freezed ? _value.token : token as Token,
    ));
  }
}

/// @nodoc
class _$DeleteToken implements DeleteToken {
  const _$DeleteToken({@required this.token}) : assert(token != null);

  @override
  final Token token;

  @override
  String toString() {
    return 'AdminTokenEvent.deleteToken(token: $token)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is DeleteToken &&
            (identical(other.token, token) ||
                const DeepCollectionEquality().equals(other.token, token)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(token);

  @JsonKey(ignore: true)
  @override
  $DeleteTokenCopyWith<DeleteToken> get copyWith =>
      _$DeleteTokenCopyWithImpl<DeleteToken>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadTokens(),
    @required TResult addToken(String name),
    @required TResult editToken(Token token),
    @required TResult deleteToken(Token token),
  }) {
    assert(loadTokens != null);
    assert(addToken != null);
    assert(editToken != null);
    assert(deleteToken != null);
    return deleteToken(token);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadTokens(),
    TResult addToken(String name),
    TResult editToken(Token token),
    TResult deleteToken(Token token),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (deleteToken != null) {
      return deleteToken(token);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadTokens(LoadedTokens value),
    @required TResult addToken(AddedToken value),
    @required TResult editToken(EditToken value),
    @required TResult deleteToken(DeleteToken value),
  }) {
    assert(loadTokens != null);
    assert(addToken != null);
    assert(editToken != null);
    assert(deleteToken != null);
    return deleteToken(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadTokens(LoadedTokens value),
    TResult addToken(AddedToken value),
    TResult editToken(EditToken value),
    TResult deleteToken(DeleteToken value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (deleteToken != null) {
      return deleteToken(this);
    }
    return orElse();
  }
}

abstract class DeleteToken implements AdminTokenEvent {
  const factory DeleteToken({@required Token token}) = _$DeleteToken;

  Token get token;
  @JsonKey(ignore: true)
  $DeleteTokenCopyWith<DeleteToken> get copyWith;
}

/// @nodoc
class _$AdminTokenStateTearOff {
  const _$AdminTokenStateTearOff();

// ignore: unused_element
  _AdminTokenState call({@required List<Token> tokens}) {
    return _AdminTokenState(
      tokens: tokens,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $AdminTokenState = _$AdminTokenStateTearOff();

/// @nodoc
mixin _$AdminTokenState {
  List<Token> get tokens;

  @JsonKey(ignore: true)
  $AdminTokenStateCopyWith<AdminTokenState> get copyWith;
}

/// @nodoc
abstract class $AdminTokenStateCopyWith<$Res> {
  factory $AdminTokenStateCopyWith(
          AdminTokenState value, $Res Function(AdminTokenState) then) =
      _$AdminTokenStateCopyWithImpl<$Res>;
  $Res call({List<Token> tokens});
}

/// @nodoc
class _$AdminTokenStateCopyWithImpl<$Res>
    implements $AdminTokenStateCopyWith<$Res> {
  _$AdminTokenStateCopyWithImpl(this._value, this._then);

  final AdminTokenState _value;
  // ignore: unused_field
  final $Res Function(AdminTokenState) _then;

  @override
  $Res call({
    Object tokens = freezed,
  }) {
    return _then(_value.copyWith(
      tokens: tokens == freezed ? _value.tokens : tokens as List<Token>,
    ));
  }
}

/// @nodoc
abstract class _$AdminTokenStateCopyWith<$Res>
    implements $AdminTokenStateCopyWith<$Res> {
  factory _$AdminTokenStateCopyWith(
          _AdminTokenState value, $Res Function(_AdminTokenState) then) =
      __$AdminTokenStateCopyWithImpl<$Res>;
  @override
  $Res call({List<Token> tokens});
}

/// @nodoc
class __$AdminTokenStateCopyWithImpl<$Res>
    extends _$AdminTokenStateCopyWithImpl<$Res>
    implements _$AdminTokenStateCopyWith<$Res> {
  __$AdminTokenStateCopyWithImpl(
      _AdminTokenState _value, $Res Function(_AdminTokenState) _then)
      : super(_value, (v) => _then(v as _AdminTokenState));

  @override
  _AdminTokenState get _value => super._value as _AdminTokenState;

  @override
  $Res call({
    Object tokens = freezed,
  }) {
    return _then(_AdminTokenState(
      tokens: tokens == freezed ? _value.tokens : tokens as List<Token>,
    ));
  }
}

/// @nodoc
class _$_AdminTokenState implements _AdminTokenState {
  const _$_AdminTokenState({@required this.tokens}) : assert(tokens != null);

  @override
  final List<Token> tokens;

  @override
  String toString() {
    return 'AdminTokenState(tokens: $tokens)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _AdminTokenState &&
            (identical(other.tokens, tokens) ||
                const DeepCollectionEquality().equals(other.tokens, tokens)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(tokens);

  @JsonKey(ignore: true)
  @override
  _$AdminTokenStateCopyWith<_AdminTokenState> get copyWith =>
      __$AdminTokenStateCopyWithImpl<_AdminTokenState>(this, _$identity);
}

abstract class _AdminTokenState implements AdminTokenState {
  const factory _AdminTokenState({@required List<Token> tokens}) =
      _$_AdminTokenState;

  @override
  List<Token> get tokens;
  @override
  @JsonKey(ignore: true)
  _$AdminTokenStateCopyWith<_AdminTokenState> get copyWith;
}
