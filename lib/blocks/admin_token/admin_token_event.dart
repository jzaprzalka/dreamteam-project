part of 'admin_token_bloc.dart';

@freezed
abstract class AdminTokenEvent with _$AdminTokenEvent {
  const factory AdminTokenEvent.loadTokens() = LoadedTokens;
  const factory AdminTokenEvent.addToken({@required String name}) = AddedToken;
  const factory AdminTokenEvent.editToken({@required Token token}) = EditToken;
  const factory AdminTokenEvent.deleteToken({@required Token token}) = DeleteToken;
}
