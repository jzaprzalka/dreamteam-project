import 'package:bloc/bloc.dart';
import 'package:dreamteam_project/repositories/token_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';
import 'package:dreamteam_project/models/Token.dart';
import 'package:dreamteam_project/main.dart';
import 'dart:async';

part 'admin_token_event.dart';
part 'admin_token_state.dart';
part 'admin_token_bloc.freezed.dart';

class AdminTokenBloc extends Bloc<AdminTokenEvent, AdminTokenState> {
  TokenRepository tokenRepository = TokenRepository();

  AdminTokenBloc() : super(AdminTokenState.initial());

  @override
  Stream<AdminTokenState> mapEventToState(AdminTokenEvent event) async* {
    try {
      yield* event.map(
        loadTokens: (e) async* {
          List<Token> tokens = await tokenRepository.getAllTokens();
          yield state.copyWith(tokens: tokens);
        },
        addToken: (e) async* {
          await tokenRepository.addToken(e.name);
          List<Token> tokens = await tokenRepository.getAllTokens();
          yield state.copyWith(tokens: tokens);
        },
        editToken: (e) async* {
          await tokenRepository.editToken(e.token);
          List<Token> tokens = await tokenRepository.getAllTokens();
          yield state.copyWith(tokens: tokens);
        },
        deleteToken: (e) async* {
          await tokenRepository.deleteToken(e.token);
          List<Token> tokens = await tokenRepository.getAllTokens();
          yield state.copyWith(tokens: tokens);
        },
      );
    } catch (e) {
      logger.e("Error mapEventToState in AdminTokenBloc: $e");
    }
  }
}
