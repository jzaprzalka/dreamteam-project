part of 'admin_token_bloc.dart';

@freezed
abstract class AdminTokenState with _$AdminTokenState {
  const factory AdminTokenState({@required List<Token> tokens}) =
      _AdminTokenState;

  factory AdminTokenState.initial() => _AdminTokenState(tokens: List.empty());
}
