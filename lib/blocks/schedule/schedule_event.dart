part of 'schedule_bloc.dart';

@freezed
abstract class ScheduleEvent with _$ScheduleEvent {
  const factory ScheduleEvent.addedSchedule(
      {@required Schedule schedule, DateTime toDate}) = AddedSchedule;
  const factory ScheduleEvent.updatedAttendance(
      {@required String scheduleId,
      @required List<Attendance> attendance}) = UpdatedAttendance;
  const factory ScheduleEvent.updatedCoaches(
      {@required String scheduleId,
      @required List<String> coachIds}) = UpdatedCoaches;
  const factory ScheduleEvent.updatedRoom(
      {@required String scheduleId, @required String roomId}) = UpdatedRoom;
  const factory ScheduleEvent.deletedSchedule({@required String scheduleId}) =
      DeletedSchedule;
  const factory ScheduleEvent.loadedSchedules({
    DateTime fromDate,
    DateTime toDate,
  }) = LoadedSchedules;
  const factory ScheduleEvent.loadGroups() = LoadGroups;
}
