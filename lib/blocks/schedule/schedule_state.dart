part of 'schedule_bloc.dart';

@freezed
abstract class ScheduleState with _$ScheduleState {
  factory ScheduleState.scheduleInitial() = ScheduleInitial;
  factory ScheduleState.scheduleUpdated(
      {@required Schedule schedule,
      @required bool isUpdated}) = ScheduleUpdated;
  factory ScheduleState.schedulesUpdated(
      {@required List<Schedule> schedules,
      @required bool isUpdated}) = SchedulesUpdated;
  factory ScheduleState.groupsUpdated({
    @required List<Group> groups,
    @required bool isUpdated,
  }) = GroupsUpdated;
}
