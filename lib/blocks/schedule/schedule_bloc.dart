import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/repositories/groups_data_repository.dart';
import 'package:dreamteam_project/repositories/schedule_repository.dart';
import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dreamteam_project/main.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';
import 'package:dreamteam_project/models/Schedule.dart';
import 'package:dreamteam_project/models/Attendance.dart';

part 'schedule_event.dart';
part 'schedule_state.dart';
part 'schedule_bloc.freezed.dart';

class ScheduleBloc extends Bloc<ScheduleEvent, ScheduleState> {
  ScheduleRepository scheduleRepository = ScheduleRepository();
  GroupsDataRepository groupsDataRepository = GroupsDataRepository();

  ScheduleBloc() : super(ScheduleState.scheduleInitial());

  Future<List<Schedule>> _getSchedules(
      {DateTime fromDate, DateTime toDate}) async {
    List<Schedule> schedules, coachSchedules, studentSchedules;
    try {
      logger.i("Loading schedules");
      schedules = await scheduleRepository.getSchedule(fromDate, toDate);
      logger.i("Fetched [${schedules.length}] schedules for student");
      // Doesn't work too well with groups List
      // logger.i("Loading coach schedules");
      // coachSchedules = await scheduleRepository.getCoachSchedule(
      //   fromDate,
      //   toDate,
      // );
      // coachSchedules = coachSchedules ?? [];
      // logger.i("Found [${coachSchedules.length}] schedules for coach");
      // logger.i("Loading student schedules");
      // studentSchedules = await scheduleRepository.getStudentSchedule(
      //   fromDate,
      //   toDate,
      // );
      // studentSchedules = studentSchedules ?? [];
      // logger.i("Found [${studentSchedules.length}] schedules for student");
      // logger.i("Joining coach and student schedules");
      // schedules = List.from(coachSchedules);
      // schedules.addAll(
      //   studentSchedules.whereNot((studentGroup) => coachSchedules
      //       .map((g) => g.groupId)
      //       .contains(studentGroup.groupId)),
      // );
      // logger.i("Fetched [${schedules.length}] schedules for student");
    } catch (e) {
      logger.e("Exception thrown when loading groups List: $e");
      return [];
    }
    return schedules ?? [];
  }

  @override
  Stream<ScheduleState> mapEventToState(ScheduleEvent event) async* {
    try {
      yield* event.map(
        addedSchedule: (e) async* {
          await scheduleRepository.addSchedule(e.schedule);
        },
        updatedAttendance: (e) async* {
          scheduleRepository.updateAttendance(e.scheduleId, e.attendance);
        },
        updatedCoaches: (e) async* {
          scheduleRepository.updateCoaches(e.scheduleId, e.coachIds);
        },
        updatedRoom: (e) async* {
          scheduleRepository.updateRoom(e.scheduleId, e.roomId);
        },
        deletedSchedule: (e) async* {
          scheduleRepository.removeSchedule(e.scheduleId);
        },
        loadedSchedules: (e) async* {
          DateTime fromDate = e.fromDate ?? DateTime.now();
          DateTime toDate = e.toDate ?? DateTime.now().add(Duration(days: 1));
          List<Schedule> schedules =
              await _getSchedules(fromDate: fromDate, toDate: toDate);
          yield ScheduleState.schedulesUpdated(
            schedules: schedules,
            isUpdated: true,
          );
        },
        loadGroups: (e) async* {
          List<Group> groups = await groupsDataRepository.getAllGroups();
          yield ScheduleState.groupsUpdated(
            groups: groups,
            isUpdated: true,
          );
        },
      );
    } catch (e) {
      logger.e("Exception thrown mapping Schedule Event to Schedule state: $e");
    }
  }
}
