// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'schedule_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$ScheduleEventTearOff {
  const _$ScheduleEventTearOff();

// ignore: unused_element
  AddedSchedule addedSchedule({@required Schedule schedule, DateTime toDate}) {
    return AddedSchedule(
      schedule: schedule,
      toDate: toDate,
    );
  }

// ignore: unused_element
  UpdatedAttendance updatedAttendance(
      {@required String scheduleId, @required List<Attendance> attendance}) {
    return UpdatedAttendance(
      scheduleId: scheduleId,
      attendance: attendance,
    );
  }

// ignore: unused_element
  UpdatedCoaches updatedCoaches(
      {@required String scheduleId, @required List<String> coachIds}) {
    return UpdatedCoaches(
      scheduleId: scheduleId,
      coachIds: coachIds,
    );
  }

// ignore: unused_element
  UpdatedRoom updatedRoom(
      {@required String scheduleId, @required String roomId}) {
    return UpdatedRoom(
      scheduleId: scheduleId,
      roomId: roomId,
    );
  }

// ignore: unused_element
  DeletedSchedule deletedSchedule({@required String scheduleId}) {
    return DeletedSchedule(
      scheduleId: scheduleId,
    );
  }

// ignore: unused_element
  LoadedSchedules loadedSchedules({DateTime fromDate, DateTime toDate}) {
    return LoadedSchedules(
      fromDate: fromDate,
      toDate: toDate,
    );
  }

// ignore: unused_element
  LoadGroups loadGroups() {
    return const LoadGroups();
  }
}

/// @nodoc
// ignore: unused_element
const $ScheduleEvent = _$ScheduleEventTearOff();

/// @nodoc
mixin _$ScheduleEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult addedSchedule(Schedule schedule, DateTime toDate),
    @required
        TResult updatedAttendance(
            String scheduleId, List<Attendance> attendance),
    @required TResult updatedCoaches(String scheduleId, List<String> coachIds),
    @required TResult updatedRoom(String scheduleId, String roomId),
    @required TResult deletedSchedule(String scheduleId),
    @required TResult loadedSchedules(DateTime fromDate, DateTime toDate),
    @required TResult loadGroups(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult addedSchedule(Schedule schedule, DateTime toDate),
    TResult updatedAttendance(String scheduleId, List<Attendance> attendance),
    TResult updatedCoaches(String scheduleId, List<String> coachIds),
    TResult updatedRoom(String scheduleId, String roomId),
    TResult deletedSchedule(String scheduleId),
    TResult loadedSchedules(DateTime fromDate, DateTime toDate),
    TResult loadGroups(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult addedSchedule(AddedSchedule value),
    @required TResult updatedAttendance(UpdatedAttendance value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult updatedRoom(UpdatedRoom value),
    @required TResult deletedSchedule(DeletedSchedule value),
    @required TResult loadedSchedules(LoadedSchedules value),
    @required TResult loadGroups(LoadGroups value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult addedSchedule(AddedSchedule value),
    TResult updatedAttendance(UpdatedAttendance value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult updatedRoom(UpdatedRoom value),
    TResult deletedSchedule(DeletedSchedule value),
    TResult loadedSchedules(LoadedSchedules value),
    TResult loadGroups(LoadGroups value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $ScheduleEventCopyWith<$Res> {
  factory $ScheduleEventCopyWith(
          ScheduleEvent value, $Res Function(ScheduleEvent) then) =
      _$ScheduleEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$ScheduleEventCopyWithImpl<$Res>
    implements $ScheduleEventCopyWith<$Res> {
  _$ScheduleEventCopyWithImpl(this._value, this._then);

  final ScheduleEvent _value;
  // ignore: unused_field
  final $Res Function(ScheduleEvent) _then;
}

/// @nodoc
abstract class $AddedScheduleCopyWith<$Res> {
  factory $AddedScheduleCopyWith(
          AddedSchedule value, $Res Function(AddedSchedule) then) =
      _$AddedScheduleCopyWithImpl<$Res>;
  $Res call({Schedule schedule, DateTime toDate});
}

/// @nodoc
class _$AddedScheduleCopyWithImpl<$Res>
    extends _$ScheduleEventCopyWithImpl<$Res>
    implements $AddedScheduleCopyWith<$Res> {
  _$AddedScheduleCopyWithImpl(
      AddedSchedule _value, $Res Function(AddedSchedule) _then)
      : super(_value, (v) => _then(v as AddedSchedule));

  @override
  AddedSchedule get _value => super._value as AddedSchedule;

  @override
  $Res call({
    Object schedule = freezed,
    Object toDate = freezed,
  }) {
    return _then(AddedSchedule(
      schedule: schedule == freezed ? _value.schedule : schedule as Schedule,
      toDate: toDate == freezed ? _value.toDate : toDate as DateTime,
    ));
  }
}

/// @nodoc
class _$AddedSchedule implements AddedSchedule {
  const _$AddedSchedule({@required this.schedule, this.toDate})
      : assert(schedule != null);

  @override
  final Schedule schedule;
  @override
  final DateTime toDate;

  @override
  String toString() {
    return 'ScheduleEvent.addedSchedule(schedule: $schedule, toDate: $toDate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AddedSchedule &&
            (identical(other.schedule, schedule) ||
                const DeepCollectionEquality()
                    .equals(other.schedule, schedule)) &&
            (identical(other.toDate, toDate) ||
                const DeepCollectionEquality().equals(other.toDate, toDate)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(schedule) ^
      const DeepCollectionEquality().hash(toDate);

  @JsonKey(ignore: true)
  @override
  $AddedScheduleCopyWith<AddedSchedule> get copyWith =>
      _$AddedScheduleCopyWithImpl<AddedSchedule>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult addedSchedule(Schedule schedule, DateTime toDate),
    @required
        TResult updatedAttendance(
            String scheduleId, List<Attendance> attendance),
    @required TResult updatedCoaches(String scheduleId, List<String> coachIds),
    @required TResult updatedRoom(String scheduleId, String roomId),
    @required TResult deletedSchedule(String scheduleId),
    @required TResult loadedSchedules(DateTime fromDate, DateTime toDate),
    @required TResult loadGroups(),
  }) {
    assert(addedSchedule != null);
    assert(updatedAttendance != null);
    assert(updatedCoaches != null);
    assert(updatedRoom != null);
    assert(deletedSchedule != null);
    assert(loadedSchedules != null);
    assert(loadGroups != null);
    return addedSchedule(schedule, toDate);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult addedSchedule(Schedule schedule, DateTime toDate),
    TResult updatedAttendance(String scheduleId, List<Attendance> attendance),
    TResult updatedCoaches(String scheduleId, List<String> coachIds),
    TResult updatedRoom(String scheduleId, String roomId),
    TResult deletedSchedule(String scheduleId),
    TResult loadedSchedules(DateTime fromDate, DateTime toDate),
    TResult loadGroups(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addedSchedule != null) {
      return addedSchedule(schedule, toDate);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult addedSchedule(AddedSchedule value),
    @required TResult updatedAttendance(UpdatedAttendance value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult updatedRoom(UpdatedRoom value),
    @required TResult deletedSchedule(DeletedSchedule value),
    @required TResult loadedSchedules(LoadedSchedules value),
    @required TResult loadGroups(LoadGroups value),
  }) {
    assert(addedSchedule != null);
    assert(updatedAttendance != null);
    assert(updatedCoaches != null);
    assert(updatedRoom != null);
    assert(deletedSchedule != null);
    assert(loadedSchedules != null);
    assert(loadGroups != null);
    return addedSchedule(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult addedSchedule(AddedSchedule value),
    TResult updatedAttendance(UpdatedAttendance value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult updatedRoom(UpdatedRoom value),
    TResult deletedSchedule(DeletedSchedule value),
    TResult loadedSchedules(LoadedSchedules value),
    TResult loadGroups(LoadGroups value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addedSchedule != null) {
      return addedSchedule(this);
    }
    return orElse();
  }
}

abstract class AddedSchedule implements ScheduleEvent {
  const factory AddedSchedule({@required Schedule schedule, DateTime toDate}) =
      _$AddedSchedule;

  Schedule get schedule;
  DateTime get toDate;
  @JsonKey(ignore: true)
  $AddedScheduleCopyWith<AddedSchedule> get copyWith;
}

/// @nodoc
abstract class $UpdatedAttendanceCopyWith<$Res> {
  factory $UpdatedAttendanceCopyWith(
          UpdatedAttendance value, $Res Function(UpdatedAttendance) then) =
      _$UpdatedAttendanceCopyWithImpl<$Res>;
  $Res call({String scheduleId, List<Attendance> attendance});
}

/// @nodoc
class _$UpdatedAttendanceCopyWithImpl<$Res>
    extends _$ScheduleEventCopyWithImpl<$Res>
    implements $UpdatedAttendanceCopyWith<$Res> {
  _$UpdatedAttendanceCopyWithImpl(
      UpdatedAttendance _value, $Res Function(UpdatedAttendance) _then)
      : super(_value, (v) => _then(v as UpdatedAttendance));

  @override
  UpdatedAttendance get _value => super._value as UpdatedAttendance;

  @override
  $Res call({
    Object scheduleId = freezed,
    Object attendance = freezed,
  }) {
    return _then(UpdatedAttendance(
      scheduleId:
          scheduleId == freezed ? _value.scheduleId : scheduleId as String,
      attendance: attendance == freezed
          ? _value.attendance
          : attendance as List<Attendance>,
    ));
  }
}

/// @nodoc
class _$UpdatedAttendance implements UpdatedAttendance {
  const _$UpdatedAttendance(
      {@required this.scheduleId, @required this.attendance})
      : assert(scheduleId != null),
        assert(attendance != null);

  @override
  final String scheduleId;
  @override
  final List<Attendance> attendance;

  @override
  String toString() {
    return 'ScheduleEvent.updatedAttendance(scheduleId: $scheduleId, attendance: $attendance)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UpdatedAttendance &&
            (identical(other.scheduleId, scheduleId) ||
                const DeepCollectionEquality()
                    .equals(other.scheduleId, scheduleId)) &&
            (identical(other.attendance, attendance) ||
                const DeepCollectionEquality()
                    .equals(other.attendance, attendance)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(scheduleId) ^
      const DeepCollectionEquality().hash(attendance);

  @JsonKey(ignore: true)
  @override
  $UpdatedAttendanceCopyWith<UpdatedAttendance> get copyWith =>
      _$UpdatedAttendanceCopyWithImpl<UpdatedAttendance>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult addedSchedule(Schedule schedule, DateTime toDate),
    @required
        TResult updatedAttendance(
            String scheduleId, List<Attendance> attendance),
    @required TResult updatedCoaches(String scheduleId, List<String> coachIds),
    @required TResult updatedRoom(String scheduleId, String roomId),
    @required TResult deletedSchedule(String scheduleId),
    @required TResult loadedSchedules(DateTime fromDate, DateTime toDate),
    @required TResult loadGroups(),
  }) {
    assert(addedSchedule != null);
    assert(updatedAttendance != null);
    assert(updatedCoaches != null);
    assert(updatedRoom != null);
    assert(deletedSchedule != null);
    assert(loadedSchedules != null);
    assert(loadGroups != null);
    return updatedAttendance(scheduleId, attendance);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult addedSchedule(Schedule schedule, DateTime toDate),
    TResult updatedAttendance(String scheduleId, List<Attendance> attendance),
    TResult updatedCoaches(String scheduleId, List<String> coachIds),
    TResult updatedRoom(String scheduleId, String roomId),
    TResult deletedSchedule(String scheduleId),
    TResult loadedSchedules(DateTime fromDate, DateTime toDate),
    TResult loadGroups(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedAttendance != null) {
      return updatedAttendance(scheduleId, attendance);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult addedSchedule(AddedSchedule value),
    @required TResult updatedAttendance(UpdatedAttendance value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult updatedRoom(UpdatedRoom value),
    @required TResult deletedSchedule(DeletedSchedule value),
    @required TResult loadedSchedules(LoadedSchedules value),
    @required TResult loadGroups(LoadGroups value),
  }) {
    assert(addedSchedule != null);
    assert(updatedAttendance != null);
    assert(updatedCoaches != null);
    assert(updatedRoom != null);
    assert(deletedSchedule != null);
    assert(loadedSchedules != null);
    assert(loadGroups != null);
    return updatedAttendance(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult addedSchedule(AddedSchedule value),
    TResult updatedAttendance(UpdatedAttendance value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult updatedRoom(UpdatedRoom value),
    TResult deletedSchedule(DeletedSchedule value),
    TResult loadedSchedules(LoadedSchedules value),
    TResult loadGroups(LoadGroups value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedAttendance != null) {
      return updatedAttendance(this);
    }
    return orElse();
  }
}

abstract class UpdatedAttendance implements ScheduleEvent {
  const factory UpdatedAttendance(
      {@required String scheduleId,
      @required List<Attendance> attendance}) = _$UpdatedAttendance;

  String get scheduleId;
  List<Attendance> get attendance;
  @JsonKey(ignore: true)
  $UpdatedAttendanceCopyWith<UpdatedAttendance> get copyWith;
}

/// @nodoc
abstract class $UpdatedCoachesCopyWith<$Res> {
  factory $UpdatedCoachesCopyWith(
          UpdatedCoaches value, $Res Function(UpdatedCoaches) then) =
      _$UpdatedCoachesCopyWithImpl<$Res>;
  $Res call({String scheduleId, List<String> coachIds});
}

/// @nodoc
class _$UpdatedCoachesCopyWithImpl<$Res>
    extends _$ScheduleEventCopyWithImpl<$Res>
    implements $UpdatedCoachesCopyWith<$Res> {
  _$UpdatedCoachesCopyWithImpl(
      UpdatedCoaches _value, $Res Function(UpdatedCoaches) _then)
      : super(_value, (v) => _then(v as UpdatedCoaches));

  @override
  UpdatedCoaches get _value => super._value as UpdatedCoaches;

  @override
  $Res call({
    Object scheduleId = freezed,
    Object coachIds = freezed,
  }) {
    return _then(UpdatedCoaches(
      scheduleId:
          scheduleId == freezed ? _value.scheduleId : scheduleId as String,
      coachIds:
          coachIds == freezed ? _value.coachIds : coachIds as List<String>,
    ));
  }
}

/// @nodoc
class _$UpdatedCoaches implements UpdatedCoaches {
  const _$UpdatedCoaches({@required this.scheduleId, @required this.coachIds})
      : assert(scheduleId != null),
        assert(coachIds != null);

  @override
  final String scheduleId;
  @override
  final List<String> coachIds;

  @override
  String toString() {
    return 'ScheduleEvent.updatedCoaches(scheduleId: $scheduleId, coachIds: $coachIds)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UpdatedCoaches &&
            (identical(other.scheduleId, scheduleId) ||
                const DeepCollectionEquality()
                    .equals(other.scheduleId, scheduleId)) &&
            (identical(other.coachIds, coachIds) ||
                const DeepCollectionEquality()
                    .equals(other.coachIds, coachIds)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(scheduleId) ^
      const DeepCollectionEquality().hash(coachIds);

  @JsonKey(ignore: true)
  @override
  $UpdatedCoachesCopyWith<UpdatedCoaches> get copyWith =>
      _$UpdatedCoachesCopyWithImpl<UpdatedCoaches>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult addedSchedule(Schedule schedule, DateTime toDate),
    @required
        TResult updatedAttendance(
            String scheduleId, List<Attendance> attendance),
    @required TResult updatedCoaches(String scheduleId, List<String> coachIds),
    @required TResult updatedRoom(String scheduleId, String roomId),
    @required TResult deletedSchedule(String scheduleId),
    @required TResult loadedSchedules(DateTime fromDate, DateTime toDate),
    @required TResult loadGroups(),
  }) {
    assert(addedSchedule != null);
    assert(updatedAttendance != null);
    assert(updatedCoaches != null);
    assert(updatedRoom != null);
    assert(deletedSchedule != null);
    assert(loadedSchedules != null);
    assert(loadGroups != null);
    return updatedCoaches(scheduleId, coachIds);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult addedSchedule(Schedule schedule, DateTime toDate),
    TResult updatedAttendance(String scheduleId, List<Attendance> attendance),
    TResult updatedCoaches(String scheduleId, List<String> coachIds),
    TResult updatedRoom(String scheduleId, String roomId),
    TResult deletedSchedule(String scheduleId),
    TResult loadedSchedules(DateTime fromDate, DateTime toDate),
    TResult loadGroups(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedCoaches != null) {
      return updatedCoaches(scheduleId, coachIds);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult addedSchedule(AddedSchedule value),
    @required TResult updatedAttendance(UpdatedAttendance value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult updatedRoom(UpdatedRoom value),
    @required TResult deletedSchedule(DeletedSchedule value),
    @required TResult loadedSchedules(LoadedSchedules value),
    @required TResult loadGroups(LoadGroups value),
  }) {
    assert(addedSchedule != null);
    assert(updatedAttendance != null);
    assert(updatedCoaches != null);
    assert(updatedRoom != null);
    assert(deletedSchedule != null);
    assert(loadedSchedules != null);
    assert(loadGroups != null);
    return updatedCoaches(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult addedSchedule(AddedSchedule value),
    TResult updatedAttendance(UpdatedAttendance value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult updatedRoom(UpdatedRoom value),
    TResult deletedSchedule(DeletedSchedule value),
    TResult loadedSchedules(LoadedSchedules value),
    TResult loadGroups(LoadGroups value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedCoaches != null) {
      return updatedCoaches(this);
    }
    return orElse();
  }
}

abstract class UpdatedCoaches implements ScheduleEvent {
  const factory UpdatedCoaches(
      {@required String scheduleId,
      @required List<String> coachIds}) = _$UpdatedCoaches;

  String get scheduleId;
  List<String> get coachIds;
  @JsonKey(ignore: true)
  $UpdatedCoachesCopyWith<UpdatedCoaches> get copyWith;
}

/// @nodoc
abstract class $UpdatedRoomCopyWith<$Res> {
  factory $UpdatedRoomCopyWith(
          UpdatedRoom value, $Res Function(UpdatedRoom) then) =
      _$UpdatedRoomCopyWithImpl<$Res>;
  $Res call({String scheduleId, String roomId});
}

/// @nodoc
class _$UpdatedRoomCopyWithImpl<$Res> extends _$ScheduleEventCopyWithImpl<$Res>
    implements $UpdatedRoomCopyWith<$Res> {
  _$UpdatedRoomCopyWithImpl(
      UpdatedRoom _value, $Res Function(UpdatedRoom) _then)
      : super(_value, (v) => _then(v as UpdatedRoom));

  @override
  UpdatedRoom get _value => super._value as UpdatedRoom;

  @override
  $Res call({
    Object scheduleId = freezed,
    Object roomId = freezed,
  }) {
    return _then(UpdatedRoom(
      scheduleId:
          scheduleId == freezed ? _value.scheduleId : scheduleId as String,
      roomId: roomId == freezed ? _value.roomId : roomId as String,
    ));
  }
}

/// @nodoc
class _$UpdatedRoom implements UpdatedRoom {
  const _$UpdatedRoom({@required this.scheduleId, @required this.roomId})
      : assert(scheduleId != null),
        assert(roomId != null);

  @override
  final String scheduleId;
  @override
  final String roomId;

  @override
  String toString() {
    return 'ScheduleEvent.updatedRoom(scheduleId: $scheduleId, roomId: $roomId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UpdatedRoom &&
            (identical(other.scheduleId, scheduleId) ||
                const DeepCollectionEquality()
                    .equals(other.scheduleId, scheduleId)) &&
            (identical(other.roomId, roomId) ||
                const DeepCollectionEquality().equals(other.roomId, roomId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(scheduleId) ^
      const DeepCollectionEquality().hash(roomId);

  @JsonKey(ignore: true)
  @override
  $UpdatedRoomCopyWith<UpdatedRoom> get copyWith =>
      _$UpdatedRoomCopyWithImpl<UpdatedRoom>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult addedSchedule(Schedule schedule, DateTime toDate),
    @required
        TResult updatedAttendance(
            String scheduleId, List<Attendance> attendance),
    @required TResult updatedCoaches(String scheduleId, List<String> coachIds),
    @required TResult updatedRoom(String scheduleId, String roomId),
    @required TResult deletedSchedule(String scheduleId),
    @required TResult loadedSchedules(DateTime fromDate, DateTime toDate),
    @required TResult loadGroups(),
  }) {
    assert(addedSchedule != null);
    assert(updatedAttendance != null);
    assert(updatedCoaches != null);
    assert(updatedRoom != null);
    assert(deletedSchedule != null);
    assert(loadedSchedules != null);
    assert(loadGroups != null);
    return updatedRoom(scheduleId, roomId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult addedSchedule(Schedule schedule, DateTime toDate),
    TResult updatedAttendance(String scheduleId, List<Attendance> attendance),
    TResult updatedCoaches(String scheduleId, List<String> coachIds),
    TResult updatedRoom(String scheduleId, String roomId),
    TResult deletedSchedule(String scheduleId),
    TResult loadedSchedules(DateTime fromDate, DateTime toDate),
    TResult loadGroups(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedRoom != null) {
      return updatedRoom(scheduleId, roomId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult addedSchedule(AddedSchedule value),
    @required TResult updatedAttendance(UpdatedAttendance value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult updatedRoom(UpdatedRoom value),
    @required TResult deletedSchedule(DeletedSchedule value),
    @required TResult loadedSchedules(LoadedSchedules value),
    @required TResult loadGroups(LoadGroups value),
  }) {
    assert(addedSchedule != null);
    assert(updatedAttendance != null);
    assert(updatedCoaches != null);
    assert(updatedRoom != null);
    assert(deletedSchedule != null);
    assert(loadedSchedules != null);
    assert(loadGroups != null);
    return updatedRoom(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult addedSchedule(AddedSchedule value),
    TResult updatedAttendance(UpdatedAttendance value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult updatedRoom(UpdatedRoom value),
    TResult deletedSchedule(DeletedSchedule value),
    TResult loadedSchedules(LoadedSchedules value),
    TResult loadGroups(LoadGroups value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updatedRoom != null) {
      return updatedRoom(this);
    }
    return orElse();
  }
}

abstract class UpdatedRoom implements ScheduleEvent {
  const factory UpdatedRoom(
      {@required String scheduleId, @required String roomId}) = _$UpdatedRoom;

  String get scheduleId;
  String get roomId;
  @JsonKey(ignore: true)
  $UpdatedRoomCopyWith<UpdatedRoom> get copyWith;
}

/// @nodoc
abstract class $DeletedScheduleCopyWith<$Res> {
  factory $DeletedScheduleCopyWith(
          DeletedSchedule value, $Res Function(DeletedSchedule) then) =
      _$DeletedScheduleCopyWithImpl<$Res>;
  $Res call({String scheduleId});
}

/// @nodoc
class _$DeletedScheduleCopyWithImpl<$Res>
    extends _$ScheduleEventCopyWithImpl<$Res>
    implements $DeletedScheduleCopyWith<$Res> {
  _$DeletedScheduleCopyWithImpl(
      DeletedSchedule _value, $Res Function(DeletedSchedule) _then)
      : super(_value, (v) => _then(v as DeletedSchedule));

  @override
  DeletedSchedule get _value => super._value as DeletedSchedule;

  @override
  $Res call({
    Object scheduleId = freezed,
  }) {
    return _then(DeletedSchedule(
      scheduleId:
          scheduleId == freezed ? _value.scheduleId : scheduleId as String,
    ));
  }
}

/// @nodoc
class _$DeletedSchedule implements DeletedSchedule {
  const _$DeletedSchedule({@required this.scheduleId})
      : assert(scheduleId != null);

  @override
  final String scheduleId;

  @override
  String toString() {
    return 'ScheduleEvent.deletedSchedule(scheduleId: $scheduleId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is DeletedSchedule &&
            (identical(other.scheduleId, scheduleId) ||
                const DeepCollectionEquality()
                    .equals(other.scheduleId, scheduleId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(scheduleId);

  @JsonKey(ignore: true)
  @override
  $DeletedScheduleCopyWith<DeletedSchedule> get copyWith =>
      _$DeletedScheduleCopyWithImpl<DeletedSchedule>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult addedSchedule(Schedule schedule, DateTime toDate),
    @required
        TResult updatedAttendance(
            String scheduleId, List<Attendance> attendance),
    @required TResult updatedCoaches(String scheduleId, List<String> coachIds),
    @required TResult updatedRoom(String scheduleId, String roomId),
    @required TResult deletedSchedule(String scheduleId),
    @required TResult loadedSchedules(DateTime fromDate, DateTime toDate),
    @required TResult loadGroups(),
  }) {
    assert(addedSchedule != null);
    assert(updatedAttendance != null);
    assert(updatedCoaches != null);
    assert(updatedRoom != null);
    assert(deletedSchedule != null);
    assert(loadedSchedules != null);
    assert(loadGroups != null);
    return deletedSchedule(scheduleId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult addedSchedule(Schedule schedule, DateTime toDate),
    TResult updatedAttendance(String scheduleId, List<Attendance> attendance),
    TResult updatedCoaches(String scheduleId, List<String> coachIds),
    TResult updatedRoom(String scheduleId, String roomId),
    TResult deletedSchedule(String scheduleId),
    TResult loadedSchedules(DateTime fromDate, DateTime toDate),
    TResult loadGroups(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (deletedSchedule != null) {
      return deletedSchedule(scheduleId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult addedSchedule(AddedSchedule value),
    @required TResult updatedAttendance(UpdatedAttendance value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult updatedRoom(UpdatedRoom value),
    @required TResult deletedSchedule(DeletedSchedule value),
    @required TResult loadedSchedules(LoadedSchedules value),
    @required TResult loadGroups(LoadGroups value),
  }) {
    assert(addedSchedule != null);
    assert(updatedAttendance != null);
    assert(updatedCoaches != null);
    assert(updatedRoom != null);
    assert(deletedSchedule != null);
    assert(loadedSchedules != null);
    assert(loadGroups != null);
    return deletedSchedule(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult addedSchedule(AddedSchedule value),
    TResult updatedAttendance(UpdatedAttendance value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult updatedRoom(UpdatedRoom value),
    TResult deletedSchedule(DeletedSchedule value),
    TResult loadedSchedules(LoadedSchedules value),
    TResult loadGroups(LoadGroups value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (deletedSchedule != null) {
      return deletedSchedule(this);
    }
    return orElse();
  }
}

abstract class DeletedSchedule implements ScheduleEvent {
  const factory DeletedSchedule({@required String scheduleId}) =
      _$DeletedSchedule;

  String get scheduleId;
  @JsonKey(ignore: true)
  $DeletedScheduleCopyWith<DeletedSchedule> get copyWith;
}

/// @nodoc
abstract class $LoadedSchedulesCopyWith<$Res> {
  factory $LoadedSchedulesCopyWith(
          LoadedSchedules value, $Res Function(LoadedSchedules) then) =
      _$LoadedSchedulesCopyWithImpl<$Res>;
  $Res call({DateTime fromDate, DateTime toDate});
}

/// @nodoc
class _$LoadedSchedulesCopyWithImpl<$Res>
    extends _$ScheduleEventCopyWithImpl<$Res>
    implements $LoadedSchedulesCopyWith<$Res> {
  _$LoadedSchedulesCopyWithImpl(
      LoadedSchedules _value, $Res Function(LoadedSchedules) _then)
      : super(_value, (v) => _then(v as LoadedSchedules));

  @override
  LoadedSchedules get _value => super._value as LoadedSchedules;

  @override
  $Res call({
    Object fromDate = freezed,
    Object toDate = freezed,
  }) {
    return _then(LoadedSchedules(
      fromDate: fromDate == freezed ? _value.fromDate : fromDate as DateTime,
      toDate: toDate == freezed ? _value.toDate : toDate as DateTime,
    ));
  }
}

/// @nodoc
class _$LoadedSchedules implements LoadedSchedules {
  const _$LoadedSchedules({this.fromDate, this.toDate});

  @override
  final DateTime fromDate;
  @override
  final DateTime toDate;

  @override
  String toString() {
    return 'ScheduleEvent.loadedSchedules(fromDate: $fromDate, toDate: $toDate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoadedSchedules &&
            (identical(other.fromDate, fromDate) ||
                const DeepCollectionEquality()
                    .equals(other.fromDate, fromDate)) &&
            (identical(other.toDate, toDate) ||
                const DeepCollectionEquality().equals(other.toDate, toDate)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(fromDate) ^
      const DeepCollectionEquality().hash(toDate);

  @JsonKey(ignore: true)
  @override
  $LoadedSchedulesCopyWith<LoadedSchedules> get copyWith =>
      _$LoadedSchedulesCopyWithImpl<LoadedSchedules>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult addedSchedule(Schedule schedule, DateTime toDate),
    @required
        TResult updatedAttendance(
            String scheduleId, List<Attendance> attendance),
    @required TResult updatedCoaches(String scheduleId, List<String> coachIds),
    @required TResult updatedRoom(String scheduleId, String roomId),
    @required TResult deletedSchedule(String scheduleId),
    @required TResult loadedSchedules(DateTime fromDate, DateTime toDate),
    @required TResult loadGroups(),
  }) {
    assert(addedSchedule != null);
    assert(updatedAttendance != null);
    assert(updatedCoaches != null);
    assert(updatedRoom != null);
    assert(deletedSchedule != null);
    assert(loadedSchedules != null);
    assert(loadGroups != null);
    return loadedSchedules(fromDate, toDate);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult addedSchedule(Schedule schedule, DateTime toDate),
    TResult updatedAttendance(String scheduleId, List<Attendance> attendance),
    TResult updatedCoaches(String scheduleId, List<String> coachIds),
    TResult updatedRoom(String scheduleId, String roomId),
    TResult deletedSchedule(String scheduleId),
    TResult loadedSchedules(DateTime fromDate, DateTime toDate),
    TResult loadGroups(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedSchedules != null) {
      return loadedSchedules(fromDate, toDate);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult addedSchedule(AddedSchedule value),
    @required TResult updatedAttendance(UpdatedAttendance value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult updatedRoom(UpdatedRoom value),
    @required TResult deletedSchedule(DeletedSchedule value),
    @required TResult loadedSchedules(LoadedSchedules value),
    @required TResult loadGroups(LoadGroups value),
  }) {
    assert(addedSchedule != null);
    assert(updatedAttendance != null);
    assert(updatedCoaches != null);
    assert(updatedRoom != null);
    assert(deletedSchedule != null);
    assert(loadedSchedules != null);
    assert(loadGroups != null);
    return loadedSchedules(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult addedSchedule(AddedSchedule value),
    TResult updatedAttendance(UpdatedAttendance value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult updatedRoom(UpdatedRoom value),
    TResult deletedSchedule(DeletedSchedule value),
    TResult loadedSchedules(LoadedSchedules value),
    TResult loadGroups(LoadGroups value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedSchedules != null) {
      return loadedSchedules(this);
    }
    return orElse();
  }
}

abstract class LoadedSchedules implements ScheduleEvent {
  const factory LoadedSchedules({DateTime fromDate, DateTime toDate}) =
      _$LoadedSchedules;

  DateTime get fromDate;
  DateTime get toDate;
  @JsonKey(ignore: true)
  $LoadedSchedulesCopyWith<LoadedSchedules> get copyWith;
}

/// @nodoc
abstract class $LoadGroupsCopyWith<$Res> {
  factory $LoadGroupsCopyWith(
          LoadGroups value, $Res Function(LoadGroups) then) =
      _$LoadGroupsCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoadGroupsCopyWithImpl<$Res> extends _$ScheduleEventCopyWithImpl<$Res>
    implements $LoadGroupsCopyWith<$Res> {
  _$LoadGroupsCopyWithImpl(LoadGroups _value, $Res Function(LoadGroups) _then)
      : super(_value, (v) => _then(v as LoadGroups));

  @override
  LoadGroups get _value => super._value as LoadGroups;
}

/// @nodoc
class _$LoadGroups implements LoadGroups {
  const _$LoadGroups();

  @override
  String toString() {
    return 'ScheduleEvent.loadGroups()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoadGroups);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult addedSchedule(Schedule schedule, DateTime toDate),
    @required
        TResult updatedAttendance(
            String scheduleId, List<Attendance> attendance),
    @required TResult updatedCoaches(String scheduleId, List<String> coachIds),
    @required TResult updatedRoom(String scheduleId, String roomId),
    @required TResult deletedSchedule(String scheduleId),
    @required TResult loadedSchedules(DateTime fromDate, DateTime toDate),
    @required TResult loadGroups(),
  }) {
    assert(addedSchedule != null);
    assert(updatedAttendance != null);
    assert(updatedCoaches != null);
    assert(updatedRoom != null);
    assert(deletedSchedule != null);
    assert(loadedSchedules != null);
    assert(loadGroups != null);
    return loadGroups();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult addedSchedule(Schedule schedule, DateTime toDate),
    TResult updatedAttendance(String scheduleId, List<Attendance> attendance),
    TResult updatedCoaches(String scheduleId, List<String> coachIds),
    TResult updatedRoom(String scheduleId, String roomId),
    TResult deletedSchedule(String scheduleId),
    TResult loadedSchedules(DateTime fromDate, DateTime toDate),
    TResult loadGroups(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadGroups != null) {
      return loadGroups();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult addedSchedule(AddedSchedule value),
    @required TResult updatedAttendance(UpdatedAttendance value),
    @required TResult updatedCoaches(UpdatedCoaches value),
    @required TResult updatedRoom(UpdatedRoom value),
    @required TResult deletedSchedule(DeletedSchedule value),
    @required TResult loadedSchedules(LoadedSchedules value),
    @required TResult loadGroups(LoadGroups value),
  }) {
    assert(addedSchedule != null);
    assert(updatedAttendance != null);
    assert(updatedCoaches != null);
    assert(updatedRoom != null);
    assert(deletedSchedule != null);
    assert(loadedSchedules != null);
    assert(loadGroups != null);
    return loadGroups(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult addedSchedule(AddedSchedule value),
    TResult updatedAttendance(UpdatedAttendance value),
    TResult updatedCoaches(UpdatedCoaches value),
    TResult updatedRoom(UpdatedRoom value),
    TResult deletedSchedule(DeletedSchedule value),
    TResult loadedSchedules(LoadedSchedules value),
    TResult loadGroups(LoadGroups value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadGroups != null) {
      return loadGroups(this);
    }
    return orElse();
  }
}

abstract class LoadGroups implements ScheduleEvent {
  const factory LoadGroups() = _$LoadGroups;
}

/// @nodoc
class _$ScheduleStateTearOff {
  const _$ScheduleStateTearOff();

// ignore: unused_element
  ScheduleInitial scheduleInitial() {
    return ScheduleInitial();
  }

// ignore: unused_element
  ScheduleUpdated scheduleUpdated(
      {@required Schedule schedule, @required bool isUpdated}) {
    return ScheduleUpdated(
      schedule: schedule,
      isUpdated: isUpdated,
    );
  }

// ignore: unused_element
  SchedulesUpdated schedulesUpdated(
      {@required List<Schedule> schedules, @required bool isUpdated}) {
    return SchedulesUpdated(
      schedules: schedules,
      isUpdated: isUpdated,
    );
  }

// ignore: unused_element
  GroupsUpdated groupsUpdated(
      {@required List<Group> groups, @required bool isUpdated}) {
    return GroupsUpdated(
      groups: groups,
      isUpdated: isUpdated,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $ScheduleState = _$ScheduleStateTearOff();

/// @nodoc
mixin _$ScheduleState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult scheduleInitial(),
    @required TResult scheduleUpdated(Schedule schedule, bool isUpdated),
    @required
        TResult schedulesUpdated(List<Schedule> schedules, bool isUpdated),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult scheduleInitial(),
    TResult scheduleUpdated(Schedule schedule, bool isUpdated),
    TResult schedulesUpdated(List<Schedule> schedules, bool isUpdated),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult scheduleInitial(ScheduleInitial value),
    @required TResult scheduleUpdated(ScheduleUpdated value),
    @required TResult schedulesUpdated(SchedulesUpdated value),
    @required TResult groupsUpdated(GroupsUpdated value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult scheduleInitial(ScheduleInitial value),
    TResult scheduleUpdated(ScheduleUpdated value),
    TResult schedulesUpdated(SchedulesUpdated value),
    TResult groupsUpdated(GroupsUpdated value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $ScheduleStateCopyWith<$Res> {
  factory $ScheduleStateCopyWith(
          ScheduleState value, $Res Function(ScheduleState) then) =
      _$ScheduleStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$ScheduleStateCopyWithImpl<$Res>
    implements $ScheduleStateCopyWith<$Res> {
  _$ScheduleStateCopyWithImpl(this._value, this._then);

  final ScheduleState _value;
  // ignore: unused_field
  final $Res Function(ScheduleState) _then;
}

/// @nodoc
abstract class $ScheduleInitialCopyWith<$Res> {
  factory $ScheduleInitialCopyWith(
          ScheduleInitial value, $Res Function(ScheduleInitial) then) =
      _$ScheduleInitialCopyWithImpl<$Res>;
}

/// @nodoc
class _$ScheduleInitialCopyWithImpl<$Res>
    extends _$ScheduleStateCopyWithImpl<$Res>
    implements $ScheduleInitialCopyWith<$Res> {
  _$ScheduleInitialCopyWithImpl(
      ScheduleInitial _value, $Res Function(ScheduleInitial) _then)
      : super(_value, (v) => _then(v as ScheduleInitial));

  @override
  ScheduleInitial get _value => super._value as ScheduleInitial;
}

/// @nodoc
class _$ScheduleInitial implements ScheduleInitial {
  _$ScheduleInitial();

  @override
  String toString() {
    return 'ScheduleState.scheduleInitial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is ScheduleInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult scheduleInitial(),
    @required TResult scheduleUpdated(Schedule schedule, bool isUpdated),
    @required
        TResult schedulesUpdated(List<Schedule> schedules, bool isUpdated),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
  }) {
    assert(scheduleInitial != null);
    assert(scheduleUpdated != null);
    assert(schedulesUpdated != null);
    assert(groupsUpdated != null);
    return scheduleInitial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult scheduleInitial(),
    TResult scheduleUpdated(Schedule schedule, bool isUpdated),
    TResult schedulesUpdated(List<Schedule> schedules, bool isUpdated),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (scheduleInitial != null) {
      return scheduleInitial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult scheduleInitial(ScheduleInitial value),
    @required TResult scheduleUpdated(ScheduleUpdated value),
    @required TResult schedulesUpdated(SchedulesUpdated value),
    @required TResult groupsUpdated(GroupsUpdated value),
  }) {
    assert(scheduleInitial != null);
    assert(scheduleUpdated != null);
    assert(schedulesUpdated != null);
    assert(groupsUpdated != null);
    return scheduleInitial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult scheduleInitial(ScheduleInitial value),
    TResult scheduleUpdated(ScheduleUpdated value),
    TResult schedulesUpdated(SchedulesUpdated value),
    TResult groupsUpdated(GroupsUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (scheduleInitial != null) {
      return scheduleInitial(this);
    }
    return orElse();
  }
}

abstract class ScheduleInitial implements ScheduleState {
  factory ScheduleInitial() = _$ScheduleInitial;
}

/// @nodoc
abstract class $ScheduleUpdatedCopyWith<$Res> {
  factory $ScheduleUpdatedCopyWith(
          ScheduleUpdated value, $Res Function(ScheduleUpdated) then) =
      _$ScheduleUpdatedCopyWithImpl<$Res>;
  $Res call({Schedule schedule, bool isUpdated});
}

/// @nodoc
class _$ScheduleUpdatedCopyWithImpl<$Res>
    extends _$ScheduleStateCopyWithImpl<$Res>
    implements $ScheduleUpdatedCopyWith<$Res> {
  _$ScheduleUpdatedCopyWithImpl(
      ScheduleUpdated _value, $Res Function(ScheduleUpdated) _then)
      : super(_value, (v) => _then(v as ScheduleUpdated));

  @override
  ScheduleUpdated get _value => super._value as ScheduleUpdated;

  @override
  $Res call({
    Object schedule = freezed,
    Object isUpdated = freezed,
  }) {
    return _then(ScheduleUpdated(
      schedule: schedule == freezed ? _value.schedule : schedule as Schedule,
      isUpdated: isUpdated == freezed ? _value.isUpdated : isUpdated as bool,
    ));
  }
}

/// @nodoc
class _$ScheduleUpdated implements ScheduleUpdated {
  _$ScheduleUpdated({@required this.schedule, @required this.isUpdated})
      : assert(schedule != null),
        assert(isUpdated != null);

  @override
  final Schedule schedule;
  @override
  final bool isUpdated;

  @override
  String toString() {
    return 'ScheduleState.scheduleUpdated(schedule: $schedule, isUpdated: $isUpdated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ScheduleUpdated &&
            (identical(other.schedule, schedule) ||
                const DeepCollectionEquality()
                    .equals(other.schedule, schedule)) &&
            (identical(other.isUpdated, isUpdated) ||
                const DeepCollectionEquality()
                    .equals(other.isUpdated, isUpdated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(schedule) ^
      const DeepCollectionEquality().hash(isUpdated);

  @JsonKey(ignore: true)
  @override
  $ScheduleUpdatedCopyWith<ScheduleUpdated> get copyWith =>
      _$ScheduleUpdatedCopyWithImpl<ScheduleUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult scheduleInitial(),
    @required TResult scheduleUpdated(Schedule schedule, bool isUpdated),
    @required
        TResult schedulesUpdated(List<Schedule> schedules, bool isUpdated),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
  }) {
    assert(scheduleInitial != null);
    assert(scheduleUpdated != null);
    assert(schedulesUpdated != null);
    assert(groupsUpdated != null);
    return scheduleUpdated(schedule, isUpdated);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult scheduleInitial(),
    TResult scheduleUpdated(Schedule schedule, bool isUpdated),
    TResult schedulesUpdated(List<Schedule> schedules, bool isUpdated),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (scheduleUpdated != null) {
      return scheduleUpdated(schedule, isUpdated);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult scheduleInitial(ScheduleInitial value),
    @required TResult scheduleUpdated(ScheduleUpdated value),
    @required TResult schedulesUpdated(SchedulesUpdated value),
    @required TResult groupsUpdated(GroupsUpdated value),
  }) {
    assert(scheduleInitial != null);
    assert(scheduleUpdated != null);
    assert(schedulesUpdated != null);
    assert(groupsUpdated != null);
    return scheduleUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult scheduleInitial(ScheduleInitial value),
    TResult scheduleUpdated(ScheduleUpdated value),
    TResult schedulesUpdated(SchedulesUpdated value),
    TResult groupsUpdated(GroupsUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (scheduleUpdated != null) {
      return scheduleUpdated(this);
    }
    return orElse();
  }
}

abstract class ScheduleUpdated implements ScheduleState {
  factory ScheduleUpdated(
      {@required Schedule schedule,
      @required bool isUpdated}) = _$ScheduleUpdated;

  Schedule get schedule;
  bool get isUpdated;
  @JsonKey(ignore: true)
  $ScheduleUpdatedCopyWith<ScheduleUpdated> get copyWith;
}

/// @nodoc
abstract class $SchedulesUpdatedCopyWith<$Res> {
  factory $SchedulesUpdatedCopyWith(
          SchedulesUpdated value, $Res Function(SchedulesUpdated) then) =
      _$SchedulesUpdatedCopyWithImpl<$Res>;
  $Res call({List<Schedule> schedules, bool isUpdated});
}

/// @nodoc
class _$SchedulesUpdatedCopyWithImpl<$Res>
    extends _$ScheduleStateCopyWithImpl<$Res>
    implements $SchedulesUpdatedCopyWith<$Res> {
  _$SchedulesUpdatedCopyWithImpl(
      SchedulesUpdated _value, $Res Function(SchedulesUpdated) _then)
      : super(_value, (v) => _then(v as SchedulesUpdated));

  @override
  SchedulesUpdated get _value => super._value as SchedulesUpdated;

  @override
  $Res call({
    Object schedules = freezed,
    Object isUpdated = freezed,
  }) {
    return _then(SchedulesUpdated(
      schedules:
          schedules == freezed ? _value.schedules : schedules as List<Schedule>,
      isUpdated: isUpdated == freezed ? _value.isUpdated : isUpdated as bool,
    ));
  }
}

/// @nodoc
class _$SchedulesUpdated implements SchedulesUpdated {
  _$SchedulesUpdated({@required this.schedules, @required this.isUpdated})
      : assert(schedules != null),
        assert(isUpdated != null);

  @override
  final List<Schedule> schedules;
  @override
  final bool isUpdated;

  @override
  String toString() {
    return 'ScheduleState.schedulesUpdated(schedules: $schedules, isUpdated: $isUpdated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SchedulesUpdated &&
            (identical(other.schedules, schedules) ||
                const DeepCollectionEquality()
                    .equals(other.schedules, schedules)) &&
            (identical(other.isUpdated, isUpdated) ||
                const DeepCollectionEquality()
                    .equals(other.isUpdated, isUpdated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(schedules) ^
      const DeepCollectionEquality().hash(isUpdated);

  @JsonKey(ignore: true)
  @override
  $SchedulesUpdatedCopyWith<SchedulesUpdated> get copyWith =>
      _$SchedulesUpdatedCopyWithImpl<SchedulesUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult scheduleInitial(),
    @required TResult scheduleUpdated(Schedule schedule, bool isUpdated),
    @required
        TResult schedulesUpdated(List<Schedule> schedules, bool isUpdated),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
  }) {
    assert(scheduleInitial != null);
    assert(scheduleUpdated != null);
    assert(schedulesUpdated != null);
    assert(groupsUpdated != null);
    return schedulesUpdated(schedules, isUpdated);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult scheduleInitial(),
    TResult scheduleUpdated(Schedule schedule, bool isUpdated),
    TResult schedulesUpdated(List<Schedule> schedules, bool isUpdated),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (schedulesUpdated != null) {
      return schedulesUpdated(schedules, isUpdated);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult scheduleInitial(ScheduleInitial value),
    @required TResult scheduleUpdated(ScheduleUpdated value),
    @required TResult schedulesUpdated(SchedulesUpdated value),
    @required TResult groupsUpdated(GroupsUpdated value),
  }) {
    assert(scheduleInitial != null);
    assert(scheduleUpdated != null);
    assert(schedulesUpdated != null);
    assert(groupsUpdated != null);
    return schedulesUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult scheduleInitial(ScheduleInitial value),
    TResult scheduleUpdated(ScheduleUpdated value),
    TResult schedulesUpdated(SchedulesUpdated value),
    TResult groupsUpdated(GroupsUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (schedulesUpdated != null) {
      return schedulesUpdated(this);
    }
    return orElse();
  }
}

abstract class SchedulesUpdated implements ScheduleState {
  factory SchedulesUpdated(
      {@required List<Schedule> schedules,
      @required bool isUpdated}) = _$SchedulesUpdated;

  List<Schedule> get schedules;
  bool get isUpdated;
  @JsonKey(ignore: true)
  $SchedulesUpdatedCopyWith<SchedulesUpdated> get copyWith;
}

/// @nodoc
abstract class $GroupsUpdatedCopyWith<$Res> {
  factory $GroupsUpdatedCopyWith(
          GroupsUpdated value, $Res Function(GroupsUpdated) then) =
      _$GroupsUpdatedCopyWithImpl<$Res>;
  $Res call({List<Group> groups, bool isUpdated});
}

/// @nodoc
class _$GroupsUpdatedCopyWithImpl<$Res>
    extends _$ScheduleStateCopyWithImpl<$Res>
    implements $GroupsUpdatedCopyWith<$Res> {
  _$GroupsUpdatedCopyWithImpl(
      GroupsUpdated _value, $Res Function(GroupsUpdated) _then)
      : super(_value, (v) => _then(v as GroupsUpdated));

  @override
  GroupsUpdated get _value => super._value as GroupsUpdated;

  @override
  $Res call({
    Object groups = freezed,
    Object isUpdated = freezed,
  }) {
    return _then(GroupsUpdated(
      groups: groups == freezed ? _value.groups : groups as List<Group>,
      isUpdated: isUpdated == freezed ? _value.isUpdated : isUpdated as bool,
    ));
  }
}

/// @nodoc
class _$GroupsUpdated implements GroupsUpdated {
  _$GroupsUpdated({@required this.groups, @required this.isUpdated})
      : assert(groups != null),
        assert(isUpdated != null);

  @override
  final List<Group> groups;
  @override
  final bool isUpdated;

  @override
  String toString() {
    return 'ScheduleState.groupsUpdated(groups: $groups, isUpdated: $isUpdated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is GroupsUpdated &&
            (identical(other.groups, groups) ||
                const DeepCollectionEquality().equals(other.groups, groups)) &&
            (identical(other.isUpdated, isUpdated) ||
                const DeepCollectionEquality()
                    .equals(other.isUpdated, isUpdated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(groups) ^
      const DeepCollectionEquality().hash(isUpdated);

  @JsonKey(ignore: true)
  @override
  $GroupsUpdatedCopyWith<GroupsUpdated> get copyWith =>
      _$GroupsUpdatedCopyWithImpl<GroupsUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult scheduleInitial(),
    @required TResult scheduleUpdated(Schedule schedule, bool isUpdated),
    @required
        TResult schedulesUpdated(List<Schedule> schedules, bool isUpdated),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
  }) {
    assert(scheduleInitial != null);
    assert(scheduleUpdated != null);
    assert(schedulesUpdated != null);
    assert(groupsUpdated != null);
    return groupsUpdated(groups, isUpdated);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult scheduleInitial(),
    TResult scheduleUpdated(Schedule schedule, bool isUpdated),
    TResult schedulesUpdated(List<Schedule> schedules, bool isUpdated),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (groupsUpdated != null) {
      return groupsUpdated(groups, isUpdated);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult scheduleInitial(ScheduleInitial value),
    @required TResult scheduleUpdated(ScheduleUpdated value),
    @required TResult schedulesUpdated(SchedulesUpdated value),
    @required TResult groupsUpdated(GroupsUpdated value),
  }) {
    assert(scheduleInitial != null);
    assert(scheduleUpdated != null);
    assert(schedulesUpdated != null);
    assert(groupsUpdated != null);
    return groupsUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult scheduleInitial(ScheduleInitial value),
    TResult scheduleUpdated(ScheduleUpdated value),
    TResult schedulesUpdated(SchedulesUpdated value),
    TResult groupsUpdated(GroupsUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (groupsUpdated != null) {
      return groupsUpdated(this);
    }
    return orElse();
  }
}

abstract class GroupsUpdated implements ScheduleState {
  factory GroupsUpdated(
      {@required List<Group> groups,
      @required bool isUpdated}) = _$GroupsUpdated;

  List<Group> get groups;
  bool get isUpdated;
  @JsonKey(ignore: true)
  $GroupsUpdatedCopyWith<GroupsUpdated> get copyWith;
}
