// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'admin_user_token_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$AdminUserTokenEventTearOff {
  const _$AdminUserTokenEventTearOff();

// ignore: unused_element
  _LoadUserTokens loadUserTokens(@required User user) {
    return _LoadUserTokens(
      user,
    );
  }

// ignore: unused_element
  _AddUserToken addUserToken(@required Token token) {
    return _AddUserToken(
      token,
    );
  }

// ignore: unused_element
  _RemoveUserToken removeUserToken(@required Token token) {
    return _RemoveUserToken(
      token,
    );
  }

// ignore: unused_element
  _Save save() {
    return const _Save();
  }

// ignore: unused_element
  _SetDate setDate(DateTime date) {
    return _SetDate(
      date,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $AdminUserTokenEvent = _$AdminUserTokenEventTearOff();

/// @nodoc
mixin _$AdminUserTokenEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadUserTokens(@required User user),
    @required TResult addUserToken(@required Token token),
    @required TResult removeUserToken(@required Token token),
    @required TResult save(),
    @required TResult setDate(DateTime date),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadUserTokens(@required User user),
    TResult addUserToken(@required Token token),
    TResult removeUserToken(@required Token token),
    TResult save(),
    TResult setDate(DateTime date),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadUserTokens(_LoadUserTokens value),
    @required TResult addUserToken(_AddUserToken value),
    @required TResult removeUserToken(_RemoveUserToken value),
    @required TResult save(_Save value),
    @required TResult setDate(_SetDate value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadUserTokens(_LoadUserTokens value),
    TResult addUserToken(_AddUserToken value),
    TResult removeUserToken(_RemoveUserToken value),
    TResult save(_Save value),
    TResult setDate(_SetDate value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $AdminUserTokenEventCopyWith<$Res> {
  factory $AdminUserTokenEventCopyWith(
          AdminUserTokenEvent value, $Res Function(AdminUserTokenEvent) then) =
      _$AdminUserTokenEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$AdminUserTokenEventCopyWithImpl<$Res>
    implements $AdminUserTokenEventCopyWith<$Res> {
  _$AdminUserTokenEventCopyWithImpl(this._value, this._then);

  final AdminUserTokenEvent _value;
  // ignore: unused_field
  final $Res Function(AdminUserTokenEvent) _then;
}

/// @nodoc
abstract class _$LoadUserTokensCopyWith<$Res> {
  factory _$LoadUserTokensCopyWith(
          _LoadUserTokens value, $Res Function(_LoadUserTokens) then) =
      __$LoadUserTokensCopyWithImpl<$Res>;
  $Res call({User user});
}

/// @nodoc
class __$LoadUserTokensCopyWithImpl<$Res>
    extends _$AdminUserTokenEventCopyWithImpl<$Res>
    implements _$LoadUserTokensCopyWith<$Res> {
  __$LoadUserTokensCopyWithImpl(
      _LoadUserTokens _value, $Res Function(_LoadUserTokens) _then)
      : super(_value, (v) => _then(v as _LoadUserTokens));

  @override
  _LoadUserTokens get _value => super._value as _LoadUserTokens;

  @override
  $Res call({
    Object user = freezed,
  }) {
    return _then(_LoadUserTokens(
      user == freezed ? _value.user : user as User,
    ));
  }
}

/// @nodoc
class _$_LoadUserTokens implements _LoadUserTokens {
  const _$_LoadUserTokens(@required this.user) : assert(user != null);

  @override
  final User user;

  @override
  String toString() {
    return 'AdminUserTokenEvent.loadUserTokens(user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _LoadUserTokens &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(user);

  @JsonKey(ignore: true)
  @override
  _$LoadUserTokensCopyWith<_LoadUserTokens> get copyWith =>
      __$LoadUserTokensCopyWithImpl<_LoadUserTokens>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadUserTokens(@required User user),
    @required TResult addUserToken(@required Token token),
    @required TResult removeUserToken(@required Token token),
    @required TResult save(),
    @required TResult setDate(DateTime date),
  }) {
    assert(loadUserTokens != null);
    assert(addUserToken != null);
    assert(removeUserToken != null);
    assert(save != null);
    assert(setDate != null);
    return loadUserTokens(user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadUserTokens(@required User user),
    TResult addUserToken(@required Token token),
    TResult removeUserToken(@required Token token),
    TResult save(),
    TResult setDate(DateTime date),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadUserTokens != null) {
      return loadUserTokens(user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadUserTokens(_LoadUserTokens value),
    @required TResult addUserToken(_AddUserToken value),
    @required TResult removeUserToken(_RemoveUserToken value),
    @required TResult save(_Save value),
    @required TResult setDate(_SetDate value),
  }) {
    assert(loadUserTokens != null);
    assert(addUserToken != null);
    assert(removeUserToken != null);
    assert(save != null);
    assert(setDate != null);
    return loadUserTokens(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadUserTokens(_LoadUserTokens value),
    TResult addUserToken(_AddUserToken value),
    TResult removeUserToken(_RemoveUserToken value),
    TResult save(_Save value),
    TResult setDate(_SetDate value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadUserTokens != null) {
      return loadUserTokens(this);
    }
    return orElse();
  }
}

abstract class _LoadUserTokens implements AdminUserTokenEvent {
  const factory _LoadUserTokens(@required User user) = _$_LoadUserTokens;

  User get user;
  @JsonKey(ignore: true)
  _$LoadUserTokensCopyWith<_LoadUserTokens> get copyWith;
}

/// @nodoc
abstract class _$AddUserTokenCopyWith<$Res> {
  factory _$AddUserTokenCopyWith(
          _AddUserToken value, $Res Function(_AddUserToken) then) =
      __$AddUserTokenCopyWithImpl<$Res>;
  $Res call({Token token});
}

/// @nodoc
class __$AddUserTokenCopyWithImpl<$Res>
    extends _$AdminUserTokenEventCopyWithImpl<$Res>
    implements _$AddUserTokenCopyWith<$Res> {
  __$AddUserTokenCopyWithImpl(
      _AddUserToken _value, $Res Function(_AddUserToken) _then)
      : super(_value, (v) => _then(v as _AddUserToken));

  @override
  _AddUserToken get _value => super._value as _AddUserToken;

  @override
  $Res call({
    Object token = freezed,
  }) {
    return _then(_AddUserToken(
      token == freezed ? _value.token : token as Token,
    ));
  }
}

/// @nodoc
class _$_AddUserToken implements _AddUserToken {
  const _$_AddUserToken(@required this.token) : assert(token != null);

  @override
  final Token token;

  @override
  String toString() {
    return 'AdminUserTokenEvent.addUserToken(token: $token)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _AddUserToken &&
            (identical(other.token, token) ||
                const DeepCollectionEquality().equals(other.token, token)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(token);

  @JsonKey(ignore: true)
  @override
  _$AddUserTokenCopyWith<_AddUserToken> get copyWith =>
      __$AddUserTokenCopyWithImpl<_AddUserToken>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadUserTokens(@required User user),
    @required TResult addUserToken(@required Token token),
    @required TResult removeUserToken(@required Token token),
    @required TResult save(),
    @required TResult setDate(DateTime date),
  }) {
    assert(loadUserTokens != null);
    assert(addUserToken != null);
    assert(removeUserToken != null);
    assert(save != null);
    assert(setDate != null);
    return addUserToken(token);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadUserTokens(@required User user),
    TResult addUserToken(@required Token token),
    TResult removeUserToken(@required Token token),
    TResult save(),
    TResult setDate(DateTime date),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addUserToken != null) {
      return addUserToken(token);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadUserTokens(_LoadUserTokens value),
    @required TResult addUserToken(_AddUserToken value),
    @required TResult removeUserToken(_RemoveUserToken value),
    @required TResult save(_Save value),
    @required TResult setDate(_SetDate value),
  }) {
    assert(loadUserTokens != null);
    assert(addUserToken != null);
    assert(removeUserToken != null);
    assert(save != null);
    assert(setDate != null);
    return addUserToken(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadUserTokens(_LoadUserTokens value),
    TResult addUserToken(_AddUserToken value),
    TResult removeUserToken(_RemoveUserToken value),
    TResult save(_Save value),
    TResult setDate(_SetDate value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addUserToken != null) {
      return addUserToken(this);
    }
    return orElse();
  }
}

abstract class _AddUserToken implements AdminUserTokenEvent {
  const factory _AddUserToken(@required Token token) = _$_AddUserToken;

  Token get token;
  @JsonKey(ignore: true)
  _$AddUserTokenCopyWith<_AddUserToken> get copyWith;
}

/// @nodoc
abstract class _$RemoveUserTokenCopyWith<$Res> {
  factory _$RemoveUserTokenCopyWith(
          _RemoveUserToken value, $Res Function(_RemoveUserToken) then) =
      __$RemoveUserTokenCopyWithImpl<$Res>;
  $Res call({Token token});
}

/// @nodoc
class __$RemoveUserTokenCopyWithImpl<$Res>
    extends _$AdminUserTokenEventCopyWithImpl<$Res>
    implements _$RemoveUserTokenCopyWith<$Res> {
  __$RemoveUserTokenCopyWithImpl(
      _RemoveUserToken _value, $Res Function(_RemoveUserToken) _then)
      : super(_value, (v) => _then(v as _RemoveUserToken));

  @override
  _RemoveUserToken get _value => super._value as _RemoveUserToken;

  @override
  $Res call({
    Object token = freezed,
  }) {
    return _then(_RemoveUserToken(
      token == freezed ? _value.token : token as Token,
    ));
  }
}

/// @nodoc
class _$_RemoveUserToken implements _RemoveUserToken {
  const _$_RemoveUserToken(@required this.token) : assert(token != null);

  @override
  final Token token;

  @override
  String toString() {
    return 'AdminUserTokenEvent.removeUserToken(token: $token)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _RemoveUserToken &&
            (identical(other.token, token) ||
                const DeepCollectionEquality().equals(other.token, token)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(token);

  @JsonKey(ignore: true)
  @override
  _$RemoveUserTokenCopyWith<_RemoveUserToken> get copyWith =>
      __$RemoveUserTokenCopyWithImpl<_RemoveUserToken>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadUserTokens(@required User user),
    @required TResult addUserToken(@required Token token),
    @required TResult removeUserToken(@required Token token),
    @required TResult save(),
    @required TResult setDate(DateTime date),
  }) {
    assert(loadUserTokens != null);
    assert(addUserToken != null);
    assert(removeUserToken != null);
    assert(save != null);
    assert(setDate != null);
    return removeUserToken(token);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadUserTokens(@required User user),
    TResult addUserToken(@required Token token),
    TResult removeUserToken(@required Token token),
    TResult save(),
    TResult setDate(DateTime date),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (removeUserToken != null) {
      return removeUserToken(token);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadUserTokens(_LoadUserTokens value),
    @required TResult addUserToken(_AddUserToken value),
    @required TResult removeUserToken(_RemoveUserToken value),
    @required TResult save(_Save value),
    @required TResult setDate(_SetDate value),
  }) {
    assert(loadUserTokens != null);
    assert(addUserToken != null);
    assert(removeUserToken != null);
    assert(save != null);
    assert(setDate != null);
    return removeUserToken(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadUserTokens(_LoadUserTokens value),
    TResult addUserToken(_AddUserToken value),
    TResult removeUserToken(_RemoveUserToken value),
    TResult save(_Save value),
    TResult setDate(_SetDate value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (removeUserToken != null) {
      return removeUserToken(this);
    }
    return orElse();
  }
}

abstract class _RemoveUserToken implements AdminUserTokenEvent {
  const factory _RemoveUserToken(@required Token token) = _$_RemoveUserToken;

  Token get token;
  @JsonKey(ignore: true)
  _$RemoveUserTokenCopyWith<_RemoveUserToken> get copyWith;
}

/// @nodoc
abstract class _$SaveCopyWith<$Res> {
  factory _$SaveCopyWith(_Save value, $Res Function(_Save) then) =
      __$SaveCopyWithImpl<$Res>;
}

/// @nodoc
class __$SaveCopyWithImpl<$Res> extends _$AdminUserTokenEventCopyWithImpl<$Res>
    implements _$SaveCopyWith<$Res> {
  __$SaveCopyWithImpl(_Save _value, $Res Function(_Save) _then)
      : super(_value, (v) => _then(v as _Save));

  @override
  _Save get _value => super._value as _Save;
}

/// @nodoc
class _$_Save implements _Save {
  const _$_Save();

  @override
  String toString() {
    return 'AdminUserTokenEvent.save()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Save);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadUserTokens(@required User user),
    @required TResult addUserToken(@required Token token),
    @required TResult removeUserToken(@required Token token),
    @required TResult save(),
    @required TResult setDate(DateTime date),
  }) {
    assert(loadUserTokens != null);
    assert(addUserToken != null);
    assert(removeUserToken != null);
    assert(save != null);
    assert(setDate != null);
    return save();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadUserTokens(@required User user),
    TResult addUserToken(@required Token token),
    TResult removeUserToken(@required Token token),
    TResult save(),
    TResult setDate(DateTime date),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (save != null) {
      return save();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadUserTokens(_LoadUserTokens value),
    @required TResult addUserToken(_AddUserToken value),
    @required TResult removeUserToken(_RemoveUserToken value),
    @required TResult save(_Save value),
    @required TResult setDate(_SetDate value),
  }) {
    assert(loadUserTokens != null);
    assert(addUserToken != null);
    assert(removeUserToken != null);
    assert(save != null);
    assert(setDate != null);
    return save(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadUserTokens(_LoadUserTokens value),
    TResult addUserToken(_AddUserToken value),
    TResult removeUserToken(_RemoveUserToken value),
    TResult save(_Save value),
    TResult setDate(_SetDate value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (save != null) {
      return save(this);
    }
    return orElse();
  }
}

abstract class _Save implements AdminUserTokenEvent {
  const factory _Save() = _$_Save;
}

/// @nodoc
abstract class _$SetDateCopyWith<$Res> {
  factory _$SetDateCopyWith(_SetDate value, $Res Function(_SetDate) then) =
      __$SetDateCopyWithImpl<$Res>;
  $Res call({DateTime date});
}

/// @nodoc
class __$SetDateCopyWithImpl<$Res>
    extends _$AdminUserTokenEventCopyWithImpl<$Res>
    implements _$SetDateCopyWith<$Res> {
  __$SetDateCopyWithImpl(_SetDate _value, $Res Function(_SetDate) _then)
      : super(_value, (v) => _then(v as _SetDate));

  @override
  _SetDate get _value => super._value as _SetDate;

  @override
  $Res call({
    Object date = freezed,
  }) {
    return _then(_SetDate(
      date == freezed ? _value.date : date as DateTime,
    ));
  }
}

/// @nodoc
class _$_SetDate implements _SetDate {
  const _$_SetDate(this.date) : assert(date != null);

  @override
  final DateTime date;

  @override
  String toString() {
    return 'AdminUserTokenEvent.setDate(date: $date)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SetDate &&
            (identical(other.date, date) ||
                const DeepCollectionEquality().equals(other.date, date)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(date);

  @JsonKey(ignore: true)
  @override
  _$SetDateCopyWith<_SetDate> get copyWith =>
      __$SetDateCopyWithImpl<_SetDate>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadUserTokens(@required User user),
    @required TResult addUserToken(@required Token token),
    @required TResult removeUserToken(@required Token token),
    @required TResult save(),
    @required TResult setDate(DateTime date),
  }) {
    assert(loadUserTokens != null);
    assert(addUserToken != null);
    assert(removeUserToken != null);
    assert(save != null);
    assert(setDate != null);
    return setDate(date);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadUserTokens(@required User user),
    TResult addUserToken(@required Token token),
    TResult removeUserToken(@required Token token),
    TResult save(),
    TResult setDate(DateTime date),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (setDate != null) {
      return setDate(date);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadUserTokens(_LoadUserTokens value),
    @required TResult addUserToken(_AddUserToken value),
    @required TResult removeUserToken(_RemoveUserToken value),
    @required TResult save(_Save value),
    @required TResult setDate(_SetDate value),
  }) {
    assert(loadUserTokens != null);
    assert(addUserToken != null);
    assert(removeUserToken != null);
    assert(save != null);
    assert(setDate != null);
    return setDate(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadUserTokens(_LoadUserTokens value),
    TResult addUserToken(_AddUserToken value),
    TResult removeUserToken(_RemoveUserToken value),
    TResult save(_Save value),
    TResult setDate(_SetDate value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (setDate != null) {
      return setDate(this);
    }
    return orElse();
  }
}

abstract class _SetDate implements AdminUserTokenEvent {
  const factory _SetDate(DateTime date) = _$_SetDate;

  DateTime get date;
  @JsonKey(ignore: true)
  _$SetDateCopyWith<_SetDate> get copyWith;
}

/// @nodoc
class _$AdminUserTokenStateTearOff {
  const _$AdminUserTokenStateTearOff();

// ignore: unused_element
  _AdminUserTokenState call(
      {@required User user,
      @required List<Token> tokens,
      @required List<UserToken> userTokens,
      @required DateTime currentDate}) {
    return _AdminUserTokenState(
      user: user,
      tokens: tokens,
      userTokens: userTokens,
      currentDate: currentDate,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $AdminUserTokenState = _$AdminUserTokenStateTearOff();

/// @nodoc
mixin _$AdminUserTokenState {
  User get user;
  List<Token> get tokens;
  List<UserToken> get userTokens;
  DateTime get currentDate;

  @JsonKey(ignore: true)
  $AdminUserTokenStateCopyWith<AdminUserTokenState> get copyWith;
}

/// @nodoc
abstract class $AdminUserTokenStateCopyWith<$Res> {
  factory $AdminUserTokenStateCopyWith(
          AdminUserTokenState value, $Res Function(AdminUserTokenState) then) =
      _$AdminUserTokenStateCopyWithImpl<$Res>;
  $Res call(
      {User user,
      List<Token> tokens,
      List<UserToken> userTokens,
      DateTime currentDate});
}

/// @nodoc
class _$AdminUserTokenStateCopyWithImpl<$Res>
    implements $AdminUserTokenStateCopyWith<$Res> {
  _$AdminUserTokenStateCopyWithImpl(this._value, this._then);

  final AdminUserTokenState _value;
  // ignore: unused_field
  final $Res Function(AdminUserTokenState) _then;

  @override
  $Res call({
    Object user = freezed,
    Object tokens = freezed,
    Object userTokens = freezed,
    Object currentDate = freezed,
  }) {
    return _then(_value.copyWith(
      user: user == freezed ? _value.user : user as User,
      tokens: tokens == freezed ? _value.tokens : tokens as List<Token>,
      userTokens: userTokens == freezed
          ? _value.userTokens
          : userTokens as List<UserToken>,
      currentDate:
          currentDate == freezed ? _value.currentDate : currentDate as DateTime,
    ));
  }
}

/// @nodoc
abstract class _$AdminUserTokenStateCopyWith<$Res>
    implements $AdminUserTokenStateCopyWith<$Res> {
  factory _$AdminUserTokenStateCopyWith(_AdminUserTokenState value,
          $Res Function(_AdminUserTokenState) then) =
      __$AdminUserTokenStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {User user,
      List<Token> tokens,
      List<UserToken> userTokens,
      DateTime currentDate});
}

/// @nodoc
class __$AdminUserTokenStateCopyWithImpl<$Res>
    extends _$AdminUserTokenStateCopyWithImpl<$Res>
    implements _$AdminUserTokenStateCopyWith<$Res> {
  __$AdminUserTokenStateCopyWithImpl(
      _AdminUserTokenState _value, $Res Function(_AdminUserTokenState) _then)
      : super(_value, (v) => _then(v as _AdminUserTokenState));

  @override
  _AdminUserTokenState get _value => super._value as _AdminUserTokenState;

  @override
  $Res call({
    Object user = freezed,
    Object tokens = freezed,
    Object userTokens = freezed,
    Object currentDate = freezed,
  }) {
    return _then(_AdminUserTokenState(
      user: user == freezed ? _value.user : user as User,
      tokens: tokens == freezed ? _value.tokens : tokens as List<Token>,
      userTokens: userTokens == freezed
          ? _value.userTokens
          : userTokens as List<UserToken>,
      currentDate:
          currentDate == freezed ? _value.currentDate : currentDate as DateTime,
    ));
  }
}

/// @nodoc
class _$_AdminUserTokenState implements _AdminUserTokenState {
  const _$_AdminUserTokenState(
      {@required this.user,
      @required this.tokens,
      @required this.userTokens,
      @required this.currentDate})
      : assert(user != null),
        assert(tokens != null),
        assert(userTokens != null),
        assert(currentDate != null);

  @override
  final User user;
  @override
  final List<Token> tokens;
  @override
  final List<UserToken> userTokens;
  @override
  final DateTime currentDate;

  @override
  String toString() {
    return 'AdminUserTokenState(user: $user, tokens: $tokens, userTokens: $userTokens, currentDate: $currentDate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _AdminUserTokenState &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)) &&
            (identical(other.tokens, tokens) ||
                const DeepCollectionEquality().equals(other.tokens, tokens)) &&
            (identical(other.userTokens, userTokens) ||
                const DeepCollectionEquality()
                    .equals(other.userTokens, userTokens)) &&
            (identical(other.currentDate, currentDate) ||
                const DeepCollectionEquality()
                    .equals(other.currentDate, currentDate)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(user) ^
      const DeepCollectionEquality().hash(tokens) ^
      const DeepCollectionEquality().hash(userTokens) ^
      const DeepCollectionEquality().hash(currentDate);

  @JsonKey(ignore: true)
  @override
  _$AdminUserTokenStateCopyWith<_AdminUserTokenState> get copyWith =>
      __$AdminUserTokenStateCopyWithImpl<_AdminUserTokenState>(
          this, _$identity);
}

abstract class _AdminUserTokenState implements AdminUserTokenState {
  const factory _AdminUserTokenState(
      {@required User user,
      @required List<Token> tokens,
      @required List<UserToken> userTokens,
      @required DateTime currentDate}) = _$_AdminUserTokenState;

  @override
  User get user;
  @override
  List<Token> get tokens;
  @override
  List<UserToken> get userTokens;
  @override
  DateTime get currentDate;
  @override
  @JsonKey(ignore: true)
  _$AdminUserTokenStateCopyWith<_AdminUserTokenState> get copyWith;
}
