part of 'admin_user_token_bloc.dart';

@freezed
abstract class AdminUserTokenEvent with _$AdminUserTokenEvent {
  const factory AdminUserTokenEvent.loadUserTokens(@required User user) = _LoadUserTokens;
  const factory AdminUserTokenEvent.addUserToken(@required Token token) = _AddUserToken;
  const factory AdminUserTokenEvent.removeUserToken(@required Token token) = _RemoveUserToken;
  const factory AdminUserTokenEvent.save() = _Save;
  const factory AdminUserTokenEvent.setDate(DateTime date) = _SetDate;
}