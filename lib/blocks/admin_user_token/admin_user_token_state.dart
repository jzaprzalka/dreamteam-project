part of 'admin_user_token_bloc.dart';

@freezed
abstract class AdminUserTokenState with _$AdminUserTokenState {
  const factory AdminUserTokenState(
      {@required User user,
      @required List<Token> tokens,
      @required List<UserToken> userTokens,
      @required DateTime currentDate}) = _AdminUserTokenState;

  factory AdminUserTokenState.initial() =>
      _AdminUserTokenState(user: User(userId: "", name: '', surname: '', ), userTokens: List.empty(), currentDate: DateTime.now(), tokens: List.empty());
}
