import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dreamteam_project/models/Token.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/models/UserToken.dart';
import 'package:dreamteam_project/repositories/token_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'admin_user_token_event.dart';
part 'admin_user_token_state.dart';
part 'admin_user_token_bloc.freezed.dart';

class AdminUserTokenBloc
    extends Bloc<AdminUserTokenEvent, AdminUserTokenState> {
  TokenRepository tokenRepository = TokenRepository();
  AdminUserTokenBloc() : super(AdminUserTokenState.initial());

  @override
  Stream<AdminUserTokenState> mapEventToState(
    AdminUserTokenEvent event,
  ) async* {
    yield* event.map(loadUserTokens: (e) async* {
      final userTokens = await tokenRepository.getTokensForUser(e.user);
      final tokens = await tokenRepository.getAllTokens();
      yield state.copyWith(
          user: e.user, userTokens: userTokens, tokens: tokens);
    }, addUserToken: (e) async* {
      final us = UserToken(userId: state.user.userId, tokenId: e.token.id);
      var newlist = state.userTokens.map((e) => e).toList();
      newlist.add(us);
      yield state.copyWith(userTokens: newlist);
    }, removeUserToken: (e) async* {
      var newlist = state.userTokens.map((e) => e).toList();
      newlist.removeWhere((us) => us.tokenId == e.token.id);
      yield state.copyWith(userTokens: newlist);
    }, save: (e) async* {
      for (var us in state.userTokens) {
        us.expirationDate = state.currentDate;
        us.userTokenId == null
            ? await tokenRepository.addUserTokenByUserToken(us)
            : await tokenRepository.updateUserToken(us);
      }
    }, setDate: (e) async* {
      yield state.copyWith(currentDate: e.date);
    });
  }
}
