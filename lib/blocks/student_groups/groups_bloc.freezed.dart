// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'groups_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$GroupsEventTearOff {
  const _$GroupsEventTearOff();

// ignore: unused_element
  LoadedAttendance loadedAttendance({@required String groupId}) {
    return LoadedAttendance(
      groupId: groupId,
    );
  }

// ignore: unused_element
  LoadedGroups loadedGroups() {
    return const LoadedGroups();
  }

// ignore: unused_element
  LoadedGroup loadedGroup({@required String groupId}) {
    return LoadedGroup(
      groupId: groupId,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $GroupsEvent = _$GroupsEventTearOff();

/// @nodoc
mixin _$GroupsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadedAttendance(String groupId),
    @required TResult loadedGroups(),
    @required TResult loadedGroup(String groupId),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadedAttendance(String groupId),
    TResult loadedGroups(),
    TResult loadedGroup(String groupId),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadedAttendance(LoadedAttendance value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedGroup(LoadedGroup value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadedAttendance(LoadedAttendance value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedGroup(LoadedGroup value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $GroupsEventCopyWith<$Res> {
  factory $GroupsEventCopyWith(
          GroupsEvent value, $Res Function(GroupsEvent) then) =
      _$GroupsEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$GroupsEventCopyWithImpl<$Res> implements $GroupsEventCopyWith<$Res> {
  _$GroupsEventCopyWithImpl(this._value, this._then);

  final GroupsEvent _value;
  // ignore: unused_field
  final $Res Function(GroupsEvent) _then;
}

/// @nodoc
abstract class $LoadedAttendanceCopyWith<$Res> {
  factory $LoadedAttendanceCopyWith(
          LoadedAttendance value, $Res Function(LoadedAttendance) then) =
      _$LoadedAttendanceCopyWithImpl<$Res>;
  $Res call({String groupId});
}

/// @nodoc
class _$LoadedAttendanceCopyWithImpl<$Res>
    extends _$GroupsEventCopyWithImpl<$Res>
    implements $LoadedAttendanceCopyWith<$Res> {
  _$LoadedAttendanceCopyWithImpl(
      LoadedAttendance _value, $Res Function(LoadedAttendance) _then)
      : super(_value, (v) => _then(v as LoadedAttendance));

  @override
  LoadedAttendance get _value => super._value as LoadedAttendance;

  @override
  $Res call({
    Object groupId = freezed,
  }) {
    return _then(LoadedAttendance(
      groupId: groupId == freezed ? _value.groupId : groupId as String,
    ));
  }
}

/// @nodoc
class _$LoadedAttendance implements LoadedAttendance {
  const _$LoadedAttendance({@required this.groupId}) : assert(groupId != null);

  @override
  final String groupId;

  @override
  String toString() {
    return 'GroupsEvent.loadedAttendance(groupId: $groupId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoadedAttendance &&
            (identical(other.groupId, groupId) ||
                const DeepCollectionEquality().equals(other.groupId, groupId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(groupId);

  @JsonKey(ignore: true)
  @override
  $LoadedAttendanceCopyWith<LoadedAttendance> get copyWith =>
      _$LoadedAttendanceCopyWithImpl<LoadedAttendance>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadedAttendance(String groupId),
    @required TResult loadedGroups(),
    @required TResult loadedGroup(String groupId),
  }) {
    assert(loadedAttendance != null);
    assert(loadedGroups != null);
    assert(loadedGroup != null);
    return loadedAttendance(groupId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadedAttendance(String groupId),
    TResult loadedGroups(),
    TResult loadedGroup(String groupId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedAttendance != null) {
      return loadedAttendance(groupId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadedAttendance(LoadedAttendance value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedGroup(LoadedGroup value),
  }) {
    assert(loadedAttendance != null);
    assert(loadedGroups != null);
    assert(loadedGroup != null);
    return loadedAttendance(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadedAttendance(LoadedAttendance value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedGroup(LoadedGroup value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedAttendance != null) {
      return loadedAttendance(this);
    }
    return orElse();
  }
}

abstract class LoadedAttendance implements GroupsEvent {
  const factory LoadedAttendance({@required String groupId}) =
      _$LoadedAttendance;

  String get groupId;
  @JsonKey(ignore: true)
  $LoadedAttendanceCopyWith<LoadedAttendance> get copyWith;
}

/// @nodoc
abstract class $LoadedGroupsCopyWith<$Res> {
  factory $LoadedGroupsCopyWith(
          LoadedGroups value, $Res Function(LoadedGroups) then) =
      _$LoadedGroupsCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoadedGroupsCopyWithImpl<$Res> extends _$GroupsEventCopyWithImpl<$Res>
    implements $LoadedGroupsCopyWith<$Res> {
  _$LoadedGroupsCopyWithImpl(
      LoadedGroups _value, $Res Function(LoadedGroups) _then)
      : super(_value, (v) => _then(v as LoadedGroups));

  @override
  LoadedGroups get _value => super._value as LoadedGroups;
}

/// @nodoc
class _$LoadedGroups implements LoadedGroups {
  const _$LoadedGroups();

  @override
  String toString() {
    return 'GroupsEvent.loadedGroups()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoadedGroups);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadedAttendance(String groupId),
    @required TResult loadedGroups(),
    @required TResult loadedGroup(String groupId),
  }) {
    assert(loadedAttendance != null);
    assert(loadedGroups != null);
    assert(loadedGroup != null);
    return loadedGroups();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadedAttendance(String groupId),
    TResult loadedGroups(),
    TResult loadedGroup(String groupId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedGroups != null) {
      return loadedGroups();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadedAttendance(LoadedAttendance value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedGroup(LoadedGroup value),
  }) {
    assert(loadedAttendance != null);
    assert(loadedGroups != null);
    assert(loadedGroup != null);
    return loadedGroups(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadedAttendance(LoadedAttendance value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedGroup(LoadedGroup value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedGroups != null) {
      return loadedGroups(this);
    }
    return orElse();
  }
}

abstract class LoadedGroups implements GroupsEvent {
  const factory LoadedGroups() = _$LoadedGroups;
}

/// @nodoc
abstract class $LoadedGroupCopyWith<$Res> {
  factory $LoadedGroupCopyWith(
          LoadedGroup value, $Res Function(LoadedGroup) then) =
      _$LoadedGroupCopyWithImpl<$Res>;
  $Res call({String groupId});
}

/// @nodoc
class _$LoadedGroupCopyWithImpl<$Res> extends _$GroupsEventCopyWithImpl<$Res>
    implements $LoadedGroupCopyWith<$Res> {
  _$LoadedGroupCopyWithImpl(
      LoadedGroup _value, $Res Function(LoadedGroup) _then)
      : super(_value, (v) => _then(v as LoadedGroup));

  @override
  LoadedGroup get _value => super._value as LoadedGroup;

  @override
  $Res call({
    Object groupId = freezed,
  }) {
    return _then(LoadedGroup(
      groupId: groupId == freezed ? _value.groupId : groupId as String,
    ));
  }
}

/// @nodoc
class _$LoadedGroup implements LoadedGroup {
  const _$LoadedGroup({@required this.groupId}) : assert(groupId != null);

  @override
  final String groupId;

  @override
  String toString() {
    return 'GroupsEvent.loadedGroup(groupId: $groupId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoadedGroup &&
            (identical(other.groupId, groupId) ||
                const DeepCollectionEquality().equals(other.groupId, groupId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(groupId);

  @JsonKey(ignore: true)
  @override
  $LoadedGroupCopyWith<LoadedGroup> get copyWith =>
      _$LoadedGroupCopyWithImpl<LoadedGroup>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadedAttendance(String groupId),
    @required TResult loadedGroups(),
    @required TResult loadedGroup(String groupId),
  }) {
    assert(loadedAttendance != null);
    assert(loadedGroups != null);
    assert(loadedGroup != null);
    return loadedGroup(groupId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadedAttendance(String groupId),
    TResult loadedGroups(),
    TResult loadedGroup(String groupId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedGroup != null) {
      return loadedGroup(groupId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadedAttendance(LoadedAttendance value),
    @required TResult loadedGroups(LoadedGroups value),
    @required TResult loadedGroup(LoadedGroup value),
  }) {
    assert(loadedAttendance != null);
    assert(loadedGroups != null);
    assert(loadedGroup != null);
    return loadedGroup(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadedAttendance(LoadedAttendance value),
    TResult loadedGroups(LoadedGroups value),
    TResult loadedGroup(LoadedGroup value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedGroup != null) {
      return loadedGroup(this);
    }
    return orElse();
  }
}

abstract class LoadedGroup implements GroupsEvent {
  const factory LoadedGroup({@required String groupId}) = _$LoadedGroup;

  String get groupId;
  @JsonKey(ignore: true)
  $LoadedGroupCopyWith<LoadedGroup> get copyWith;
}

/// @nodoc
class _$GroupsStateTearOff {
  const _$GroupsStateTearOff();

// ignore: unused_element
  GroupsInitial groupsInitial() {
    return GroupsInitial();
  }

// ignore: unused_element
  GroupUpdated groupUpdated({@required Group group, @required bool isUpdated}) {
    return GroupUpdated(
      group: group,
      isUpdated: isUpdated,
    );
  }

// ignore: unused_element
  GroupsUpdated groupsUpdated(
      {@required List<Group> groups, @required bool isUpdated}) {
    return GroupsUpdated(
      groups: groups,
      isUpdated: isUpdated,
    );
  }

// ignore: unused_element
  AttendanceUpdated attendanceUpdated(
      {@required List<Attendance> attendance, @required bool isUpdated}) {
    return AttendanceUpdated(
      attendance: attendance,
      isUpdated: isUpdated,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $GroupsState = _$GroupsStateTearOff();

/// @nodoc
mixin _$GroupsState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult groupsInitial(),
    @required TResult groupUpdated(Group group, bool isUpdated),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required
        TResult attendanceUpdated(List<Attendance> attendance, bool isUpdated),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult groupsInitial(),
    TResult groupUpdated(Group group, bool isUpdated),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    TResult attendanceUpdated(List<Attendance> attendance, bool isUpdated),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult groupsInitial(GroupsInitial value),
    @required TResult groupUpdated(GroupUpdated value),
    @required TResult groupsUpdated(GroupsUpdated value),
    @required TResult attendanceUpdated(AttendanceUpdated value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult groupsInitial(GroupsInitial value),
    TResult groupUpdated(GroupUpdated value),
    TResult groupsUpdated(GroupsUpdated value),
    TResult attendanceUpdated(AttendanceUpdated value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $GroupsStateCopyWith<$Res> {
  factory $GroupsStateCopyWith(
          GroupsState value, $Res Function(GroupsState) then) =
      _$GroupsStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$GroupsStateCopyWithImpl<$Res> implements $GroupsStateCopyWith<$Res> {
  _$GroupsStateCopyWithImpl(this._value, this._then);

  final GroupsState _value;
  // ignore: unused_field
  final $Res Function(GroupsState) _then;
}

/// @nodoc
abstract class $GroupsInitialCopyWith<$Res> {
  factory $GroupsInitialCopyWith(
          GroupsInitial value, $Res Function(GroupsInitial) then) =
      _$GroupsInitialCopyWithImpl<$Res>;
}

/// @nodoc
class _$GroupsInitialCopyWithImpl<$Res> extends _$GroupsStateCopyWithImpl<$Res>
    implements $GroupsInitialCopyWith<$Res> {
  _$GroupsInitialCopyWithImpl(
      GroupsInitial _value, $Res Function(GroupsInitial) _then)
      : super(_value, (v) => _then(v as GroupsInitial));

  @override
  GroupsInitial get _value => super._value as GroupsInitial;
}

/// @nodoc
class _$GroupsInitial implements GroupsInitial {
  _$GroupsInitial();

  @override
  String toString() {
    return 'GroupsState.groupsInitial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is GroupsInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult groupsInitial(),
    @required TResult groupUpdated(Group group, bool isUpdated),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required
        TResult attendanceUpdated(List<Attendance> attendance, bool isUpdated),
  }) {
    assert(groupsInitial != null);
    assert(groupUpdated != null);
    assert(groupsUpdated != null);
    assert(attendanceUpdated != null);
    return groupsInitial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult groupsInitial(),
    TResult groupUpdated(Group group, bool isUpdated),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    TResult attendanceUpdated(List<Attendance> attendance, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (groupsInitial != null) {
      return groupsInitial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult groupsInitial(GroupsInitial value),
    @required TResult groupUpdated(GroupUpdated value),
    @required TResult groupsUpdated(GroupsUpdated value),
    @required TResult attendanceUpdated(AttendanceUpdated value),
  }) {
    assert(groupsInitial != null);
    assert(groupUpdated != null);
    assert(groupsUpdated != null);
    assert(attendanceUpdated != null);
    return groupsInitial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult groupsInitial(GroupsInitial value),
    TResult groupUpdated(GroupUpdated value),
    TResult groupsUpdated(GroupsUpdated value),
    TResult attendanceUpdated(AttendanceUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (groupsInitial != null) {
      return groupsInitial(this);
    }
    return orElse();
  }
}

abstract class GroupsInitial implements GroupsState {
  factory GroupsInitial() = _$GroupsInitial;
}

/// @nodoc
abstract class $GroupUpdatedCopyWith<$Res> {
  factory $GroupUpdatedCopyWith(
          GroupUpdated value, $Res Function(GroupUpdated) then) =
      _$GroupUpdatedCopyWithImpl<$Res>;
  $Res call({Group group, bool isUpdated});
}

/// @nodoc
class _$GroupUpdatedCopyWithImpl<$Res> extends _$GroupsStateCopyWithImpl<$Res>
    implements $GroupUpdatedCopyWith<$Res> {
  _$GroupUpdatedCopyWithImpl(
      GroupUpdated _value, $Res Function(GroupUpdated) _then)
      : super(_value, (v) => _then(v as GroupUpdated));

  @override
  GroupUpdated get _value => super._value as GroupUpdated;

  @override
  $Res call({
    Object group = freezed,
    Object isUpdated = freezed,
  }) {
    return _then(GroupUpdated(
      group: group == freezed ? _value.group : group as Group,
      isUpdated: isUpdated == freezed ? _value.isUpdated : isUpdated as bool,
    ));
  }
}

/// @nodoc
class _$GroupUpdated implements GroupUpdated {
  _$GroupUpdated({@required this.group, @required this.isUpdated})
      : assert(group != null),
        assert(isUpdated != null);

  @override
  final Group group;
  @override
  final bool isUpdated;

  @override
  String toString() {
    return 'GroupsState.groupUpdated(group: $group, isUpdated: $isUpdated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is GroupUpdated &&
            (identical(other.group, group) ||
                const DeepCollectionEquality().equals(other.group, group)) &&
            (identical(other.isUpdated, isUpdated) ||
                const DeepCollectionEquality()
                    .equals(other.isUpdated, isUpdated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(group) ^
      const DeepCollectionEquality().hash(isUpdated);

  @JsonKey(ignore: true)
  @override
  $GroupUpdatedCopyWith<GroupUpdated> get copyWith =>
      _$GroupUpdatedCopyWithImpl<GroupUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult groupsInitial(),
    @required TResult groupUpdated(Group group, bool isUpdated),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required
        TResult attendanceUpdated(List<Attendance> attendance, bool isUpdated),
  }) {
    assert(groupsInitial != null);
    assert(groupUpdated != null);
    assert(groupsUpdated != null);
    assert(attendanceUpdated != null);
    return groupUpdated(group, isUpdated);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult groupsInitial(),
    TResult groupUpdated(Group group, bool isUpdated),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    TResult attendanceUpdated(List<Attendance> attendance, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (groupUpdated != null) {
      return groupUpdated(group, isUpdated);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult groupsInitial(GroupsInitial value),
    @required TResult groupUpdated(GroupUpdated value),
    @required TResult groupsUpdated(GroupsUpdated value),
    @required TResult attendanceUpdated(AttendanceUpdated value),
  }) {
    assert(groupsInitial != null);
    assert(groupUpdated != null);
    assert(groupsUpdated != null);
    assert(attendanceUpdated != null);
    return groupUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult groupsInitial(GroupsInitial value),
    TResult groupUpdated(GroupUpdated value),
    TResult groupsUpdated(GroupsUpdated value),
    TResult attendanceUpdated(AttendanceUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (groupUpdated != null) {
      return groupUpdated(this);
    }
    return orElse();
  }
}

abstract class GroupUpdated implements GroupsState {
  factory GroupUpdated({@required Group group, @required bool isUpdated}) =
      _$GroupUpdated;

  Group get group;
  bool get isUpdated;
  @JsonKey(ignore: true)
  $GroupUpdatedCopyWith<GroupUpdated> get copyWith;
}

/// @nodoc
abstract class $GroupsUpdatedCopyWith<$Res> {
  factory $GroupsUpdatedCopyWith(
          GroupsUpdated value, $Res Function(GroupsUpdated) then) =
      _$GroupsUpdatedCopyWithImpl<$Res>;
  $Res call({List<Group> groups, bool isUpdated});
}

/// @nodoc
class _$GroupsUpdatedCopyWithImpl<$Res> extends _$GroupsStateCopyWithImpl<$Res>
    implements $GroupsUpdatedCopyWith<$Res> {
  _$GroupsUpdatedCopyWithImpl(
      GroupsUpdated _value, $Res Function(GroupsUpdated) _then)
      : super(_value, (v) => _then(v as GroupsUpdated));

  @override
  GroupsUpdated get _value => super._value as GroupsUpdated;

  @override
  $Res call({
    Object groups = freezed,
    Object isUpdated = freezed,
  }) {
    return _then(GroupsUpdated(
      groups: groups == freezed ? _value.groups : groups as List<Group>,
      isUpdated: isUpdated == freezed ? _value.isUpdated : isUpdated as bool,
    ));
  }
}

/// @nodoc
class _$GroupsUpdated implements GroupsUpdated {
  _$GroupsUpdated({@required this.groups, @required this.isUpdated})
      : assert(groups != null),
        assert(isUpdated != null);

  @override
  final List<Group> groups;
  @override
  final bool isUpdated;

  @override
  String toString() {
    return 'GroupsState.groupsUpdated(groups: $groups, isUpdated: $isUpdated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is GroupsUpdated &&
            (identical(other.groups, groups) ||
                const DeepCollectionEquality().equals(other.groups, groups)) &&
            (identical(other.isUpdated, isUpdated) ||
                const DeepCollectionEquality()
                    .equals(other.isUpdated, isUpdated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(groups) ^
      const DeepCollectionEquality().hash(isUpdated);

  @JsonKey(ignore: true)
  @override
  $GroupsUpdatedCopyWith<GroupsUpdated> get copyWith =>
      _$GroupsUpdatedCopyWithImpl<GroupsUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult groupsInitial(),
    @required TResult groupUpdated(Group group, bool isUpdated),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required
        TResult attendanceUpdated(List<Attendance> attendance, bool isUpdated),
  }) {
    assert(groupsInitial != null);
    assert(groupUpdated != null);
    assert(groupsUpdated != null);
    assert(attendanceUpdated != null);
    return groupsUpdated(groups, isUpdated);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult groupsInitial(),
    TResult groupUpdated(Group group, bool isUpdated),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    TResult attendanceUpdated(List<Attendance> attendance, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (groupsUpdated != null) {
      return groupsUpdated(groups, isUpdated);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult groupsInitial(GroupsInitial value),
    @required TResult groupUpdated(GroupUpdated value),
    @required TResult groupsUpdated(GroupsUpdated value),
    @required TResult attendanceUpdated(AttendanceUpdated value),
  }) {
    assert(groupsInitial != null);
    assert(groupUpdated != null);
    assert(groupsUpdated != null);
    assert(attendanceUpdated != null);
    return groupsUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult groupsInitial(GroupsInitial value),
    TResult groupUpdated(GroupUpdated value),
    TResult groupsUpdated(GroupsUpdated value),
    TResult attendanceUpdated(AttendanceUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (groupsUpdated != null) {
      return groupsUpdated(this);
    }
    return orElse();
  }
}

abstract class GroupsUpdated implements GroupsState {
  factory GroupsUpdated(
      {@required List<Group> groups,
      @required bool isUpdated}) = _$GroupsUpdated;

  List<Group> get groups;
  bool get isUpdated;
  @JsonKey(ignore: true)
  $GroupsUpdatedCopyWith<GroupsUpdated> get copyWith;
}

/// @nodoc
abstract class $AttendanceUpdatedCopyWith<$Res> {
  factory $AttendanceUpdatedCopyWith(
          AttendanceUpdated value, $Res Function(AttendanceUpdated) then) =
      _$AttendanceUpdatedCopyWithImpl<$Res>;
  $Res call({List<Attendance> attendance, bool isUpdated});
}

/// @nodoc
class _$AttendanceUpdatedCopyWithImpl<$Res>
    extends _$GroupsStateCopyWithImpl<$Res>
    implements $AttendanceUpdatedCopyWith<$Res> {
  _$AttendanceUpdatedCopyWithImpl(
      AttendanceUpdated _value, $Res Function(AttendanceUpdated) _then)
      : super(_value, (v) => _then(v as AttendanceUpdated));

  @override
  AttendanceUpdated get _value => super._value as AttendanceUpdated;

  @override
  $Res call({
    Object attendance = freezed,
    Object isUpdated = freezed,
  }) {
    return _then(AttendanceUpdated(
      attendance: attendance == freezed
          ? _value.attendance
          : attendance as List<Attendance>,
      isUpdated: isUpdated == freezed ? _value.isUpdated : isUpdated as bool,
    ));
  }
}

/// @nodoc
class _$AttendanceUpdated implements AttendanceUpdated {
  _$AttendanceUpdated({@required this.attendance, @required this.isUpdated})
      : assert(attendance != null),
        assert(isUpdated != null);

  @override
  final List<Attendance> attendance;
  @override
  final bool isUpdated;

  @override
  String toString() {
    return 'GroupsState.attendanceUpdated(attendance: $attendance, isUpdated: $isUpdated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AttendanceUpdated &&
            (identical(other.attendance, attendance) ||
                const DeepCollectionEquality()
                    .equals(other.attendance, attendance)) &&
            (identical(other.isUpdated, isUpdated) ||
                const DeepCollectionEquality()
                    .equals(other.isUpdated, isUpdated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(attendance) ^
      const DeepCollectionEquality().hash(isUpdated);

  @JsonKey(ignore: true)
  @override
  $AttendanceUpdatedCopyWith<AttendanceUpdated> get copyWith =>
      _$AttendanceUpdatedCopyWithImpl<AttendanceUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult groupsInitial(),
    @required TResult groupUpdated(Group group, bool isUpdated),
    @required TResult groupsUpdated(List<Group> groups, bool isUpdated),
    @required
        TResult attendanceUpdated(List<Attendance> attendance, bool isUpdated),
  }) {
    assert(groupsInitial != null);
    assert(groupUpdated != null);
    assert(groupsUpdated != null);
    assert(attendanceUpdated != null);
    return attendanceUpdated(attendance, isUpdated);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult groupsInitial(),
    TResult groupUpdated(Group group, bool isUpdated),
    TResult groupsUpdated(List<Group> groups, bool isUpdated),
    TResult attendanceUpdated(List<Attendance> attendance, bool isUpdated),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (attendanceUpdated != null) {
      return attendanceUpdated(attendance, isUpdated);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult groupsInitial(GroupsInitial value),
    @required TResult groupUpdated(GroupUpdated value),
    @required TResult groupsUpdated(GroupsUpdated value),
    @required TResult attendanceUpdated(AttendanceUpdated value),
  }) {
    assert(groupsInitial != null);
    assert(groupUpdated != null);
    assert(groupsUpdated != null);
    assert(attendanceUpdated != null);
    return attendanceUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult groupsInitial(GroupsInitial value),
    TResult groupUpdated(GroupUpdated value),
    TResult groupsUpdated(GroupsUpdated value),
    TResult attendanceUpdated(AttendanceUpdated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (attendanceUpdated != null) {
      return attendanceUpdated(this);
    }
    return orElse();
  }
}

abstract class AttendanceUpdated implements GroupsState {
  factory AttendanceUpdated(
      {@required List<Attendance> attendance,
      @required bool isUpdated}) = _$AttendanceUpdated;

  List<Attendance> get attendance;
  bool get isUpdated;
  @JsonKey(ignore: true)
  $AttendanceUpdatedCopyWith<AttendanceUpdated> get copyWith;
}
