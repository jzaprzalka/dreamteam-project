part of 'groups_bloc.dart';

@freezed
abstract class GroupsEvent with _$GroupsEvent {
  const factory GroupsEvent.loadedAttendance({@required String groupId}) =
      LoadedAttendance;
  const factory GroupsEvent.loadedGroups() = LoadedGroups;
  const factory GroupsEvent.loadedGroup({@required String groupId}) =
      LoadedGroup;
}
