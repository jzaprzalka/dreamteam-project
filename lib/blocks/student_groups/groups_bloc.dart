import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/Attendance.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/repositories/groups_data_repository.dart';
import 'package:dreamteam_project/repositories/user_data_repository.dart';
// import 'package:dreamteam_project/providers/schedule_provider.dart';

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';

part 'groups_event.dart';
part 'groups_state.dart';

part 'groups_bloc.freezed.dart';

class GroupsBloc extends Bloc<GroupsEvent, GroupsState> {
  GroupsDataRepository groupsDataRepository = GroupsDataRepository();
  UserDataRepository userDataRepository = UserDataRepository();

  GroupsBloc() : super(GroupsState.groupsInitial());

  Future<Group> _getGroup(String groupId) async {
    Group group;
    try {
      logger.i("Loading students in group $groupId");
      group = await groupsDataRepository.getGroup(groupId);
      if (group != null && group.participants == null)
        group.participants = List.empty(growable: true);
      for (String studentId in group?.participantIds ?? []) {
        try {
          group.participants.add(await userDataRepository.getUser(studentId));
        } catch (e) {
          logger.e("Error loading students to group: $e");
        }
      }
      for (String coachId in group?.coachIds ?? []) {
        try {
          group.participants.add(await userDataRepository.getUser(coachId));
        } catch (e) {
          logger.e("Error loading coaches to group: $e");
        }
      }
    } catch (e) {
      logger.e("Exception thrown when loading group [$groupId]: $e");
      return null;
    }
    return group;
  }

  Future<List<Group>> _getGroups() async {
    List<Group> groups;
    try {
      logger.i("Loading student groups");
      groups = await groupsDataRepository.getStudentGroups();
    } catch (e) {
      logger.e("Exception thrown when loading groups List: $e");
      return List.empty();
    }
    return groups ?? List.empty();
  }

  Future<List<Attendance>> _getAttendance(String groupId) async {
    List<Attendance> entries = [];
    try {
      logger.i("Loading current student's attandance in group $groupId");
      // TODO: Add Attendance history loading for current User
    } catch (e) {
      logger.e(
          "Exception thrown when loading student's attendance for group [$groupId]: $e");
      return null;
    }
    return entries;
  }

  @override
  Stream<GroupsState> mapEventToState(GroupsEvent event) async* {
    try {
      yield* event.map(
        loadedAttendance: (e) async* {
          yield GroupsState.attendanceUpdated(
              attendance: await _getAttendance(e.groupId), isUpdated: true);
        },
        loadedGroups: (e) async* {
          yield GroupsState.groupsUpdated(
              groups: await _getGroups(), isUpdated: true);
        },
        loadedGroup: (e) async* {
          yield GroupsState.groupUpdated(
              group: await _getGroup(e.groupId), isUpdated: true);
        },
      );
    } catch (e) {
      logger.e("Exception thrown mapping Group Event to Group State: $e");
    }
  }
}
