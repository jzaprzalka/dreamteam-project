part of 'authentication_bloc.dart';

@freezed
abstract class AuthenticationState with _$AuthenticationState {
  factory AuthenticationState.authenticated({@required User user}) =
      Authenticated;
  factory AuthenticationState.unAuthenticated(
      {@required bool isRegister, @required bool isLoading, BaseDreamTeamException exception}) = UnAuthenticated;
  factory AuthenticationState.initial() =>
      AuthenticationState.unAuthenticated(isRegister: false, isLoading: true);
}