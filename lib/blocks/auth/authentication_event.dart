part of 'authentication_bloc.dart';

@freezed
abstract class AuthenticationEvent with _$AuthenticationEvent {
  const factory AuthenticationEvent.authCheckRequested() = AuthCheckRequested;
  const factory AuthenticationEvent.signedOut() = SignedOut;
  const factory AuthenticationEvent.logIn(
      {@required String email, @required String password}) = LoginIn;
  const factory AuthenticationEvent.receivedUserData(User user) = ReceivedUserData;
}
