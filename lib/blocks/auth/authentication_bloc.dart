import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dreamteam_project/exception/core/base_exception.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/repositories/token_repository.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase;
import 'package:dreamteam_project/repositories/authentication_repository.dart';
import 'package:dreamteam_project/repositories/user_data_repository.dart';

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';

import '../../main.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

part 'authentication_bloc.freezed.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationRepository authenticationRepository =
      AuthenticationRepository();
  UserDataRepository userDataRepository = UserDataRepository();
  final TokenRepository tokenRepository = TokenRepository();
  StreamSubscription userSubscription;
  StreamSubscription userTokensSubscription;

  AuthenticationBloc() : super(AuthenticationState.initial());

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    yield* event.map(authCheckRequested: (e) async* {
      final isLoggedIn = await authenticationRepository.isLoggedIn();
      if (isLoggedIn) {
        yield AuthenticationState.unAuthenticated(
            isRegister: false, isLoading: true);
        firebase.User firebaseUser =
            await authenticationRepository.getCurrentUser();
        userSubscription?.cancel();
        userSubscription = userDataRepository
            .getUserStream(firebaseUser.uid)
            .listen((user) async => {
                  logger.i('dispatching $user'),
                  add(AuthenticationEvent.receivedUserData(user)),
                  if (userTokensSubscription == null)
                    {
                      userTokensSubscription = tokenRepository
                          .getTokensForUserStream(user)
                          .listen((userTokens) async {
                        logger.i('dispatching $userTokens');
                        user.userTokens = userTokens;
                        add(AuthenticationEvent.receivedUserData(user));
                      })
                    }
                });

        // userTokensSubscription = tokenRepository
        //     .getTokensForUserStream(
        //         state.when(authenticated: (u) => u, unAuthenticated: null))
        //     .listen((userTokens) async {
        //   logger.i('dispatching $userTokens');
        //   if (state is Authenticated) {
        //     add(AuthenticationEvent.receivedUserData(state.when(
        //         authenticated: (u) {
        //           u.userTokens = userTokens;
        //           return u;
        //         },
        //         unAuthenticated: null)));
        //   }
        // });

        // User user = await userDataRepository.getUser(firebaseUser.uid);
        // yield AuthenticationState.authenticated(user: user);
      } else {
        yield AuthenticationState.unAuthenticated(
            isRegister: false, isLoading: false);
      }
    }, signedOut: (e) async* {
      await authenticationRepository.signOutUser();
      userSubscription?.cancel();
      userTokensSubscription?.cancel();
      yield AuthenticationState.unAuthenticated(
          isRegister: false, isLoading: false);
    }, logIn: (e) async* {
      try {
        await authenticationRepository.signInUser(e.email, e.password);
        add(AuthenticationEvent.authCheckRequested());
      } on BaseDreamTeamException catch (e) {
        logger.e("Exception when trying to log in: ${e.message}");
        yield AuthenticationState.unAuthenticated(
            isRegister: false, isLoading: false, exception: e);
      }
    }, receivedUserData: (e) async* {
      yield AuthenticationState.authenticated(user: e.user);
    });
  }
}
