// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'authentication_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$AuthenticationEventTearOff {
  const _$AuthenticationEventTearOff();

// ignore: unused_element
  AuthCheckRequested authCheckRequested() {
    return const AuthCheckRequested();
  }

// ignore: unused_element
  SignedOut signedOut() {
    return const SignedOut();
  }

// ignore: unused_element
  LoginIn logIn({@required String email, @required String password}) {
    return LoginIn(
      email: email,
      password: password,
    );
  }

// ignore: unused_element
  ReceivedUserData receivedUserData(User user) {
    return ReceivedUserData(
      user,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $AuthenticationEvent = _$AuthenticationEventTearOff();

/// @nodoc
mixin _$AuthenticationEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult authCheckRequested(),
    @required TResult signedOut(),
    @required TResult logIn(String email, String password),
    @required TResult receivedUserData(User user),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult authCheckRequested(),
    TResult signedOut(),
    TResult logIn(String email, String password),
    TResult receivedUserData(User user),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult authCheckRequested(AuthCheckRequested value),
    @required TResult signedOut(SignedOut value),
    @required TResult logIn(LoginIn value),
    @required TResult receivedUserData(ReceivedUserData value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult authCheckRequested(AuthCheckRequested value),
    TResult signedOut(SignedOut value),
    TResult logIn(LoginIn value),
    TResult receivedUserData(ReceivedUserData value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $AuthenticationEventCopyWith<$Res> {
  factory $AuthenticationEventCopyWith(
          AuthenticationEvent value, $Res Function(AuthenticationEvent) then) =
      _$AuthenticationEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$AuthenticationEventCopyWithImpl<$Res>
    implements $AuthenticationEventCopyWith<$Res> {
  _$AuthenticationEventCopyWithImpl(this._value, this._then);

  final AuthenticationEvent _value;
  // ignore: unused_field
  final $Res Function(AuthenticationEvent) _then;
}

/// @nodoc
abstract class $AuthCheckRequestedCopyWith<$Res> {
  factory $AuthCheckRequestedCopyWith(
          AuthCheckRequested value, $Res Function(AuthCheckRequested) then) =
      _$AuthCheckRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class _$AuthCheckRequestedCopyWithImpl<$Res>
    extends _$AuthenticationEventCopyWithImpl<$Res>
    implements $AuthCheckRequestedCopyWith<$Res> {
  _$AuthCheckRequestedCopyWithImpl(
      AuthCheckRequested _value, $Res Function(AuthCheckRequested) _then)
      : super(_value, (v) => _then(v as AuthCheckRequested));

  @override
  AuthCheckRequested get _value => super._value as AuthCheckRequested;
}

/// @nodoc
class _$AuthCheckRequested implements AuthCheckRequested {
  const _$AuthCheckRequested();

  @override
  String toString() {
    return 'AuthenticationEvent.authCheckRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is AuthCheckRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult authCheckRequested(),
    @required TResult signedOut(),
    @required TResult logIn(String email, String password),
    @required TResult receivedUserData(User user),
  }) {
    assert(authCheckRequested != null);
    assert(signedOut != null);
    assert(logIn != null);
    assert(receivedUserData != null);
    return authCheckRequested();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult authCheckRequested(),
    TResult signedOut(),
    TResult logIn(String email, String password),
    TResult receivedUserData(User user),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (authCheckRequested != null) {
      return authCheckRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult authCheckRequested(AuthCheckRequested value),
    @required TResult signedOut(SignedOut value),
    @required TResult logIn(LoginIn value),
    @required TResult receivedUserData(ReceivedUserData value),
  }) {
    assert(authCheckRequested != null);
    assert(signedOut != null);
    assert(logIn != null);
    assert(receivedUserData != null);
    return authCheckRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult authCheckRequested(AuthCheckRequested value),
    TResult signedOut(SignedOut value),
    TResult logIn(LoginIn value),
    TResult receivedUserData(ReceivedUserData value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (authCheckRequested != null) {
      return authCheckRequested(this);
    }
    return orElse();
  }
}

abstract class AuthCheckRequested implements AuthenticationEvent {
  const factory AuthCheckRequested() = _$AuthCheckRequested;
}

/// @nodoc
abstract class $SignedOutCopyWith<$Res> {
  factory $SignedOutCopyWith(SignedOut value, $Res Function(SignedOut) then) =
      _$SignedOutCopyWithImpl<$Res>;
}

/// @nodoc
class _$SignedOutCopyWithImpl<$Res>
    extends _$AuthenticationEventCopyWithImpl<$Res>
    implements $SignedOutCopyWith<$Res> {
  _$SignedOutCopyWithImpl(SignedOut _value, $Res Function(SignedOut) _then)
      : super(_value, (v) => _then(v as SignedOut));

  @override
  SignedOut get _value => super._value as SignedOut;
}

/// @nodoc
class _$SignedOut implements SignedOut {
  const _$SignedOut();

  @override
  String toString() {
    return 'AuthenticationEvent.signedOut()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is SignedOut);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult authCheckRequested(),
    @required TResult signedOut(),
    @required TResult logIn(String email, String password),
    @required TResult receivedUserData(User user),
  }) {
    assert(authCheckRequested != null);
    assert(signedOut != null);
    assert(logIn != null);
    assert(receivedUserData != null);
    return signedOut();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult authCheckRequested(),
    TResult signedOut(),
    TResult logIn(String email, String password),
    TResult receivedUserData(User user),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (signedOut != null) {
      return signedOut();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult authCheckRequested(AuthCheckRequested value),
    @required TResult signedOut(SignedOut value),
    @required TResult logIn(LoginIn value),
    @required TResult receivedUserData(ReceivedUserData value),
  }) {
    assert(authCheckRequested != null);
    assert(signedOut != null);
    assert(logIn != null);
    assert(receivedUserData != null);
    return signedOut(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult authCheckRequested(AuthCheckRequested value),
    TResult signedOut(SignedOut value),
    TResult logIn(LoginIn value),
    TResult receivedUserData(ReceivedUserData value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (signedOut != null) {
      return signedOut(this);
    }
    return orElse();
  }
}

abstract class SignedOut implements AuthenticationEvent {
  const factory SignedOut() = _$SignedOut;
}

/// @nodoc
abstract class $LoginInCopyWith<$Res> {
  factory $LoginInCopyWith(LoginIn value, $Res Function(LoginIn) then) =
      _$LoginInCopyWithImpl<$Res>;
  $Res call({String email, String password});
}

/// @nodoc
class _$LoginInCopyWithImpl<$Res>
    extends _$AuthenticationEventCopyWithImpl<$Res>
    implements $LoginInCopyWith<$Res> {
  _$LoginInCopyWithImpl(LoginIn _value, $Res Function(LoginIn) _then)
      : super(_value, (v) => _then(v as LoginIn));

  @override
  LoginIn get _value => super._value as LoginIn;

  @override
  $Res call({
    Object email = freezed,
    Object password = freezed,
  }) {
    return _then(LoginIn(
      email: email == freezed ? _value.email : email as String,
      password: password == freezed ? _value.password : password as String,
    ));
  }
}

/// @nodoc
class _$LoginIn implements LoginIn {
  const _$LoginIn({@required this.email, @required this.password})
      : assert(email != null),
        assert(password != null);

  @override
  final String email;
  @override
  final String password;

  @override
  String toString() {
    return 'AuthenticationEvent.logIn(email: $email, password: $password)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoginIn &&
            (identical(other.email, email) ||
                const DeepCollectionEquality().equals(other.email, email)) &&
            (identical(other.password, password) ||
                const DeepCollectionEquality()
                    .equals(other.password, password)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(email) ^
      const DeepCollectionEquality().hash(password);

  @JsonKey(ignore: true)
  @override
  $LoginInCopyWith<LoginIn> get copyWith =>
      _$LoginInCopyWithImpl<LoginIn>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult authCheckRequested(),
    @required TResult signedOut(),
    @required TResult logIn(String email, String password),
    @required TResult receivedUserData(User user),
  }) {
    assert(authCheckRequested != null);
    assert(signedOut != null);
    assert(logIn != null);
    assert(receivedUserData != null);
    return logIn(email, password);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult authCheckRequested(),
    TResult signedOut(),
    TResult logIn(String email, String password),
    TResult receivedUserData(User user),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (logIn != null) {
      return logIn(email, password);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult authCheckRequested(AuthCheckRequested value),
    @required TResult signedOut(SignedOut value),
    @required TResult logIn(LoginIn value),
    @required TResult receivedUserData(ReceivedUserData value),
  }) {
    assert(authCheckRequested != null);
    assert(signedOut != null);
    assert(logIn != null);
    assert(receivedUserData != null);
    return logIn(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult authCheckRequested(AuthCheckRequested value),
    TResult signedOut(SignedOut value),
    TResult logIn(LoginIn value),
    TResult receivedUserData(ReceivedUserData value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (logIn != null) {
      return logIn(this);
    }
    return orElse();
  }
}

abstract class LoginIn implements AuthenticationEvent {
  const factory LoginIn({@required String email, @required String password}) =
      _$LoginIn;

  String get email;
  String get password;
  @JsonKey(ignore: true)
  $LoginInCopyWith<LoginIn> get copyWith;
}

/// @nodoc
abstract class $ReceivedUserDataCopyWith<$Res> {
  factory $ReceivedUserDataCopyWith(
          ReceivedUserData value, $Res Function(ReceivedUserData) then) =
      _$ReceivedUserDataCopyWithImpl<$Res>;
  $Res call({User user});
}

/// @nodoc
class _$ReceivedUserDataCopyWithImpl<$Res>
    extends _$AuthenticationEventCopyWithImpl<$Res>
    implements $ReceivedUserDataCopyWith<$Res> {
  _$ReceivedUserDataCopyWithImpl(
      ReceivedUserData _value, $Res Function(ReceivedUserData) _then)
      : super(_value, (v) => _then(v as ReceivedUserData));

  @override
  ReceivedUserData get _value => super._value as ReceivedUserData;

  @override
  $Res call({
    Object user = freezed,
  }) {
    return _then(ReceivedUserData(
      user == freezed ? _value.user : user as User,
    ));
  }
}

/// @nodoc
class _$ReceivedUserData implements ReceivedUserData {
  const _$ReceivedUserData(this.user) : assert(user != null);

  @override
  final User user;

  @override
  String toString() {
    return 'AuthenticationEvent.receivedUserData(user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ReceivedUserData &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(user);

  @JsonKey(ignore: true)
  @override
  $ReceivedUserDataCopyWith<ReceivedUserData> get copyWith =>
      _$ReceivedUserDataCopyWithImpl<ReceivedUserData>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult authCheckRequested(),
    @required TResult signedOut(),
    @required TResult logIn(String email, String password),
    @required TResult receivedUserData(User user),
  }) {
    assert(authCheckRequested != null);
    assert(signedOut != null);
    assert(logIn != null);
    assert(receivedUserData != null);
    return receivedUserData(user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult authCheckRequested(),
    TResult signedOut(),
    TResult logIn(String email, String password),
    TResult receivedUserData(User user),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (receivedUserData != null) {
      return receivedUserData(user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult authCheckRequested(AuthCheckRequested value),
    @required TResult signedOut(SignedOut value),
    @required TResult logIn(LoginIn value),
    @required TResult receivedUserData(ReceivedUserData value),
  }) {
    assert(authCheckRequested != null);
    assert(signedOut != null);
    assert(logIn != null);
    assert(receivedUserData != null);
    return receivedUserData(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult authCheckRequested(AuthCheckRequested value),
    TResult signedOut(SignedOut value),
    TResult logIn(LoginIn value),
    TResult receivedUserData(ReceivedUserData value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (receivedUserData != null) {
      return receivedUserData(this);
    }
    return orElse();
  }
}

abstract class ReceivedUserData implements AuthenticationEvent {
  const factory ReceivedUserData(User user) = _$ReceivedUserData;

  User get user;
  @JsonKey(ignore: true)
  $ReceivedUserDataCopyWith<ReceivedUserData> get copyWith;
}

/// @nodoc
class _$AuthenticationStateTearOff {
  const _$AuthenticationStateTearOff();

// ignore: unused_element
  Authenticated authenticated({@required User user}) {
    return Authenticated(
      user: user,
    );
  }

// ignore: unused_element
  UnAuthenticated unAuthenticated(
      {@required bool isRegister,
      @required bool isLoading,
      BaseDreamTeamException exception}) {
    return UnAuthenticated(
      isRegister: isRegister,
      isLoading: isLoading,
      exception: exception,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $AuthenticationState = _$AuthenticationStateTearOff();

/// @nodoc
mixin _$AuthenticationState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult authenticated(User user),
    @required
        TResult unAuthenticated(
            bool isRegister, bool isLoading, BaseDreamTeamException exception),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult authenticated(User user),
    TResult unAuthenticated(
        bool isRegister, bool isLoading, BaseDreamTeamException exception),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult authenticated(Authenticated value),
    @required TResult unAuthenticated(UnAuthenticated value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult authenticated(Authenticated value),
    TResult unAuthenticated(UnAuthenticated value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $AuthenticationStateCopyWith<$Res> {
  factory $AuthenticationStateCopyWith(
          AuthenticationState value, $Res Function(AuthenticationState) then) =
      _$AuthenticationStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$AuthenticationStateCopyWithImpl<$Res>
    implements $AuthenticationStateCopyWith<$Res> {
  _$AuthenticationStateCopyWithImpl(this._value, this._then);

  final AuthenticationState _value;
  // ignore: unused_field
  final $Res Function(AuthenticationState) _then;
}

/// @nodoc
abstract class $AuthenticatedCopyWith<$Res> {
  factory $AuthenticatedCopyWith(
          Authenticated value, $Res Function(Authenticated) then) =
      _$AuthenticatedCopyWithImpl<$Res>;
  $Res call({User user});
}

/// @nodoc
class _$AuthenticatedCopyWithImpl<$Res>
    extends _$AuthenticationStateCopyWithImpl<$Res>
    implements $AuthenticatedCopyWith<$Res> {
  _$AuthenticatedCopyWithImpl(
      Authenticated _value, $Res Function(Authenticated) _then)
      : super(_value, (v) => _then(v as Authenticated));

  @override
  Authenticated get _value => super._value as Authenticated;

  @override
  $Res call({
    Object user = freezed,
  }) {
    return _then(Authenticated(
      user: user == freezed ? _value.user : user as User,
    ));
  }
}

/// @nodoc
class _$Authenticated implements Authenticated {
  _$Authenticated({@required this.user}) : assert(user != null);

  @override
  final User user;

  @override
  String toString() {
    return 'AuthenticationState.authenticated(user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is Authenticated &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(user);

  @JsonKey(ignore: true)
  @override
  $AuthenticatedCopyWith<Authenticated> get copyWith =>
      _$AuthenticatedCopyWithImpl<Authenticated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult authenticated(User user),
    @required
        TResult unAuthenticated(
            bool isRegister, bool isLoading, BaseDreamTeamException exception),
  }) {
    assert(authenticated != null);
    assert(unAuthenticated != null);
    return authenticated(user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult authenticated(User user),
    TResult unAuthenticated(
        bool isRegister, bool isLoading, BaseDreamTeamException exception),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (authenticated != null) {
      return authenticated(user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult authenticated(Authenticated value),
    @required TResult unAuthenticated(UnAuthenticated value),
  }) {
    assert(authenticated != null);
    assert(unAuthenticated != null);
    return authenticated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult authenticated(Authenticated value),
    TResult unAuthenticated(UnAuthenticated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (authenticated != null) {
      return authenticated(this);
    }
    return orElse();
  }
}

abstract class Authenticated implements AuthenticationState {
  factory Authenticated({@required User user}) = _$Authenticated;

  User get user;
  @JsonKey(ignore: true)
  $AuthenticatedCopyWith<Authenticated> get copyWith;
}

/// @nodoc
abstract class $UnAuthenticatedCopyWith<$Res> {
  factory $UnAuthenticatedCopyWith(
          UnAuthenticated value, $Res Function(UnAuthenticated) then) =
      _$UnAuthenticatedCopyWithImpl<$Res>;
  $Res call(
      {bool isRegister, bool isLoading, BaseDreamTeamException exception});
}

/// @nodoc
class _$UnAuthenticatedCopyWithImpl<$Res>
    extends _$AuthenticationStateCopyWithImpl<$Res>
    implements $UnAuthenticatedCopyWith<$Res> {
  _$UnAuthenticatedCopyWithImpl(
      UnAuthenticated _value, $Res Function(UnAuthenticated) _then)
      : super(_value, (v) => _then(v as UnAuthenticated));

  @override
  UnAuthenticated get _value => super._value as UnAuthenticated;

  @override
  $Res call({
    Object isRegister = freezed,
    Object isLoading = freezed,
    Object exception = freezed,
  }) {
    return _then(UnAuthenticated(
      isRegister:
          isRegister == freezed ? _value.isRegister : isRegister as bool,
      isLoading: isLoading == freezed ? _value.isLoading : isLoading as bool,
      exception: exception == freezed
          ? _value.exception
          : exception as BaseDreamTeamException,
    ));
  }
}

/// @nodoc
class _$UnAuthenticated implements UnAuthenticated {
  _$UnAuthenticated(
      {@required this.isRegister, @required this.isLoading, this.exception})
      : assert(isRegister != null),
        assert(isLoading != null);

  @override
  final bool isRegister;
  @override
  final bool isLoading;
  @override
  final BaseDreamTeamException exception;

  @override
  String toString() {
    return 'AuthenticationState.unAuthenticated(isRegister: $isRegister, isLoading: $isLoading, exception: $exception)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UnAuthenticated &&
            (identical(other.isRegister, isRegister) ||
                const DeepCollectionEquality()
                    .equals(other.isRegister, isRegister)) &&
            (identical(other.isLoading, isLoading) ||
                const DeepCollectionEquality()
                    .equals(other.isLoading, isLoading)) &&
            (identical(other.exception, exception) ||
                const DeepCollectionEquality()
                    .equals(other.exception, exception)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(isRegister) ^
      const DeepCollectionEquality().hash(isLoading) ^
      const DeepCollectionEquality().hash(exception);

  @JsonKey(ignore: true)
  @override
  $UnAuthenticatedCopyWith<UnAuthenticated> get copyWith =>
      _$UnAuthenticatedCopyWithImpl<UnAuthenticated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult authenticated(User user),
    @required
        TResult unAuthenticated(
            bool isRegister, bool isLoading, BaseDreamTeamException exception),
  }) {
    assert(authenticated != null);
    assert(unAuthenticated != null);
    return unAuthenticated(isRegister, isLoading, exception);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult authenticated(User user),
    TResult unAuthenticated(
        bool isRegister, bool isLoading, BaseDreamTeamException exception),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (unAuthenticated != null) {
      return unAuthenticated(isRegister, isLoading, exception);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult authenticated(Authenticated value),
    @required TResult unAuthenticated(UnAuthenticated value),
  }) {
    assert(authenticated != null);
    assert(unAuthenticated != null);
    return unAuthenticated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult authenticated(Authenticated value),
    TResult unAuthenticated(UnAuthenticated value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (unAuthenticated != null) {
      return unAuthenticated(this);
    }
    return orElse();
  }
}

abstract class UnAuthenticated implements AuthenticationState {
  factory UnAuthenticated(
      {@required bool isRegister,
      @required bool isLoading,
      BaseDreamTeamException exception}) = _$UnAuthenticated;

  bool get isRegister;
  bool get isLoading;
  BaseDreamTeamException get exception;
  @JsonKey(ignore: true)
  $UnAuthenticatedCopyWith<UnAuthenticated> get copyWith;
}
