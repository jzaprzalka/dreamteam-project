part of 'token_bloc.dart';

@freezed
abstract class TokenState with _$TokenState {
  factory TokenState.tokenInitial() = TokenInitial;
  factory TokenState.tokensForUserUpdated(
      {@required User user,
      @required List<UserToken> tokens,
      @required bool isUpdated}) = UserTokenUpdated;
  factory TokenState.tokensUpdated(
      {@required List<Token> tokens, @required bool isUpdated}) = TokensUpdated;
  factory TokenState.veryficationSucceeded(
      {@required User user, @required Token token}) = VeryficationSucceeded;
  factory TokenState.veryficationFailed(
      {@required User user, @required Token token}) = VeryficationFailed;
}
