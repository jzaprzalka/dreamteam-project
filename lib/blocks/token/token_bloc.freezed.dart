// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'token_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$TokenEventTearOff {
  const _$TokenEventTearOff();

// ignore: unused_element
  AddedToken addedToken({@required String name}) {
    return AddedToken(
      name: name,
    );
  }

// ignore: unused_element
  AddedTokenToUser addedTokenToUser(
      {@required User user,
      @required Token token,
      @required DateTime expirationDate}) {
    return AddedTokenToUser(
      user: user,
      token: token,
      expirationDate: expirationDate,
    );
  }

// ignore: unused_element
  LoadedTokens loadedTokens() {
    return const LoadedTokens();
  }

// ignore: unused_element
  LoadedTokensForUser loadedTokensForUser(User user) {
    return LoadedTokensForUser(
      user,
    );
  }

// ignore: unused_element
  VeryfiedToken veryfiedToken({@required User user, @required Token token}) {
    return VeryfiedToken(
      user: user,
      token: token,
    );
  }

// ignore: unused_element
  ExtendedExpirationTime extendedExpirationTime(
      {@required UserToken userToken, Duration duration}) {
    return ExtendedExpirationTime(
      userToken: userToken,
      duration: duration,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $TokenEvent = _$TokenEventTearOff();

/// @nodoc
mixin _$TokenEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult addedToken(String name),
    @required
        TResult addedTokenToUser(
            User user, Token token, DateTime expirationDate),
    @required TResult loadedTokens(),
    @required TResult loadedTokensForUser(User user),
    @required TResult veryfiedToken(User user, Token token),
    @required
        TResult extendedExpirationTime(UserToken userToken, Duration duration),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult addedToken(String name),
    TResult addedTokenToUser(User user, Token token, DateTime expirationDate),
    TResult loadedTokens(),
    TResult loadedTokensForUser(User user),
    TResult veryfiedToken(User user, Token token),
    TResult extendedExpirationTime(UserToken userToken, Duration duration),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult addedToken(AddedToken value),
    @required TResult addedTokenToUser(AddedTokenToUser value),
    @required TResult loadedTokens(LoadedTokens value),
    @required TResult loadedTokensForUser(LoadedTokensForUser value),
    @required TResult veryfiedToken(VeryfiedToken value),
    @required TResult extendedExpirationTime(ExtendedExpirationTime value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult addedToken(AddedToken value),
    TResult addedTokenToUser(AddedTokenToUser value),
    TResult loadedTokens(LoadedTokens value),
    TResult loadedTokensForUser(LoadedTokensForUser value),
    TResult veryfiedToken(VeryfiedToken value),
    TResult extendedExpirationTime(ExtendedExpirationTime value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $TokenEventCopyWith<$Res> {
  factory $TokenEventCopyWith(
          TokenEvent value, $Res Function(TokenEvent) then) =
      _$TokenEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$TokenEventCopyWithImpl<$Res> implements $TokenEventCopyWith<$Res> {
  _$TokenEventCopyWithImpl(this._value, this._then);

  final TokenEvent _value;
  // ignore: unused_field
  final $Res Function(TokenEvent) _then;
}

/// @nodoc
abstract class $AddedTokenCopyWith<$Res> {
  factory $AddedTokenCopyWith(
          AddedToken value, $Res Function(AddedToken) then) =
      _$AddedTokenCopyWithImpl<$Res>;
  $Res call({String name});
}

/// @nodoc
class _$AddedTokenCopyWithImpl<$Res> extends _$TokenEventCopyWithImpl<$Res>
    implements $AddedTokenCopyWith<$Res> {
  _$AddedTokenCopyWithImpl(AddedToken _value, $Res Function(AddedToken) _then)
      : super(_value, (v) => _then(v as AddedToken));

  @override
  AddedToken get _value => super._value as AddedToken;

  @override
  $Res call({
    Object name = freezed,
  }) {
    return _then(AddedToken(
      name: name == freezed ? _value.name : name as String,
    ));
  }
}

/// @nodoc
class _$AddedToken implements AddedToken {
  const _$AddedToken({@required this.name}) : assert(name != null);

  @override
  final String name;

  @override
  String toString() {
    return 'TokenEvent.addedToken(name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AddedToken &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(name);

  @JsonKey(ignore: true)
  @override
  $AddedTokenCopyWith<AddedToken> get copyWith =>
      _$AddedTokenCopyWithImpl<AddedToken>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult addedToken(String name),
    @required
        TResult addedTokenToUser(
            User user, Token token, DateTime expirationDate),
    @required TResult loadedTokens(),
    @required TResult loadedTokensForUser(User user),
    @required TResult veryfiedToken(User user, Token token),
    @required
        TResult extendedExpirationTime(UserToken userToken, Duration duration),
  }) {
    assert(addedToken != null);
    assert(addedTokenToUser != null);
    assert(loadedTokens != null);
    assert(loadedTokensForUser != null);
    assert(veryfiedToken != null);
    assert(extendedExpirationTime != null);
    return addedToken(name);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult addedToken(String name),
    TResult addedTokenToUser(User user, Token token, DateTime expirationDate),
    TResult loadedTokens(),
    TResult loadedTokensForUser(User user),
    TResult veryfiedToken(User user, Token token),
    TResult extendedExpirationTime(UserToken userToken, Duration duration),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addedToken != null) {
      return addedToken(name);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult addedToken(AddedToken value),
    @required TResult addedTokenToUser(AddedTokenToUser value),
    @required TResult loadedTokens(LoadedTokens value),
    @required TResult loadedTokensForUser(LoadedTokensForUser value),
    @required TResult veryfiedToken(VeryfiedToken value),
    @required TResult extendedExpirationTime(ExtendedExpirationTime value),
  }) {
    assert(addedToken != null);
    assert(addedTokenToUser != null);
    assert(loadedTokens != null);
    assert(loadedTokensForUser != null);
    assert(veryfiedToken != null);
    assert(extendedExpirationTime != null);
    return addedToken(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult addedToken(AddedToken value),
    TResult addedTokenToUser(AddedTokenToUser value),
    TResult loadedTokens(LoadedTokens value),
    TResult loadedTokensForUser(LoadedTokensForUser value),
    TResult veryfiedToken(VeryfiedToken value),
    TResult extendedExpirationTime(ExtendedExpirationTime value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addedToken != null) {
      return addedToken(this);
    }
    return orElse();
  }
}

abstract class AddedToken implements TokenEvent {
  const factory AddedToken({@required String name}) = _$AddedToken;

  String get name;
  @JsonKey(ignore: true)
  $AddedTokenCopyWith<AddedToken> get copyWith;
}

/// @nodoc
abstract class $AddedTokenToUserCopyWith<$Res> {
  factory $AddedTokenToUserCopyWith(
          AddedTokenToUser value, $Res Function(AddedTokenToUser) then) =
      _$AddedTokenToUserCopyWithImpl<$Res>;
  $Res call({User user, Token token, DateTime expirationDate});
}

/// @nodoc
class _$AddedTokenToUserCopyWithImpl<$Res>
    extends _$TokenEventCopyWithImpl<$Res>
    implements $AddedTokenToUserCopyWith<$Res> {
  _$AddedTokenToUserCopyWithImpl(
      AddedTokenToUser _value, $Res Function(AddedTokenToUser) _then)
      : super(_value, (v) => _then(v as AddedTokenToUser));

  @override
  AddedTokenToUser get _value => super._value as AddedTokenToUser;

  @override
  $Res call({
    Object user = freezed,
    Object token = freezed,
    Object expirationDate = freezed,
  }) {
    return _then(AddedTokenToUser(
      user: user == freezed ? _value.user : user as User,
      token: token == freezed ? _value.token : token as Token,
      expirationDate: expirationDate == freezed
          ? _value.expirationDate
          : expirationDate as DateTime,
    ));
  }
}

/// @nodoc
class _$AddedTokenToUser implements AddedTokenToUser {
  const _$AddedTokenToUser(
      {@required this.user,
      @required this.token,
      @required this.expirationDate})
      : assert(user != null),
        assert(token != null),
        assert(expirationDate != null);

  @override
  final User user;
  @override
  final Token token;
  @override
  final DateTime expirationDate;

  @override
  String toString() {
    return 'TokenEvent.addedTokenToUser(user: $user, token: $token, expirationDate: $expirationDate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AddedTokenToUser &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)) &&
            (identical(other.token, token) ||
                const DeepCollectionEquality().equals(other.token, token)) &&
            (identical(other.expirationDate, expirationDate) ||
                const DeepCollectionEquality()
                    .equals(other.expirationDate, expirationDate)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(user) ^
      const DeepCollectionEquality().hash(token) ^
      const DeepCollectionEquality().hash(expirationDate);

  @JsonKey(ignore: true)
  @override
  $AddedTokenToUserCopyWith<AddedTokenToUser> get copyWith =>
      _$AddedTokenToUserCopyWithImpl<AddedTokenToUser>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult addedToken(String name),
    @required
        TResult addedTokenToUser(
            User user, Token token, DateTime expirationDate),
    @required TResult loadedTokens(),
    @required TResult loadedTokensForUser(User user),
    @required TResult veryfiedToken(User user, Token token),
    @required
        TResult extendedExpirationTime(UserToken userToken, Duration duration),
  }) {
    assert(addedToken != null);
    assert(addedTokenToUser != null);
    assert(loadedTokens != null);
    assert(loadedTokensForUser != null);
    assert(veryfiedToken != null);
    assert(extendedExpirationTime != null);
    return addedTokenToUser(user, token, expirationDate);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult addedToken(String name),
    TResult addedTokenToUser(User user, Token token, DateTime expirationDate),
    TResult loadedTokens(),
    TResult loadedTokensForUser(User user),
    TResult veryfiedToken(User user, Token token),
    TResult extendedExpirationTime(UserToken userToken, Duration duration),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addedTokenToUser != null) {
      return addedTokenToUser(user, token, expirationDate);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult addedToken(AddedToken value),
    @required TResult addedTokenToUser(AddedTokenToUser value),
    @required TResult loadedTokens(LoadedTokens value),
    @required TResult loadedTokensForUser(LoadedTokensForUser value),
    @required TResult veryfiedToken(VeryfiedToken value),
    @required TResult extendedExpirationTime(ExtendedExpirationTime value),
  }) {
    assert(addedToken != null);
    assert(addedTokenToUser != null);
    assert(loadedTokens != null);
    assert(loadedTokensForUser != null);
    assert(veryfiedToken != null);
    assert(extendedExpirationTime != null);
    return addedTokenToUser(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult addedToken(AddedToken value),
    TResult addedTokenToUser(AddedTokenToUser value),
    TResult loadedTokens(LoadedTokens value),
    TResult loadedTokensForUser(LoadedTokensForUser value),
    TResult veryfiedToken(VeryfiedToken value),
    TResult extendedExpirationTime(ExtendedExpirationTime value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addedTokenToUser != null) {
      return addedTokenToUser(this);
    }
    return orElse();
  }
}

abstract class AddedTokenToUser implements TokenEvent {
  const factory AddedTokenToUser(
      {@required User user,
      @required Token token,
      @required DateTime expirationDate}) = _$AddedTokenToUser;

  User get user;
  Token get token;
  DateTime get expirationDate;
  @JsonKey(ignore: true)
  $AddedTokenToUserCopyWith<AddedTokenToUser> get copyWith;
}

/// @nodoc
abstract class $LoadedTokensCopyWith<$Res> {
  factory $LoadedTokensCopyWith(
          LoadedTokens value, $Res Function(LoadedTokens) then) =
      _$LoadedTokensCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoadedTokensCopyWithImpl<$Res> extends _$TokenEventCopyWithImpl<$Res>
    implements $LoadedTokensCopyWith<$Res> {
  _$LoadedTokensCopyWithImpl(
      LoadedTokens _value, $Res Function(LoadedTokens) _then)
      : super(_value, (v) => _then(v as LoadedTokens));

  @override
  LoadedTokens get _value => super._value as LoadedTokens;
}

/// @nodoc
class _$LoadedTokens implements LoadedTokens {
  const _$LoadedTokens();

  @override
  String toString() {
    return 'TokenEvent.loadedTokens()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoadedTokens);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult addedToken(String name),
    @required
        TResult addedTokenToUser(
            User user, Token token, DateTime expirationDate),
    @required TResult loadedTokens(),
    @required TResult loadedTokensForUser(User user),
    @required TResult veryfiedToken(User user, Token token),
    @required
        TResult extendedExpirationTime(UserToken userToken, Duration duration),
  }) {
    assert(addedToken != null);
    assert(addedTokenToUser != null);
    assert(loadedTokens != null);
    assert(loadedTokensForUser != null);
    assert(veryfiedToken != null);
    assert(extendedExpirationTime != null);
    return loadedTokens();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult addedToken(String name),
    TResult addedTokenToUser(User user, Token token, DateTime expirationDate),
    TResult loadedTokens(),
    TResult loadedTokensForUser(User user),
    TResult veryfiedToken(User user, Token token),
    TResult extendedExpirationTime(UserToken userToken, Duration duration),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedTokens != null) {
      return loadedTokens();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult addedToken(AddedToken value),
    @required TResult addedTokenToUser(AddedTokenToUser value),
    @required TResult loadedTokens(LoadedTokens value),
    @required TResult loadedTokensForUser(LoadedTokensForUser value),
    @required TResult veryfiedToken(VeryfiedToken value),
    @required TResult extendedExpirationTime(ExtendedExpirationTime value),
  }) {
    assert(addedToken != null);
    assert(addedTokenToUser != null);
    assert(loadedTokens != null);
    assert(loadedTokensForUser != null);
    assert(veryfiedToken != null);
    assert(extendedExpirationTime != null);
    return loadedTokens(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult addedToken(AddedToken value),
    TResult addedTokenToUser(AddedTokenToUser value),
    TResult loadedTokens(LoadedTokens value),
    TResult loadedTokensForUser(LoadedTokensForUser value),
    TResult veryfiedToken(VeryfiedToken value),
    TResult extendedExpirationTime(ExtendedExpirationTime value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedTokens != null) {
      return loadedTokens(this);
    }
    return orElse();
  }
}

abstract class LoadedTokens implements TokenEvent {
  const factory LoadedTokens() = _$LoadedTokens;
}

/// @nodoc
abstract class $LoadedTokensForUserCopyWith<$Res> {
  factory $LoadedTokensForUserCopyWith(
          LoadedTokensForUser value, $Res Function(LoadedTokensForUser) then) =
      _$LoadedTokensForUserCopyWithImpl<$Res>;
  $Res call({User user});
}

/// @nodoc
class _$LoadedTokensForUserCopyWithImpl<$Res>
    extends _$TokenEventCopyWithImpl<$Res>
    implements $LoadedTokensForUserCopyWith<$Res> {
  _$LoadedTokensForUserCopyWithImpl(
      LoadedTokensForUser _value, $Res Function(LoadedTokensForUser) _then)
      : super(_value, (v) => _then(v as LoadedTokensForUser));

  @override
  LoadedTokensForUser get _value => super._value as LoadedTokensForUser;

  @override
  $Res call({
    Object user = freezed,
  }) {
    return _then(LoadedTokensForUser(
      user == freezed ? _value.user : user as User,
    ));
  }
}

/// @nodoc
class _$LoadedTokensForUser implements LoadedTokensForUser {
  const _$LoadedTokensForUser(this.user) : assert(user != null);

  @override
  final User user;

  @override
  String toString() {
    return 'TokenEvent.loadedTokensForUser(user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoadedTokensForUser &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(user);

  @JsonKey(ignore: true)
  @override
  $LoadedTokensForUserCopyWith<LoadedTokensForUser> get copyWith =>
      _$LoadedTokensForUserCopyWithImpl<LoadedTokensForUser>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult addedToken(String name),
    @required
        TResult addedTokenToUser(
            User user, Token token, DateTime expirationDate),
    @required TResult loadedTokens(),
    @required TResult loadedTokensForUser(User user),
    @required TResult veryfiedToken(User user, Token token),
    @required
        TResult extendedExpirationTime(UserToken userToken, Duration duration),
  }) {
    assert(addedToken != null);
    assert(addedTokenToUser != null);
    assert(loadedTokens != null);
    assert(loadedTokensForUser != null);
    assert(veryfiedToken != null);
    assert(extendedExpirationTime != null);
    return loadedTokensForUser(user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult addedToken(String name),
    TResult addedTokenToUser(User user, Token token, DateTime expirationDate),
    TResult loadedTokens(),
    TResult loadedTokensForUser(User user),
    TResult veryfiedToken(User user, Token token),
    TResult extendedExpirationTime(UserToken userToken, Duration duration),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedTokensForUser != null) {
      return loadedTokensForUser(user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult addedToken(AddedToken value),
    @required TResult addedTokenToUser(AddedTokenToUser value),
    @required TResult loadedTokens(LoadedTokens value),
    @required TResult loadedTokensForUser(LoadedTokensForUser value),
    @required TResult veryfiedToken(VeryfiedToken value),
    @required TResult extendedExpirationTime(ExtendedExpirationTime value),
  }) {
    assert(addedToken != null);
    assert(addedTokenToUser != null);
    assert(loadedTokens != null);
    assert(loadedTokensForUser != null);
    assert(veryfiedToken != null);
    assert(extendedExpirationTime != null);
    return loadedTokensForUser(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult addedToken(AddedToken value),
    TResult addedTokenToUser(AddedTokenToUser value),
    TResult loadedTokens(LoadedTokens value),
    TResult loadedTokensForUser(LoadedTokensForUser value),
    TResult veryfiedToken(VeryfiedToken value),
    TResult extendedExpirationTime(ExtendedExpirationTime value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadedTokensForUser != null) {
      return loadedTokensForUser(this);
    }
    return orElse();
  }
}

abstract class LoadedTokensForUser implements TokenEvent {
  const factory LoadedTokensForUser(User user) = _$LoadedTokensForUser;

  User get user;
  @JsonKey(ignore: true)
  $LoadedTokensForUserCopyWith<LoadedTokensForUser> get copyWith;
}

/// @nodoc
abstract class $VeryfiedTokenCopyWith<$Res> {
  factory $VeryfiedTokenCopyWith(
          VeryfiedToken value, $Res Function(VeryfiedToken) then) =
      _$VeryfiedTokenCopyWithImpl<$Res>;
  $Res call({User user, Token token});
}

/// @nodoc
class _$VeryfiedTokenCopyWithImpl<$Res> extends _$TokenEventCopyWithImpl<$Res>
    implements $VeryfiedTokenCopyWith<$Res> {
  _$VeryfiedTokenCopyWithImpl(
      VeryfiedToken _value, $Res Function(VeryfiedToken) _then)
      : super(_value, (v) => _then(v as VeryfiedToken));

  @override
  VeryfiedToken get _value => super._value as VeryfiedToken;

  @override
  $Res call({
    Object user = freezed,
    Object token = freezed,
  }) {
    return _then(VeryfiedToken(
      user: user == freezed ? _value.user : user as User,
      token: token == freezed ? _value.token : token as Token,
    ));
  }
}

/// @nodoc
class _$VeryfiedToken implements VeryfiedToken {
  const _$VeryfiedToken({@required this.user, @required this.token})
      : assert(user != null),
        assert(token != null);

  @override
  final User user;
  @override
  final Token token;

  @override
  String toString() {
    return 'TokenEvent.veryfiedToken(user: $user, token: $token)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is VeryfiedToken &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)) &&
            (identical(other.token, token) ||
                const DeepCollectionEquality().equals(other.token, token)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(user) ^
      const DeepCollectionEquality().hash(token);

  @JsonKey(ignore: true)
  @override
  $VeryfiedTokenCopyWith<VeryfiedToken> get copyWith =>
      _$VeryfiedTokenCopyWithImpl<VeryfiedToken>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult addedToken(String name),
    @required
        TResult addedTokenToUser(
            User user, Token token, DateTime expirationDate),
    @required TResult loadedTokens(),
    @required TResult loadedTokensForUser(User user),
    @required TResult veryfiedToken(User user, Token token),
    @required
        TResult extendedExpirationTime(UserToken userToken, Duration duration),
  }) {
    assert(addedToken != null);
    assert(addedTokenToUser != null);
    assert(loadedTokens != null);
    assert(loadedTokensForUser != null);
    assert(veryfiedToken != null);
    assert(extendedExpirationTime != null);
    return veryfiedToken(user, token);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult addedToken(String name),
    TResult addedTokenToUser(User user, Token token, DateTime expirationDate),
    TResult loadedTokens(),
    TResult loadedTokensForUser(User user),
    TResult veryfiedToken(User user, Token token),
    TResult extendedExpirationTime(UserToken userToken, Duration duration),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (veryfiedToken != null) {
      return veryfiedToken(user, token);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult addedToken(AddedToken value),
    @required TResult addedTokenToUser(AddedTokenToUser value),
    @required TResult loadedTokens(LoadedTokens value),
    @required TResult loadedTokensForUser(LoadedTokensForUser value),
    @required TResult veryfiedToken(VeryfiedToken value),
    @required TResult extendedExpirationTime(ExtendedExpirationTime value),
  }) {
    assert(addedToken != null);
    assert(addedTokenToUser != null);
    assert(loadedTokens != null);
    assert(loadedTokensForUser != null);
    assert(veryfiedToken != null);
    assert(extendedExpirationTime != null);
    return veryfiedToken(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult addedToken(AddedToken value),
    TResult addedTokenToUser(AddedTokenToUser value),
    TResult loadedTokens(LoadedTokens value),
    TResult loadedTokensForUser(LoadedTokensForUser value),
    TResult veryfiedToken(VeryfiedToken value),
    TResult extendedExpirationTime(ExtendedExpirationTime value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (veryfiedToken != null) {
      return veryfiedToken(this);
    }
    return orElse();
  }
}

abstract class VeryfiedToken implements TokenEvent {
  const factory VeryfiedToken({@required User user, @required Token token}) =
      _$VeryfiedToken;

  User get user;
  Token get token;
  @JsonKey(ignore: true)
  $VeryfiedTokenCopyWith<VeryfiedToken> get copyWith;
}

/// @nodoc
abstract class $ExtendedExpirationTimeCopyWith<$Res> {
  factory $ExtendedExpirationTimeCopyWith(ExtendedExpirationTime value,
          $Res Function(ExtendedExpirationTime) then) =
      _$ExtendedExpirationTimeCopyWithImpl<$Res>;
  $Res call({UserToken userToken, Duration duration});
}

/// @nodoc
class _$ExtendedExpirationTimeCopyWithImpl<$Res>
    extends _$TokenEventCopyWithImpl<$Res>
    implements $ExtendedExpirationTimeCopyWith<$Res> {
  _$ExtendedExpirationTimeCopyWithImpl(ExtendedExpirationTime _value,
      $Res Function(ExtendedExpirationTime) _then)
      : super(_value, (v) => _then(v as ExtendedExpirationTime));

  @override
  ExtendedExpirationTime get _value => super._value as ExtendedExpirationTime;

  @override
  $Res call({
    Object userToken = freezed,
    Object duration = freezed,
  }) {
    return _then(ExtendedExpirationTime(
      userToken:
          userToken == freezed ? _value.userToken : userToken as UserToken,
      duration: duration == freezed ? _value.duration : duration as Duration,
    ));
  }
}

/// @nodoc
class _$ExtendedExpirationTime implements ExtendedExpirationTime {
  const _$ExtendedExpirationTime({@required this.userToken, this.duration})
      : assert(userToken != null);

  @override
  final UserToken userToken;
  @override
  final Duration duration;

  @override
  String toString() {
    return 'TokenEvent.extendedExpirationTime(userToken: $userToken, duration: $duration)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ExtendedExpirationTime &&
            (identical(other.userToken, userToken) ||
                const DeepCollectionEquality()
                    .equals(other.userToken, userToken)) &&
            (identical(other.duration, duration) ||
                const DeepCollectionEquality()
                    .equals(other.duration, duration)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(userToken) ^
      const DeepCollectionEquality().hash(duration);

  @JsonKey(ignore: true)
  @override
  $ExtendedExpirationTimeCopyWith<ExtendedExpirationTime> get copyWith =>
      _$ExtendedExpirationTimeCopyWithImpl<ExtendedExpirationTime>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult addedToken(String name),
    @required
        TResult addedTokenToUser(
            User user, Token token, DateTime expirationDate),
    @required TResult loadedTokens(),
    @required TResult loadedTokensForUser(User user),
    @required TResult veryfiedToken(User user, Token token),
    @required
        TResult extendedExpirationTime(UserToken userToken, Duration duration),
  }) {
    assert(addedToken != null);
    assert(addedTokenToUser != null);
    assert(loadedTokens != null);
    assert(loadedTokensForUser != null);
    assert(veryfiedToken != null);
    assert(extendedExpirationTime != null);
    return extendedExpirationTime(userToken, duration);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult addedToken(String name),
    TResult addedTokenToUser(User user, Token token, DateTime expirationDate),
    TResult loadedTokens(),
    TResult loadedTokensForUser(User user),
    TResult veryfiedToken(User user, Token token),
    TResult extendedExpirationTime(UserToken userToken, Duration duration),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (extendedExpirationTime != null) {
      return extendedExpirationTime(userToken, duration);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult addedToken(AddedToken value),
    @required TResult addedTokenToUser(AddedTokenToUser value),
    @required TResult loadedTokens(LoadedTokens value),
    @required TResult loadedTokensForUser(LoadedTokensForUser value),
    @required TResult veryfiedToken(VeryfiedToken value),
    @required TResult extendedExpirationTime(ExtendedExpirationTime value),
  }) {
    assert(addedToken != null);
    assert(addedTokenToUser != null);
    assert(loadedTokens != null);
    assert(loadedTokensForUser != null);
    assert(veryfiedToken != null);
    assert(extendedExpirationTime != null);
    return extendedExpirationTime(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult addedToken(AddedToken value),
    TResult addedTokenToUser(AddedTokenToUser value),
    TResult loadedTokens(LoadedTokens value),
    TResult loadedTokensForUser(LoadedTokensForUser value),
    TResult veryfiedToken(VeryfiedToken value),
    TResult extendedExpirationTime(ExtendedExpirationTime value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (extendedExpirationTime != null) {
      return extendedExpirationTime(this);
    }
    return orElse();
  }
}

abstract class ExtendedExpirationTime implements TokenEvent {
  const factory ExtendedExpirationTime(
      {@required UserToken userToken,
      Duration duration}) = _$ExtendedExpirationTime;

  UserToken get userToken;
  Duration get duration;
  @JsonKey(ignore: true)
  $ExtendedExpirationTimeCopyWith<ExtendedExpirationTime> get copyWith;
}

/// @nodoc
class _$TokenStateTearOff {
  const _$TokenStateTearOff();

// ignore: unused_element
  TokenInitial tokenInitial() {
    return TokenInitial();
  }

// ignore: unused_element
  UserTokenUpdated tokensForUserUpdated(
      {@required User user,
      @required List<UserToken> tokens,
      @required bool isUpdated}) {
    return UserTokenUpdated(
      user: user,
      tokens: tokens,
      isUpdated: isUpdated,
    );
  }

// ignore: unused_element
  TokensUpdated tokensUpdated(
      {@required List<Token> tokens, @required bool isUpdated}) {
    return TokensUpdated(
      tokens: tokens,
      isUpdated: isUpdated,
    );
  }

// ignore: unused_element
  VeryficationSucceeded veryficationSucceeded(
      {@required User user, @required Token token}) {
    return VeryficationSucceeded(
      user: user,
      token: token,
    );
  }

// ignore: unused_element
  VeryficationFailed veryficationFailed(
      {@required User user, @required Token token}) {
    return VeryficationFailed(
      user: user,
      token: token,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $TokenState = _$TokenStateTearOff();

/// @nodoc
mixin _$TokenState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult tokenInitial(),
    @required
        TResult tokensForUserUpdated(
            User user, List<UserToken> tokens, bool isUpdated),
    @required TResult tokensUpdated(List<Token> tokens, bool isUpdated),
    @required TResult veryficationSucceeded(User user, Token token),
    @required TResult veryficationFailed(User user, Token token),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult tokenInitial(),
    TResult tokensForUserUpdated(
        User user, List<UserToken> tokens, bool isUpdated),
    TResult tokensUpdated(List<Token> tokens, bool isUpdated),
    TResult veryficationSucceeded(User user, Token token),
    TResult veryficationFailed(User user, Token token),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult tokenInitial(TokenInitial value),
    @required TResult tokensForUserUpdated(UserTokenUpdated value),
    @required TResult tokensUpdated(TokensUpdated value),
    @required TResult veryficationSucceeded(VeryficationSucceeded value),
    @required TResult veryficationFailed(VeryficationFailed value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult tokenInitial(TokenInitial value),
    TResult tokensForUserUpdated(UserTokenUpdated value),
    TResult tokensUpdated(TokensUpdated value),
    TResult veryficationSucceeded(VeryficationSucceeded value),
    TResult veryficationFailed(VeryficationFailed value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $TokenStateCopyWith<$Res> {
  factory $TokenStateCopyWith(
          TokenState value, $Res Function(TokenState) then) =
      _$TokenStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$TokenStateCopyWithImpl<$Res> implements $TokenStateCopyWith<$Res> {
  _$TokenStateCopyWithImpl(this._value, this._then);

  final TokenState _value;
  // ignore: unused_field
  final $Res Function(TokenState) _then;
}

/// @nodoc
abstract class $TokenInitialCopyWith<$Res> {
  factory $TokenInitialCopyWith(
          TokenInitial value, $Res Function(TokenInitial) then) =
      _$TokenInitialCopyWithImpl<$Res>;
}

/// @nodoc
class _$TokenInitialCopyWithImpl<$Res> extends _$TokenStateCopyWithImpl<$Res>
    implements $TokenInitialCopyWith<$Res> {
  _$TokenInitialCopyWithImpl(
      TokenInitial _value, $Res Function(TokenInitial) _then)
      : super(_value, (v) => _then(v as TokenInitial));

  @override
  TokenInitial get _value => super._value as TokenInitial;
}

/// @nodoc
class _$TokenInitial implements TokenInitial {
  _$TokenInitial();

  @override
  String toString() {
    return 'TokenState.tokenInitial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is TokenInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult tokenInitial(),
    @required
        TResult tokensForUserUpdated(
            User user, List<UserToken> tokens, bool isUpdated),
    @required TResult tokensUpdated(List<Token> tokens, bool isUpdated),
    @required TResult veryficationSucceeded(User user, Token token),
    @required TResult veryficationFailed(User user, Token token),
  }) {
    assert(tokenInitial != null);
    assert(tokensForUserUpdated != null);
    assert(tokensUpdated != null);
    assert(veryficationSucceeded != null);
    assert(veryficationFailed != null);
    return tokenInitial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult tokenInitial(),
    TResult tokensForUserUpdated(
        User user, List<UserToken> tokens, bool isUpdated),
    TResult tokensUpdated(List<Token> tokens, bool isUpdated),
    TResult veryficationSucceeded(User user, Token token),
    TResult veryficationFailed(User user, Token token),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (tokenInitial != null) {
      return tokenInitial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult tokenInitial(TokenInitial value),
    @required TResult tokensForUserUpdated(UserTokenUpdated value),
    @required TResult tokensUpdated(TokensUpdated value),
    @required TResult veryficationSucceeded(VeryficationSucceeded value),
    @required TResult veryficationFailed(VeryficationFailed value),
  }) {
    assert(tokenInitial != null);
    assert(tokensForUserUpdated != null);
    assert(tokensUpdated != null);
    assert(veryficationSucceeded != null);
    assert(veryficationFailed != null);
    return tokenInitial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult tokenInitial(TokenInitial value),
    TResult tokensForUserUpdated(UserTokenUpdated value),
    TResult tokensUpdated(TokensUpdated value),
    TResult veryficationSucceeded(VeryficationSucceeded value),
    TResult veryficationFailed(VeryficationFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (tokenInitial != null) {
      return tokenInitial(this);
    }
    return orElse();
  }
}

abstract class TokenInitial implements TokenState {
  factory TokenInitial() = _$TokenInitial;
}

/// @nodoc
abstract class $UserTokenUpdatedCopyWith<$Res> {
  factory $UserTokenUpdatedCopyWith(
          UserTokenUpdated value, $Res Function(UserTokenUpdated) then) =
      _$UserTokenUpdatedCopyWithImpl<$Res>;
  $Res call({User user, List<UserToken> tokens, bool isUpdated});
}

/// @nodoc
class _$UserTokenUpdatedCopyWithImpl<$Res>
    extends _$TokenStateCopyWithImpl<$Res>
    implements $UserTokenUpdatedCopyWith<$Res> {
  _$UserTokenUpdatedCopyWithImpl(
      UserTokenUpdated _value, $Res Function(UserTokenUpdated) _then)
      : super(_value, (v) => _then(v as UserTokenUpdated));

  @override
  UserTokenUpdated get _value => super._value as UserTokenUpdated;

  @override
  $Res call({
    Object user = freezed,
    Object tokens = freezed,
    Object isUpdated = freezed,
  }) {
    return _then(UserTokenUpdated(
      user: user == freezed ? _value.user : user as User,
      tokens: tokens == freezed ? _value.tokens : tokens as List<UserToken>,
      isUpdated: isUpdated == freezed ? _value.isUpdated : isUpdated as bool,
    ));
  }
}

/// @nodoc
class _$UserTokenUpdated implements UserTokenUpdated {
  _$UserTokenUpdated(
      {@required this.user, @required this.tokens, @required this.isUpdated})
      : assert(user != null),
        assert(tokens != null),
        assert(isUpdated != null);

  @override
  final User user;
  @override
  final List<UserToken> tokens;
  @override
  final bool isUpdated;

  @override
  String toString() {
    return 'TokenState.tokensForUserUpdated(user: $user, tokens: $tokens, isUpdated: $isUpdated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UserTokenUpdated &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)) &&
            (identical(other.tokens, tokens) ||
                const DeepCollectionEquality().equals(other.tokens, tokens)) &&
            (identical(other.isUpdated, isUpdated) ||
                const DeepCollectionEquality()
                    .equals(other.isUpdated, isUpdated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(user) ^
      const DeepCollectionEquality().hash(tokens) ^
      const DeepCollectionEquality().hash(isUpdated);

  @JsonKey(ignore: true)
  @override
  $UserTokenUpdatedCopyWith<UserTokenUpdated> get copyWith =>
      _$UserTokenUpdatedCopyWithImpl<UserTokenUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult tokenInitial(),
    @required
        TResult tokensForUserUpdated(
            User user, List<UserToken> tokens, bool isUpdated),
    @required TResult tokensUpdated(List<Token> tokens, bool isUpdated),
    @required TResult veryficationSucceeded(User user, Token token),
    @required TResult veryficationFailed(User user, Token token),
  }) {
    assert(tokenInitial != null);
    assert(tokensForUserUpdated != null);
    assert(tokensUpdated != null);
    assert(veryficationSucceeded != null);
    assert(veryficationFailed != null);
    return tokensForUserUpdated(user, tokens, isUpdated);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult tokenInitial(),
    TResult tokensForUserUpdated(
        User user, List<UserToken> tokens, bool isUpdated),
    TResult tokensUpdated(List<Token> tokens, bool isUpdated),
    TResult veryficationSucceeded(User user, Token token),
    TResult veryficationFailed(User user, Token token),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (tokensForUserUpdated != null) {
      return tokensForUserUpdated(user, tokens, isUpdated);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult tokenInitial(TokenInitial value),
    @required TResult tokensForUserUpdated(UserTokenUpdated value),
    @required TResult tokensUpdated(TokensUpdated value),
    @required TResult veryficationSucceeded(VeryficationSucceeded value),
    @required TResult veryficationFailed(VeryficationFailed value),
  }) {
    assert(tokenInitial != null);
    assert(tokensForUserUpdated != null);
    assert(tokensUpdated != null);
    assert(veryficationSucceeded != null);
    assert(veryficationFailed != null);
    return tokensForUserUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult tokenInitial(TokenInitial value),
    TResult tokensForUserUpdated(UserTokenUpdated value),
    TResult tokensUpdated(TokensUpdated value),
    TResult veryficationSucceeded(VeryficationSucceeded value),
    TResult veryficationFailed(VeryficationFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (tokensForUserUpdated != null) {
      return tokensForUserUpdated(this);
    }
    return orElse();
  }
}

abstract class UserTokenUpdated implements TokenState {
  factory UserTokenUpdated(
      {@required User user,
      @required List<UserToken> tokens,
      @required bool isUpdated}) = _$UserTokenUpdated;

  User get user;
  List<UserToken> get tokens;
  bool get isUpdated;
  @JsonKey(ignore: true)
  $UserTokenUpdatedCopyWith<UserTokenUpdated> get copyWith;
}

/// @nodoc
abstract class $TokensUpdatedCopyWith<$Res> {
  factory $TokensUpdatedCopyWith(
          TokensUpdated value, $Res Function(TokensUpdated) then) =
      _$TokensUpdatedCopyWithImpl<$Res>;
  $Res call({List<Token> tokens, bool isUpdated});
}

/// @nodoc
class _$TokensUpdatedCopyWithImpl<$Res> extends _$TokenStateCopyWithImpl<$Res>
    implements $TokensUpdatedCopyWith<$Res> {
  _$TokensUpdatedCopyWithImpl(
      TokensUpdated _value, $Res Function(TokensUpdated) _then)
      : super(_value, (v) => _then(v as TokensUpdated));

  @override
  TokensUpdated get _value => super._value as TokensUpdated;

  @override
  $Res call({
    Object tokens = freezed,
    Object isUpdated = freezed,
  }) {
    return _then(TokensUpdated(
      tokens: tokens == freezed ? _value.tokens : tokens as List<Token>,
      isUpdated: isUpdated == freezed ? _value.isUpdated : isUpdated as bool,
    ));
  }
}

/// @nodoc
class _$TokensUpdated implements TokensUpdated {
  _$TokensUpdated({@required this.tokens, @required this.isUpdated})
      : assert(tokens != null),
        assert(isUpdated != null);

  @override
  final List<Token> tokens;
  @override
  final bool isUpdated;

  @override
  String toString() {
    return 'TokenState.tokensUpdated(tokens: $tokens, isUpdated: $isUpdated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is TokensUpdated &&
            (identical(other.tokens, tokens) ||
                const DeepCollectionEquality().equals(other.tokens, tokens)) &&
            (identical(other.isUpdated, isUpdated) ||
                const DeepCollectionEquality()
                    .equals(other.isUpdated, isUpdated)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(tokens) ^
      const DeepCollectionEquality().hash(isUpdated);

  @JsonKey(ignore: true)
  @override
  $TokensUpdatedCopyWith<TokensUpdated> get copyWith =>
      _$TokensUpdatedCopyWithImpl<TokensUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult tokenInitial(),
    @required
        TResult tokensForUserUpdated(
            User user, List<UserToken> tokens, bool isUpdated),
    @required TResult tokensUpdated(List<Token> tokens, bool isUpdated),
    @required TResult veryficationSucceeded(User user, Token token),
    @required TResult veryficationFailed(User user, Token token),
  }) {
    assert(tokenInitial != null);
    assert(tokensForUserUpdated != null);
    assert(tokensUpdated != null);
    assert(veryficationSucceeded != null);
    assert(veryficationFailed != null);
    return tokensUpdated(tokens, isUpdated);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult tokenInitial(),
    TResult tokensForUserUpdated(
        User user, List<UserToken> tokens, bool isUpdated),
    TResult tokensUpdated(List<Token> tokens, bool isUpdated),
    TResult veryficationSucceeded(User user, Token token),
    TResult veryficationFailed(User user, Token token),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (tokensUpdated != null) {
      return tokensUpdated(tokens, isUpdated);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult tokenInitial(TokenInitial value),
    @required TResult tokensForUserUpdated(UserTokenUpdated value),
    @required TResult tokensUpdated(TokensUpdated value),
    @required TResult veryficationSucceeded(VeryficationSucceeded value),
    @required TResult veryficationFailed(VeryficationFailed value),
  }) {
    assert(tokenInitial != null);
    assert(tokensForUserUpdated != null);
    assert(tokensUpdated != null);
    assert(veryficationSucceeded != null);
    assert(veryficationFailed != null);
    return tokensUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult tokenInitial(TokenInitial value),
    TResult tokensForUserUpdated(UserTokenUpdated value),
    TResult tokensUpdated(TokensUpdated value),
    TResult veryficationSucceeded(VeryficationSucceeded value),
    TResult veryficationFailed(VeryficationFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (tokensUpdated != null) {
      return tokensUpdated(this);
    }
    return orElse();
  }
}

abstract class TokensUpdated implements TokenState {
  factory TokensUpdated(
      {@required List<Token> tokens,
      @required bool isUpdated}) = _$TokensUpdated;

  List<Token> get tokens;
  bool get isUpdated;
  @JsonKey(ignore: true)
  $TokensUpdatedCopyWith<TokensUpdated> get copyWith;
}

/// @nodoc
abstract class $VeryficationSucceededCopyWith<$Res> {
  factory $VeryficationSucceededCopyWith(VeryficationSucceeded value,
          $Res Function(VeryficationSucceeded) then) =
      _$VeryficationSucceededCopyWithImpl<$Res>;
  $Res call({User user, Token token});
}

/// @nodoc
class _$VeryficationSucceededCopyWithImpl<$Res>
    extends _$TokenStateCopyWithImpl<$Res>
    implements $VeryficationSucceededCopyWith<$Res> {
  _$VeryficationSucceededCopyWithImpl(
      VeryficationSucceeded _value, $Res Function(VeryficationSucceeded) _then)
      : super(_value, (v) => _then(v as VeryficationSucceeded));

  @override
  VeryficationSucceeded get _value => super._value as VeryficationSucceeded;

  @override
  $Res call({
    Object user = freezed,
    Object token = freezed,
  }) {
    return _then(VeryficationSucceeded(
      user: user == freezed ? _value.user : user as User,
      token: token == freezed ? _value.token : token as Token,
    ));
  }
}

/// @nodoc
class _$VeryficationSucceeded implements VeryficationSucceeded {
  _$VeryficationSucceeded({@required this.user, @required this.token})
      : assert(user != null),
        assert(token != null);

  @override
  final User user;
  @override
  final Token token;

  @override
  String toString() {
    return 'TokenState.veryficationSucceeded(user: $user, token: $token)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is VeryficationSucceeded &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)) &&
            (identical(other.token, token) ||
                const DeepCollectionEquality().equals(other.token, token)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(user) ^
      const DeepCollectionEquality().hash(token);

  @JsonKey(ignore: true)
  @override
  $VeryficationSucceededCopyWith<VeryficationSucceeded> get copyWith =>
      _$VeryficationSucceededCopyWithImpl<VeryficationSucceeded>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult tokenInitial(),
    @required
        TResult tokensForUserUpdated(
            User user, List<UserToken> tokens, bool isUpdated),
    @required TResult tokensUpdated(List<Token> tokens, bool isUpdated),
    @required TResult veryficationSucceeded(User user, Token token),
    @required TResult veryficationFailed(User user, Token token),
  }) {
    assert(tokenInitial != null);
    assert(tokensForUserUpdated != null);
    assert(tokensUpdated != null);
    assert(veryficationSucceeded != null);
    assert(veryficationFailed != null);
    return veryficationSucceeded(user, token);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult tokenInitial(),
    TResult tokensForUserUpdated(
        User user, List<UserToken> tokens, bool isUpdated),
    TResult tokensUpdated(List<Token> tokens, bool isUpdated),
    TResult veryficationSucceeded(User user, Token token),
    TResult veryficationFailed(User user, Token token),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (veryficationSucceeded != null) {
      return veryficationSucceeded(user, token);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult tokenInitial(TokenInitial value),
    @required TResult tokensForUserUpdated(UserTokenUpdated value),
    @required TResult tokensUpdated(TokensUpdated value),
    @required TResult veryficationSucceeded(VeryficationSucceeded value),
    @required TResult veryficationFailed(VeryficationFailed value),
  }) {
    assert(tokenInitial != null);
    assert(tokensForUserUpdated != null);
    assert(tokensUpdated != null);
    assert(veryficationSucceeded != null);
    assert(veryficationFailed != null);
    return veryficationSucceeded(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult tokenInitial(TokenInitial value),
    TResult tokensForUserUpdated(UserTokenUpdated value),
    TResult tokensUpdated(TokensUpdated value),
    TResult veryficationSucceeded(VeryficationSucceeded value),
    TResult veryficationFailed(VeryficationFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (veryficationSucceeded != null) {
      return veryficationSucceeded(this);
    }
    return orElse();
  }
}

abstract class VeryficationSucceeded implements TokenState {
  factory VeryficationSucceeded({@required User user, @required Token token}) =
      _$VeryficationSucceeded;

  User get user;
  Token get token;
  @JsonKey(ignore: true)
  $VeryficationSucceededCopyWith<VeryficationSucceeded> get copyWith;
}

/// @nodoc
abstract class $VeryficationFailedCopyWith<$Res> {
  factory $VeryficationFailedCopyWith(
          VeryficationFailed value, $Res Function(VeryficationFailed) then) =
      _$VeryficationFailedCopyWithImpl<$Res>;
  $Res call({User user, Token token});
}

/// @nodoc
class _$VeryficationFailedCopyWithImpl<$Res>
    extends _$TokenStateCopyWithImpl<$Res>
    implements $VeryficationFailedCopyWith<$Res> {
  _$VeryficationFailedCopyWithImpl(
      VeryficationFailed _value, $Res Function(VeryficationFailed) _then)
      : super(_value, (v) => _then(v as VeryficationFailed));

  @override
  VeryficationFailed get _value => super._value as VeryficationFailed;

  @override
  $Res call({
    Object user = freezed,
    Object token = freezed,
  }) {
    return _then(VeryficationFailed(
      user: user == freezed ? _value.user : user as User,
      token: token == freezed ? _value.token : token as Token,
    ));
  }
}

/// @nodoc
class _$VeryficationFailed implements VeryficationFailed {
  _$VeryficationFailed({@required this.user, @required this.token})
      : assert(user != null),
        assert(token != null);

  @override
  final User user;
  @override
  final Token token;

  @override
  String toString() {
    return 'TokenState.veryficationFailed(user: $user, token: $token)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is VeryficationFailed &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)) &&
            (identical(other.token, token) ||
                const DeepCollectionEquality().equals(other.token, token)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(user) ^
      const DeepCollectionEquality().hash(token);

  @JsonKey(ignore: true)
  @override
  $VeryficationFailedCopyWith<VeryficationFailed> get copyWith =>
      _$VeryficationFailedCopyWithImpl<VeryficationFailed>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult tokenInitial(),
    @required
        TResult tokensForUserUpdated(
            User user, List<UserToken> tokens, bool isUpdated),
    @required TResult tokensUpdated(List<Token> tokens, bool isUpdated),
    @required TResult veryficationSucceeded(User user, Token token),
    @required TResult veryficationFailed(User user, Token token),
  }) {
    assert(tokenInitial != null);
    assert(tokensForUserUpdated != null);
    assert(tokensUpdated != null);
    assert(veryficationSucceeded != null);
    assert(veryficationFailed != null);
    return veryficationFailed(user, token);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult tokenInitial(),
    TResult tokensForUserUpdated(
        User user, List<UserToken> tokens, bool isUpdated),
    TResult tokensUpdated(List<Token> tokens, bool isUpdated),
    TResult veryficationSucceeded(User user, Token token),
    TResult veryficationFailed(User user, Token token),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (veryficationFailed != null) {
      return veryficationFailed(user, token);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult tokenInitial(TokenInitial value),
    @required TResult tokensForUserUpdated(UserTokenUpdated value),
    @required TResult tokensUpdated(TokensUpdated value),
    @required TResult veryficationSucceeded(VeryficationSucceeded value),
    @required TResult veryficationFailed(VeryficationFailed value),
  }) {
    assert(tokenInitial != null);
    assert(tokensForUserUpdated != null);
    assert(tokensUpdated != null);
    assert(veryficationSucceeded != null);
    assert(veryficationFailed != null);
    return veryficationFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult tokenInitial(TokenInitial value),
    TResult tokensForUserUpdated(UserTokenUpdated value),
    TResult tokensUpdated(TokensUpdated value),
    TResult veryficationSucceeded(VeryficationSucceeded value),
    TResult veryficationFailed(VeryficationFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (veryficationFailed != null) {
      return veryficationFailed(this);
    }
    return orElse();
  }
}

abstract class VeryficationFailed implements TokenState {
  factory VeryficationFailed({@required User user, @required Token token}) =
      _$VeryficationFailed;

  User get user;
  Token get token;
  @JsonKey(ignore: true)
  $VeryficationFailedCopyWith<VeryficationFailed> get copyWith;
}
