part of 'token_bloc.dart';

@freezed
abstract class TokenEvent with _$TokenEvent {
  const factory TokenEvent.addedToken({@required String name}) = AddedToken;
  const factory TokenEvent.addedTokenToUser(
      {@required User user,
      @required Token token,
      @required DateTime expirationDate}) = AddedTokenToUser;
  const factory TokenEvent.loadedTokens() = LoadedTokens;
  const factory TokenEvent.loadedTokensForUser(User user) = LoadedTokensForUser;
  const factory TokenEvent.veryfiedToken(
      {@required User user, @required Token token}) = VeryfiedToken;
  const factory TokenEvent.extendedExpirationTime(
      {@required UserToken userToken,
      Duration duration}) = ExtendedExpirationTime;
}
