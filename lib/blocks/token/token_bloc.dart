import 'package:bloc/bloc.dart';
import 'package:dreamteam_project/repositories/token_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/models/Token.dart';
import 'package:dreamteam_project/models/UserToken.dart';
import 'package:dreamteam_project/main.dart';
import 'dart:async';

part 'token_event.dart';
part 'token_state.dart';

part 'token_bloc.freezed.dart';

class TokenBloc extends Bloc<TokenEvent, TokenState> {
  TokenRepository tokenRepository = TokenRepository();

  TokenBloc() : super(TokenState.tokenInitial());

  @override
  Stream<TokenState> mapEventToState(TokenEvent event) async* {
    tokenRepository.checkBaseTokensExistance();
    try {
      event.map(
          addedToken: (e) => tokenRepository.addToken(e.name),
          addedTokenToUser: (e) =>
              tokenRepository.addUserToken(e.token, e.expirationDate),
          loadedTokens: null,
          veryfiedToken: null,
          extendedExpirationTime: (e) =>
              tokenRepository.extendExpirationDate(e.userToken, e.duration),
          loadedTokensForUser: null);
      if (event is VeryfiedToken) {
        if (await tokenRepository.verifyToken(event.user, event.token)) {
          yield TokenState.veryficationSucceeded(
              user: event.user, token: event.token);
        } else {
          yield TokenState.veryficationFailed(
              user: event.user, token: event.token);
        }
      }
      if (event is LoadedTokens) {
        List<Token> tokens = await tokenRepository.getAllTokens();
        yield TokenState.tokensUpdated(tokens: tokens, isUpdated: true);
      }
      if (event is LoadedTokensForUser) {
        List<UserToken> tokensForUser =
            await tokenRepository.getTokensForUser(event.user);
        yield TokenState.tokensForUserUpdated(
            user: event.user, tokens: tokensForUser, isUpdated: true);
      }
    } catch (e) {
      logger.e("Error mapEventToState in TokenBloc: $e");
    }
  }
}
