import 'package:dreamteam_project/exception/core/base_exception.dart';
import 'package:dreamteam_project/generated/l10n.dart';

class InvalidEmailAndPasswordCombinationException extends BaseDreamTeamException{
  InvalidEmailAndPasswordCombinationException() : super(S.current.invalidEmailAndPasswordCombination);
}

class InvalidEmailException extends BaseDreamTeamException{
  InvalidEmailException() : super(S.current.invalidEmail);
}