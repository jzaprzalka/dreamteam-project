import 'package:dreamteam_project/generated/l10n.dart';

class BaseDreamTeamException implements Exception {
  final String message;

  const BaseDreamTeamException(this.message);

  String toString() => "$message";
}

class ServerErrorException extends BaseDreamTeamException {
  ServerErrorException() : super(S.current.serverError);
}

class UnExpectedException extends BaseDreamTeamException {
  UnExpectedException() : super(S.current.unExpectedError);
}
