import 'dart:developer';

import 'package:logger/logger.dart';
import 'dart:io';
import 'package:path/path.dart' as p;

class DebugConsoleLogOutput extends LogOutput {
  @override
  void output(OutputEvent event) {
    for (var line in event?.lines) {
      print(line);
    }
  }
}

class DebugFileLogOutput extends LogOutput {
  Future<String> get _localPath async {
    final directory =
        Directory.current; // await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    // Create date signature
    final date =
        new DateTime.now().toString().replaceAll(':', '').replaceAll(' ', '_');
    String fullPath = p.join(path, "logs", "debugLogs($date).txt");
    log("Saving debug logs to: $fullPath");
    return File(fullPath);
  }

  @override
  void output(OutputEvent event) async {
    try {
      final file = await _localFile;
      for (var line in event?.lines) {
        file.writeAsString(line, mode: FileMode.append);
      }
    } catch (e) {
      log("Error occured writing to debug log: $e");
    }
  }
}

class DeployFileLogOutput extends LogOutput {
  Future<String> get _localPath async {
    final directory =
        Directory.current; // await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    // Create date signature
    final date =
        new DateTime.now().toString().replaceAll(':', '').replaceAll(' ', '_');

    String fullPath = p.join(path, "logs", "deployLogs($date).txt");
    log("Saving deploy logs to: $fullPath");
    return File(fullPath);
  }

  @override
  void output(OutputEvent event) async {
    try {
      final file = await _localFile;
      for (var line in event?.lines) {
        file.writeAsString(line, mode: FileMode.append);
      }
    } catch (e) {
      log("Error occured writing to deploy log: $e");
    }
  }
}
