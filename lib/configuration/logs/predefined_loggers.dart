import 'package:logger/logger.dart';

import 'log_filters.dart';
import 'log_outputs.dart';

Logger get fullDebugLogger => Logger(
      filter: DebugLogFilter(),
      printer: PrettyPrinter(
        methodCount: 2,
        errorMethodCount: 8,
        lineLength: 150,
        colors: true,
        printEmojis: true,
        printTime: true,
      ),
      output: DebugConsoleLogOutput(),
    );

Logger get simpleDebugLogger => Logger(
      filter: DebugLogFilter(),
      printer: SimplePrinter(printTime: false, colors: true),
      output: DebugConsoleLogOutput(),
    );

Logger get fileDebugLogger => Logger(
      filter: DebugLogFilter(),
      printer: PrettyPrinter(
        methodCount: 4,
        errorMethodCount: 10,
        lineLength: 120,
        colors: false,
        printEmojis: true,
        printTime: true,
      ),
      output: DebugFileLogOutput(),
    );

Logger get deployLogger => Logger(
      filter: DeployLogFilter(),
      printer: SimplePrinter(printTime: true, colors: false),
      output: DeployFileLogOutput(),
    );
