import 'package:logger/logger.dart';

class DebugLogFilter extends LogFilter {
  @override
  bool shouldLog(LogEvent event) {
    return true;
  }
}

class DeployLogFilter extends LogFilter {
  @override
  bool shouldLog(LogEvent event) {
    return event.level != Level.debug &&
        event.level != Level.wtf &&
        event.level != Level.warning;
  }
}
