// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Schedule.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Schedule _$ScheduleFromJson(Map<String, dynamic> json) {
  return Schedule(
    scheduleId: json['scheduleId'] as String,
    groupId: json['groupId'] as String,
    coachIds: (json['coachIds'] as List)?.map((e) => e as String)?.toList(),
    classSubject: json['classSubject'] as String,
    roomId: json['roomId'] as String,
    dateStart: fromFirestoreDate(json['dateStart']),
    dateEnd: fromFirestoreDate(json['dateEnd']),
    attendance: fromFirestoreAttendanceList(json['attendance']),
  );
}

Map<String, dynamic> _$ScheduleToJson(Schedule instance) => <String, dynamic>{
      'scheduleId': instance.scheduleId,
      'groupId': instance.groupId,
      'coachIds': instance.coachIds,
      'classSubject': instance.classSubject,
      'roomId': instance.roomId,
      'dateStart': toFirestoreDate(instance.dateStart),
      'dateEnd': toFirestoreDate(instance.dateEnd),
      'attendance': toFirestoreAttendanceList(instance.attendance),
    };
