import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dreamteam_project/models/Document.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'annotations/field_converters.dart';

part 'Payment.g.dart';

@JsonSerializable()
class Payment {
  String paymentId;
  String userId;

  @JsonKey(ignore: true)
  User user;
  int amount;
  String paymentName;

  @JsonKey(includeIfNull: false)
  String documentId;

  @JsonKey(ignore: true)
  Document document;

  @JsonKey(
      fromJson: fromFirestoreDate,
      toJson: toFirestoreDate,
      includeIfNull: false)
  DateTime dateDue;

  @JsonKey(
      fromJson: fromFirestoreDate,
      toJson: toFirestoreDate,
      includeIfNull: false)
  DateTime datePaid;

  @JsonKey(defaultValue: false)
  bool paid;

  @JsonKey(fromJson: fromFirestoreDate, toJson: toFirestoreDate)
  DateTime dateAdded;

  Payment({
    this.paymentId,
    this.userId,
    this.amount,
    this.paymentName,
    this.documentId,
    this.dateDue,
    this.datePaid,
    this.paid,
    this.dateAdded,
  });

  factory Payment.fromMap(Map map) => _$PaymentFromJson(map);
  Map<String, dynamic> toMap() => _$PaymentToJson(this);

  factory Payment.fromFirebase(DocumentSnapshot doc) {
    Map map = doc.data();
    map["paymentId"] = doc.id;
    return Payment.fromMap(map);
  }
}
