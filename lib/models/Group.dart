import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'User.dart';

part 'Group.g.dart';

@JsonSerializable()
class Group {
  String groupId;
  String name;
  List<String> coachIds;

  @JsonKey(ignore: true)
  List<User> coaches;
  List<String> participantIds;

  @JsonKey(ignore: true)
  List<User> participants;

  Group({
    this.groupId,
    @required this.name,
    this.coachIds,
    this.coaches,
    this.participantIds,
    this.participants,
  });

  factory Group.fromMap(Map map) => _$GroupFromJson(map);
  Map<String, dynamic> toMap() => _$GroupToJson(this);

  factory Group.fromFirebase(DocumentSnapshot doc) {
    Map map = doc.data();
    map["groupId"] = doc.id;
    return Group.fromMap(map);
  }
}
