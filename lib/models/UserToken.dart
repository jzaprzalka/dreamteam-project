import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'Token.dart';
import 'User.dart';
import 'annotations/field_converters.dart';

part 'UserToken.g.dart';

@JsonSerializable()
class UserToken {
  String userTokenId;
  String userId;
  String tokenId;

  User _user;
  Token _token;

  @JsonKey(
    fromJson: fromFirestoreDate,
    toJson: toFirestoreDate,
    includeIfNull: false,
  )
  DateTime expirationDate;

  @JsonKey(ignore: true)
  set token(Token token) {
    tokenId = token.id;
    this._token = token;
  }

  @JsonKey(ignore: true)
  Token get token {
    return this._token;
  }

  @JsonKey(ignore: true)
  set user(User user) {
    userId = user.userId;
    this._user = user;
  }

  @JsonKey(ignore: true)
  User get user {
    return this._user;
  }

  UserToken({
    this.userId,
    this.tokenId,
    this.expirationDate,
    this.userTokenId,
  });

  factory UserToken.fromMap(Map map) => _$UserTokenFromJson(map);
  Map<String, dynamic> toMap() => _$UserTokenToJson(this);

  factory UserToken.fromFirebase(DocumentSnapshot doc) {
    Map map = doc.data();
    map["userTokenId"] = doc.id;
    return UserToken.fromMap(map);
  }
}
