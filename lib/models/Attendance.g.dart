// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Attendance.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Attendance _$AttendanceFromJson(Map<String, dynamic> json) {
  return Attendance(
    userId: json['userId'] as String,
    dateModified: fromFirestoreDate(json['dateModified']),
    presence: json['presence'] as bool,
  );
}

Map<String, dynamic> _$AttendanceToJson(Attendance instance) =>
    <String, dynamic>{
      'userId': instance.userId,
      'dateModified': toFirestoreDate(instance.dateModified),
      'presence': instance.presence,
    };
