// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Payment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Payment _$PaymentFromJson(Map<String, dynamic> json) {
  return Payment(
    paymentId: json['paymentId'] as String,
    userId: json['userId'] as String,
    amount: json['amount'] as int,
    paymentName: json['paymentName'] as String,
    documentId: json['documentId'] as String,
    dateDue: fromFirestoreDate(json['dateDue']),
    datePaid: fromFirestoreDate(json['datePaid']),
    paid: json['paid'] as bool ?? false,
    dateAdded: fromFirestoreDate(json['dateAdded']),
  );
}

Map<String, dynamic> _$PaymentToJson(Payment instance) {
  final val = <String, dynamic>{
    'paymentId': instance.paymentId,
    'userId': instance.userId,
    'amount': instance.amount,
    'paymentName': instance.paymentName,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('documentId', instance.documentId);
  writeNotNull('dateDue', toFirestoreDate(instance.dateDue));
  writeNotNull('datePaid', toFirestoreDate(instance.datePaid));
  val['paid'] = instance.paid;
  val['dateAdded'] = toFirestoreDate(instance.dateAdded);
  return val;
}
