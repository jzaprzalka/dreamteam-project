// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'RoomReservation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomReservation _$RoomReservationFromJson(Map<String, dynamic> json) {
  return RoomReservation(
    reservationId: json['reservationId'] as String,
    bookingPersonId: json['bookingPersonId'] as String,
    roomId: json['roomId'] as String,
    addDate: fromFirestoreDate(json['addDate']),
    reservationStart: fromFirestoreDate(json['reservationStart']),
    reservationEnd: fromFirestoreDate(json['reservationEnd']),
    private: json['private'] as bool,
  );
}

Map<String, dynamic> _$RoomReservationToJson(RoomReservation instance) =>
    <String, dynamic>{
      'reservationId': instance.reservationId,
      'bookingPersonId': instance.bookingPersonId,
      'roomId': instance.roomId,
      'addDate': toFirestoreDate(instance.addDate),
      'reservationStart': toFirestoreDate(instance.reservationStart),
      'reservationEnd': toFirestoreDate(instance.reservationEnd),
      'private': instance.private,
    };
