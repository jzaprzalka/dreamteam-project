import 'package:cloud_firestore/cloud_firestore.dart';

import '../Attendance.dart';

toFirestoreDate(DateTime value) =>
    value != null ? Timestamp.fromDate(value) : null;
fromFirestoreDate(value) => (value as Timestamp)?.toDate() ?? null;

toFirestoreAttendanceList(attendanceList) => (attendanceList as List)
    ?.map((attendance) => attendance == null ? [] : attendance.toMap())
    ?.toList();
fromFirestoreAttendanceList(attendanceList) => (attendanceList as List)
    ?.map(
        (e) => e == null ? null : Attendance.fromMap(e as Map<String, dynamic>))
    ?.toList();

// T modelFromMap<T>(Map<String, dynamic> instanceMap, String id) {
//   Attendance a = Attendance.fromMap(instanceMap);
//   a.scheduleId = id;
//   return a;
// }
