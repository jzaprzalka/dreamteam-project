import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'Room.g.dart';

@JsonSerializable()
class Room {
  String roomId;
  String number;
  String buildingAddress;
  List<String> reservations;

  Room({
    this.roomId,
    this.number,
    this.buildingAddress,
    this.reservations,
  });

  factory Room.fromMap(Map map) => _$RoomFromJson(map);
  Map<String, dynamic> toMap() => _$RoomToJson(this);

  factory Room.fromFirebase(DocumentSnapshot doc) {
    Map map = doc.data();
    map["roomId"] = doc.id;
    return Room.fromMap(map);
  }
}
