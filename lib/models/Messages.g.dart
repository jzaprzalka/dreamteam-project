// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Messages.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TextMessage _$TextMessageFromJson(Map<String, dynamic> json) {
  return TextMessage(
    json['text'] as String,
    fromFirestoreDate(json['timestamp']),
    json['from'],
  )..messageId = json['messageId'] as String;
}

Map<String, dynamic> _$TextMessageToJson(TextMessage instance) =>
    <String, dynamic>{
      'messageId': instance.messageId,
      'timestamp': toFirestoreDate(instance.timestamp),
      'from': instance.fromUID,
      'text': instance.text,
    };

ImageMessage _$ImageMessageFromJson(Map<String, dynamic> json) {
  return ImageMessage(
    json['imageUrl'] as String,
    fromFirestoreDate(json['timestamp']),
    json['from'],
  )..messageId = json['messageId'] as String;
}

Map<String, dynamic> _$ImageMessageToJson(ImageMessage instance) =>
    <String, dynamic>{
      'messageId': instance.messageId,
      'timestamp': toFirestoreDate(instance.timestamp),
      'from': instance.fromUID,
      'imageUrl': instance.imageUrl,
    };
