// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Document.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Document _$DocumentFromJson(Map<String, dynamic> json) {
  return Document(
    documentId: json['documentId'] as String,
    accessTokenId: json['accessTokenId'] as String,
    dateCreated: fromFirestoreDate(json['dateCreated']),
    type: json['type'] as String,
    filePath: json['filePath'] as String,
  );
}

Map<String, dynamic> _$DocumentToJson(Document instance) => <String, dynamic>{
      'documentId': instance.documentId,
      'accessTokenId': instance.accessTokenId,
      'dateCreated': toFirestoreDate(instance.dateCreated),
      'type': instance.type,
      'filePath': instance.filePath,
    };
