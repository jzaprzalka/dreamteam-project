import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'UserToken.dart';
import 'annotations/field_converters.dart';

part 'User.g.dart';

@JsonSerializable()
class User {
  String userId;
  String pesel;
  String name;
  String password;
  String surname;

  @JsonKey(fromJson: fromFirestoreDate, toJson: toFirestoreDate)
  DateTime birthDate;
  String phoneNumber;
  String email;
  String nip;

  @JsonKey(ignore: true)
  List<String> groupIds;

  @JsonKey(ignore: true)
  List<UserToken> userTokens = List.empty();

  User({
    @required this.userId,
    this.pesel,
    @required this.name,
    this.password,
    @required this.surname,
    this.birthDate,
    this.phoneNumber,
    this.email,
    this.nip,
    this.groupIds,
  });

  factory User.fromMap(Map map) => _$UserFromJson(map);
  Map<String, dynamic> toMap() => _$UserToJson(this);

  factory User.fromFirebase(DocumentSnapshot doc) {
    Map map = doc.data();
    map["userId"] = doc.id;
    return User.fromMap(map);
  }
}
