// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Announcement.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Announcement _$AnnouncementFromJson(Map<String, dynamic> json) {
  return Announcement(
    announcementId: json['announcementId'] as String,
    announcementDate: fromFirestoreDate(json['announcementDate']),
    announcement: json['announcement'] as String,
    groupIds: (json['groupIds'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$AnnouncementToJson(Announcement instance) =>
    <String, dynamic>{
      'announcementId': instance.announcementId,
      'announcementDate': toFirestoreDate(instance.announcementDate),
      'announcement': instance.announcement,
      'groupIds': instance.groupIds,
    };
