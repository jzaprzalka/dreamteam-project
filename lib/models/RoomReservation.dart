import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'Room.dart';
import 'User.dart';
import 'annotations/field_converters.dart';

part 'RoomReservation.g.dart';

@JsonSerializable()
class RoomReservation {
  String reservationId;
  String bookingPersonId;

  @JsonKey(ignore: true)
  User bookingPerson;
  String roomId;

  @JsonKey(ignore: true)
  Room room;

  @JsonKey(fromJson: fromFirestoreDate, toJson: toFirestoreDate)
  DateTime addDate;

  @JsonKey(fromJson: fromFirestoreDate, toJson: toFirestoreDate)
  DateTime reservationStart;

  @JsonKey(fromJson: fromFirestoreDate, toJson: toFirestoreDate)
  DateTime reservationEnd;
  bool private;

  RoomReservation({
    this.reservationId,
    this.bookingPersonId,
    this.roomId,
    this.addDate,
    this.reservationStart,
    this.reservationEnd,
    this.private,
  });

  factory RoomReservation.fromMap(Map map) => _$RoomReservationFromJson(map);
  Map<String, dynamic> toMap() => _$RoomReservationToJson(this);

  factory RoomReservation.fromFirebase(DocumentSnapshot doc) {
    Map map = doc.data();
    map["reservationId"] = doc.id;
    return RoomReservation.fromMap(map);
  }
}
