// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CompetitionApplication.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CompetitionApplication _$CompetitionApplicationFromJson(
    Map<String, dynamic> json) {
  return CompetitionApplication(
    userId: json['userId'] as String,
    competitionId: json['competitionId'] as String,
    applicationDate: fromFirestoreDate(json['applicationDate']),
    approved: json['approved'] as bool ?? false,
    details: json['details'] as String,
  )..competitionApplicationId = json['competitionApplicationId'] as String;
}

Map<String, dynamic> _$CompetitionApplicationToJson(
        CompetitionApplication instance) =>
    <String, dynamic>{
      'competitionApplicationId': instance.competitionApplicationId,
      'userId': instance.userId,
      'competitionId': instance.competitionId,
      'applicationDate': toFirestoreDate(instance.applicationDate),
      'approved': instance.approved,
      'details': instance.details,
    };
