import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'Group.dart';
import 'annotations/field_converters.dart';

part 'Announcement.g.dart';

@JsonSerializable()
class Announcement {
  String announcementId;

  @JsonKey(fromJson: fromFirestoreDate, toJson: toFirestoreDate)
  DateTime announcementDate;
  String announcement;
  List<String> groupIds;

  @JsonKey(ignore: true)
  List<Group> groups;

  Announcement(
      {this.announcementId,
      this.announcementDate,
      this.announcement,
      this.groupIds,
      this.groups});

  factory Announcement.fromMap(Map map) => _$AnnouncementFromJson(map);
  Map<String, dynamic> toMap() => _$AnnouncementToJson(this);

  factory Announcement.fromFirebase(DocumentSnapshot doc) {
    Map map = doc.data();
    map["announcementId"] = doc.id;
    return Announcement.fromMap(map);
  }
}
