// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Room.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Room _$RoomFromJson(Map<String, dynamic> json) {
  return Room(
    roomId: json['roomId'] as String,
    number: json['number'] as String,
    buildingAddress: json['buildingAddress'] as String,
    reservations:
        (json['reservations'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$RoomToJson(Room instance) => <String, dynamic>{
      'roomId': instance.roomId,
      'number': instance.number,
      'buildingAddress': instance.buildingAddress,
      'reservations': instance.reservations,
    };
