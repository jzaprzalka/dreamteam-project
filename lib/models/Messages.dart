import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'User.dart';
import 'annotations/field_converters.dart';

part 'Messages.g.dart';

abstract class Message {
  String messageId;

  @JsonKey(fromJson: fromFirestoreDate, toJson: toFirestoreDate)
  DateTime timestamp;

  @JsonKey(name: 'from')
  String fromUID;
  int get type;

  @JsonKey(ignore: true)
  User from;

  Message(this.timestamp, this.fromUID);

  factory Message.fromFireStore(DocumentSnapshot doc) {
    Map map = doc.data();
    final int type = map['type'];
    Message message;
    switch (type) {
      case 1:
        message = ImageMessage.fromMap(doc.data());
        break;
      case 0:
      default:
        message = TextMessage.fromMap(doc.data());
        break;
    }
    return message;
  }

  Map<String, dynamic> toMap();
}

@JsonSerializable()
class TextMessage extends Message {
  String text;
  int get type => 0;

  TextMessage(this.text, timestamp, fromUID) : super(timestamp, fromUID);

  factory TextMessage.fromMap(Map data) => _$TextMessageFromJson(data);

  @override
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = _$TextMessageToJson(this);
    map['type'] = type;
    return map;
  }
}

@JsonSerializable()
class ImageMessage extends Message {
  String imageUrl;
  int get type => 1;

  ImageMessage(this.imageUrl, timestamp, fromUID) : super(timestamp, fromUID);

  factory ImageMessage.fromMap(Map data) => _$ImageMessageFromJson(data);

  @override
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = _$ImageMessageToJson(this);
    map['type'] = type;
    return map;
  }
}
