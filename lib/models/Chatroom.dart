import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/models/Messages.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'Chatroom.g.dart';

@JsonSerializable()
class Chatroom {
  String chatroomId;
  String name;
  List<String> userIds;
  List<String> messageIds;

  @JsonKey(ignore: true)
  Map<String,User> users;
  @JsonKey(ignore: true)
  List<Message> messages;

  Chatroom({
    this.chatroomId,
    this.name,
    this.messageIds,
    this.messages,
    this.users,
    this.userIds,
  });

  factory Chatroom.fromMap(Map map) => _$ChatroomFromJson(map);
  Map<String, dynamic> toMap() => _$ChatroomToJson(this);

  factory Chatroom.fromFirebase(DocumentSnapshot doc) {
    Map map = doc.data();
    map["chatroomId"] = doc.id;
    return Chatroom.fromMap(map);
  }
}
