// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Group.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Group _$GroupFromJson(Map<String, dynamic> json) {
  return Group(
    groupId: json['groupId'] as String,
    name: json['name'] as String,
    coachIds: (json['coachIds'] as List)?.map((e) => e as String)?.toList(),
    participantIds:
        (json['participantIds'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$GroupToJson(Group instance) => <String, dynamic>{
      'groupId': instance.groupId,
      'name': instance.name,
      'coachIds': instance.coachIds,
      'participantIds': instance.participantIds,
    };
