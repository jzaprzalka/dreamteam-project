import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'Competition.dart';
import 'User.dart';
import 'annotations/field_converters.dart';

part 'CompetitionApplication.g.dart';

@JsonSerializable()
class CompetitionApplication {
  String competitionApplicationId;
  String userId;

  @JsonKey(ignore: true)
  User user;
  String competitionId;

  @JsonKey(ignore: true)
  Competition competition;

  @JsonKey(fromJson: fromFirestoreDate, toJson: toFirestoreDate)
  DateTime applicationDate;

  @JsonKey(defaultValue: false)
  bool approved;
  String details;

  CompetitionApplication({
    this.userId,
    this.competitionId,
    this.applicationDate,
    this.approved,
    this.details,
  });

  factory CompetitionApplication.fromMap(Map map) =>
      _$CompetitionApplicationFromJson(map);
  Map<String, dynamic> toMap() => _$CompetitionApplicationToJson(this);

  factory CompetitionApplication.fromFirebase(DocumentSnapshot doc) {
    Map map = doc.data();
    map["competitionApplicationId"] = doc.id;
    return CompetitionApplication.fromMap(map);
  }
}
