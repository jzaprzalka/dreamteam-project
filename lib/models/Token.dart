import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'Token.g.dart';

@JsonSerializable()
class Token {
  String id;
  String name;

  Token({this.id, this.name});

  factory Token.fromMap(Map map) => _$TokenFromJson(map);
  Map<String, dynamic> toMap() => _$TokenToJson(this);

  factory Token.fromFirebase(DocumentSnapshot doc) {
    Map map = doc.data();
    if (map == null) return Token(id: "<None>", name: "Doesn't exist");
    map["id"] = doc.id;
    return Token.fromMap(map);
  }
}
