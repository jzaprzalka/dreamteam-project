// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'User.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    userId: json['userId'] as String,
    pesel: json['pesel'] as String,
    name: json['name'] as String,
    password: json['password'] as String,
    surname: json['surname'] as String,
    birthDate: fromFirestoreDate(json['birthDate']),
    phoneNumber: json['phoneNumber'] as String,
    email: json['email'] as String,
    nip: json['nip'] as String,
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'userId': instance.userId,
      'pesel': instance.pesel,
      'name': instance.name,
      'password': instance.password,
      'surname': instance.surname,
      'birthDate': toFirestoreDate(instance.birthDate),
      'phoneNumber': instance.phoneNumber,
      'email': instance.email,
      'nip': instance.nip,
    };
