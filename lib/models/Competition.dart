import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'annotations/field_converters.dart';

part 'Competition.g.dart';

@JsonSerializable()
class Competition {
  String competitionId;
  String name;

  @JsonKey(fromJson: fromFirestoreDate, toJson: toFirestoreDate)
  DateTime competitionDate;
  String address;
  String description;
  List<String> applicationIds;

  Competition({
    this.competitionId,
    this.name,
    this.competitionDate,
    this.address,
    this.description,
    this.applicationIds,
  });

  factory Competition.fromMap(Map map) => _$CompetitionFromJson(map);
  Map<String, dynamic> toMap() => _$CompetitionToJson(this);

  factory Competition.fromFirebase(DocumentSnapshot doc) {
    Map map = doc.data();
    map["competitionId"] = doc.id;
    return Competition.fromMap(map);
  }
}
