// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Chatroom.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Chatroom _$ChatroomFromJson(Map<String, dynamic> json) {
  return Chatroom(
    chatroomId: json['chatroomId'] as String,
    name: json['name'] as String,
    messageIds: (json['messageIds'] as List)?.map((e) => e as String)?.toList(),
    userIds: (json['userIds'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$ChatroomToJson(Chatroom instance) => <String, dynamic>{
      'chatroomId': instance.chatroomId,
      'name': instance.name,
      'userIds': instance.userIds,
      'messageIds': instance.messageIds,
    };
