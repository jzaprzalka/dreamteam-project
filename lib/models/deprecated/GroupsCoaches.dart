import 'package:cloud_firestore/cloud_firestore.dart';

@deprecated
class GroupsCoaches {
  int groupId;
  int coachId;

  GroupsCoaches({this.groupId, this.coachId});

  factory GroupsCoaches.fromMap(Map map) {
    return GroupsCoaches(groupId: map["groupId"], coachId: map["coachId"]);
  }

  factory GroupsCoaches.fromFirebase(DocumentSnapshot doc) {
    Map map = doc.data();
    return GroupsCoaches.fromMap(map);
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map["groupId"] = groupId;
    map["coachId"] = coachId;
    return map;
  }
}
