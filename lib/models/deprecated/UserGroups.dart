import 'package:cloud_firestore/cloud_firestore.dart';

@deprecated
class UserGroups {
  int groupId;
  int userId;

  UserGroups({this.groupId, this.userId});

  factory UserGroups.fromMap(Map map) {
    return UserGroups(groupId: map["groupId"], userId: map["userId"]);
  }

  factory UserGroups.fromFirebase(DocumentSnapshot doc) {
    Map map = doc.data();
    return UserGroups.fromMap(map);
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map["groupId"] = groupId;
    map["userId"] = userId;
    return map;
  }
}
