import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'Attendance.dart';
import 'Group.dart';
import 'Room.dart';
import 'User.dart';
import 'annotations/field_converters.dart';

part 'Schedule.g.dart';

@JsonSerializable()
class Schedule {
  String scheduleId;
  String groupId;

  @JsonKey(ignore: true)
  Group group;
  List<String> coachIds;

  @JsonKey(ignore: true)
  List<User> coaches;
  String classSubject;
  String roomId;

  @JsonKey(ignore: true)
  Room room;

  @JsonKey(fromJson: fromFirestoreDate, toJson: toFirestoreDate)
  DateTime dateStart;

  @JsonKey(fromJson: fromFirestoreDate, toJson: toFirestoreDate)
  DateTime dateEnd;

  @JsonKey(
    fromJson: fromFirestoreAttendanceList,
    toJson: toFirestoreAttendanceList,
  )
  List<Attendance> attendance;

  Schedule({
    this.scheduleId,
    this.groupId,
    this.coachIds,
    this.coaches,
    this.classSubject,
    this.roomId,
    this.dateStart,
    this.dateEnd,
    this.attendance,
  });

  factory Schedule.fromMap(Map map) => _$ScheduleFromJson(map);
  Map<String, dynamic> toMap() => _$ScheduleToJson(this);

  factory Schedule.fromFirebase(DocumentSnapshot doc) {
    Map map = doc.data();
    map["scheduleId"] = doc.id;
    Schedule s = Schedule.fromMap(map);
    for (var a in s.attendance ?? []) {
      a.scheduleId = doc.id;
    }
    return s;
  }
}
