import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'annotations/field_converters.dart';

part 'Event.g.dart';

@JsonSerializable()
class Event {
  String eventId;
  String name;

  @JsonKey(fromJson: fromFirestoreDate, toJson: toFirestoreDate)
  DateTime date;
  String description;
  String address;

  Event({
    this.eventId,
    this.name,
    this.date,
    this.description,
    this.address,
  });

  factory Event.fromMap(Map map) => _$EventFromJson(map);
  Map<String, dynamic> toMap() => _$EventToJson(this);

  factory Event.fromFirebase(DocumentSnapshot doc) {
    Map map = doc.data();
    map["eventId"] = doc.id;
    return Event.fromMap(map);
  }
}
