// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Event.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Event _$EventFromJson(Map<String, dynamic> json) {
  return Event(
    eventId: json['eventId'] as String,
    name: json['name'] as String,
    date: fromFirestoreDate(json['date']),
    description: json['description'] as String,
    address: json['address'] as String,
  );
}

Map<String, dynamic> _$EventToJson(Event instance) => <String, dynamic>{
      'eventId': instance.eventId,
      'name': instance.name,
      'date': toFirestoreDate(instance.date),
      'description': instance.description,
      'address': instance.address,
    };
