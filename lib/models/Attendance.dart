import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'User.dart';
import 'annotations/field_converters.dart';

part 'Attendance.g.dart';

@JsonSerializable()
class Attendance {
  @JsonKey(ignore: true)
  String scheduleId;
  String userId;

  @JsonKey(ignore: true)
  User user;

  @JsonKey(fromJson: fromFirestoreDate, toJson: toFirestoreDate)
  DateTime dateModified;
  bool presence;

  Attendance({
    this.scheduleId,
    this.userId,
    this.dateModified,
    this.presence,
  });

  factory Attendance.fromMap(Map map) => _$AttendanceFromJson(map);
  Map<String, dynamic> toMap() => _$AttendanceToJson(this);
}
