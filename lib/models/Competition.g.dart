// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Competition.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Competition _$CompetitionFromJson(Map<String, dynamic> json) {
  return Competition(
    competitionId: json['competitionId'] as String,
    name: json['name'] as String,
    competitionDate: fromFirestoreDate(json['competitionDate']),
    address: json['address'] as String,
    description: json['description'] as String,
    applicationIds:
        (json['applicationIds'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$CompetitionToJson(Competition instance) =>
    <String, dynamic>{
      'competitionId': instance.competitionId,
      'name': instance.name,
      'competitionDate': toFirestoreDate(instance.competitionDate),
      'address': instance.address,
      'description': instance.description,
      'applicationIds': instance.applicationIds,
    };
