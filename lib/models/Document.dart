import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'Token.dart';
import 'annotations/field_converters.dart';

part 'Document.g.dart';

@JsonSerializable()
class Document {
  String documentId;
  String accessTokenId;
  @JsonKey(ignore: true)
  Token accessToken;

  @JsonKey(fromJson: fromFirestoreDate, toJson: toFirestoreDate)
  DateTime dateCreated;
  String type;
  String filePath;

  Document({
    this.documentId,
    this.accessTokenId,
    this.dateCreated,
    this.type,
    this.filePath,
  });

  factory Document.fromMap(Map map) => _$DocumentFromJson(map);
  Map<String, dynamic> toMap() => _$DocumentToJson(this);

  factory Document.fromFirebase(DocumentSnapshot doc) {
    Map map = doc.data();
    map["documentId"] = doc.id;
    return Document.fromMap(map);
  }
}
