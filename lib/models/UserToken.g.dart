// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'UserToken.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserToken _$UserTokenFromJson(Map<String, dynamic> json) {
  return UserToken(
    userId: json['userId'] as String,
    tokenId: json['tokenId'] as String,
    expirationDate: fromFirestoreDate(json['expirationDate']),
    userTokenId: json['userTokenId'] as String,
  );
}

Map<String, dynamic> _$UserTokenToJson(UserToken instance) {
  final val = <String, dynamic>{
    'userTokenId': instance.userTokenId,
    'userId': instance.userId,
    'tokenId': instance.tokenId,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('expirationDate', toFirestoreDate(instance.expirationDate));
  return val;
}
