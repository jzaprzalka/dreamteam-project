// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Dream Team`
  String get applicationName {
    return Intl.message(
      'Dream Team',
      name: 'applicationName',
      desc: '',
      args: [],
    );
  }

  /// `Email`
  String get email {
    return Intl.message(
      'Email',
      name: 'email',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get password {
    return Intl.message(
      'Password',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `Old Password`
  String get oldPass {
    return Intl.message(
      'Old Password',
      name: 'oldPass',
      desc: '',
      args: [],
    );
  }

  /// `New Password`
  String get newPass {
    return Intl.message(
      'New Password',
      name: 'newPass',
      desc: '',
      args: [],
    );
  }

  /// `Confirm Password`
  String get confirmPassword {
    return Intl.message(
      'Confirm Password',
      name: 'confirmPassword',
      desc: '',
      args: [],
    );
  }

  /// `Authentication Failed!`
  String get authFailed {
    return Intl.message(
      'Authentication Failed!',
      name: 'authFailed',
      desc: '',
      args: [],
    );
  }

  /// `Password and confirm password values don't match!`
  String get passDontMatch {
    return Intl.message(
      'Password and confirm password values don\'t match!',
      name: 'passDontMatch',
      desc: '',
      args: [],
    );
  }

  /// `Please provide a valid value for {field}`
  String invalidField(Object field) {
    return Intl.message(
      'Please provide a valid value for $field',
      name: 'invalidField',
      desc: '',
      args: [field],
    );
  }

  /// `Authentication required for {propertyName} change.\nPlease enter Email and Password`
  String authRequired(Object propertyName) {
    return Intl.message(
      'Authentication required for $propertyName change.\nPlease enter Email and Password',
      name: 'authRequired',
      desc: '',
      args: [propertyName],
    );
  }

  /// `Submit`
  String get submitBtn {
    return Intl.message(
      'Submit',
      name: 'submitBtn',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get cancelBtn {
    return Intl.message(
      'Cancel',
      name: 'cancelBtn',
      desc: '',
      args: [],
    );
  }

  /// `Log In`
  String get loginBtn {
    return Intl.message(
      'Log In',
      name: 'loginBtn',
      desc: '',
      args: [],
    );
  }

  /// `Log Out`
  String get logout {
    return Intl.message(
      'Log Out',
      name: 'logout',
      desc: '',
      args: [],
    );
  }

  /// `Forgot Password?`
  String get forgotPassword {
    return Intl.message(
      'Forgot Password?',
      name: 'forgotPassword',
      desc: '',
      args: [],
    );
  }

  /// `Register now!`
  String get register {
    return Intl.message(
      'Register now!',
      name: 'register',
      desc: '',
      args: [],
    );
  }

  /// `No Account?`
  String get noAccount {
    return Intl.message(
      'No Account?',
      name: 'noAccount',
      desc: '',
      args: [],
    );
  }

  /// `Server Error`
  String get serverError {
    return Intl.message(
      'Server Error',
      name: 'serverError',
      desc: '',
      args: [],
    );
  }

  /// `Un expected error, please contact support`
  String get unExpectedError {
    return Intl.message(
      'Un expected error, please contact support',
      name: 'unExpectedError',
      desc: '',
      args: [],
    );
  }

  /// `Invalid e-mail`
  String get invalidEmail {
    return Intl.message(
      'Invalid e-mail',
      name: 'invalidEmail',
      desc: '',
      args: [],
    );
  }

  /// `Invalid e-mail and password combination`
  String get invalidEmailAndPasswordCombination {
    return Intl.message(
      'Invalid e-mail and password combination',
      name: 'invalidEmailAndPasswordCombination',
      desc: '',
      args: [],
    );
  }

  /// `Name`
  String get name {
    return Intl.message(
      'Name',
      name: 'name',
      desc: '',
      args: [],
    );
  }

  /// `Surname`
  String get surname {
    return Intl.message(
      'Surname',
      name: 'surname',
      desc: '',
      args: [],
    );
  }

  /// `Birthdate`
  String get birthdate {
    return Intl.message(
      'Birthdate',
      name: 'birthdate',
      desc: '',
      args: [],
    );
  }

  /// `Phone Number`
  String get phoneNumber {
    return Intl.message(
      'Phone Number',
      name: 'phoneNumber',
      desc: '',
      args: [],
    );
  }

  /// `NIP`
  String get nip {
    return Intl.message(
      'NIP',
      name: 'nip',
      desc: '',
      args: [],
    );
  }

  /// `<No value provided>`
  String get no_value {
    return Intl.message(
      '<No value provided>',
      name: 'no_value',
      desc: '',
      args: [],
    );
  }

  /// `Click edit icon to edit password`
  String get edit_password {
    return Intl.message(
      'Click edit icon to edit password',
      name: 'edit_password',
      desc: '',
      args: [],
    );
  }

  /// `Access Tokens`
  String get accessTokens {
    return Intl.message(
      'Access Tokens',
      name: 'accessTokens',
      desc: '',
      args: [],
    );
  }

  /// `Overview`
  String get Overview {
    return Intl.message(
      'Overview',
      name: 'Overview',
      desc: '',
      args: [],
    );
  }

  /// `Calendar`
  String get Calendar {
    return Intl.message(
      'Calendar',
      name: 'Calendar',
      desc: '',
      args: [],
    );
  }

  /// `Groups`
  String get Groups {
    return Intl.message(
      'Groups',
      name: 'Groups',
      desc: '',
      args: [],
    );
  }

  /// `Payments`
  String get Payments {
    return Intl.message(
      'Payments',
      name: 'Payments',
      desc: '',
      args: [],
    );
  }

  /// `Profile`
  String get Profil {
    return Intl.message(
      'Profile',
      name: 'Profil',
      desc: '',
      args: [],
    );
  }

  /// `More`
  String get More {
    return Intl.message(
      'More',
      name: 'More',
      desc: '',
      args: [],
    );
  }

  /// `Month`
  String get Month {
    return Intl.message(
      'Month',
      name: 'Month',
      desc: '',
      args: [],
    );
  }

  /// `2 weeks`
  String get TwoWeeks {
    return Intl.message(
      '2 weeks',
      name: 'TwoWeeks',
      desc: '',
      args: [],
    );
  }

  /// `Week`
  String get Week {
    return Intl.message(
      'Week',
      name: 'Week',
      desc: '',
      args: [],
    );
  }

  /// `Add group`
  String get addGroup {
    return Intl.message(
      'Add group',
      name: 'addGroup',
      desc: '',
      args: [],
    );
  }

  /// `Group {name} added successfully`
  String addGroupSuccess(Object name) {
    return Intl.message(
      'Group $name added successfully',
      name: 'addGroupSuccess',
      desc: '',
      args: [name],
    );
  }

  /// `Payment for {forWhom} with recurring {repeatMode} added/updated successfully`
  String addPaymentSuccess(Object forWhom, Object repeatMode) {
    return Intl.message(
      'Payment for $forWhom with recurring $repeatMode added/updated successfully',
      name: 'addPaymentSuccess',
      desc: '',
      args: [forWhom, repeatMode],
    );
  }

  /// `Cannot add/update payment. Please fill all the fields`
  String get addPaymentFailed {
    return Intl.message(
      'Cannot add/update payment. Please fill all the fields',
      name: 'addPaymentFailed',
      desc: '',
      args: [],
    );
  }

  /// `Group name`
  String get groupName {
    return Intl.message(
      'Group name',
      name: 'groupName',
      desc: '',
      args: [],
    );
  }

  /// `Add person`
  String get addPerson {
    return Intl.message(
      'Add person',
      name: 'addPerson',
      desc: '',
      args: [],
    );
  }

  /// `Add`
  String get add {
    return Intl.message(
      'Add',
      name: 'add',
      desc: '',
      args: [],
    );
  }

  /// `Students added`
  String get studentsAdded {
    return Intl.message(
      'Students added',
      name: 'studentsAdded',
      desc: '',
      args: [],
    );
  }

  /// `Edit group`
  String get editGroup {
    return Intl.message(
      'Edit group',
      name: 'editGroup',
      desc: '',
      args: [],
    );
  }

  /// `Delete group`
  String get deleteGroup {
    return Intl.message(
      'Delete group',
      name: 'deleteGroup',
      desc: '',
      args: [],
    );
  }

  /// `Delete person`
  String get deletePerson {
    return Intl.message(
      'Delete person',
      name: 'deletePerson',
      desc: '',
      args: [],
    );
  }

  /// `Delete`
  String get delete {
    return Intl.message(
      'Delete',
      name: 'delete',
      desc: '',
      args: [],
    );
  }

  /// `Payments Paid`
  String get paymentsPaid {
    return Intl.message(
      'Payments Paid',
      name: 'paymentsPaid',
      desc: '',
      args: [],
    );
  }

  /// `Payments Past Due`
  String get paymentsPastDue {
    return Intl.message(
      'Payments Past Due',
      name: 'paymentsPastDue',
      desc: '',
      args: [],
    );
  }

  /// `Payments Approaching Due`
  String get paymentsApproachingDue {
    return Intl.message(
      'Payments Approaching Due',
      name: 'paymentsApproachingDue',
      desc: '',
      args: [],
    );
  }

  /// `Time due`
  String get timeDue {
    return Intl.message(
      'Time due',
      name: 'timeDue',
      desc: '',
      args: [],
    );
  }

  /// `Time paid`
  String get timePaid {
    return Intl.message(
      'Time paid',
      name: 'timePaid',
      desc: '',
      args: [],
    );
  }

  /// `More...`
  String get more {
    return Intl.message(
      'More...',
      name: 'more',
      desc: '',
      args: [],
    );
  }

  /// `Upcominig classes`
  String get UpcominigClasses {
    return Intl.message(
      'Upcominig classes',
      name: 'UpcominigClasses',
      desc: '',
      args: [],
    );
  }

  /// `My groups`
  String get MyGroups {
    return Intl.message(
      'My groups',
      name: 'MyGroups',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get Settings {
    return Intl.message(
      'Settings',
      name: 'Settings',
      desc: '',
      args: [],
    );
  }

  /// `Messages`
  String get Messages {
    return Intl.message(
      'Messages',
      name: 'Messages',
      desc: '',
      args: [],
    );
  }

  /// `Posts`
  String get Posts {
    return Intl.message(
      'Posts',
      name: 'Posts',
      desc: '',
      args: [],
    );
  }

  /// `Coach View`
  String get Coach_View {
    return Intl.message(
      'Coach View',
      name: 'Coach_View',
      desc: '',
      args: [],
    );
  }

  /// `Student View`
  String get Student_View {
    return Intl.message(
      'Student View',
      name: 'Student_View',
      desc: '',
      args: [],
    );
  }

  /// `Admin View`
  String get Admin_View {
    return Intl.message(
      'Admin View',
      name: 'Admin_View',
      desc: '',
      args: [],
    );
  }

  /// `Coaches`
  String get coaches {
    return Intl.message(
      'Coaches',
      name: 'coaches',
      desc: '',
      args: [],
    );
  }

  /// `Participants`
  String get participants {
    return Intl.message(
      'Participants',
      name: 'participants',
      desc: '',
      args: [],
    );
  }

  /// `Nothing to show`
  String get nothingToShow {
    return Intl.message(
      'Nothing to show',
      name: 'nothingToShow',
      desc: '',
      args: [],
    );
  }

  /// `Loading group data...`
  String get loadingGroup {
    return Intl.message(
      'Loading group data...',
      name: 'loadingGroup',
      desc: '',
      args: [],
    );
  }

  /// `Loading data from server...`
  String get loading {
    return Intl.message(
      'Loading data from server...',
      name: 'loading',
      desc: '',
      args: [],
    );
  }

  /// `Loading...`
  String get loadingShort {
    return Intl.message(
      'Loading...',
      name: 'loadingShort',
      desc: '',
      args: [],
    );
  }

  /// `Successfully added a new announcement`
  String get addAnnouncementSuccess {
    return Intl.message(
      'Successfully added a new announcement',
      name: 'addAnnouncementSuccess',
      desc: '',
      args: [],
    );
  }

  /// `Announcement`
  String get post {
    return Intl.message(
      'Announcement',
      name: 'post',
      desc: '',
      args: [],
    );
  }

  /// `Enter announcement...`
  String get enterAnnouncement {
    return Intl.message(
      'Enter announcement...',
      name: 'enterAnnouncement',
      desc: '',
      args: [],
    );
  }

  /// `Date`
  String get date {
    return Intl.message(
      'Date',
      name: 'date',
      desc: '',
      args: [],
    );
  }

  /// `Access Token #{id}`
  String accessTokenN(Object id) {
    return Intl.message(
      'Access Token #$id',
      name: 'accessTokenN',
      desc: '',
      args: [id],
    );
  }

  /// `Expires on {date}`
  String expiresOn(Object date) {
    return Intl.message(
      'Expires on $date',
      name: 'expiresOn',
      desc: '',
      args: [date],
    );
  }

  /// `User Details`
  String get userDetails {
    return Intl.message(
      'User Details',
      name: 'userDetails',
      desc: '',
      args: [],
    );
  }

  /// `Added coaches`
  String get coachesAdded {
    return Intl.message(
      'Added coaches',
      name: 'coachesAdded',
      desc: '',
      args: [],
    );
  }

  /// `Add coach`
  String get addCoach {
    return Intl.message(
      'Add coach',
      name: 'addCoach',
      desc: '',
      args: [],
    );
  }

  /// `Delete coach`
  String get deleteCoach {
    return Intl.message(
      'Delete coach',
      name: 'deleteCoach',
      desc: '',
      args: [],
    );
  }

  /// `Add Token`
  String get addToken {
    return Intl.message(
      'Add Token',
      name: 'addToken',
      desc: '',
      args: [],
    );
  }

  /// `Token Name`
  String get tokenName {
    return Intl.message(
      'Token Name',
      name: 'tokenName',
      desc: '',
      args: [],
    );
  }

  /// `Enter token name...`
  String get enterTokenName {
    return Intl.message(
      'Enter token name...',
      name: 'enterTokenName',
      desc: '',
      args: [],
    );
  }

  /// `Access to:`
  String get accessTo {
    return Intl.message(
      'Access to:',
      name: 'accessTo',
      desc: '',
      args: [],
    );
  }

  /// `Edit Token`
  String get editToken {
    return Intl.message(
      'Edit Token',
      name: 'editToken',
      desc: '',
      args: [],
    );
  }

  /// `Token List`
  String get tokenList {
    return Intl.message(
      'Token List',
      name: 'tokenList',
      desc: '',
      args: [],
    );
  }

  /// `User Token List`
  String get userTokenList {
    return Intl.message(
      'User Token List',
      name: 'userTokenList',
      desc: '',
      args: [],
    );
  }

  /// `Edit User Token`
  String get editUserToken {
    return Intl.message(
      'Edit User Token',
      name: 'editUserToken',
      desc: '',
      args: [],
    );
  }

  /// `Expire date`
  String get expireDate {
    return Intl.message(
      'Expire date',
      name: 'expireDate',
      desc: '',
      args: [],
    );
  }

  /// `Change expire date`
  String get changeExpireDate {
    return Intl.message(
      'Change expire date',
      name: 'changeExpireDate',
      desc: '',
      args: [],
    );
  }

  /// `Message...`
  String get Message3dot {
    return Intl.message(
      'Message...',
      name: 'Message3dot',
      desc: '',
      args: [],
    );
  }

  /// `Students payment list`
  String get studentsPaymentsList {
    return Intl.message(
      'Students payment list',
      name: 'studentsPaymentsList',
      desc: '',
      args: [],
    );
  }

  /// `Groups payments list`
  String get groupsPaymentsList {
    return Intl.message(
      'Groups payments list',
      name: 'groupsPaymentsList',
      desc: '',
      args: [],
    );
  }

  /// `Student payment list`
  String get studentPaymentsList {
    return Intl.message(
      'Student payment list',
      name: 'studentPaymentsList',
      desc: '',
      args: [],
    );
  }

  /// `History`
  String get history {
    return Intl.message(
      'History',
      name: 'history',
      desc: '',
      args: [],
    );
  }

  /// `No`
  String get no {
    return Intl.message(
      'No',
      name: 'no',
      desc: '',
      args: [],
    );
  }

  /// `Yes`
  String get yes {
    return Intl.message(
      'Yes',
      name: 'yes',
      desc: '',
      args: [],
    );
  }

  /// `Account Payment`
  String get accountPayment {
    return Intl.message(
      'Account Payment',
      name: 'accountPayment',
      desc: '',
      args: [],
    );
  }

  /// `Would you like to account payment?`
  String get accountQuestion {
    return Intl.message(
      'Would you like to account payment?',
      name: 'accountQuestion',
      desc: '',
      args: [],
    );
  }

  /// `Don't repeat`
  String get dontRepeat {
    return Intl.message(
      'Don\'t repeat',
      name: 'dontRepeat',
      desc: '',
      args: [],
    );
  }

  /// `Add student payment`
  String get addStudentPayment {
    return Intl.message(
      'Add student payment',
      name: 'addStudentPayment',
      desc: '',
      args: [],
    );
  }

  /// `Payment name`
  String get paymentName {
    return Intl.message(
      'Payment name',
      name: 'paymentName',
      desc: '',
      args: [],
    );
  }

  /// `Enter payment name...`
  String get enterPaymentName {
    return Intl.message(
      'Enter payment name...',
      name: 'enterPaymentName',
      desc: '',
      args: [],
    );
  }

  /// `Payment amount`
  String get paymentAmount {
    return Intl.message(
      'Payment amount',
      name: 'paymentAmount',
      desc: '',
      args: [],
    );
  }

  /// `Enter payment amount...`
  String get enterPaymentAmount {
    return Intl.message(
      'Enter payment amount...',
      name: 'enterPaymentAmount',
      desc: '',
      args: [],
    );
  }

  /// `Payment deadline`
  String get paymentDeadline {
    return Intl.message(
      'Payment deadline',
      name: 'paymentDeadline',
      desc: '',
      args: [],
    );
  }

  /// `Change payment deadline`
  String get changePaymentDeadline {
    return Intl.message(
      'Change payment deadline',
      name: 'changePaymentDeadline',
      desc: '',
      args: [],
    );
  }

  /// `Repeat payment?`
  String get repeatPayment {
    return Intl.message(
      'Repeat payment?',
      name: 'repeatPayment',
      desc: '',
      args: [],
    );
  }

  /// `Repeat every week`
  String get repeatEveryWeek {
    return Intl.message(
      'Repeat every week',
      name: 'repeatEveryWeek',
      desc: '',
      args: [],
    );
  }

  /// `Repeat every month`
  String get repeatEveryMonth {
    return Intl.message(
      'Repeat every month',
      name: 'repeatEveryMonth',
      desc: '',
      args: [],
    );
  }

  /// `Is payment accounted?`
  String get isAccounted {
    return Intl.message(
      'Is payment accounted?',
      name: 'isAccounted',
      desc: '',
      args: [],
    );
  }

  /// `Accounted payment`
  String get accounted {
    return Intl.message(
      'Accounted payment',
      name: 'accounted',
      desc: '',
      args: [],
    );
  }

  /// `Add group payment`
  String get addGroupPayment {
    return Intl.message(
      'Add group payment',
      name: 'addGroupPayment',
      desc: '',
      args: [],
    );
  }

  /// `Chatroom list`
  String get chatroomList {
    return Intl.message(
      'Chatroom list',
      name: 'chatroomList',
      desc: '',
      args: [],
    );
  }

  /// `Chatroom name`
  String get chatName {
    return Intl.message(
      'Chatroom name',
      name: 'chatName',
      desc: '',
      args: [],
    );
  }

  /// `Enter chatroom name...`
  String get enterChatName {
    return Intl.message(
      'Enter chatroom name...',
      name: 'enterChatName',
      desc: '',
      args: [],
    );
  }

  /// `Select people`
  String get addPeople {
    return Intl.message(
      'Select people',
      name: 'addPeople',
      desc: '',
      args: [],
    );
  }

  /// `First class`
  String get firstClass {
    return Intl.message(
      'First class',
      name: 'firstClass',
      desc: '',
      args: [],
    );
  }

  /// `Change the date of classes`
  String get changeSchedule {
    return Intl.message(
      'Change the date of classes',
      name: 'changeSchedule',
      desc: '',
      args: [],
    );
  }

  /// `Change class time`
  String get changeTime {
    return Intl.message(
      'Change class time',
      name: 'changeTime',
      desc: '',
      args: [],
    );
  }

  /// `Repeat?`
  String get repeat {
    return Intl.message(
      'Repeat?',
      name: 'repeat',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'pl', countryCode: 'PL'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}