// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static m0(id) => "Access Token #${id}";

  static m1(name) => "Group ${name} added successfully";

  static m2(forWhom, repeatMode) => "Payment for ${forWhom} with recurring ${repeatMode} added/updated successfully";

  static m3(propertyName) => "Authentication required for ${propertyName} change.\nPlease enter Email and Password";

  static m4(date) => "Expires on ${date}";

  static m5(field) => "Please provide a valid value for ${field}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "Admin_View" : MessageLookupByLibrary.simpleMessage("Admin View"),
    "Calendar" : MessageLookupByLibrary.simpleMessage("Calendar"),
    "Coach_View" : MessageLookupByLibrary.simpleMessage("Coach View"),
    "Groups" : MessageLookupByLibrary.simpleMessage("Groups"),
    "Message3dot" : MessageLookupByLibrary.simpleMessage("Message..."),
    "Messages" : MessageLookupByLibrary.simpleMessage("Messages"),
    "Month" : MessageLookupByLibrary.simpleMessage("Month"),
    "More" : MessageLookupByLibrary.simpleMessage("More"),
    "MyGroups" : MessageLookupByLibrary.simpleMessage("My groups"),
    "Overview" : MessageLookupByLibrary.simpleMessage("Overview"),
    "Payments" : MessageLookupByLibrary.simpleMessage("Payments"),
    "Posts" : MessageLookupByLibrary.simpleMessage("Posts"),
    "Profil" : MessageLookupByLibrary.simpleMessage("Profile"),
    "Settings" : MessageLookupByLibrary.simpleMessage("Settings"),
    "Student_View" : MessageLookupByLibrary.simpleMessage("Student View"),
    "TwoWeeks" : MessageLookupByLibrary.simpleMessage("2 weeks"),
    "UpcominigClasses" : MessageLookupByLibrary.simpleMessage("Upcominig classes"),
    "Week" : MessageLookupByLibrary.simpleMessage("Week"),
    "accessTo" : MessageLookupByLibrary.simpleMessage("Access to:"),
    "accessTokenN" : m0,
    "accessTokens" : MessageLookupByLibrary.simpleMessage("Access Tokens"),
    "accountPayment" : MessageLookupByLibrary.simpleMessage("Account Payment"),
    "accountQuestion" : MessageLookupByLibrary.simpleMessage("Would you like to account payment?"),
    "accounted" : MessageLookupByLibrary.simpleMessage("Accounted payment"),
    "add" : MessageLookupByLibrary.simpleMessage("Add"),
    "addAnnouncementSuccess" : MessageLookupByLibrary.simpleMessage("Successfully added a new announcement"),
    "addCoach" : MessageLookupByLibrary.simpleMessage("Add coach"),
    "addGroup" : MessageLookupByLibrary.simpleMessage("Add group"),
    "addGroupPayment" : MessageLookupByLibrary.simpleMessage("Add group payment"),
    "addGroupSuccess" : m1,
    "addPaymentFailed" : MessageLookupByLibrary.simpleMessage("Cannot add/update payment. Please fill all the fields"),
    "addPaymentSuccess" : m2,
    "addPeople" : MessageLookupByLibrary.simpleMessage("Select people"),
    "addPerson" : MessageLookupByLibrary.simpleMessage("Add person"),
    "addStudentPayment" : MessageLookupByLibrary.simpleMessage("Add student payment"),
    "addToken" : MessageLookupByLibrary.simpleMessage("Add Token"),
    "applicationName" : MessageLookupByLibrary.simpleMessage("Dream Team"),
    "authFailed" : MessageLookupByLibrary.simpleMessage("Authentication Failed!"),
    "authRequired" : m3,
    "birthdate" : MessageLookupByLibrary.simpleMessage("Birthdate"),
    "cancelBtn" : MessageLookupByLibrary.simpleMessage("Cancel"),
    "changeExpireDate" : MessageLookupByLibrary.simpleMessage("Change expire date"),
    "changePaymentDeadline" : MessageLookupByLibrary.simpleMessage("Change payment deadline"),
    "changeSchedule" : MessageLookupByLibrary.simpleMessage("Change the date of classes"),
    "changeTime" : MessageLookupByLibrary.simpleMessage("Change class time"),
    "chatName" : MessageLookupByLibrary.simpleMessage("Chatroom name"),
    "chatroomList" : MessageLookupByLibrary.simpleMessage("Chatroom list"),
    "coaches" : MessageLookupByLibrary.simpleMessage("Coaches"),
    "coachesAdded" : MessageLookupByLibrary.simpleMessage("Added coaches"),
    "confirmPassword" : MessageLookupByLibrary.simpleMessage("Confirm Password"),
    "date" : MessageLookupByLibrary.simpleMessage("Date"),
    "delete" : MessageLookupByLibrary.simpleMessage("Delete"),
    "deleteCoach" : MessageLookupByLibrary.simpleMessage("Delete coach"),
    "deleteGroup" : MessageLookupByLibrary.simpleMessage("Delete group"),
    "deletePerson" : MessageLookupByLibrary.simpleMessage("Delete person"),
    "dontRepeat" : MessageLookupByLibrary.simpleMessage("Don\'t repeat"),
    "editGroup" : MessageLookupByLibrary.simpleMessage("Edit group"),
    "editToken" : MessageLookupByLibrary.simpleMessage("Edit Token"),
    "editUserToken" : MessageLookupByLibrary.simpleMessage("Edit User Token"),
    "edit_password" : MessageLookupByLibrary.simpleMessage("Click edit icon to edit password"),
    "email" : MessageLookupByLibrary.simpleMessage("Email"),
    "enterAnnouncement" : MessageLookupByLibrary.simpleMessage("Enter announcement..."),
    "enterChatName" : MessageLookupByLibrary.simpleMessage("Enter chatroom name..."),
    "enterPaymentAmount" : MessageLookupByLibrary.simpleMessage("Enter payment amount..."),
    "enterPaymentName" : MessageLookupByLibrary.simpleMessage("Enter payment name..."),
    "enterTokenName" : MessageLookupByLibrary.simpleMessage("Enter token name..."),
    "expireDate" : MessageLookupByLibrary.simpleMessage("Expire date"),
    "expiresOn" : m4,
    "firstClass" : MessageLookupByLibrary.simpleMessage("First class"),
    "forgotPassword" : MessageLookupByLibrary.simpleMessage("Forgot Password?"),
    "groupName" : MessageLookupByLibrary.simpleMessage("Group name"),
    "groupsPaymentsList" : MessageLookupByLibrary.simpleMessage("Groups payments list"),
    "history" : MessageLookupByLibrary.simpleMessage("History"),
    "invalidEmail" : MessageLookupByLibrary.simpleMessage("Invalid e-mail"),
    "invalidEmailAndPasswordCombination" : MessageLookupByLibrary.simpleMessage("Invalid e-mail and password combination"),
    "invalidField" : m5,
    "isAccounted" : MessageLookupByLibrary.simpleMessage("Is payment accounted?"),
    "loading" : MessageLookupByLibrary.simpleMessage("Loading data from server..."),
    "loadingGroup" : MessageLookupByLibrary.simpleMessage("Loading group data..."),
    "loadingShort" : MessageLookupByLibrary.simpleMessage("Loading..."),
    "loginBtn" : MessageLookupByLibrary.simpleMessage("Log In"),
    "logout" : MessageLookupByLibrary.simpleMessage("Log Out"),
    "more" : MessageLookupByLibrary.simpleMessage("More..."),
    "name" : MessageLookupByLibrary.simpleMessage("Name"),
    "newPass" : MessageLookupByLibrary.simpleMessage("New Password"),
    "nip" : MessageLookupByLibrary.simpleMessage("NIP"),
    "no" : MessageLookupByLibrary.simpleMessage("No"),
    "noAccount" : MessageLookupByLibrary.simpleMessage("No Account?"),
    "no_value" : MessageLookupByLibrary.simpleMessage("<No value provided>"),
    "nothingToShow" : MessageLookupByLibrary.simpleMessage("Nothing to show"),
    "oldPass" : MessageLookupByLibrary.simpleMessage("Old Password"),
    "participants" : MessageLookupByLibrary.simpleMessage("Participants"),
    "passDontMatch" : MessageLookupByLibrary.simpleMessage("Password and confirm password values don\'t match!"),
    "password" : MessageLookupByLibrary.simpleMessage("Password"),
    "paymentAmount" : MessageLookupByLibrary.simpleMessage("Payment amount"),
    "paymentDeadline" : MessageLookupByLibrary.simpleMessage("Payment deadline"),
    "paymentName" : MessageLookupByLibrary.simpleMessage("Payment name"),
    "paymentsApproachingDue" : MessageLookupByLibrary.simpleMessage("Payments Approaching Due"),
    "paymentsPaid" : MessageLookupByLibrary.simpleMessage("Payments Paid"),
    "paymentsPastDue" : MessageLookupByLibrary.simpleMessage("Payments Past Due"),
    "phoneNumber" : MessageLookupByLibrary.simpleMessage("Phone Number"),
    "post" : MessageLookupByLibrary.simpleMessage("Announcement"),
    "register" : MessageLookupByLibrary.simpleMessage("Register now!"),
    "repeat" : MessageLookupByLibrary.simpleMessage("Repeat?"),
    "repeatEveryMonth" : MessageLookupByLibrary.simpleMessage("Repeat every month"),
    "repeatEveryWeek" : MessageLookupByLibrary.simpleMessage("Repeat every week"),
    "repeatPayment" : MessageLookupByLibrary.simpleMessage("Repeat payment?"),
    "serverError" : MessageLookupByLibrary.simpleMessage("Server Error"),
    "studentPaymentsList" : MessageLookupByLibrary.simpleMessage("Student payment list"),
    "studentsAdded" : MessageLookupByLibrary.simpleMessage("Students added"),
    "studentsPaymentsList" : MessageLookupByLibrary.simpleMessage("Students payment list"),
    "submitBtn" : MessageLookupByLibrary.simpleMessage("Submit"),
    "surname" : MessageLookupByLibrary.simpleMessage("Surname"),
    "timeDue" : MessageLookupByLibrary.simpleMessage("Time due"),
    "timePaid" : MessageLookupByLibrary.simpleMessage("Time paid"),
    "tokenList" : MessageLookupByLibrary.simpleMessage("Token List"),
    "tokenName" : MessageLookupByLibrary.simpleMessage("Token Name"),
    "unExpectedError" : MessageLookupByLibrary.simpleMessage("Un expected error, please contact support"),
    "userDetails" : MessageLookupByLibrary.simpleMessage("User Details"),
    "userTokenList" : MessageLookupByLibrary.simpleMessage("User Token List"),
    "yes" : MessageLookupByLibrary.simpleMessage("Yes")
  };
}
