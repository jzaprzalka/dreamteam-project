// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a pl_PL locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'pl_PL';

  static m0(id) => "Token dostępowy #${id}";

  static m1(name) => "Pomyślnie dodano grupę ${name}";

  static m2(forWhom, repeatMode) => "Płatność dla ${forWhom} powtarzająca się co [${repeatMode}] została pomyślnie dodana/zaktualizowana";

  static m3(propertyName) => "Wymagana autentykacja do zmiany pola ${propertyName}.\nWprowadź Email i Hasło";

  static m4(date) => "Ważny do ${date}";

  static m5(field) => "Wprowadź poprawną wartość dla ${field}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "Admin_View" : MessageLookupByLibrary.simpleMessage("Widok admina"),
    "Calendar" : MessageLookupByLibrary.simpleMessage("Kalendarz"),
    "Coach_View" : MessageLookupByLibrary.simpleMessage("Widok trenera"),
    "Groups" : MessageLookupByLibrary.simpleMessage("Grupy"),
    "Message3dot" : MessageLookupByLibrary.simpleMessage("Wiadomość..."),
    "Messages" : MessageLookupByLibrary.simpleMessage("Wiadomości"),
    "Month" : MessageLookupByLibrary.simpleMessage("Miesiąc"),
    "More" : MessageLookupByLibrary.simpleMessage("Więcej"),
    "MyGroups" : MessageLookupByLibrary.simpleMessage("Moje grupy"),
    "Overview" : MessageLookupByLibrary.simpleMessage("Przegląd"),
    "Payments" : MessageLookupByLibrary.simpleMessage("Płatności"),
    "Posts" : MessageLookupByLibrary.simpleMessage("Posty"),
    "Profil" : MessageLookupByLibrary.simpleMessage("Profil"),
    "Settings" : MessageLookupByLibrary.simpleMessage("Ustawienia"),
    "Student_View" : MessageLookupByLibrary.simpleMessage("Widok ucznia"),
    "TwoWeeks" : MessageLookupByLibrary.simpleMessage("2 tygodnie"),
    "UpcominigClasses" : MessageLookupByLibrary.simpleMessage("Nadchodzące zajęcia"),
    "Week" : MessageLookupByLibrary.simpleMessage("Tydzień"),
    "accessTo" : MessageLookupByLibrary.simpleMessage("Dostęp do:"),
    "accessTokenN" : m0,
    "accessTokens" : MessageLookupByLibrary.simpleMessage("Tokeny dostępowe"),
    "accountPayment" : MessageLookupByLibrary.simpleMessage("Zaksięgowanie płatności"),
    "accountQuestion" : MessageLookupByLibrary.simpleMessage("Czy chcesz rozliczyć płatność?"),
    "accounted" : MessageLookupByLibrary.simpleMessage("Płatność zaksięgowana"),
    "add" : MessageLookupByLibrary.simpleMessage("Dodaj"),
    "addAnnouncementSuccess" : MessageLookupByLibrary.simpleMessage("Pomyślnie dodano nowe ogłoszenie"),
    "addCoach" : MessageLookupByLibrary.simpleMessage("Dodaj trenera"),
    "addGroup" : MessageLookupByLibrary.simpleMessage("Dodaj grupę"),
    "addGroupPayment" : MessageLookupByLibrary.simpleMessage("Dodaj płatność do grupy"),
    "addGroupSuccess" : m1,
    "addPaymentFailed" : MessageLookupByLibrary.simpleMessage("Nie można dodać/zaktualizować płatności. Upewnij się że wszystkie pola są wypełnione poprawnie"),
    "addPaymentSuccess" : m2,
    "addPeople" : MessageLookupByLibrary.simpleMessage("Wybierz osoby"),
    "addPerson" : MessageLookupByLibrary.simpleMessage("Dodaj osobę"),
    "addStudentPayment" : MessageLookupByLibrary.simpleMessage("Dodaj płatność do studenta"),
    "addToken" : MessageLookupByLibrary.simpleMessage("Dodaj oken dostępowy"),
    "applicationName" : MessageLookupByLibrary.simpleMessage("Dream Team"),
    "authFailed" : MessageLookupByLibrary.simpleMessage("Autentykacja nie powiodła się!"),
    "authRequired" : m3,
    "birthdate" : MessageLookupByLibrary.simpleMessage("Data urodzenia"),
    "cancelBtn" : MessageLookupByLibrary.simpleMessage("Anuluj"),
    "changeExpireDate" : MessageLookupByLibrary.simpleMessage("Zmień datę ważności"),
    "changePaymentDeadline" : MessageLookupByLibrary.simpleMessage("Zmień termin ważności"),
    "changeSchedule" : MessageLookupByLibrary.simpleMessage("Zmień termin zajęć"),
    "changeTime" : MessageLookupByLibrary.simpleMessage("Zmień godzinę zajęć"),
    "chatName" : MessageLookupByLibrary.simpleMessage("Nazwa czatu"),
    "chatroomList" : MessageLookupByLibrary.simpleMessage("Lista czatów"),
    "coaches" : MessageLookupByLibrary.simpleMessage("Trenerzy"),
    "coachesAdded" : MessageLookupByLibrary.simpleMessage("Dodani trenerzy"),
    "confirmPassword" : MessageLookupByLibrary.simpleMessage("Potwierdź nowe hasło"),
    "date" : MessageLookupByLibrary.simpleMessage("Data"),
    "delete" : MessageLookupByLibrary.simpleMessage("Usuń"),
    "deleteCoach" : MessageLookupByLibrary.simpleMessage("Usuń trenera"),
    "deleteGroup" : MessageLookupByLibrary.simpleMessage("Usuń grupę"),
    "deletePerson" : MessageLookupByLibrary.simpleMessage("Usuń osobę"),
    "dontRepeat" : MessageLookupByLibrary.simpleMessage("Nie powtarzaj"),
    "editGroup" : MessageLookupByLibrary.simpleMessage("Edytuj grupę"),
    "editToken" : MessageLookupByLibrary.simpleMessage("Edytuj token dostępowy"),
    "editUserToken" : MessageLookupByLibrary.simpleMessage("Edytuj token dostępowy użytkownika"),
    "edit_password" : MessageLookupByLibrary.simpleMessage("Kliknij by zmienić hasło"),
    "email" : MessageLookupByLibrary.simpleMessage("Email"),
    "enterAnnouncement" : MessageLookupByLibrary.simpleMessage("Dodaj treść..."),
    "enterChatName" : MessageLookupByLibrary.simpleMessage("Podaj nazwę czatu..."),
    "enterPaymentAmount" : MessageLookupByLibrary.simpleMessage("Podaj kwotę płatności..."),
    "enterPaymentName" : MessageLookupByLibrary.simpleMessage("Podaj nazwę płatności..."),
    "enterTokenName" : MessageLookupByLibrary.simpleMessage("Podaj nazwę tokena dostępowego..."),
    "expireDate" : MessageLookupByLibrary.simpleMessage("Data ważności"),
    "expiresOn" : m4,
    "firstClass" : MessageLookupByLibrary.simpleMessage("Pierwsze zajęcia"),
    "forgotPassword" : MessageLookupByLibrary.simpleMessage("Zapomniałeś hasła?"),
    "groupName" : MessageLookupByLibrary.simpleMessage("Nazwa grupy"),
    "groupsPaymentsList" : MessageLookupByLibrary.simpleMessage("Lista płatności grup"),
    "history" : MessageLookupByLibrary.simpleMessage("Historia"),
    "invalidEmail" : MessageLookupByLibrary.simpleMessage("Niepoprawne e-mail"),
    "invalidEmailAndPasswordCombination" : MessageLookupByLibrary.simpleMessage("Niepoprawne e-mail lub hasło"),
    "invalidField" : m5,
    "isAccounted" : MessageLookupByLibrary.simpleMessage("Czy płatność jest zaksięgowana?"),
    "loading" : MessageLookupByLibrary.simpleMessage("Ładowanie danych z serwera..."),
    "loadingGroup" : MessageLookupByLibrary.simpleMessage("Ładowanie danych grupy..."),
    "loadingShort" : MessageLookupByLibrary.simpleMessage("Ładowanie..."),
    "loginBtn" : MessageLookupByLibrary.simpleMessage("Zaloguj się"),
    "logout" : MessageLookupByLibrary.simpleMessage("Wyloguj się"),
    "more" : MessageLookupByLibrary.simpleMessage("Więcej..."),
    "name" : MessageLookupByLibrary.simpleMessage("Imię"),
    "newPass" : MessageLookupByLibrary.simpleMessage("Nowe hasło"),
    "nip" : MessageLookupByLibrary.simpleMessage("NIP"),
    "no" : MessageLookupByLibrary.simpleMessage("Nie"),
    "noAccount" : MessageLookupByLibrary.simpleMessage("Nie masz jeszcze konta?"),
    "no_value" : MessageLookupByLibrary.simpleMessage("<Brak danych>"),
    "nothingToShow" : MessageLookupByLibrary.simpleMessage("Brak danych do wyświetlenia"),
    "oldPass" : MessageLookupByLibrary.simpleMessage("Stare hasło"),
    "participants" : MessageLookupByLibrary.simpleMessage("Kursanci"),
    "passDontMatch" : MessageLookupByLibrary.simpleMessage("Hasła się nie zgadzają!"),
    "password" : MessageLookupByLibrary.simpleMessage("Hasło"),
    "paymentAmount" : MessageLookupByLibrary.simpleMessage("Kwota płatności"),
    "paymentDeadline" : MessageLookupByLibrary.simpleMessage("Termin płatności"),
    "paymentName" : MessageLookupByLibrary.simpleMessage("Nazwa płatności"),
    "paymentsApproachingDue" : MessageLookupByLibrary.simpleMessage("Płatności ze zbliżającym się terminem zapłaty"),
    "paymentsPaid" : MessageLookupByLibrary.simpleMessage("Płatności opłacone"),
    "paymentsPastDue" : MessageLookupByLibrary.simpleMessage("Płatności do opłacenia (po terminie)"),
    "phoneNumber" : MessageLookupByLibrary.simpleMessage("Numer telefonu"),
    "post" : MessageLookupByLibrary.simpleMessage("Ogłoszenie"),
    "register" : MessageLookupByLibrary.simpleMessage("Zarejestruj się!"),
    "repeat" : MessageLookupByLibrary.simpleMessage("Powtarzać?"),
    "repeatEveryMonth" : MessageLookupByLibrary.simpleMessage("Powtarzaj co miesiąc"),
    "repeatEveryWeek" : MessageLookupByLibrary.simpleMessage("Powtarzaj co tydzień"),
    "repeatPayment" : MessageLookupByLibrary.simpleMessage("Powtarzać płatność?"),
    "serverError" : MessageLookupByLibrary.simpleMessage("Problemy z serwerem"),
    "studentPaymentsList" : MessageLookupByLibrary.simpleMessage("Lista płatności ucznia"),
    "studentsAdded" : MessageLookupByLibrary.simpleMessage("Dodano studentów"),
    "studentsPaymentsList" : MessageLookupByLibrary.simpleMessage("Lista płatności uczniów"),
    "submitBtn" : MessageLookupByLibrary.simpleMessage("Potwierdź"),
    "surname" : MessageLookupByLibrary.simpleMessage("Nazwisko"),
    "timeDue" : MessageLookupByLibrary.simpleMessage("Termin"),
    "timePaid" : MessageLookupByLibrary.simpleMessage("Opłacono"),
    "tokenList" : MessageLookupByLibrary.simpleMessage("Lista tokenów dostępowych"),
    "tokenName" : MessageLookupByLibrary.simpleMessage("Nazwa tokena dostępowego"),
    "unExpectedError" : MessageLookupByLibrary.simpleMessage("Nie oczekiwany bład, skontaktuj sie z supportem"),
    "userDetails" : MessageLookupByLibrary.simpleMessage("Szczegóły użytkownika"),
    "userTokenList" : MessageLookupByLibrary.simpleMessage("Lista tokenów dostępowych użytkownika"),
    "yes" : MessageLookupByLibrary.simpleMessage("Tak")
  };
}
