import 'package:dreamteam_project/exception/auth/auth_exception.dart';
import 'package:dreamteam_project/exception/core/base_exception.dart';
import 'package:dreamteam_project/providers/base_providers.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'base_providers.dart';

class AuthenticationProvider extends BaseAuthenticationProvider {
  final FirebaseAuth firebaseAuth;

  AuthenticationProvider({FirebaseAuth firebaseAuth})
      : firebaseAuth = firebaseAuth ?? FirebaseAuth.instance;

  @override
  Future<void> signOutUser() async {
    return await firebaseAuth.signOut();
  }

  @override
  Future<bool> signInUser(String email, String password) async {
    try {
      await firebaseAuth.signInWithEmailAndPassword(
          email: email, password: password);
      return true;
    } on FirebaseAuthException catch (e) {
      print(e.toString());
      if (e.code == 'wrong-password' || e.code == 'user-not-found') {
        throw InvalidEmailAndPasswordCombinationException();
      } else if (e.code == 'invalid-email') {
        throw InvalidEmailException();
      } else {
        throw ServerErrorException();
      }
    }
  }

  @override
  Future<User> getCurrentUser() async {
    return firebaseAuth.currentUser;
  }

  @override
  Future<bool> isLoggedIn() async {
    final user = firebaseAuth.currentUser;
    return user != null;
  }

  @override
  Future<void> updateEmail(
      String email, String password, String newEmail) async {
    if (await signInUser(email, password)) {
      final user = firebaseAuth.currentUser;
      user.updateEmail(newEmail);
    }
  }

  @override
  Future<void> updatePassword(
      String email, String password, String newPassword) async {
    if (await signInUser(email, password)) {
      final user = firebaseAuth.currentUser;
      user.updatePassword(newPassword);
    }
  }
}
