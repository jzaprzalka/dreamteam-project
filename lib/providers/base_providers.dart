import 'package:dreamteam_project/models/Announcement.dart';
import 'package:dreamteam_project/models/Attendance.dart';
import 'package:dreamteam_project/models/Chatroom.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/models/Messages.dart';
import 'package:dreamteam_project/models/Schedule.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:dreamteam_project/models/Token.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/models/UserToken.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase;

abstract class BaseAuthenticationProvider {
  Future<void> signInUser(String email, String password);
  Future<void> signOutUser();
  Future<firebase.User> getCurrentUser();
  Future<bool> isLoggedIn();
  Future<void> updateEmail(String email, String password, String newEmail);
  Future<void> updatePassword(
      String email, String password, String newPassword);
}

abstract class BaseUserDataProvider {
  Future<User> getUser(String userId);
  Future<Map<String, User>> getUsers(List<String> userIds);
  Stream<User> getUserStream(String userId);
  Future<void> updateName(String userId, String name);
  Future<void> updateSurname(String userId, String surname);
  Future<void> updatePhone(String userId, String phoneNumber);
}

abstract class BasePaymentsProvider {
  Future<List<Payment>> getPaymentsFor(String userId);
  Future<List<Payment>> getGroupPayments(Group group);
  Future<List<Payment>> getGroupsPayments(List<Group> groups);
  // Admin only!
  Future<List<Payment>> getAllPayments();
  Future<List<Payment>> getAllPaymentsPaid();
  Future<List<Payment>> getAllPaymentsPastDue();
  Future<List<Payment>> getAllPaymentsApproachingDue({int nDaysBefore = 10});
  // Paid
  Future<List<Payment>> getPaymentsPaid(String userId);
  Future<List<Payment>> getGroupsPaymentsPaid(List<Group> groups);
  // Past Due
  Future<List<Payment>> getPaymentsPastDue(String userId);
  Future<List<Payment>> getGroupsPaymentsPastDue(List<Group> groups);
  // Approaching due: N days prior to due (N = 10)
  Future<List<Payment>> getPaymentsApproachingDue(String userId,
      {int nDaysBefore = 10});
  Future<List<Payment>> getGroupsPaymentsApproachingDue(List<Group> groups,
      {int nDaysBefore = 10});
  // Modifing payments
  Future<void> accountPayment(Payment payment, DateTime dateAccounted);
  Future<void> addPayments(List<Payment> payment);
  Future<void> addPayment(Payment payment);
  Future<void> updatePayment(String paymentId,
      {DateTime newDueDate, String newName, int newAmount, bool isAccounted});
  Future<void> addPaymentForGroup(Payment payment, Group group);
}

abstract class BaseGroupDataProvider {
  Future<void> updateName(String groupId, String newName);
  Future<void> updateCoaches(String groupId, List<String> coachIds);
  Future<void> removeCoach(String groupId, String coachId);
  Future<void> addCoach(String groupId, String coachId);
  Future<Group> addGroup(
      String name, List<String> coachIds, List<String> userIds);
  Future<void> deleteGroup(String groupId);
  Future<Group> getGroup(String groupId);
  Future<List<Group>> getGroups(List<String> groupIds);
  Future<void> addUserToGroup(String groupId, String userId);
  Future<void> deleteUserFromGroup(String groupId, String userId);
  Future<List<Group>> getAllGroups();
  Future<List<User>> getAllUsers();
}

abstract class BaseScheduleProvider {
  Future<List<Schedule>> getSchedule(DateTime fromDate, DateTime toDate,
      {List<String> groupIds});
  Future<Schedule> addSchedule(Schedule schedule);
  Future<void> addSchedules(Schedule schedule, DateTime toDate);
  Future<void> updateCoaches(String scheduleId, List<String> coachIds);
  Future<void> updateRoom(String scheduleId, String roomId);
  Future<void> updateClassSubject(String scheduleId, String classSubject);
  Future<void> updateAttendance(String scheduleId, List<Attendance> attendance);
}

abstract class BaseTokenProvider {
  Future<bool> verifyToken(User user, Token token);
  Future<void> addUserToken(User user, Token token, DateTime expirationDate);
  Future<void> extendExpirationDate(UserToken userToken, Duration duration);
  Future<void> updateUserToken(UserToken userToken);
  Future<Token> addToken(String name);
  Future<void> editToken(Token token);
  Future<void> deleteToken(Token token);
  Future<List<UserToken>> getTokensForUser(User user);
  Stream<List<UserToken>> getTokensForUserStream(User user);
  Future<List<Token>> getAllTokens();
  Future<void> addUserTokenByUserToken(UserToken userToken);
}

abstract class BaseAnnouncementProvider {
  Future<Announcement> addAnnouncement(
      DateTime announcementDate, String announcement, List<String> groupIds);
  Future<void> deleteAnnouncement(String announcementId);
  Future<List<Announcement>> getAllAnnouncements();
  Future<List<Announcement>> getGroupsAnnouncements(List<String> groupIds);
  Future<Announcement> getAnnouncement(String announcementId);
}

abstract class BaseMessagesProvider {
  Future<void> addMessage(Message message, String chatroomId);
  Future<void> createChatroom(Chatroom chatroom);
  Future<void> deleteChatroom(String chatroomId);
  Future<void> updateChatroomParticipants(
      String chatroomId, List<String> participantIds);
  Future<List<Chatroom>> getAllChatrooms();
  Future<List<Message>> getMessagesForChatroom(String chatroomId);
  Future<Chatroom> getChatroom(String chatroomId);
  Stream<List<Message>> getMessagesForChatroomStream(String chatroomId);
  Future<List<Chatroom>> getUsersChatrooms(String userId);
}
