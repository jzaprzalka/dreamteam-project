import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dreamteam_project/models/UserToken.dart';

import 'package:dreamteam_project/models/User.dart';

import 'package:dreamteam_project/models/Token.dart';

import 'base_providers.dart';

class TokenProvider extends BaseTokenProvider {
  final FirebaseFirestore fireStoreDb;
  static const String TOKENS_COLLECTION_PATH = "tokens";
  static const String USERS_COLLECTION_PATH = "users";
  static const String USERS_TOKENS_COLLECTION_PATH = "userTokens";

  factory TokenProvider() =>
      TokenProvider.fromFirestore(FirebaseFirestore.instance);
  TokenProvider.fromFirestore(this.fireStoreDb);

  @override
  Future<Token> addToken(String name) async {
    DocumentReference createdToken = await fireStoreDb
        .collection(TOKENS_COLLECTION_PATH)
        .add(Token(name: name).toMap());
    return Token.fromFirebase(await createdToken.get());
  }

  @override
  Future<void> addUserToken(
      User user, Token token, DateTime expirationDate) async {
    // await fireStoreDb
    //     .collection(USERS_COLLECTION_PATH)
    //     .doc(user.userId)
    //     .update({
    //   'userTokens': FieldValue.arrayUnion([
    //     UserToken(
    //       userId: user.userId,
    //       tokenId: token.id,
    //       expirationDate: expirationDate,
    //     ).toMap()
    //   ])
    // });
    await fireStoreDb
        .collection(USERS_COLLECTION_PATH)
        .doc(user.userId)
        .collection(USERS_TOKENS_COLLECTION_PATH)
        .add(UserToken(
          userId: user.userId,
          tokenId: token.id,
          expirationDate: expirationDate,
        ).toMap());
  }

@override
  Future<void> addUserTokenByUserToken(
      UserToken userToken) async {
    await fireStoreDb
        .collection(USERS_COLLECTION_PATH)
        .doc(userToken.userId)
        .collection(USERS_TOKENS_COLLECTION_PATH)
        .add(userToken.toMap());
  }


  @override
  Future<bool> verifyToken(User user, Token token) async {
    final documentSnapshot = await fireStoreDb
        .collection(USERS_COLLECTION_PATH)
        .doc(user.userId)
        .collection(USERS_TOKENS_COLLECTION_PATH)
        .get();
    final userTokensData = documentSnapshot.docs;
    List<UserToken> userTokens =
        List.from(userTokensData.map((data) => UserToken.fromFirebase(data)));
    bool containsToken = !userTokens
        .indexWhere((userToken) => userToken.tokenId == token.id)
        .isNegative;
    return containsToken;
  }

  @override
  Future<void> extendExpirationDate(
      UserToken userToken, Duration duration) async {
    // final documentSnapshot = await fireStoreDb
    //     .collection(USERS_COLLECTION_PATH)
    //     .doc(userToken.userId)
    //     .collection(USERS_TOKENS_COLLECTION_PATH)
    //     .get();
    //final userTokensData = documentSnapshot.docs;
    userToken.expirationDate = userToken.expirationDate.add(duration);
    // userTokensData
    //     .removeWhere((element) => element["tokenId"] == userToken.tokenId);
    // userTokensData.add(userToken.toMap());
    await fireStoreDb.collection(USERS_COLLECTION_PATH)
        .doc(userToken.userId)
        .collection(USERS_TOKENS_COLLECTION_PATH)
        .doc(userToken.userTokenId)
        .update(userToken.toMap());
    // await fireStoreDb
    //     .collection(USERS_COLLECTION_PATH)
    //     .doc(userToken.userId)
    //     .update({
    //   'userTokens': userTokensData,
    // });
  }

  @override
  Future<void> updateUserToken(
      UserToken userToken) async {
    await fireStoreDb.collection(USERS_COLLECTION_PATH)
        .doc(userToken.userId)
        .collection(USERS_TOKENS_COLLECTION_PATH)
        .doc(userToken.userTokenId)
        .update(userToken.toMap());
  }

  @override
  Future<List<Token>> getAllTokens() async {
    final querySnapshot =
        await fireStoreDb.collection(TOKENS_COLLECTION_PATH).get();
    List<Token> tokens =
        List.from(querySnapshot.docs.map((doc) => Token.fromFirebase(doc)));
    return tokens;
  }

  @override
  Stream<List<UserToken>> getTokensForUserStream(User user) {
    CollectionReference ref = fireStoreDb.collection(USERS_COLLECTION_PATH).doc(user.userId).collection(USERS_TOKENS_COLLECTION_PATH);
    return ref.snapshots().transform(StreamTransformer.fromHandlers(
        handleData: (data, sink) => _mapDocumentToListUserTokens(data, sink, user)));
  }

  void _mapDocumentToListUserTokens(
      QuerySnapshot querySnapshot, Sink sink, User user) async {
    final userTokensData = querySnapshot.docs;

    List<UserToken> userTokens =
        List.from(userTokensData.map((data) => UserToken.fromFirebase(data)));
    for (var userToken in userTokens) {
      userToken.user = user;
      final token = await fireStoreDb
          .collection(TOKENS_COLLECTION_PATH)
          .doc(userToken.tokenId)
          .get();
      userToken.token = Token.fromFirebase(token);
    }
    sink.add(userTokens);
  }

  @override
  Future<List<UserToken>> getTokensForUser(User user) async {
    final querySnapshot = await fireStoreDb
        .collection(USERS_COLLECTION_PATH)
        .doc(user.userId)
        .collection(USERS_TOKENS_COLLECTION_PATH)
        .get();
    final userTokensData = querySnapshot.docs;

    // Option 1
    // ------------
    List<UserToken> userTokens =
        List.from(userTokensData.map((data) => UserToken.fromFirebase(data)));
    for (var userToken in userTokens) {
      userToken.user = user;
      final token = await fireStoreDb
          .collection(TOKENS_COLLECTION_PATH)
          .doc(userToken.tokenId)
          .get();
      userToken.token = Token.fromFirebase(token);
    }
    user.userTokens = userTokens;
    // -----------------

    // Option 2
    // ------------
    // List<UserToken> userTokens = List.from(userTokensData.map((data) async* {
    //   final userToken = UserToken.fromMap(data);
    //   userToken.user = user;
    //   final token = await fireStoreDb
    //       .collection(TOKENS_COLLECTION_PATH)
    //       .doc(userToken.tokenId)
    //       .get();
    //   userToken.token = Token.fromFirebase(token);
    //   yield userToken;
    // }));
    // user.userTokens = userTokens;
    // ---------------

    return userTokens;
  }

  @override
  Future<void> editToken(Token token) async{
    await fireStoreDb
        .collection(TOKENS_COLLECTION_PATH)
        .doc(token.id)
        .set(token.toMap());
  }

  @override
  Future<void> deleteToken(Token token) async{
    await fireStoreDb
        .collection(TOKENS_COLLECTION_PATH)
        .doc(token.id)
        .delete();
  }
}
