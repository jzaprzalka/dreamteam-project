import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:dreamteam_project/providers/base_providers.dart';

class PaymentsProvider extends BasePaymentsProvider {
  final FirebaseFirestore fireStoreDb;
  static const String PAYMENTS_COLLECTION_PATH = "payments";

  DateTime get _pastDue => DateTime.now();

  DateTime _approachingDue({int nDaysBefore: 10}) =>
      DateTime.now().subtract(Duration(days: nDaysBefore));

  factory PaymentsProvider() =>
      PaymentsProvider.fromFirestore(FirebaseFirestore.instance);
  PaymentsProvider.fromFirestore(this.fireStoreDb);

  List<String> _groupsUserIds(List<Group> groups) {
    List<String> userIds = [];
    groups.forEach((group) => userIds
        .addAll(group.participantIds.where((id) => !userIds.contains(id))));
    return userIds;
  }

  Future<List<Payment>> _getPaymentsFromQuery(final Query query) async {
    final querySnapshot = await query.get();
    return querySnapshot.docs.map((doc) => Payment.fromFirebase(doc)).toList();
  }

  @override
  Future<void> accountPayment(Payment payment, DateTime dateAccounted) {
    DocumentReference ref =
        fireStoreDb.collection(PAYMENTS_COLLECTION_PATH).doc(payment.paymentId);
    Map<String, dynamic> map = Map<String, dynamic>();
    map['paid'] = true;
    map['datePaid'] = dateAccounted;

    return ref.set(map, SetOptions(merge: true));
  }

  @override
  Future<Payment> addPayment(Payment payment) async {
    CollectionReference ref = fireStoreDb.collection(PAYMENTS_COLLECTION_PATH);
    DocumentReference createdPayment = await ref.add(payment.toMap());
    return Payment.fromFirebase(await createdPayment.get());
  }

  @override
  Future<void> addPayments(List<Payment> payments) async {
    CollectionReference ref = fireStoreDb.collection(PAYMENTS_COLLECTION_PATH);
    payments.forEach((payment) async {
      await ref.add(payment.toMap());
    });
  }

  @override
  Future<void> addPaymentForGroup(Payment payment, Group group) async {
    CollectionReference ref = fireStoreDb.collection(PAYMENTS_COLLECTION_PATH);
    for (String userId in group.participantIds) {
      await ref.add(
        Payment(
          userId: userId,
          amount: payment.amount,
          paymentName: payment.paymentName,
          documentId: payment.documentId,
          dateDue: payment.dateDue,
          datePaid: payment.datePaid,
          paid: payment.paid,
          dateAdded: DateTime.now(),
        ).toMap(),
      );
    }
  }

  @override
  Future<List<Payment>> getAllPayments() async {
    CollectionReference ref = fireStoreDb.collection(PAYMENTS_COLLECTION_PATH);
    return await _getPaymentsFromQuery(ref);
  }

  @override
  Future<List<Payment>> getAllPaymentsPaid() async {
    CollectionReference ref = fireStoreDb.collection(PAYMENTS_COLLECTION_PATH);
    return await _getPaymentsFromQuery(
      ref.where("paid", isEqualTo: true),
    );
  }

  @override
  Future<List<Payment>> getAllPaymentsPastDue() async {
    CollectionReference ref = fireStoreDb.collection(PAYMENTS_COLLECTION_PATH);
    return await _getPaymentsFromQuery(
      ref
          .where("dateDue", isLessThan: _pastDue)
          .where("paid", isEqualTo: false),
    );
  }

  @override
  Future<List<Payment>> getAllPaymentsApproachingDue(
      {int nDaysBefore = 10}) async {
    CollectionReference ref = fireStoreDb.collection(PAYMENTS_COLLECTION_PATH);
    return await _getPaymentsFromQuery(
      ref.where("paid", isEqualTo: false).where("dateDue",
          isGreaterThanOrEqualTo: _approachingDue(nDaysBefore: nDaysBefore)),
    );
  }

  @override
  Future<List<Payment>> getGroupPayments(Group group) async {
    CollectionReference ref = fireStoreDb.collection(PAYMENTS_COLLECTION_PATH);
    return await _getPaymentsFromQuery(
      // TODO: FIXME: whereIn supports lists of up to 10 elements. Fix by fetching all and filtering afterwards?
      ref.where("userId", whereIn: group.participantIds),
    );
  }

  @override
  Future<List<Payment>> getGroupsPayments(List<Group> groups) async {
    CollectionReference ref = fireStoreDb.collection(PAYMENTS_COLLECTION_PATH);
    return await _getPaymentsFromQuery(
      // TODO: FIXME: whereIn supports lists of up to 10 elements. Fix by fetching all and filtering afterwards?
      ref.where("userId", whereIn: _groupsUserIds(groups)),
    );
  }

  @override
  Future<List<Payment>> getGroupsPaymentsApproachingDue(List<Group> groups,
      {int nDaysBefore = 10}) async {
    List<String> groupsUserIds = _groupsUserIds(groups);
    if (groupsUserIds.isEmpty) return List.empty(growable: false);
    CollectionReference ref = fireStoreDb.collection(PAYMENTS_COLLECTION_PATH);
    return await _getPaymentsFromQuery(
      ref
          // TODO: FIXME: whereIn supports lists of up to 10 elements. Fix by fetching all and filtering afterwards?
          .where("userId", whereIn: groupsUserIds)
          .where("paid", isEqualTo: false)
          .where("dateDue",
              isGreaterThanOrEqualTo:
                  _approachingDue(nDaysBefore: nDaysBefore)),
    );
  }

  @override
  Future<List<Payment>> getGroupsPaymentsPaid(List<Group> groups) async {
    List<String> groupsUserIds = _groupsUserIds(groups);
    if (groupsUserIds.isEmpty) return List.empty(growable: false);
    CollectionReference ref = fireStoreDb.collection(PAYMENTS_COLLECTION_PATH);
    return await _getPaymentsFromQuery(
      ref
          // TODO: FIXME: whereIn supports lists of up to 10 elements. Fix by fetching all and filtering afterwards?
          .where("userId", whereIn: groupsUserIds)
          .where("paid", isEqualTo: true),
    );
  }

  @override
  Future<List<Payment>> getGroupsPaymentsPastDue(List<Group> groups) async {
    List<String> groupsUserIds = _groupsUserIds(groups);
    if (groupsUserIds.isEmpty) return List.empty(growable: false);
    CollectionReference ref = fireStoreDb.collection(PAYMENTS_COLLECTION_PATH);
    return await _getPaymentsFromQuery(
      ref
          // TODO: FIXME: whereIn supports lists of up to 10 elements. Fix by fetching all and filtering afterwards?
          .where("userId", whereIn: groupsUserIds)
          .where("paid", isEqualTo: false)
          .where("dateDue", isLessThan: _pastDue),
    );
  }

  @override
  Future<List<Payment>> getPaymentsApproachingDue(String userId,
      {int nDaysBefore = 10}) async {
    CollectionReference ref = fireStoreDb.collection(PAYMENTS_COLLECTION_PATH);
    return await _getPaymentsFromQuery(
      ref
          .where("userId", isEqualTo: userId)
          .where("paid", isEqualTo: false)
          .where("dateDue",
              isGreaterThanOrEqualTo:
                  _approachingDue(nDaysBefore: nDaysBefore)),
    );
  }

  @override
  Future<List<Payment>> getPaymentsFor(String userId) async {
    CollectionReference ref = fireStoreDb.collection(PAYMENTS_COLLECTION_PATH);
    return await _getPaymentsFromQuery(
      ref.where("userId", isEqualTo: userId),
    );
  }

  @override
  Future<List<Payment>> getPaymentsPaid(String userId) async {
    CollectionReference ref = fireStoreDb.collection(PAYMENTS_COLLECTION_PATH);
    return await _getPaymentsFromQuery(
      ref.where("userId", isEqualTo: userId).where("paid", isEqualTo: true),
    );
  }

  @override
  Future<List<Payment>> getPaymentsPastDue(String userId) async {
    CollectionReference ref = fireStoreDb.collection(PAYMENTS_COLLECTION_PATH);
    return await _getPaymentsFromQuery(
      ref
          .where("userId", isEqualTo: userId)
          .where("paid", isEqualTo: false)
          .where("dateDue", isLessThan: _pastDue),
    );
  }

  @override
  Future<void> updatePayment(String paymentId,
      {DateTime newDueDate,
      String newName,
      int newAmount,
      bool isAccounted}) async {
    DocumentReference ref =
        fireStoreDb.collection(PAYMENTS_COLLECTION_PATH).doc(paymentId);
    final doc = await ref.get();
    Payment p = Payment.fromFirebase(doc);
    p.dateDue = newDueDate ?? p.dateDue;
    p.paymentName = newName ?? p.paymentName;
    p.amount = newAmount ?? p.amount;
    p.paid = isAccounted ?? p.paid;
    return ref.set(p.toMap(), SetOptions(merge: true));
  }
}
