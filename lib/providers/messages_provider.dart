import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/Chatroom.dart';
import 'package:dreamteam_project/models/Messages.dart';
import 'package:dreamteam_project/providers/base_providers.dart';

class MessagesProvider extends BaseMessagesProvider {
  final FirebaseFirestore firestoreDB;
  static const String CHATROOM_COLLECTION_PATH = "chatrooms";
  static const String MESSAGE_PATH = "messages";
  static const String USER_IDS_PATH = "userIds";

  factory MessagesProvider() =>
      MessagesProvider.fromFirestore(FirebaseFirestore.instance);
  MessagesProvider.fromFirestore(this.firestoreDB);

  @override
  Future<void> addMessage(Message message, String chatroomId) async {
    await firestoreDB
        .collection(CHATROOM_COLLECTION_PATH)
        .doc(chatroomId)
        .collection(MESSAGE_PATH)
        .add(message.toMap());
  }

  @override
  Future<void> createChatroom(Chatroom chatroom) async {
    await firestoreDB
        .collection(CHATROOM_COLLECTION_PATH)
        .add(chatroom.toMap());
  }

  @override
  Future<void> deleteChatroom(String chatroomId) async {
    await firestoreDB
        .collection(CHATROOM_COLLECTION_PATH)
        .doc(chatroomId)
        .delete();
  }

  @override
  Future<void> updateChatroomParticipants(
      String chatroomId, List<String> participantIds) async {
    DocumentReference ref =
        firestoreDB.collection(CHATROOM_COLLECTION_PATH).doc(chatroomId);
    Map<String, dynamic> map = Map<String, dynamic>();
    map[USER_IDS_PATH] = participantIds;
    return ref.set(map, SetOptions(merge: true));
  }

  @override
  Future<List<Chatroom>> getAllChatrooms() async {
    final querySnapshot =
        await firestoreDB.collection(CHATROOM_COLLECTION_PATH).get();
    List<Chatroom> chatrooms =
        List.from(querySnapshot.docs.map((doc) => Chatroom.fromFirebase(doc)));
    return chatrooms;
  }

  @override
  Future<List<Message>> getMessagesForChatroom(String chatroomId) async {
    return await firestoreDB
        .collection(CHATROOM_COLLECTION_PATH)
        .doc(chatroomId)
        .collection(MESSAGE_PATH)
        .orderBy('timestamp', descending: true)
        .get()
        .then((querySnapshot) {
      return querySnapshot.docs.map((d) => Message.fromFireStore(d)).toList();
    });
  }

  @override
  Future<Chatroom> getChatroom(String chatroomId) async {
    final querySnapshot =
        firestoreDB.collection(CHATROOM_COLLECTION_PATH).doc(chatroomId);
    return Chatroom.fromFirebase(await querySnapshot.get());
  }

  @override
  Stream<List<Message>> getMessagesForChatroomStream(String chatId) {
    CollectionReference messagesCollection = firestoreDB
        .collection(CHATROOM_COLLECTION_PATH)
        .doc(chatId)
        .collection(MESSAGE_PATH);

    return messagesCollection
        .orderBy('timestamp', descending: true)
        .snapshots()
        .transform(StreamTransformer<QuerySnapshot, List<Message>>.fromHandlers(
            handleData:
                (QuerySnapshot querySnapshot, EventSink<List<Message>> sink) =>
                    mapDocumentToMessage(querySnapshot, sink)));
  }

  void mapDocumentToMessage(QuerySnapshot querySnapshot, EventSink sink) async {
    List<Message> messages = [];
    for (DocumentSnapshot document in querySnapshot.docs) {
      messages.add(Message.fromFireStore(document));
    }
    sink.add(messages);
  }

  @override
  Future<List<Chatroom>> getUsersChatrooms(String userId) async {
    final querySnapshot = await firestoreDB
        .collection(CHATROOM_COLLECTION_PATH)
        .where(USER_IDS_PATH, arrayContains: userId)
        .get();
    List<Chatroom> chatrooms =
        List.from(querySnapshot.docs.map((doc) => Chatroom.fromFirebase(doc)));
    return chatrooms;
  }
}
