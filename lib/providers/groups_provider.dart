import 'dart:async';

import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase;
import 'base_providers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class GroupsDataProvider extends BaseGroupDataProvider {
  final FirebaseFirestore fireStoreDb;
  final firebase.FirebaseAuth firebaseAuth = firebase.FirebaseAuth.instance;
  static const String GROUPS_COLLECTION_PATH = "groups";

  factory GroupsDataProvider() =>
      GroupsDataProvider.fromFirestore(FirebaseFirestore.instance);
  GroupsDataProvider.fromFirestore(this.fireStoreDb);

  @override
  Future<void> updateName(String groupId, String newName) async {
    DocumentReference ref =
        fireStoreDb.collection(GROUPS_COLLECTION_PATH).doc(groupId.toString());
    Map<String, String> map = Map<String, String>();
    map['name'] = newName;
    return ref.set(map, SetOptions(merge: true));
  }

  @override
  Future<void> addCoach(String groupId, String coachId) async {
    fireStoreDb
        .collection(GROUPS_COLLECTION_PATH)
        .doc(groupId.toString())
        .update({
      'coachIds': FieldValue.arrayUnion([coachId])
    });
  }

  @override
  Future<void> removeCoach(String groupId, String coachId) async {
    var val = [];
    val.add(coachId);
    fireStoreDb
        .collection(GROUPS_COLLECTION_PATH)
        .doc(groupId.toString())
        .update({"coachIds": FieldValue.arrayRemove(val)});
  }

  @override
  Future<void> updateCoaches(String groupId, List<String> coachIds) async {
    DocumentReference ref =
        fireStoreDb.collection(GROUPS_COLLECTION_PATH).doc(groupId);
    Map<String, dynamic> map = Map<String, dynamic>();
    map['coachIds'] = coachIds;
    return ref.set(map, SetOptions(merge: true));
  }

  @override
  Future<Group> addGroup(
      String name, List<String> coachIds, List<String> userIds) async {
    DocumentReference createdGroup =
        await fireStoreDb.collection(GROUPS_COLLECTION_PATH).add(Group(
              name: name,
              coachIds: coachIds,
              participantIds: userIds,
            ).toMap());
    return Group.fromFirebase(await createdGroup.get());
  }

  @override
  Future<void> deleteGroup(String groupId) async {
    fireStoreDb.collection(GROUPS_COLLECTION_PATH).doc(groupId).delete();
  }

  @override
  Future<Group> getGroup(String groupId) async {
    DocumentReference ref =
        fireStoreDb.collection(GROUPS_COLLECTION_PATH).doc(groupId);
    DocumentSnapshot snapshot = await ref.get();
    if (snapshot != null && snapshot.exists)
      return Group.fromFirebase(snapshot);
    else
      return null;
  }

  @override
  Future<void> addUserToGroup(String groupId, String userId) async {
    fireStoreDb.collection(GROUPS_COLLECTION_PATH).doc(groupId).update({
      'participantIds': FieldValue.arrayUnion([userId])
    });
  }

  @override
  Future<void> deleteUserFromGroup(String groupId, String userId) async {
    var val = [];
    val.add(userId);
    fireStoreDb
        .collection(GROUPS_COLLECTION_PATH)
        .doc(groupId)
        .update({"participantIds": FieldValue.arrayRemove(val)});
  }

  @override
  Future<List<Group>> getGroups(List<String> groupIds) async {
    CollectionReference ref = fireStoreDb.collection(GROUPS_COLLECTION_PATH);
    final querySnapshot = await ref.where("groupId", whereIn: groupIds).get();
    List<Group> groups =
        querySnapshot.docs.map((doc) => Group.fromFirebase(doc)).toList();
    return groups;
  }

  @override
  Future<List<User>> getAllUsers() async {
    final querySnapshot = await fireStoreDb.collection("users").get();
    List<User> users = querySnapshot.docs
        .map((doc) => User.fromFirebase(doc))
        .toList(growable: true);
    return users;
  }

  @override
  Future<List<Group>> getAllGroups() async {
    final querySnapshot = await fireStoreDb.collection("groups").get();
    List<Group> groups =
        List.from(querySnapshot.docs.map((doc) => Group.fromFirebase(doc)));
    return groups;
  }
}
