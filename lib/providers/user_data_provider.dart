import 'dart:async';

import 'package:dreamteam_project/models/User.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase;
import 'base_providers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class UserDataProvider extends BaseUserDataProvider {
  final FirebaseFirestore fireStoreDb = FirebaseFirestore.instance;
  final firebase.FirebaseAuth firebaseAuth = firebase.FirebaseAuth.instance;

  @override
  Future<User> getUser(String userId) async {
    DocumentReference ref = fireStoreDb.collection("users").doc(userId);

    DocumentSnapshot snapshot = await ref.get();
    if (snapshot != null && snapshot.exists) {
      firebase.User fu = firebaseAuth.currentUser;
      User u = User.fromFirebase(snapshot);
      if (fu.uid == userId) u.email = fu.email;
      u.userId = userId;
      u.password = "";
      return u;
    } else
      return null;
  }

  @override
  Stream<User> getUserStream(String userId) {
    DocumentReference ref = fireStoreDb.collection("users").doc(userId);
    return ref.snapshots().transform(StreamTransformer.fromHandlers(
        handleData: (data, sink) => _mapDocumentToUser(data, sink, userId)));
  }

  void _mapDocumentToUser(
      DocumentSnapshot snapshot, Sink sink, String userId) async {
    firebase.User fu = firebaseAuth.currentUser;
    User u = User.fromFirebase(snapshot);
    if (fu.uid == userId) u.email = fu.email;
    u.userId = userId;
    u.password = "";
    sink.add(u);
  }

  @override
  Future<void> updateName(String userId, String name) async {
    DocumentReference ref = fireStoreDb.collection("users").doc(userId);
    Map<String, String> map = Map<String, String>();
    map['name'] = name;
    return ref.set(map, SetOptions(merge: true));
  }

  @override
  Future<void> updatePhone(String userId, String phoneNumber) async {
    DocumentReference ref = fireStoreDb.collection("users").doc(userId);
    Map<String, String> map = Map<String, String>();
    map['phoneNumber'] = phoneNumber;
    return ref.set(map, SetOptions(merge: true));
  }

  @override
  Future<void> updateSurname(String userId, String surname) async {
    DocumentReference ref = fireStoreDb.collection("users").doc(userId);
    Map<String, String> map = Map<String, String>();
    map['surname'] = surname;
    return ref.set(map, SetOptions(merge: true));
  }

  @override
  Future<Map<String,User>> getUsers(List<String> userIds) async{
    final users = Map<String,User>();
    for( var uId in userIds){
      var user = await getUser(uId);
      users[uId] = user;
    }
    return users;
  }
}
