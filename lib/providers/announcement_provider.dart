import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dreamteam_project/models/Announcement.dart';
import 'package:dreamteam_project/providers/base_providers.dart';

class AnnouncementProvider extends BaseAnnouncementProvider {
  final FirebaseFirestore fireStoreDb;
  static const String ANNOUNCEMENTS_COLLECTION_PATH = "announcements";

  factory AnnouncementProvider() =>
      AnnouncementProvider.fromFirestore(FirebaseFirestore.instance);
  AnnouncementProvider.fromFirestore(this.fireStoreDb);

  @override
  Future<Announcement> addAnnouncement(DateTime announcementDate,
      String announcement, List<String> groupIds) async {
    DocumentReference createdAnnouncement = await fireStoreDb
        .collection(ANNOUNCEMENTS_COLLECTION_PATH)
        .add(Announcement(
          announcementDate: announcementDate,
          announcement: announcement,
          groupIds: groupIds,
        ).toMap());
    return Announcement.fromFirebase(await createdAnnouncement.get());
  }

  @override
  Future<void> deleteAnnouncement(String announcementId) async {
    fireStoreDb
        .collection(ANNOUNCEMENTS_COLLECTION_PATH)
        .doc(announcementId)
        .delete();
  }

  @override
  Future<List<Announcement>> getAllAnnouncements() async {
    final querySnapshot =
        await fireStoreDb.collection(ANNOUNCEMENTS_COLLECTION_PATH).get();
    List<Announcement> announcements = List.from(
        querySnapshot.docs.map((doc) => Announcement.fromFirebase(doc)));
    return announcements;
  }

  @override
  Future<List<Announcement>> getGroupsAnnouncements(
      List<String> groupIds) async {
    final querySnapshot = await fireStoreDb
        .collection(ANNOUNCEMENTS_COLLECTION_PATH)
        .where("groupIds", arrayContainsAny: groupIds)
        .get();
    List<Announcement> announcements = querySnapshot.docs
        .map((doc) => Announcement.fromFirebase(doc))
        .toList();
    return announcements;
  }

  @override
  Future<Announcement> getAnnouncement(String announcementId) async {
    final documentSnapshot = await fireStoreDb
        .collection(ANNOUNCEMENTS_COLLECTION_PATH)
        .doc(announcementId)
        .get();
    Announcement announcement = Announcement.fromFirebase(documentSnapshot);
    return announcement;
  }
}
