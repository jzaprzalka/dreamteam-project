import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dreamteam_project/models/Schedule.dart';
import 'package:dreamteam_project/models/Attendance.dart';
import 'package:dreamteam_project/providers/base_providers.dart';

class ScheduleProvider extends BaseScheduleProvider {
  final FirebaseFirestore fireStoreDb;
  static const String SCHEDULE_COLLECTION_PATH = "schedule";

  factory ScheduleProvider() =>
      ScheduleProvider.fromFirestore(FirebaseFirestore.instance);
  ScheduleProvider.fromFirestore(this.fireStoreDb);

  @override
  Future<Schedule> addSchedule(Schedule schedule) async {
    CollectionReference ref = fireStoreDb.collection(SCHEDULE_COLLECTION_PATH);
    DocumentReference createdSchedule = await ref.add(schedule.toMap());
    return Schedule.fromFirebase(await createdSchedule.get());
  }

  @override
  Future<void> addSchedules(Schedule schedule, DateTime toDate) async {
    CollectionReference ref = fireStoreDb.collection(SCHEDULE_COLLECTION_PATH);
    DateTime end = DateTime(toDate.year, toDate.month, toDate.day,
        schedule.dateEnd.hour, schedule.dateEnd.minute);
    while (schedule.dateStart.compareTo(end) <= 0) {
      ref.add(schedule.toMap());
      schedule.dateStart.add(Duration(days: 7));
      schedule.dateEnd.add(Duration(days: 7));
    }
  }

  // @override
  // Future<List<Schedule>> getSchedule(
  //     List<String> groupIds, DateTime toDate) async {
  //   CollectionReference ref = fireStoreDb.collection(SCHEDULE_COLLECTION_PATH);
  //   final querySnapshot = await ref
  //       // .where("groupId", whereIn: groupIds)
  //       .where("dateEnd", isLessThanOrEqualTo: Timestamp.fromDate(toDate))
  //       .get();
  //   List<Schedule> schedules =
  //       querySnapshot.docs.map((doc) => Schedule.fromFirebase(doc)).toList();
  //   return schedules;
  // }

  @override
  Future<List<Schedule>> getSchedule(DateTime fromDate, DateTime toDate,
      {List<String> groupIds}) async {
    CollectionReference ref = fireStoreDb.collection(SCHEDULE_COLLECTION_PATH);
    final querySnapshot = await ref
        .where("dateEnd",
            isGreaterThanOrEqualTo: fromDate.subtract(Duration(days: 1)))
        .where("dateEnd", isLessThanOrEqualTo: toDate.add(Duration(days: 1)))
        .get();
    List<Schedule> schedules =
        querySnapshot.docs.map((doc) => Schedule.fromFirebase(doc)).toList();
    if (groupIds != null)
      schedules = schedules.where((s) => groupIds.contains(s.groupId)).toList();
    return schedules;
  }

  @override
  Future<void> updateAttendance(
      String scheduleId, List<Attendance> attendance) async {
    DocumentReference ref =
        fireStoreDb.collection(SCHEDULE_COLLECTION_PATH).doc(scheduleId);
    List<Map<String, dynamic>> attendanceList =
        List.from(attendance.map((e) => e.toMap()));
    Map<String, dynamic> map = Map<String, dynamic>();
    map['attendance'] = attendanceList;
    return ref.set(map, SetOptions(merge: true));
  }

  @override
  Future<void> updateClassSubject(
      String scheduleId, String classSubject) async {
    DocumentReference ref =
        fireStoreDb.collection(SCHEDULE_COLLECTION_PATH).doc(scheduleId);
    Map<String, dynamic> map = Map<String, dynamic>();
    map['classSubject'] = classSubject;
    return ref.set(map, SetOptions(merge: true));
  }

  @override
  Future<void> updateCoaches(String scheduleId, List<String> coachIds) async {
    DocumentReference ref =
        fireStoreDb.collection(SCHEDULE_COLLECTION_PATH).doc(scheduleId);
    Map<String, dynamic> map = Map<String, dynamic>();
    map['coachesId'] = coachIds;
    return ref.set(map, SetOptions(merge: true));
  }

  @override
  Future<void> updateRoom(String scheduleId, String roomId) async {
    DocumentReference ref =
        fireStoreDb.collection(SCHEDULE_COLLECTION_PATH).doc(scheduleId);
    Map<String, String> map = Map<String, String>();
    map['roomId'] = roomId;
    return ref.set(map, SetOptions(merge: true));
  }
}
