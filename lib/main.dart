import 'package:dreamteam_project/configuration/logs/predefined_loggers.dart';
import 'package:dreamteam_project/models/value_notifier/user_notifier.dart';
import 'package:dreamteam_project/screens/calendar_screen/calendar_screen.dart';
import 'package:dreamteam_project/screens/coach/groups_screen/groups_list_screen.dart';
import 'package:dreamteam_project/screens/coach/payments_screen/payments_screen.dart';
import 'package:dreamteam_project/screens/root_screen/root_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:logger/logger.dart';
import 'package:provider/provider.dart';
import 'blocks/auth/authentication_bloc.dart';
import 'generated/l10n.dart';

Logger logger;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  logger = simpleDebugLogger;
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => UserNotifier(null),
      child: MaterialApp(
        localizationsDelegates: [
          S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: S.delegate.supportedLocales,
        title: 'DreamTeam Project',
        routes: {
          '/groups': (context) => GroupsScreen(),
          '/calendar': (context) => CalendarScreen(),
          '/payments': (context) => PaymentsScreen(),
        },
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primaryColor: Colors.lightBlueAccent,
          scaffoldBackgroundColor: Colors.blue[100],
          primarySwatch: Colors.lightBlue,
        ),
        home: BlocProvider(
          create: (context) => AuthenticationBloc()
            ..add(AuthenticationEvent.authCheckRequested()),
          child: RootScreen(), // MainScreen(),
        ),
      ),
    );
  }
}
