import 'package:dreamteam_project/providers/base_providers.dart';
import 'package:dreamteam_project/providers/schedule_provider.dart';
import 'package:dreamteam_project/models/Schedule.dart';
import 'package:dreamteam_project/models/Attendance.dart';
import 'package:dreamteam_project/providers/groups_provider.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase;
import 'package:dreamteam_project/providers/authentication_provider.dart';
import 'package:dreamteam_project/models/Group.dart';

class ScheduleRepository {
  BaseScheduleProvider scheduleProvider = ScheduleProvider();
  BaseGroupDataProvider groupsDataProvider = GroupsDataProvider();
  BaseAuthenticationProvider authenticationProvider = AuthenticationProvider();

  Future<List<String>> _getCoachGroupIds() async {
    List<Group> allGroups = await groupsDataProvider.getAllGroups();
    firebase.User currentUser = await authenticationProvider.getCurrentUser();
    List<Group> coachGroups =
        allGroups.where((g) => g.coachIds.contains(currentUser.uid)).toList();
    return coachGroups.map((g) => g.groupId).toList();
  }

  Future<List<String>> _getStudentGroupIds() async {
    List<Group> allGroups = await groupsDataProvider.getAllGroups();
    firebase.User currentUser = await authenticationProvider.getCurrentUser();
    List<Group> studentGroups = allGroups
        .where((g) => g.participantIds.contains(currentUser.uid))
        .toList();
    return studentGroups.map((g) => g.groupId).toList();
  }

  Future<void> addSchedule(Schedule schedule, {DateTime toDate = null}) async {
    if (toDate == null) {
      await scheduleProvider.addSchedule(schedule);
    } else {
      await scheduleProvider.addSchedules(schedule, toDate);
    }
  }

  // Future<List<Schedule>> getCoachSchedules(DateTime toDate) async {
  //   List<String> groupIds = await _getCoachGroupIds();
  //   return await scheduleProvider.getSchedule(groupIds, toDate);
  // }

  // Future<List<Schedule>> getStudentSchedules(DateTime toDate) async {
  //   List<String> groupIds = await _getStudentGroupIds();
  //   return await scheduleProvider.getSchedule(groupIds, toDate);
  // }

  Future<List<Schedule>> getCoachSchedule(
      DateTime fromDate, DateTime toDate) async {
    List<String> groupIds = await _getCoachGroupIds();
    return await scheduleProvider.getSchedule(
      fromDate,
      toDate,
      groupIds: groupIds,
    );
  }

  Future<List<Schedule>> getStudentSchedule(
      DateTime fromDate, DateTime toDate) async {
    List<String> groupIds = await _getStudentGroupIds();
    return await scheduleProvider.getSchedule(
      fromDate,
      toDate,
      groupIds: groupIds,
    );
  }

  Future<List<Schedule>> getSchedule(DateTime fromDate, DateTime toDate) async {
    return await scheduleProvider.getSchedule(fromDate, toDate);
  }

  Future<void> updateAttendance(
          String scheduleId, List<Attendance> attendance) =>
      scheduleProvider.updateAttendance(scheduleId, attendance);
  Future<void> updateCoaches(String scheduleId, List<String> coachIds) =>
      scheduleProvider.updateCoaches(scheduleId, coachIds);
  Future<void> updateRoom(String scheduleId, String roomId) =>
      scheduleProvider.updateRoom(scheduleId, roomId);
  // Idk if needed
  Future<void> removeSchedule(String scheduleId) => null;
}
