import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:dreamteam_project/models/Schedule.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/providers/authentication_provider.dart';
import 'package:dreamteam_project/providers/base_providers.dart';
import 'package:dreamteam_project/providers/groups_provider.dart';
import 'package:dreamteam_project/providers/schedule_provider.dart';
import 'package:dreamteam_project/providers/user_data_provider.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase;

class HomeRepository {
  BaseAuthenticationProvider authenticationProvider = AuthenticationProvider();
  BaseGroupDataProvider groupProvider = GroupsDataProvider();
  BaseScheduleProvider scheduleProvider = ScheduleProvider();
  BaseUserDataProvider userProvider = UserDataProvider();
  BasePaymentsProvider paymentsProvider;

  Future<List<Group>> getCoachesGroups() async {
    List<Group> allGroups = await groupProvider.getAllGroups();
    firebase.User coach = await authenticationProvider.getCurrentUser();
    List<Group> groups =
        List.from(allGroups.where((g) => g.coachIds.contains(coach.uid)));
    return groups;
  }

  Future<List<Group>> getUsersGroups() async {
    List<Group> allGroups = await groupProvider.getAllGroups();
    firebase.User user = await authenticationProvider.getCurrentUser();
    List<Group> groups =
        List.from(allGroups.where((g) => g.participantIds.contains(user.uid)));
    return groups;
  }

  Future<List<Schedule>> getCoachesSchedule() async {
    List<Group> coachesGroups = await getCoachesGroups();
    List<String> groupIds;
    for (Group g in coachesGroups) {
      groupIds.add(g.groupId);
    }
    List<Schedule> schedules = await scheduleProvider
        .getSchedule(DateTime.now(), DateTime.now(), groupIds: groupIds);
    return schedules;
  }

  Future<List<Schedule>> getUsersSchedule() async {
    firebase.User userFirebase = await authenticationProvider.getCurrentUser();
    User user = await userProvider.getUser(userFirebase.uid);
    List<Schedule> schedules = await scheduleProvider
        .getSchedule(DateTime.now(), DateTime.now(), groupIds: user.groupIds);
    return schedules;
  }

  Future<List<Payment>> getCurrentUsersPayments() async {
    firebase.User userFirebase = await authenticationProvider.getCurrentUser();
    User user = await userProvider.getUser(userFirebase.uid);
    return paymentsProvider.getPaymentsFor(user.userId);
  }

  Future<List<Payment>> getCurrentUsersPaymentsPaid() async {
    List<Payment> allPayments = await getCurrentUsersPayments();
    List<Payment> paidPayments =
        List.from(allPayments.where((p) => p.paid == true));
    return paidPayments;
  }

  Future<List<Payment>> getCurrentUsersPaymentsPastDue() async {
    List<Payment> allPayments = await getCurrentUsersPayments();
    List<Payment> pastDuePayments =
        List.from(allPayments.where((p) => p.dateDue.isAfter(DateTime.now())));
    return pastDuePayments;
  }

  Future<List<Payment>> getPaymentsFor(User user) =>
      paymentsProvider.getPaymentsFor(user.userId);
  Future<List<Payment>> getGroupsPayments(List<Group> groups) =>
      paymentsProvider.getGroupsPayments(groups);
  Future<List<Payment>> getGroupsPaymentsPaid(List<Group> groups) =>
      paymentsProvider.getGroupsPaymentsPaid(groups);
  Future<List<Payment>> getGroupsPaymentsPastDue(List<Group> groups) =>
      paymentsProvider.getGroupsPaymentsPastDue(groups);
}
