import 'package:firebase_auth/firebase_auth.dart';
import 'package:dreamteam_project/providers/base_providers.dart';
import 'package:dreamteam_project/providers/authentication_provider.dart';

class AuthenticationRepository {
  BaseAuthenticationProvider authenticationProvider = AuthenticationProvider();

  Future<void> signInUser(String email, String password) =>
      authenticationProvider.signInUser(email, password);
  Future<void> signOutUser() => authenticationProvider.signOutUser();
  Future<User> getCurrentUser() => authenticationProvider.getCurrentUser();
  Future<bool> isLoggedIn() => authenticationProvider.isLoggedIn();
}
