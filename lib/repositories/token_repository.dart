import 'package:dreamteam_project/models/UserToken.dart';
import 'package:dreamteam_project/providers/authentication_provider.dart';
import 'package:dreamteam_project/providers/base_providers.dart';
import 'package:dreamteam_project/providers/token_provider.dart';
import 'package:dreamteam_project/models/Token.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase;
import 'package:dreamteam_project/providers/user_data_provider.dart';

class TokenRepository {
  BaseTokenProvider tokenProvider = TokenProvider();
  BaseAuthenticationProvider authenticationProvider = AuthenticationProvider();
  BaseUserDataProvider userDataProvider = UserDataProvider();
  static const String ADMIN_TOKEN_NAME = "admin";
  static const String USER_TOKEN_NAME = "user";
  static const String COACH_TOKEN_NAME = "coach";

  Future<void> updateUserToken(UserToken usertoken) =>
      tokenProvider.updateUserToken(usertoken);
  Future<void> addUserTokenByUserToken(UserToken userToken) =>
      tokenProvider.addUserTokenByUserToken(userToken);
  Future<Token> addToken(String name) => tokenProvider.addToken(name);
  Future<void> editToken(Token token) => tokenProvider.editToken(token);
  Future<void> deleteToken(Token token) => tokenProvider.deleteToken(token);
  Future<void> addUserToken(Token token, DateTime expirationDate,
      {User user = null}) async {
    if (user == null) {
      firebase.User currentUser = await authenticationProvider.getCurrentUser();
      user = await userDataProvider.getUser(currentUser.uid);
    }
    tokenProvider.addUserToken(user, token, expirationDate);
  }

  Future<List<UserToken>> getTokensForUser(User user) =>
      tokenProvider.getTokensForUser(user);

  Future<List<UserToken>> getTokensForUserFromId(String userId) async {
    User user = await userDataProvider.getUser(userId);
    List<UserToken> userTokens = await tokenProvider.getTokensForUser(user);
    return userTokens;
  }

  Future<bool> verifyToken(User user, Token token) =>
      tokenProvider.verifyToken(user, token);
  Future<void> extendExpirationDate(UserToken userToken, Duration duration) =>
      tokenProvider.extendExpirationDate(userToken, duration);

  Future<List<Token>> checkBaseTokensExistance() async {
    List<Token> allTokens = await tokenProvider.getAllTokens();
    if (allTokens.where((t) => t.name == ADMIN_TOKEN_NAME).isEmpty) {
      tokenProvider.addToken(ADMIN_TOKEN_NAME);
    }
    if (allTokens.where((t) => t.name == COACH_TOKEN_NAME).isEmpty) {
      tokenProvider.addToken(COACH_TOKEN_NAME);
    }
    if (allTokens.where((t) => t.name == USER_TOKEN_NAME).isEmpty) {
      tokenProvider.addToken(USER_TOKEN_NAME);
    }
    return await tokenProvider.getAllTokens();
  }

  Future<List<Token>> getAllTokens() async {
    return await tokenProvider.getAllTokens();
  }

  Stream<List<UserToken>> getTokensForUserStream(User user) =>
      tokenProvider.getTokensForUserStream(user);
}
