import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/providers/authentication_provider.dart';
import 'package:dreamteam_project/providers/base_providers.dart';
import 'package:dreamteam_project/providers/groups_provider.dart';
import 'package:dreamteam_project/providers/user_data_provider.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase;

class GroupsDataRepository {
  BaseAuthenticationProvider authenticationProvider = AuthenticationProvider();
  BaseGroupDataProvider groupsDataProvider = GroupsDataProvider();
  BaseUserDataProvider userDataProvider = UserDataProvider();

  Future<List<Group>> getCoachGroups() async {
    List<Group> allGroups = await groupsDataProvider.getAllGroups();
    firebase.User currentUser = await authenticationProvider.getCurrentUser();
    List<Group> coachGroups =
        List.from(allGroups.where((g) => g.coachIds.contains(currentUser.uid)));
    return coachGroups;
  }

  Future<List<Group>> getStudentGroups() async {
    List<Group> allGroups = await groupsDataProvider.getAllGroups();
    firebase.User currentUser = await authenticationProvider.getCurrentUser();
    List<Group> coachGroups = List.from(
        allGroups.where((g) => g.participantIds.contains(currentUser.uid)));
    return coachGroups;
  }

  Future<void> addGroupAsCoach(String name, List<String> usersId) async =>
      groupsDataProvider.addGroup(
          name, [(await authenticationProvider.getCurrentUser()).uid], usersId);
  Future<void> addGroupAsAdmin(
          String name, List<String> coachIds, List<String> usersId) async =>
      groupsDataProvider.addGroup(name, coachIds, usersId);
  Future<void> addCoach(String groupId, String coachId) =>
      groupsDataProvider.addCoach(groupId, coachId);
  Future<void> removeCoach(String groupId, String coachId) =>
      groupsDataProvider.removeCoach(groupId, coachId);
  Future<void> updateName(String groupId, String newName) =>
      groupsDataProvider.updateName(groupId, newName);

  Future<void> updateCoaches(String groupId, List<String> coachIds) async {
    Group group = await groupsDataProvider.getGroup(groupId);
    List<User> newCoaches;
    for (String id in coachIds) {
      newCoaches.add(await userDataProvider.getUser(id));
    }
    group.coachIds = coachIds;
    group.coaches = newCoaches;
    groupsDataProvider.updateCoaches(groupId, coachIds);
  }

  Future<void> deleteGroup(String groupId) =>
      groupsDataProvider.deleteGroup(groupId);
  Future<Group> getGroup(String groupId) async {
    return await groupsDataProvider.getGroup(groupId);
  }

  Future<void> addUserToGroup(String groupId, String userId) =>
      groupsDataProvider.addUserToGroup(groupId, userId);
  Future<void> deleteUserFromGroup(String groupId, String userId) =>
      groupsDataProvider.deleteUserFromGroup(groupId, userId);
  Future<List<Group>> getAllGroups() async {
    return await groupsDataProvider.getAllGroups();
  }

  Future<List<Group>> getGroups(List<String> groupId) async {
    return await groupsDataProvider.getGroups(groupId);
  }

  Future<void> updateGroup(String groupId, String newName,
      List<String> coachIds, List<String> participantIds) async {
    groupsDataProvider.updateName(groupId, newName);
    groupsDataProvider.updateCoaches(groupId, coachIds);
    // TODO: The below seems unnecessary
    Group groupToUpdate = await groupsDataProvider.getGroup(groupId);
    // -----------
    for (String participantId in participantIds) {
      if (!participantIds.contains(participantId)) {
        groupsDataProvider.addUserToGroup(groupId, participantId);
      }
    }
  }

  Future<List<User>> getAllUsers() async =>
      await groupsDataProvider.getAllUsers();

  Future<Group> loadParticipants(Group group) async {
    group.participants = [];
    for (String userId in group.participantIds)
      group.participants.add(await userDataProvider.getUser(userId));
    return group;
  }

  Future<List<User>> getNotParticipants(String groupId) async {
    List<User> allUsers = await groupsDataProvider.getAllUsers();
    Group group = await groupsDataProvider.getGroup(groupId);
    List<User> notParticipants;
    for (User user in allUsers) {
      if (!group.participants.contains(user)) {
        notParticipants.add(user);
      }
    }
    return Future.value(notParticipants);
  }
}
