import 'package:dreamteam_project/models/Chatroom.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/models/Messages.dart';
import 'package:dreamteam_project/providers/authentication_provider.dart';
import 'package:dreamteam_project/providers/base_providers.dart';
import 'package:dreamteam_project/providers/messages_provider.dart';
import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart' as firebase;
import 'package:dreamteam_project/providers/user_data_provider.dart';

class MessagesRepository {
  BaseMessagesProvider messagesProvider = MessagesProvider();
  BaseAuthenticationProvider authenticationProvider = AuthenticationProvider();
  BaseUserDataProvider userDataProvider = UserDataProvider();

  Future<void> addMessage(Message message, String chatroomId) async =>
      messagesProvider.addMessage(message, chatroomId);

  Future<void> createChatroom(Chatroom chatroom) async =>
      messagesProvider.createChatroom(chatroom);

  Future<void> deleteChatroom(String chatroomId) async =>
      messagesProvider.deleteChatroom(chatroomId);

  Future<void> updateChatroomParticipants(
          String chatroomId, List<String> participantIds) async =>
      await messagesProvider.updateChatroomParticipants(
        chatroomId,
        participantIds,
      );

  Future<List<Chatroom>> getAllChatrooms() async =>
      messagesProvider.getAllChatrooms();

  Future<List<Chatroom>> getUsersChatrooms(String userId) async =>
      messagesProvider.getUsersChatrooms(userId);

  Future<List<Message>> getMessagesForChatroom(String chatroomId) async =>
      messagesProvider.getMessagesForChatroom(chatroomId);

  Future<Chatroom> getChatroom(String chatroomId) async =>
      messagesProvider.getChatroom(chatroomId);

  Stream<List<Message>> getMessagesForChatroomStream(String chatroomId) =>
      messagesProvider.getMessagesForChatroomStream(chatroomId);
}
