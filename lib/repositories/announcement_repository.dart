import 'package:dreamteam_project/models/Announcement.dart';
import 'package:dreamteam_project/providers/base_providers.dart';
import 'package:dreamteam_project/providers/announcement_provider.dart';

class AnnouncementRepository {
  BaseAnnouncementProvider announcementProvider = AnnouncementProvider();

  Future<Announcement> addAnnouncement(
    DateTime announcementDate,
    String announcement,
    List<String> groupIds,
  ) =>
      announcementProvider.addAnnouncement(
          announcementDate, announcement, groupIds);
  Future<void> deleteAnnouncement(String announcementId) =>
      announcementProvider.deleteAnnouncement(announcementId);
  Future<List<Announcement>> getAllAnnouncements() =>
      announcementProvider.getAllAnnouncements();
  Future<List<Announcement>> getGroupsAnnouncements(List<String> groupIds) =>
      announcementProvider.getGroupsAnnouncements(groupIds);
  Future<Announcement> getAnnouncement(String announcementId) =>
      announcementProvider.getAnnouncement(announcementId);
}
