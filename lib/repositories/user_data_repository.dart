import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/providers/authentication_provider.dart';
import 'package:dreamteam_project/providers/base_providers.dart';
import 'package:dreamteam_project/providers/user_data_provider.dart';

class UserDataRepository {
  BaseUserDataProvider userDataProvider = UserDataProvider();
  BaseAuthenticationProvider authenticationProvider = AuthenticationProvider();

  Future<User> getUser(String userId) => userDataProvider.getUser(userId);
  Stream<User> getUserStream(String userId) =>
      userDataProvider.getUserStream(userId);
  Future<void> updateEmail(String email, String password, String newEmail) =>
      authenticationProvider.updateEmail(email, password, newEmail);
  Future<void> updatePassword(
          String email, String password, String newPassword) =>
      authenticationProvider.updatePassword(email, password, newPassword);
  Future<void> updateName(String userId, String name) =>
      userDataProvider.updateName(userId, name);
  Future<void> updateSurname(String userId, String surname) =>
      userDataProvider.updateSurname(userId, surname);
  Future<void> updatePhone(String userId, String phoneNumber) =>
      userDataProvider.updatePhone(userId, phoneNumber);
  Future<Map<String,User>> getUsers(List<String> userIds) =>
      userDataProvider.getUsers(userIds);
}
