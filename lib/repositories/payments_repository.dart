import 'package:dreamteam_project/main.dart';
import 'package:dreamteam_project/models/Group.dart';
import 'package:dreamteam_project/models/Payment.dart';
import 'package:dreamteam_project/models/User.dart';
import 'package:dreamteam_project/providers/base_providers.dart';
import 'package:dreamteam_project/providers/groups_provider.dart';
import 'package:dreamteam_project/providers/payments_provider.dart';

class PaymentsDataRepository {
  BasePaymentsProvider paymentsProvider = PaymentsProvider();
  BaseGroupDataProvider groupsProvider = GroupsDataProvider();

  Future<List<Payment>> getPaymentsFor(String userId) async =>
      await paymentsProvider.getPaymentsFor(userId);
  Future<List<Payment>> getUserPaymentsPaid(String userId) async =>
      await paymentsProvider.getPaymentsPaid(userId);
  Future<List<Payment>> getUserPaymentsPastDue(String userId) async =>
      await paymentsProvider.getPaymentsPastDue(userId);
  Future<List<Payment>> getUserPaymentsApproachingDue(
    String userId, {
    int nDaysBefore = 10,
  }) async =>
      await paymentsProvider.getPaymentsApproachingDue(userId,
          nDaysBefore: nDaysBefore);

  Future<List<Payment>> getGroupsPayments(List<Group> groups) async =>
      await paymentsProvider.getGroupsPayments(groups);
  Future<List<Payment>> getGroupsPaymentsPaid(List<Group> groups) async =>
      await paymentsProvider.getGroupsPaymentsPaid(groups);
  Future<List<Payment>> getGroupsPaymentsPastDue(List<Group> groups) async =>
      await paymentsProvider.getGroupsPaymentsPastDue(groups);
  Future<List<Payment>> getGroupsPaymentsApproachingDue(
    List<Group> groups, {
    int nDaysBefore = 10,
  }) async =>
      await paymentsProvider.getGroupsPaymentsApproachingDue(groups,
          nDaysBefore: nDaysBefore);

  Future<List<Payment>> getAllPayments() async =>
      await paymentsProvider.getAllPayments();
  Future<List<Payment>> getAllPaymentsPaid() async =>
      await paymentsProvider.getAllPaymentsPaid();
  Future<List<Payment>> getAllPaymentsPastDue() async =>
      await paymentsProvider.getAllPaymentsPastDue();
  Future<List<Payment>> getAllPaymentsApproachingDue(
          {int nDaysBefore = 10}) async =>
      await paymentsProvider.getAllPaymentsApproachingDue(
          nDaysBefore: nDaysBefore);

  // Modifing payments
  Future<void> accountPayment(Payment payment, DateTime dateAccounted) async =>
      await paymentsProvider.accountPayment(payment, dateAccounted);
  Future<void> addPayment(Payment payment) async =>
      await paymentsProvider.addPayment(payment);

  Future<void> updatePayment(
    String paymentId, {
    DateTime newDueDate,
    String newName,
    int newAmount,
    bool isAccounted,
  }) async {
    await paymentsProvider.updatePayment(
      paymentId,
      newDueDate: newDueDate,
      newName: newName,
      newAmount: newAmount,
      isAccounted: isAccounted,
    );
  }

  Future<void> addUsersPayment(
    List<String> userIds,
    DateTime dueDate,
    String paymentName,
    int amount,
  ) async {
    for (String userId in userIds) {
      try {
        await addPayment(Payment(
          userId: userId,
          paymentName: paymentName,
          amount: amount,
          dateAdded: DateTime.now(),
          dateDue: dueDate,
          paid: false,
        ));
      } catch (e) {
        logger.e("Exception when batch adding payment for user $userId: $e");
      }
    }
  }

  Future<void> addMonthlyPayment(
    List<String> userIds,
    DateTime startDueDate,
    DateTime endDueDate,
    String paymentName,
    int amount,
  ) async {
    for (DateTime dt = startDueDate;
        dt.isBefore(endDueDate);
        dt = DateTime(
      dt.month == 12 ? dt.year + 1 : dt.year,
      dt.month == 12 ? dt.month : dt.month + 1,
      dt.day,
    )) {
      await addUsersPayment(userIds, dt, paymentName, amount);
    }
  }

  Future<void> addWeeklyPayment(
    List<String> userIds,
    DateTime startDueDate,
    DateTime endDueDate,
    String paymentName,
    int amount,
  ) async {
    for (DateTime dt = startDueDate;
        dt.isBefore(endDueDate);
        dt = dt.add(Duration(days: 7))) {
      await addUsersPayment(userIds, dt, paymentName, amount);
    }
  }

  Future<void> addGroupsPayment(
    List<String> groupIds,
    DateTime dueDate,
    String paymentName,
    int amount,
  ) async {
    for (String groupId in groupIds) {
      try {
        Group g = await groupsProvider.getGroup(groupId);
        List<String> userIds = g.participantIds ?? [];
        await addUsersPayment(userIds, dueDate, paymentName, amount);
      } catch (e) {
        logger.e("Exception when batch adding payment to group $groupId: $e");
      }
    }
  }

  Future<void> addWeeklyGroupPayment(
    List<String> groupIds,
    DateTime startDueDate,
    DateTime endDueDate,
    String paymentName,
    int amount,
  ) async {
    for (DateTime dt = startDueDate;
        dt.isBefore(endDueDate);
        dt = dt.add(Duration(days: 7))) {
      await addGroupsPayment(groupIds, dt, paymentName, amount);
    }
  }

  Future<void> addMonthlyGroupPayment(
    List<String> groupIds,
    DateTime startDueDate,
    DateTime endDueDate,
    String paymentName,
    int amount,
  ) async {
    for (DateTime dt = startDueDate;
        dt.isBefore(endDueDate);
        dt = DateTime(
      dt.month == 12 ? dt.year + 1 : dt.year,
      dt.month == 12 ? dt.month : dt.month + 1,
      dt.day,
    )) {
      await addGroupsPayment(groupIds, dt, paymentName, amount);
    }
  }
}
